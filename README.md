# Project Init 

Project init merupakan sebuah kerangka untuk memulai develop aplikasi baru.

## What's Inside ?

* 	_PHP Framework_ menggunakan Silex 2.*
* 	_ORM_ menggunakan Propel 2.*
* 	_Templating Engine_ menggunakan Twig 1.*
* 	_CSS Template_ menggunakan Bootstrap & AdminLTE

## Getting Started With Project Init
*	Update composer terlebih dahulu
  
	```
	composer update
	```

*	Update script ``` config/database/propel.yml ```
*	Update script ``` build.sh & reverse.sh ``` (Linux)
* 	Update script ``` set_path.bat ``` (Windows)
*	Generate model (Linux)

	```
	sh build.sh
	```

*	Generate model (Windows)

	```
	set_path.bat
	```


Enjoy!
Ext.define('SimpegGundul.store.Sekolah', {
    extend: 'Ext.data.Store',
    requires: 'SimpegGundul.model.Sekolah',
    model: 'SimpegGundul.model.Sekolah',
    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: 'Sekolah/getData',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});
Ext.define('SimpegGundul.store.KenaikanPangkat', {
    extend: 'Ext.data.Store',
    requires: 'SimpegGundul.model.KenaikanPangkat',
    model: 'SimpegGundul.model.KenaikanPangkat',
    pageSize: 50,
    autoLoad: false,
    proxy: {
        type: 'rest',
        url: 'KenaikanPangkat/getData',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function (proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data KenaikanPangkat ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});
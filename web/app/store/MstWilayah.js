Ext.define('SimpegGundul.store.MstWilayah', {
    extend: 'Ext.data.Store',
    requires: 'SimpegGundul.model.MstWilayah',
    model: 'SimpegGundul.model.MstWilayah',
    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: 'MstWilayah/getData',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});
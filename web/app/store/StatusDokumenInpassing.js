Ext.define('SimpegGundul.model.StatusDokumenInpassing', {
    extend: 'Ext.data.Model',
    idProperty: 'status_dokumen_id',
    fields: [
        { name: 'status_dokumen_id', type: 'int' },
        { name: 'nama', type: 'string' },
        { name: 'level', type: 'string' }
    ]
});


Ext.define('SimpegGundul.store.StatusDokumenInpassing', {
    extend: 'Ext.data.Store',
    model: 'SimpegGundul.model.StatusDokumenInpassing',
    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'data/status_dokumen_inpassing.json',
        reader: {
            type: 'json'
        },
        listeners: {
            exception: function (proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data Penyesuaian ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});
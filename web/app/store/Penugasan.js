Ext.define('SimpegGundul.store.Penugasan', {
    extend: 'Ext.data.Store',
    requires: 'SimpegGundul.model.Penugasan',
    model: 'SimpegGundul.model.Penugasan',
    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: 'Penugasan/getData',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});
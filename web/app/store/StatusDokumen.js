Ext.define('SimpegGundul.model.StatusDokumen', {
    extend: 'Ext.data.Model',
    idProperty: 'status_dokumen_id',
    fields: [
        { name: 'status_dokumen_id', type: 'int' },
        { name: 'kode', type: 'string' },
        { name: 'nama', type: 'string' },
        { name: 'keterangan', type: 'string' }
    ]
});


Ext.define('SimpegGundul.store.StatusDokumen', {
    extend: 'Ext.data.Store',
    model: 'SimpegGundul.model.StatusDokumen',
    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'data/status_dokumen.json',
        reader: {
            type: 'json'
        },
        listeners: {
            exception: function (proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data Penyesuaian ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});
Ext.define('SimpegGundul.store.Kegiatan', {
    extend: 'Ext.data.Store',
    requires: 'SimpegGundul.model.Kegiatan',
    model: 'SimpegGundul.model.Kegiatan',
    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: 'Kegiatan/getData',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});
Ext.define('SimpegGundul.store.KepalaSekolah', {
    extend: 'Ext.data.Store',
    requires: 'SimpegGundul.model.KepalaSekolah',
    model: 'SimpegGundul.model.KepalaSekolah',
    pageSize: 250,
    autoLoad: true,
    proxy: {
        type: 'rest',
        url: 'KepalaSekolah/getData',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function (proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    },
    sorters: [{
        property: 'tmt',
        direction: 'ASC'
    }],
});
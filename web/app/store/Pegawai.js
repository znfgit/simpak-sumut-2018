Ext.define('SimpegGundul.store.Pegawai', {
    extend: 'Ext.data.Store',
    requires: 'SimpegGundul.model.Pegawai',
    model: 'SimpegGundul.model.Pegawai',
    pageSize: 50,
    autoLoad: false,
    listeners: {
        write: function (store, operation) {
            var record = operation.getRecords()[0],
                name = Ext.String.capitalize(operation.action),
                verb;

            var msg = "";
            switch (name) {
                case 'Create':
                    msg = 'dibuat';
                    break;
                case 'Update':
                    msg = 'diperbarui';
                    break;
                case 'Destroy':
                    msg = 'dihapus';
                    break;
            }
            if (operation.wasSuccessful()) {
                Xond.msg('Info', 'Data Pegawai berhasil ' + msg);
            } else {
                var errorMsg = operation.getError();
                //Xond.msg('Info', 'Gagal menyimpan data Pegawai ('+ errorMsg +')');
                Ext.Msg.alert('Error', 'Gagal menyimpan data Pegawai (' + errorMsg + ')');
            }
        }
    },
    proxy: {
        type: 'rest',
        url: 'Pegawai/getData',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function (proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data Pegawai ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});
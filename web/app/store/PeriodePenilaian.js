Ext.define('SimpegGundul.store.PeriodePenilaian', {
    extend: 'Ext.data.Store',
    requires: 'SimpegGundul.model.PeriodePenilaian',
    model: 'SimpegGundul.model.PeriodePenilaian',
    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: 'PeriodePenilaian/getData',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});
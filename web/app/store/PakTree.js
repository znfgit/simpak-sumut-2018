Ext.define('SimpegGundul.store.PakTree', {
    extend: 'Ext.data.TreeStore',
    requires: 'SimpegGundul.model.PakTree',
    model: 'SimpegGundul.model.PakTree',
    // autoLoad: true,
    proxy: {
        type: 'ajax',
        reader: 'json',
        url: 'Penilaian/getDataPakTree',
        listeners: {
            exception: function (proxy, response, operation, eOpts) {
                var json = Ext.decode(response.responseText);

                Ext.toast(json.msg, 'Error');
            }
        }
    }
});
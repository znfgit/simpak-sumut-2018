Ext.define('SimpegGundul.store.PengajuanKartu', {
    extend: 'Ext.data.Store',
    requires: 'SimpegGundul.model.PengajuanKartu',
    model: 'SimpegGundul.model.PengajuanKartu',
    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: 'PengajuanKartu/getData',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});
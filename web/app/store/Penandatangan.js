Ext.define('SimpegGundul.store.Penandatangan', {
    extend: 'Ext.data.Store',
    requires: 'SimpegGundul.model.Penandatangan',
    model: 'SimpegGundul.model.Penandatangan',
    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: 'Penandatangan/getData',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});
Ext.define('SimpegGundul.store.Pengguna', {
    extend: 'Ext.data.Store',
    requires: 'SimpegGundul.model.Pengguna',
    model: 'SimpegGundul.model.Pengguna',
    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: 'Pengguna/getData',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});
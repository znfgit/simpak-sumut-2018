Ext.define('SimpegGundul.store.Peran', {
    extend: 'Ext.data.Store',
    requires: 'SimpegGundul.model.Peran',
    model: 'SimpegGundul.model.Peran',
    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: 'Peran/getData',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});
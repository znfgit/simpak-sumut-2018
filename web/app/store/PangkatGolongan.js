Ext.define('SimpegGundul.store.PangkatGolongan', {
    extend: 'Ext.data.Store',
    requires: 'SimpegGundul.model.PangkatGolongan',
    model: 'SimpegGundul.model.PangkatGolongan',
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'rows'
        }
    }
    ,
    autoLoad: true,
    data: {
        rows: [
            { pangkat_golongan_id: 1 , nama: "Juru Muda, (Gol. I\/a)", golongan: "I\/a", pangkat: "Pembantu Pelaksana", jabatan_guru: "-", jabatan_pengawas: "-", akk_min: ".00", pd_min: ".00", piki_min: ".00", akp_min: ".00", kumulatif_min: ".00", pangkat_golongan_kenaikan: 2  },
            { pangkat_golongan_id: 10, nama: "Penata Muda Tk. I, ( Gol. III\/b )", golongan: "III\/b", pangkat: "Guru Madya Tk. I", jabatan_guru: "Guru Madya Tk. I", jabatan_pengawas: "Pengawas Sekolah Pratama", akk_min: "100.00", pd_min: "3.00", piki_min: "6.00", akp_min: "10.00", kumulatif_min: "200.00", pangkat_golongan_kenaikan: 11 },
            { pangkat_golongan_id: 11, nama: "Penata, ( Gol. III\/c )", golongan: "III\/c", pangkat: "Guru Dewasa", jabatan_guru: "Guru Dewasa", jabatan_pengawas: "Pengawas Sekolah Muda", akk_min: "100.00", pd_min: "4.00", piki_min: "8.00", akp_min: "10.00", kumulatif_min: "300.00", pangkat_golongan_kenaikan: 12 },
            { pangkat_golongan_id: 12, nama: "Penata Tk. I, ( Gol. III\/d )", golongan: "III\/d", pangkat: "Guru Dewasa Tk. I", jabatan_guru: "Guru Dewasa Tk. I", jabatan_pengawas: "Pengawas Sekolah Muda", akk_min: "150.00", pd_min: "4.00", piki_min: "12.00", akp_min: "15.00", kumulatif_min: "400.00", pangkat_golongan_kenaikan: 13 },
            { pangkat_golongan_id: 13, nama: "Pembina, ( Gol. IV\/a )", golongan: "IV\/a", pangkat: "Guru Pembina", jabatan_guru: "Guru Pembina", jabatan_pengawas: "Pengawas Sekolah Madya", akk_min: "150.00", pd_min: "4.00", piki_min: "12.00", akp_min: "15.00", kumulatif_min: "550.00", pangkat_golongan_kenaikan: 14 },
            { pangkat_golongan_id: 14, nama: "Pembina Tk. I, ( Gol. IV\/b )", golongan: "IV\/b", pangkat: "Guru Pembina Tk. I", jabatan_guru: "Guru Pembina Tk. I", jabatan_pengawas: "Pengawas Sekolah Madya", akk_min: "150.00", pd_min: "5.00", piki_min: "14.00", akp_min: "15.00", kumulatif_min: "700.00", pangkat_golongan_kenaikan: 15 },
            { pangkat_golongan_id: 15, nama: "Pembina Utama Muda, ( Gol. IV\/c )", golongan: "IV\/c", pangkat: "Guru Utama Muda", jabatan_guru: "Guru Utama Muda", jabatan_pengawas: "Pengawas Sekolah Madya", akk_min: "200.00", pd_min: "5.00", piki_min: "20.00", akp_min: "20.00", kumulatif_min: "850.00", pangkat_golongan_kenaikan: 16 },
            { pangkat_golongan_id: 16, nama: "Pembina Utama Madya, ( Gol. IV\/d )", golongan: "IV\/d", pangkat: "Guru Utama Madya", jabatan_guru: "Guru Utama Madya", jabatan_pengawas: "Pengawas Sekolah Utama", akk_min: ".00", pd_min: ".00", piki_min: ".00", akp_min: ".00", kumulatif_min: "1050.00", pangkat_golongan_kenaikan: 17 },
            { pangkat_golongan_id: 17, nama: "Pembina Utama, (Gol. IV\/e)", golongan: "IV\/e", pangkat: "Guru Utama", jabatan_guru: "Guru Utama", jabatan_pengawas: "Pengawas Sekolah Utama", akk_min: ".00", pd_min: ".00", piki_min: ".00", akp_min: ".00", kumulatif_min: ".00", pangkat_golongan_kenaikan: null },
            { pangkat_golongan_id: 2 , nama: "Juru Muda Tk. I, (Gol. I\/b)", golongan: "I\/b", pangkat: "Pembantu Pelaksana", jabatan_guru: "-", jabatan_pengawas: "-", akk_min: ".00", pd_min: ".00", piki_min: ".00", akp_min: ".00", kumulatif_min: ".00", pangkat_golongan_kenaikan: 3  },
            { pangkat_golongan_id: 3 , nama: "Juru, (Gol. I\/c)", golongan: "I\/c", pangkat: "Pembantu Pelaksana", jabatan_guru: "-", jabatan_pengawas: "-", akk_min: ".00", pd_min: ".00", piki_min: ".00", akp_min: ".00", kumulatif_min: ".00", pangkat_golongan_kenaikan: 4  },
            { pangkat_golongan_id: 4 , nama: "Juru Tk. I, (Gol. I\/d)", golongan: "I\/d", pangkat: "Pembantu Pelaksana", jabatan_guru: "-", jabatan_pengawas: "-", akk_min: ".00", pd_min: ".00", piki_min: ".00", akp_min: ".00", kumulatif_min: ".00", pangkat_golongan_kenaikan: 5  },
            { pangkat_golongan_id: 5 , nama: "Pengatur Muda, ( Gol. II\/a )", golongan: "II\/a", pangkat: "Guru Pratama", jabatan_guru: "Guru Pratama", jabatan_pengawas: "-", akk_min: ".00", pd_min: ".00", piki_min: ".00", akp_min: ".00", kumulatif_min: "40.00", pangkat_golongan_kenaikan: 6  },
            { pangkat_golongan_id: 6 , nama: "Pengatur Muda Tk. I, ( Gol. II\/b )", golongan: "II\/b", pangkat: "Guru Pratama Tk. I", jabatan_guru: "Guru Pratama Tk. I", jabatan_pengawas: "-", akk_min: ".00", pd_min: ".00", piki_min: ".00", akp_min: ".00", kumulatif_min: "60.00", pangkat_golongan_kenaikan: 7  },
            { pangkat_golongan_id: 7 , nama: "Pengatur, ( Gol. II\/c )", golongan: "II\/c", pangkat: "Guru Muda", jabatan_guru: "Guru Muda", jabatan_pengawas: "-", akk_min: ".00", pd_min: ".00", piki_min: ".00", akp_min: ".00", kumulatif_min: "80.00", pangkat_golongan_kenaikan: 8  },
            { pangkat_golongan_id: 8 , nama: "Pengatur Tk. I, ( Gol. II\/d )", golongan: "II\/d", pangkat: "Guru Muda Tk. I", jabatan_guru: "Guru Muda Tk. I", jabatan_pengawas: "-", akk_min: "50.00", pd_min: "3.00", piki_min: ".00", akp_min: "5.00", kumulatif_min: "100.00", pangkat_golongan_kenaikan: 9  },
            { pangkat_golongan_id: 9 , nama: "Penata Muda, ( Gol. III\/a )", golongan: "III\/a", pangkat: "Guru Madya", jabatan_guru: "Guru Madya", jabatan_pengawas: "Pengawas Sekolah Pratama", akk_min: "50.00", pd_min: "3.00", piki_min: "4.00", akp_min: "10.00", kumulatif_min: "150.00", pangkat_golongan_kenaikan: 10 }
        ]
    }
});

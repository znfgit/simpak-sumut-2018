Ext.define('SimpegGundul.store.JenisPtk', {
    extend: 'Ext.data.Store',
    requires: 'SimpegGundul.model.JenisPtk',
    model: 'SimpegGundul.model.JenisPtk',
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'rows'
        }
    }
    ,
    autoLoad: true,
    data: {
        rows: [
            { "jenis_ptk_id": 3, "jenis_ptk": "Guru Kelas" },
            { "jenis_ptk_id": 4, "jenis_ptk": "Guru Mapel" },
            { "jenis_ptk_id": 5, "jenis_ptk": "Guru BK" },
            { "jenis_ptk_id": 6, "jenis_ptk": "Guru Inklusi" },
            { "jenis_ptk_id": 12, "jenis_ptk": "Guru Pendamping" },
            { "jenis_ptk_id": 13, "jenis_ptk": "Guru Magang" },
            { "jenis_ptk_id": 14, "jenis_ptk": "Guru TIK" },
            { "jenis_ptk_id": 11, "jenis_ptk": "Tenaga Administrasi Sekolah" },
            { "jenis_ptk_id": 30, "jenis_ptk": "Laboran" },
            { "jenis_ptk_id": 40, "jenis_ptk": "Tenaga Perpustakaan" },
            { "jenis_ptk_id": 41, "jenis_ptk": "Tukang Kebun" },
            { "jenis_ptk_id": 42, "jenis_ptk": "Penjaga Sekolah" },
            { "jenis_ptk_id": 43, "jenis_ptk": "Petugas Keamanan" },
            { "jenis_ptk_id": 44, "jenis_ptk": "Pesuruh/Office Boy" },
        ]
    }
});

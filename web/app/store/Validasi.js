Ext.define('SimpegGundul.store.Validasi', {
    extend: 'Ext.data.Store',
    requires: 'SimpegGundul.model.Validasi',
    model: 'SimpegGundul.model.Validasi',
    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: 'Ptk/getValidasiBkn',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});
/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('SimpegGundul.view.penandatangan.PenandatanganController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.penandatangan',

    onRenderGrid: function (grid) {
        var store = grid.getStore();

        // store.on('beforeload', function (cmp) {
        //     Ext.apply(store.proxy.extraParams, {
        //         status: "[2,4,5]",
        //     });
        // });

        store.reload();
    },

    addRecord: function (btn) {
        // Dari button, 'look up'/lihat ke atas, cari container dengan xtype = 'form'
        var grid = btn.up('gridpanel');
        grid.rowEditing.cancelEdit();

        // Create a model instance
        var r = this.getNewRecord(grid);

        grid.store.insert(0, r);
        grid.rowEditing.startEdit(0, 0);
    },

    editRecord: function (btn) {
        // Defaults using row editor. Override this to use other method, such as form etc        
        var grid = btn.up('gridpanel');
        grid.rowEditing.cancelEdit();

        //var selections = grid.getSelectionModel().getSelection();
        var selections = grid.getSelection();
        var r = selections[0];
        if (!r) {
            Ext.toast('Mohon pilih salah satu baris', 'Error');
            return;
        }
        var startEditingColumnNumber = 0;
        for (var i = 0; i < grid.columns.length; i++) {
            if (grid.columns[i].isVisible()) {
                var startEditingColumnNumber = i;
                break;
            }
        }
        grid.rowEditing.startEdit(r, startEditingColumnNumber);
    },

    saveRecord: function (btn) {
        var grid = btn.up('gridpanel');

        var stores = grid.store;
        var arr = [];

        stores.each(function (record) {

            if (record.dirty) {
                // console.log(record);
                var arrRec = { penandatangan_id: record.data.penandatangan_id };

                for (var mod in record.modified) {
                    arrRec[mod] = record.data[mod];
                }

                arr.push(arrRec);
            }

        });

        if (Array.isArray(arr) !== true) {
            Ext.toast('Tidak ada yang diperbaharui', 'Info');
            return;
        }

        // console.log(JSO N.stringify(arr));
        Ext.getBody().mask('Tunggu sebentar...');
        Ext.Ajax.request({
            url: 'Penandatangan/save',
            method: 'GET',
            params: {
                data: JSON.stringify(arr)
            },
            success: function (response) {
                var text = response.responseText;

                var obj = Ext.JSON.decode(response.responseText);

                Ext.toast('Menyimpan data Penandatangan (Berhasil: ' + obj.berhasil + ', Gagal: ' + obj.gagal + ')', 'Berhasil');
                Ext.getBody().unmask();
                stores.reload();
            }
        });
    },

    deleteRecord: function (btn) {
        // Defaults using row editor. Override this to use other method, such as form etc        
        var grid = btn.up('gridpanel');
        grid.rowEditing.cancelEdit();

        //var selections = grid.getSelectionModel().getSelection();
        var selections = grid.getSelection();
        var r = selections[0];
        if (!r) {
            Ext.toast('Mohon pilih salah satu baris', 'Error');
            return;
        }

        Ext.Msg.show({
            title: 'Konfirmasi',
            msg: 'Anda yakin? ',
            width: 300,
            closable: false,
            buttons: Ext.Msg.YESNO,
            multiline: false,
            fn: function (buttonValue, inputText, showConfig) {
                if (buttonValue === "yes") {
                    Ext.getBody().mask('Tunggu sebentar...');

                    Ext.Ajax.request({
                        url: 'Penandatangan/delete',
                        method: 'GET',
                        params: {
                            id: r.data.penandatangan_id
                        },
                        success: function (response) {
                            var text = response.responseText;

                            var obj = Ext.JSON.decode(response.responseText);

                            Ext.toast(obj.msg, 'Info');
                            Ext.getBody().unmask();
                            grid.getStore().reload();
                        },
                        failure: function () {
                            Ext.toast('Terjadi Kesalahan, hubungi admin');
                        }
                    });
                }
            },
            icon: Ext.Msg.QUESTION
        });
    },

    getNewRecord: function (grid) {
        var recordConfig = {
            penandatangan_id: '',
            atas_nama: '',
            jabatan: '',
            instansi: '',
            nama: '',
            nip: '',
            pangkat_golongan_id: ''
        };
        Ext.apply(recordConfig, grid.newRecordCfg);
        var r = new SimpegGundul.model.Penandatangan(recordConfig);
        return r;
    }

});

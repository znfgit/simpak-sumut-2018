Ext.define('SimpegGundul.view.penandatangan.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.penandatangangrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    columnLines: true,
    controller: 'penandatangan',
    autoLoad: true,

    viewConfig: {
        listeners: {
            refresh: function (dataview) {
                Ext.each(dataview.panel.columns, function (column) {
                    // if (column.autoSizeColumn === true)
                    column.autoSize();
                })
            }
        }
    },

    createStore: function () {
        return Ext.create('SimpegGundul.store.Penandatangan');
    },

    createEditing: function () {
        return Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2,
            pluginId: 'editing'
        });
    },

    createDockedItems: function () {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                itemId: 'btnTambah',
                text: 'Tambah',
                iconCls: 'x-fa fa-plus-circle',
                handler: 'addRecord'
            }, {
                xtype: 'button',
                itemId: 'btnUbah',
                text: 'Ubah',
                iconCls: 'x-fa fa-pencil-square',
                handler: 'editRecord'
            }, {
                xtype: 'button',
                text: 'Simpan',
                iconCls: 'x-fa fa-check fa-lg glyph-blue glyph-shadow',
                itemId: 'save',
                handler: 'saveRecord'
            }, {
                xtype: 'button',
                text: 'Hapus',
                iconCls: 'x-fa fa-remove fa-lg glyph-red glyph-shadow',
                itemId: 'delete',
                handler: 'deleteRecord'
            }]
        }];
    },

    createBbar: function () {
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    initComponent: function () {

        var grid = this;

        this.store = this.createStore();

        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        // You can instaniate the component with basic parameters that affects filtering of the store
        // For example:
        // {
        //     xtype: 'yourgrid',
        //     baseParams: {
        //         area_id: 120,
        //         status_id: 1
        //     }
        // }
        if (this.initialConfig.baseParams) {

            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function (store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });

        }

        // this.getSelectionModel().setSelectionMode('SINGLE');

        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            listeners: {
                cancelEdit: function (rowEditing, context) {
                    // Canceling editing of a locally added, unsaved record: remove it
                    if (context.record.phantom) {
                        grid.store.remove(context.record);
                    }
                }
            }
        });

        var editing = this.createEditing();

        this.rowEditing = editing;
        this.plugins = [editing];

        this.lookupStorePangkatGolongan = new SimpegGundul.store.PangkatGolongan({
            autoLoad: true
        });

        this.columns = [{
            header: 'Id',
            tooltip: 'Id',
            width: 1020,
            sortable: true,
            dataIndex: 'penandatangan_id',
            hideable: false,
            hidden: true,
            field: {
                xtype: 'hidden'
                , maxLength: 255
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Atas Nama',
            tooltip: 'Atas Nama',
            width: 1020,
            sortable: true,
            dataIndex: 'atas_nama',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 255
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Jabatan',
            tooltip: 'Jabatan',
            width: 1020,
            sortable: true,
            dataIndex: 'jabatan',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 255
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Instansi',
            tooltip: 'Instansi',
            width: 1020,
            sortable: true,
            dataIndex: 'instansi',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 255
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Nama',
            tooltip: 'Nama',
            width: 1020,
            sortable: true,
            dataIndex: 'nama',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 255
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Nip',
            tooltip: 'Nip',
            width: 200,
            sortable: true,
            dataIndex: 'nip',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 50
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Pangkat Golongan',
            tooltip: 'Pangkat Golongan',
            width: 80,
            sortable: true,
            dataIndex: 'pangkat_golongan_id',
            hideable: false,
            renderer: function (v, p, r) {
                var record = this.lookupStorePangkatGolongan.getById(v);
                if (record) {
                    return record.get('nama');
                } else {
                    return v;
                }
            },
            field: {
                displayField: 'nama',
                xtype: 'combo',
                valueField: 'pangkat_golongan_id',
                displayField: 'nama',
                queryMode: 'local',
                store: Ext.create('SimpegGundul.store.PangkatGolongan', {
                    sorters: ['pangkat_golongan_id']
                })
            }

        }];

        this.dockedItems = this.createDockedItems();

        this.bbar = this.createBbar();

        this.callParent(arguments);
    }
});
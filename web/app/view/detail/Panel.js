Ext.define('SimpegGundul.view.detail.Panel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.detailpanel',
    bodyPadding: 10,
    defaults: {
        labelWidth: 280,
        anchor: '100%'
    },
    initComponent: function () {

        var form = this;

        var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record) {
            this.listeners = {
                afterrender: function (form, options) {
                    form.loadRecord(record);
                }
            };
        }

        // var storeAgama = new KarirGundul.store.Agama({
        //     autoLoad: true
        // });

        /*this.on('beforeadd', function(form, field){
            if (!field.allowBlank)
              field.labelSeparator += '<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>';
        });*/

        this.items = [/* {
            xtype: 'panel',
            height: 200,
            border: true
        },  */{
            xtype: 'fieldset'
            , title: 'Identitas'
            , collapsible: true
            , labelAlign: 'right'
            , defaults: {
                labelWidth: 185
                , margin: "0 0 -10 0"
                , anchor: '100%'
                , renderer: function (v) {
                    if (v) {
                        return '<b>' + v + '</b>';
                    } else {
                        return "-";
                    }
                }
            }
            , items: [{
                xtype: 'hidden'
                , fieldLabel: 'Id'
                , labelAlign: 'right'
                , labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
                , allowBlank: false
                , minValue: 0
                , maxLength: 200
                , enforceMaxLength: true
                , name: 'ptk_id'
                , tabIndex: 0
            }, {
                xtype: 'displayfield'
                , fieldLabel: 'Nama'
                , labelAlign: 'right'
                , minValue: 0
                , maxLength: 100
                , enforceMaxLength: true
                , name: 'nama'
                , tabIndex: 1
            }, {
                xtype: 'displayfield'
                , fieldLabel: 'Jenis kelamin'
                , labelAlign: 'right'
                , maxValue: 9999
                , minValue: 0
                , hideTrigger: true
                , maxLength: 4
                , enforceMaxLength: true
                , name: 'jenis_kelamin'
                , tabIndex: 2
                , renderer: function (v) {
                    if (v) {
                        if (v == 1) {
                            return '<b>Laki - laki</b>';
                        } else {
                            return "<b>Perempuan</b>"
                        }
                    }
                }
            }, {
                xtype: 'displayfield'
                , fieldLabel: 'NIY/NIGK'
                , labelAlign: 'right'
                , minValue: 0
                , maxLength: 30
                , enforceMaxLength: true
                , name: 'niy_nigk'
                , tabIndex: 7
            }, {
                xtype: 'displayfield'
                , fieldLabel: 'NUPTK'
                , labelAlign: 'right'
                , minValue: 0
                , maxLength: 20
                , enforceMaxLength: true
                , name: 'nuptk'
                , tabIndex: 8
            }, {
                xtype: 'displayfield'
                , fieldLabel: 'Tempat lahir'
                , labelAlign: 'right'
                , minValue: 0
                , maxLength: 100
                , enforceMaxLength: true
                , name: 'tempat_lahir'
                , tabIndex: 9
            }, {
                xtype: 'displayfield'
                , fieldLabel: 'Tgl lahir'
                , labelAlign: 'right'
                , minValue: 0
                , maxLength: 20
                , enforceMaxLength: true
                , name: 'tgl_lahir'
                , tabIndex: 10
                , renderer: Ext.util.Format.dateRenderer('d/m/Y')
            }, {
                xtype: 'displayfield'
                , fieldLabel: 'NIK'
                , labelAlign: 'right'
                , minValue: 0
                , maxLength: 30
                , enforceMaxLength: true
                , name: 'nik'
                , tabIndex: 11
            }, {
                xtype: 'displayfield'
                , fieldLabel: 'Agama'
                , labelAlign: 'right'
                , maxValue: 9999
                , minValue: 0
                , hideTrigger: true
                , maxLength: 4
                , enforceMaxLength: true
                , name: 'agama_id'
                , tabIndex: 12
                , renderer: function (v) {
                    // if (v) {
                    //     var record = storeAgama.getById(v);
                    //     if (record) {
                    //         return "<b>" + record.get('nama') + "</b>";
                    //     } else {
                            return v;
                    //     }
                    // }
                }
            }, {
                xtype: 'displayfield'
                , fieldLabel: 'Status perkawinan'
                , labelAlign: 'right'
                , maxValue: 9999
                , minValue: 0
                , hideTrigger: true
                , maxLength: 4
                , enforceMaxLength: true
                , name: 'status_perkawinan'
                , tabIndex: 13
                , renderer: function (v) {
                    if (v) {
                        if (v == 1) {
                            return "<b>Kawin</b>";
                        } else if (v == 2) {
                            return "<b>Belum Kawin</b>";
                        } else if (v == 3) {
                            return "<b>Janda/Duda</b>";
                        } else {
                            return v;
                        }
                    }
                }
            }, {
                xtype: 'displayfield'
                , fieldLabel: 'Nama ibu kandung'
                , labelAlign: 'right'
                , minValue: 0
                , maxLength: 100
                , enforceMaxLength: true
                , name: 'nama_ibu_kandung'
                , tabIndex: 14
            }, {
                xtype: 'displayfield'
                , fieldLabel: 'Alamat jalan'
                , labelAlign: 'right'
                , minValue: 0
                , maxLength: 255
                , enforceMaxLength: true
                , name: 'alamat_jalan'
                , tabIndex: 15
            }, {
                xtype: 'displayfield'
                , fieldLabel: 'RT/RW'
                , labelAlign: 'right'
                , minValue: 0
                , maxLength: 3
                , enforceMaxLength: true
                , name: 'rt'
                , tabIndex: 16
            }, {
                xtype: 'displayfield'
                , fieldLabel: 'Desa/Kelurahan'
                , labelAlign: 'right'
                , minValue: 0
                , maxLength: 80
                , enforceMaxLength: true
                , name: 'nama_desa_kelurahan'
                , tabIndex: 18
            }, {
                xtype: 'displayfield'
                , fieldLabel: 'Kode pos'
                , labelAlign: 'right'
                , minValue: 0
                , maxLength: 10
                , enforceMaxLength: true
                , name: 'kode_pos'
                , tabIndex: 19
            }, {
                xtype: 'displayfield'
                , fieldLabel: 'Kecamatan'
                , labelAlign: 'right'
                , minValue: 0
                , maxLength: 256
                , enforceMaxLength: true
                , name: 'Nama_Kecamatan'
                , tabIndex: 181
            }, {
                xtype: 'displayfield'
                , fieldLabel: 'kabupaten/Kota'
                , labelAlign: 'right'
                , minValue: 0
                , maxLength: 100
                , enforceMaxLength: true
                , name: 'nama_kabupaten_kota'
                , tabIndex: 53
            }]
        }, {
            xtype: 'fieldset'
            , title: 'Kepegawaian'
            , collapsible: true
            , labelAlign: 'right'
            , defaults: {
                labelWidth: 185
                , margin: "0 0 -10 0"
                , anchor: '100%'
                , renderer: function (v) {
                    if (v) {
                        return '<b>' + v + '</b>';
                    } else {
                        return "-";
                    }
                }
            }
            , items: [{
                xtype: 'displayfield'
                , fieldLabel: 'Jabatan fungsional'
                , labelAlign: 'right'
                , maxValue: 9999
                , minValue: 0
                , hideTrigger: true
                , maxLength: 4
                , enforceMaxLength: true
                , name: 'jabatan_fungsional'
                , tabIndex: 40
            }, {
                xtype: 'displayfield'
                , fieldLabel: 'Lembaga pengangkat'
                , labelAlign: 'right'
                , minValue: 0
                , maxLength: 50
                , enforceMaxLength: true
                , name: 'lembaga_pengangkat'
                , tabIndex: 41
            }, {
                xtype: 'displayfield'
                , fieldLabel: 'Nomor sk pengangkatan'
                , labelAlign: 'right'
                , minValue: 0
                , maxLength: 100
                , enforceMaxLength: true
                , name: 'nomor_sk_pengangkatan'
                , tabIndex: 42
            }, {
                xtype: 'displayfield'
                , fieldLabel: 'Tmt pengangkatan'
                , labelAlign: 'right'
                , minValue: 0
                , maxLength: 20
                , enforceMaxLength: true
                , name: 'tmt_pengangkatan'
                , tabIndex: 43
                , renderer: Ext.util.Format.dateRenderer('d/m/Y')
            }, {
                xtype: 'displayfield'
                , fieldLabel: 'Sumber gaji'
                , labelAlign: 'right'
                , maxValue: 9999
                , minValue: 0
                , hideTrigger: true
                , maxLength: 4
                , enforceMaxLength: true
                , name: 'Nama_Sumber_Gaji'
                , tabIndex: 44
            }, {
                xtype: 'displayfield'
                , labelAlign: 'right'
                , fieldLabel: 'Nomor SK KGB'
                , minValue: 0
                , maxLength: 100
                , enforceMaxLength: true
                , name: 'nomor_sk_kgb'
                , tabIndex: 45
            }, {
                xtype: 'displayfield'
                , fieldLabel: 'TMT KGB'
                , labelAlign: 'right'
                , minValue: 0
                , maxLength: 20
                , enforceMaxLength: true
                , name: 'tmt_kgb'
                , tabIndex: 46
            }]
        }
            // ,{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Ijazah terakhir id'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'ijazah_terakhir_id'
            //     ,tabIndex: 3
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tahun ijazah terakhir'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'tahun_ijazah_terakhir'
            //     ,tabIndex: 4
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Gelar akademik depan id'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'gelar_akademik_depan_id'
            //     ,tabIndex: 5
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Gelar akademik belakang id'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'gelar_akademik_belakang_id'
            //     ,tabIndex: 6
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'No telepon rumah'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 50
            //     ,enforceMaxLength: true
            //     ,name: 'no_telepon_rumah'
            //     ,tabIndex: 22
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'No hp'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 100
            //     ,enforceMaxLength: true
            //     ,name: 'no_hp'
            //     ,tabIndex: 23
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Email'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 100
            //     ,enforceMaxLength: true
            //     ,name: 'email'
            //     ,tabIndex: 24
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Status kepegawaian id'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'status_kepegawaian_id'
            //     ,tabIndex: 25
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tmt sekolah'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 20
            //     ,enforceMaxLength: true
            //     ,name: 'tmt_sekolah'
            //     ,tabIndex: 26
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jabatan ptk id'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'jabatan_ptk_id'
            //     ,tabIndex: 27
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tmt jabatan'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 20
            //     ,enforceMaxLength: true
            //     ,name: 'tmt_jabatan'
            //     ,tabIndex: 28
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jabatan ptk sebelumnya id'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'jabatan_ptk_sebelumnya_id'
            //     ,tabIndex: 29
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Sertifikasi jabatan'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'sertifikasi_jabatan'
            //     ,tabIndex: 30
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tahun sertifikasi jabatan'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'tahun_sertifikasi_jabatan'
            //     ,tabIndex: 31
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nomor sertifikat'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 100
            //     ,enforceMaxLength: true
            //     ,name: 'nomor_sertifikat'
            //     ,tabIndex: 32
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nip'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 30
            //     ,enforceMaxLength: true
            //     ,name: 'nip'
            //     ,tabIndex: 33
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tmt pns'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 20
            //     ,enforceMaxLength: true
            //     ,name: 'tmt_pns'
            //     ,tabIndex: 34
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Pangkat golongan id'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'pangkat_golongan_id'
            //     ,tabIndex: 35
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tmt pangkat gol'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 20
            //     ,enforceMaxLength: true
            //     ,name: 'tmt_pangkat_gol'
            //     ,tabIndex: 36
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Kode program keahlian laboran id'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'kode_program_keahlian_laboran_id'
            //     ,tabIndex: 37
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Sudah lisensi kepala sekolah'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'sudah_lisensi_kepala_sekolah'
            //     ,tabIndex: 38
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Sekolah id'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 255
            //     ,enforceMaxLength: true
            //     ,name: 'sekolah_id'
            //     ,tabIndex: 39
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nama ijazah terakhir'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 50
            //     ,enforceMaxLength: true
            //     ,name: 'nama_ijazah_terakhir'
            //     ,tabIndex: 47
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Kode bidang studi sertifikasi'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 3
            //     ,enforceMaxLength: true
            //     ,name: 'kode_bidang_studi_sertifikasi'
            //     ,tabIndex: 48
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nama sekolah'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 255
            //     ,enforceMaxLength: true
            //     ,name: 'nama_sekolah'
            //     ,tabIndex: 49
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Status sekolah'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'status_sekolah'
            //     ,tabIndex: 50
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nama jenis sekolah'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 50
            //     ,enforceMaxLength: true
            //     ,name: 'nama_jenis_sekolah'
            //     ,tabIndex: 51
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jenis sekolah id'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'jenis_sekolah_id'
            //     ,tabIndex: 52
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Kabupaten kota sekolah id'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'kabupaten_kota_sekolah_id'
            //     ,tabIndex: 54
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nama propinsi'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 100
            //     ,enforceMaxLength: true
            //     ,name: 'nama_propinsi'
            //     ,tabIndex: 55
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Propinsi sekolah id'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'propinsi_sekolah_id'
            //     ,tabIndex: 56
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nama pangkat golongan'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 60
            //     ,enforceMaxLength: true
            //     ,name: 'nama_pangkat_golongan'
            //     ,tabIndex: 57
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nama status kepegawaian'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 60
            //     ,enforceMaxLength: true
            //     ,name: 'nama_status_kepegawaian'
            //     ,tabIndex: 58
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Is pns'
            //     ,labelAlign: 'right'
            //     ,maxValue: 99
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 2
            //     ,enforceMaxLength: true
            //     ,name: 'is_pns'
            //     ,tabIndex: 59
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Status inpassing'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Status_Inpassing'
            //     ,tabIndex: 60
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'No sk inpassing'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 100
            //     ,enforceMaxLength: true
            //     ,name: 'No_SK_Inpassing'
            //     ,tabIndex: 61
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tahun sk inpassing'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Tahun_SK_Inpassing'
            //     ,tabIndex: 62
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nama tugas tambahan'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 50
            //     ,enforceMaxLength: true
            //     ,name: 'nama_tugas_tambahan'
            //     ,tabIndex: 63
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tugas tambahan id'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'tugas_tambahan_id'
            //     ,tabIndex: 64
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jam tugas tambahan'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'jam_tugas_tambahan'
            //     ,tabIndex: 65
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tmt tugas tambahan'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 20
            //     ,enforceMaxLength: true
            //     ,name: 'tmt_tugas_tambahan'
            //     ,tabIndex: 66
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tst tugas tambahan'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 20
            //     ,enforceMaxLength: true
            //     ,name: 'tst_tugas_tambahan'
            //     ,tabIndex: 67
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Sk tugas tambahan'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 100
            //     ,enforceMaxLength: true
            //     ,name: 'sk_tugas_tambahan'
            //     ,tabIndex: 68
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Sk tugas tambahan valid'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'sk_tugas_tambahan_Valid'
            //     ,tabIndex: 69
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Validasi tugas tambahan'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'validasi_tugas_tambahan'
            //     ,tabIndex: 70
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Keterangan validasi tugas tambahan'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 512
            //     ,enforceMaxLength: true
            //     ,name: 'keterangan_validasi_tugas_tambahan'
            //     ,tabIndex: 71
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jumlah jam mengajar'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'jumlah_jam_mengajar'
            //     ,tabIndex: 72
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jumlah jam mengajar sesuai'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'jumlah_jam_mengajar_sesuai'
            //     ,tabIndex: 73
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jumlah jam mengajar sesuai ktsp'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'jumlah_jam_mengajar_sesuai_ktsp'
            //     ,tabIndex: 74
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Total jam mengajar sesuai'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'total_jam_mengajar_sesuai'
            //     ,tabIndex: 75
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Masa kerja tahun'
            //     ,labelAlign: 'right'
            //     ,maxValue: 99
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 2
            //     ,enforceMaxLength: true
            //     ,name: 'masa_kerja_tahun'
            //     ,tabIndex: 76
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Masa kerja bulan'
            //     ,labelAlign: 'right'
            //     ,maxValue: 99
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 2
            //     ,enforceMaxLength: true
            //     ,name: 'masa_kerja_bulan'
            //     ,tabIndex: 77
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tgl pensiun'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 20
            //     ,enforceMaxLength: true
            //     ,name: 'tgl_pensiun'
            //     ,tabIndex: 78
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Sedang kuliah'
            //     ,labelAlign: 'right'
            //     ,maxValue: 99
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 2
            //     ,enforceMaxLength: true
            //     ,name: 'sedang_kuliah'
            //     ,tabIndex: 79
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nim'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 50
            //     ,enforceMaxLength: true
            //     ,name: 'NIM'
            //     ,tabIndex: 80
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Ipk'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'IPK'
            //     ,tabIndex: 81
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Bertugas di wilayah terpencil'
            //     ,labelAlign: 'right'
            //     ,maxValue: 99
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 2
            //     ,enforceMaxLength: true
            //     ,name: 'bertugas_di_wilayah_terpencil'
            //     ,tabIndex: 82
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nama rekening'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 50
            //     ,enforceMaxLength: true
            //     ,name: 'nama_rekening'
            //     ,tabIndex: 83
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'No rekening'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 50
            //     ,enforceMaxLength: true
            //     ,name: 'no_rekening'
            //     ,tabIndex: 84
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nama bank'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 50
            //     ,enforceMaxLength: true
            //     ,name: 'nama_bank'
            //     ,tabIndex: 85
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Cabang bank'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 50
            //     ,enforceMaxLength: true
            //     ,name: 'cabang_bank'
            //     ,tabIndex: 86
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Satuan pendidikan formal'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 100
            //     ,enforceMaxLength: true
            //     ,name: 'satuan_pendidikan_formal'
            //     ,tabIndex: 87
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Fakultas'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 30
            //     ,enforceMaxLength: true
            //     ,name: 'fakultas'
            //     ,tabIndex: 88
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Bidang studi id'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'bidang_studi_id'
            //     ,tabIndex: 89
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jenjang pendidikan id'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'jenjang_pendidikan_id'
            //     ,tabIndex: 90
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tahun masuk'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'tahun_masuk'
            //     ,tabIndex: 91
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Semester'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'semester'
            //     ,tabIndex: 92
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Status nuptk'
            //     ,labelAlign: 'right'
            //     ,maxValue: 99
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 2
            //     ,enforceMaxLength: true
            //     ,name: 'status_nuptk'
            //     ,tabIndex: 93
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Status syarat t profesi'
            //     ,labelAlign: 'right'
            //     ,maxValue: 99
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 2
            //     ,enforceMaxLength: true
            //     ,name: 'status_syarat_t_profesi'
            //     ,tabIndex: 94
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Status syarat t fungsional'
            //     ,labelAlign: 'right'
            //     ,maxValue: 99
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 2
            //     ,enforceMaxLength: true
            //     ,name: 'status_syarat_t_fungsional'
            //     ,tabIndex: 95
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Status syarat t kualifikasi'
            //     ,labelAlign: 'right'
            //     ,maxValue: 99
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 2
            //     ,enforceMaxLength: true
            //     ,name: 'status_syarat_t_kualifikasi'
            //     ,tabIndex: 96
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Status syarat t khusus'
            //     ,labelAlign: 'right'
            //     ,maxValue: 99
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 2
            //     ,enforceMaxLength: true
            //     ,name: 'status_syarat_t_khusus'
            //     ,tabIndex: 97
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nuptk link'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 50
            //     ,enforceMaxLength: true
            //     ,name: 'nuptk_link'
            //     ,tabIndex: 98
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nuptk link level'
            //     ,labelAlign: 'right'
            //     ,maxValue: 99
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 2
            //     ,enforceMaxLength: true
            //     ,name: 'nuptk_link_level'
            //     ,tabIndex: 99
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nama nuptk'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 160
            //     ,enforceMaxLength: true
            //     ,name: 'nama_nuptk'
            //     ,tabIndex: 100
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nama instansi nuptk'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 100
            //     ,enforceMaxLength: true
            //     ,name: 'nama_instansi_nuptk'
            //     ,tabIndex: 101
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Memenuhi syarat tunjangan profesi'
            //     ,labelAlign: 'right'
            //     ,maxValue: 99
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 2
            //     ,enforceMaxLength: true
            //     ,name: 'memenuhi_syarat_tunjangan_profesi'
            //     ,tabIndex: 102
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jumlah jam mengajar diluar dikdas'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'jumlah_jam_mengajar_diluar_dikdas'
            //     ,tabIndex: 103
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nuptk link2'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 16
            //     ,enforceMaxLength: true
            //     ,name: 'nuptk_link2'
            //     ,tabIndex: 104
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Is satap'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 1
            //     ,enforceMaxLength: true
            //     ,name: 'is_satap'
            //     ,tabIndex: 105
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jenis guru'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 32
            //     ,enforceMaxLength: true
            //     ,name: 'Jenis_Guru'
            //     ,tabIndex: 106
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Is sekolah induk flag2'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'is_sekolah_induk_flag2'
            //     ,tabIndex: 107
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tugas mengajar'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 200
            //     ,enforceMaxLength: true
            //     ,name: 'TugasMengajar'
            //     ,tabIndex: 108
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nuptk shadow'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 256
            //     ,enforceMaxLength: true
            //     ,name: 'nuptk_shadow'
            //     ,tabIndex: 109
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Sudah sertifikasi'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Sudah_Sertifikasi'
            //     ,tabIndex: 110
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jumlah sekolah terdaftar'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Jumlah_Sekolah_Terdaftar'
            //     ,tabIndex: 111
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jumlah sekolah induk'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Jumlah_Sekolah_Induk'
            //     ,tabIndex: 112
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Riwayat pendidikan formal terakhir id'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 200
            //     ,enforceMaxLength: true
            //     ,name: 'Riwayat_pendidikan_formal_terakhir_id'
            //     ,tabIndex: 113
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Riwayat kepangkatan terakhir id'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 200
            //     ,enforceMaxLength: true
            //     ,name: 'Riwayat_Kepangkatan_terakhir_id'
            //     ,tabIndex: 114
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Is sekolah induk'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 1
            //     ,enforceMaxLength: true
            //     ,name: 'is_sekolah_induk'
            //     ,tabIndex: 115
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Is sekolah induk flag'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'is_sekolah_induk_flag'
            //     ,tabIndex: 116
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Validasi sekolah induk'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Validasi_sekolah_induk'
            //     ,tabIndex: 117
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Keterangan validasi sekolah induk'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 256
            //     ,enforceMaxLength: true
            //     ,name: 'Keterangan_Validasi_sekolah_induk'
            //     ,tabIndex: 118
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jumlah murid terdaftar'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Jumlah_Murid_Terdaftar'
            //     ,tabIndex: 119
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Gaji pokok'
            //     ,labelAlign: 'right'
            //     ,maxValue: 1.0E+21
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 21
            //     ,enforceMaxLength: true
            //     ,name: 'Gaji_Pokok'
            //     ,tabIndex: 120
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Dasar penetapan'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 100
            //     ,enforceMaxLength: true
            //     ,name: 'Dasar_penetapan'
            //     ,tabIndex: 121
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nama sumber gaji'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 100
            //     ,enforceMaxLength: true
            //     ,name: 'Nama_Sumber_Gaji'
            //     ,tabIndex: 122
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jumlah rombel'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Jumlah_Rombel'
            //     ,tabIndex: 123
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jumlah wakasek diakui'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Jumlah_Wakasek_Diakui'
            //     ,tabIndex: 124
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Validasi wakasek'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'validasi_Wakasek'
            //     ,tabIndex: 125
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Catatan masalah'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 512
            //     ,enforceMaxLength: true
            //     ,name: 'catatan_masalah'
            //     ,tabIndex: 126
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Lulus sertifikasi'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'lulus_sertifikasi'
            //     ,tabIndex: 127
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jenis verifikasi tunjangan profesi'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Jenis_verifikasi_Tunjangan_Profesi'
            //     ,tabIndex: 128
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Verifikasi tunjangan profesi'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Verifikasi_Tunjangan_Profesi'
            //     ,tabIndex: 129
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Caatan dari pusat'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 1024
            //     ,enforceMaxLength: true
            //     ,name: 'Caatan_Dari_Pusat'
            //     ,tabIndex: 130
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Is daerah khusus'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'is_daerah_khusus'
            //     ,tabIndex: 131
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Is skb 5 menteri'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'is_skb_5_menteri'
            //     ,tabIndex: 132
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Catatan verifikasi dapodik untuk tp'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 1024
            //     ,enforceMaxLength: true
            //     ,name: 'catatan_verifikasi_dapodik_untuk_TP'
            //     ,tabIndex: 133
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Is bsd'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 1
            //     ,enforceMaxLength: true
            //     ,name: 'is_bsd'
            //     ,tabIndex: 134
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jumlah jam mengajar smt 1'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'jumlah_jam_mengajar_smt_1'
            //     ,tabIndex: 135
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jumlah jam mengajar sesuai smt 1'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'jumlah_jam_mengajar_sesuai_smt_1'
            //     ,tabIndex: 136
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Is perlakuan khusus id'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'is_perlakuan_khusus_id'
            //     ,tabIndex: 137
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Is slb'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'is_SLB'
            //     ,tabIndex: 138
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Is kj'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'is_KJ'
            //     ,tabIndex: 139
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Is inklusi'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'is_Inklusi'
            //     ,tabIndex: 140
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Is guru bk'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'is_Guru_BK'
            //     ,tabIndex: 141
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Rasio murid guru bk'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'rasio_Murid_GuruBK'
            //     ,tabIndex: 142
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jumlah guru bk'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Jumlah_Guru_BK'
            //     ,tabIndex: 143
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Status aktif'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'status_aktif'
            //     ,tabIndex: 144
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Validasi guru bk'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 128
            //     ,enforceMaxLength: true
            //     ,name: 'Validasi_Guru_BK'
            //     ,tabIndex: 145
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Mapel diajarkan'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 128
            //     ,enforceMaxLength: true
            //     ,name: 'mapel_diajarkan'
            //     ,tabIndex: 146
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Catatan verifikasi jjm'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 512
            //     ,enforceMaxLength: true
            //     ,name: 'catatan_verifikasi_JJM'
            //     ,tabIndex: 147
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jumlah tt pararel'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Jumlah_TT_Pararel'
            //     ,tabIndex: 148
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jumlah tt diakui'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Jumlah_TT_Diakui'
            //     ,tabIndex: 149
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tt aktif'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'TT_Aktif'
            //     ,tabIndex: 150
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Rasio murid guru bk penyesuaian'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Rasio_Murid_GuruBK_Penyesuaian'
            //     ,tabIndex: 151
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Status aktif tw1'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Status_aktif_tw1'
            //     ,tabIndex: 152
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Status aktif tw2'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Status_aktif_tw2'
            //     ,tabIndex: 153
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Status aktif tw3'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Status_aktif_tw3'
            //     ,tabIndex: 154
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Status aktif tw4'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Status_aktif_tw4'
            //     ,tabIndex: 155
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Sisa murid guru bk'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Sisa_Murid_Guru_BK'
            //     ,tabIndex: 156
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jum sekolah induk'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Jum_Sekolah_Induk'
            //     ,tabIndex: 157
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jum data'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Jum_Data'
            //     ,tabIndex: 158
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Catatan peringatan'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 1024
            //     ,enforceMaxLength: true
            //     ,name: 'catatan_peringatan'
            //     ,tabIndex: 159
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Sktp cadangan'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'SKTP_Cadangan'
            //     ,tabIndex: 160
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jumlah tt sudah sk'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'jumlah_tt_sudahSK'
            //     ,tabIndex: 161
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jumlah tt belum sk'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'jumlah_tt_BelumSK'
            //     ,tabIndex: 162
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Status dokumen sktp'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Status_dokumen_SKTP'
            //     ,tabIndex: 163
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jumlah jam mengajar unlocked'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'jumlah_jam_mengajar_unlocked'
            //     ,tabIndex: 164
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jumlah jam mengajar sesuai unlocked'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'jumlah_jam_mengajar_sesuai_unlocked'
            //     ,tabIndex: 165
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Total jam mengajar sesuai unlocked'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'total_jam_mengajar_sesuai_unlocked'
            //     ,tabIndex: 166
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jadi memenuhi'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Jadi_Memenuhi'
            //     ,tabIndex: 167
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jadi tidak memenuhi'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Jadi_Tidak_Memenuhi'
            //     ,tabIndex: 168
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Guru tik'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Guru_TIK'
            //     ,tabIndex: 169
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Syarat inpassing'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'syarat_inpassing'
            //     ,tabIndex: 170
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Is guru tik'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'is_Guru_TIK'
            //     ,tabIndex: 171
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Validasi guru tik'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 256
            //     ,enforceMaxLength: true
            //     ,name: 'validasi_Guru_TIK'
            //     ,tabIndex: 172
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Rasio murid guru tik'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'rasio_murid_GuruTIK'
            //     ,tabIndex: 173
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Rasio murid guru tik penyesuaian'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Rasio_Murid_GuruTIK_Penyesuaian'
            //     ,tabIndex: 174
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jumlah guru tik'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Jumlah_Guru_TIK'
            //     ,tabIndex: 175
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Is locked'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'is_locked'
            //     ,tabIndex: 176
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Validasi status kepegawaian'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Validasi_status_kepegawaian'
            //     ,tabIndex: 177
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Keterangan validasi status kepegawaian'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 256
            //     ,enforceMaxLength: true
            //     ,name: 'Keterangan_validasi_status_kepegawaian'
            //     ,tabIndex: 178
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Catatan kelulusan'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 1024
            //     ,enforceMaxLength: true
            //     ,name: 'catatan_kelulusan'
            //     ,tabIndex: 179
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Hasil verifikasi inpassing'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 265
            //     ,enforceMaxLength: true
            //     ,name: 'Hasil_Verifikasi_Inpassing'
            //     ,tabIndex: 180
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Catatan verifikasi usia'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 64
            //     ,enforceMaxLength: true
            //     ,name: 'catatan_verifikasi_usia'
            //     ,tabIndex: 182
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Catatan verifikasi kelengkapan data'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 1000
            //     ,enforceMaxLength: true
            //     ,name: 'catatan_verifikasi_Kelengkapan_data'
            //     ,tabIndex: 183
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Catatan verifikasi nuptk'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 64
            //     ,enforceMaxLength: true
            //     ,name: 'catatan_verifikasi_NUPTK'
            //     ,tabIndex: 184
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Validasi nuptk'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'validasi_NUPTK'
            //     ,tabIndex: 185
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Validasi kelulusan'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'validasi_Kelulusan'
            //     ,tabIndex: 186
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Validasi jjm'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'validasi_JJM'
            //     ,tabIndex: 187
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Validasi kelengkapan data'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'validasi_Kelengkapan_data'
            //     ,tabIndex: 188
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Validasi data'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'validasi_Data'
            //     ,tabIndex: 189
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Validasi masalah'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'validasi_Masalah'
            //     ,tabIndex: 190
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Validasi usia'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'validasi_usia'
            //     ,tabIndex: 191
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Keterangan validasi data'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 256
            //     ,enforceMaxLength: true
            //     ,name: 'Keterangan_Validasi_data'
            //     ,tabIndex: 192
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Catatan verifikasi pkg'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 128
            //     ,enforceMaxLength: true
            //     ,name: 'Catatan_Verifikasi_PKG'
            //     ,tabIndex: 193
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Validasi pkg'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Validasi_PKG'
            //     ,tabIndex: 194
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Catatan rekening'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 256
            //     ,enforceMaxLength: true
            //     ,name: 'catatan_rekening'
            //     ,tabIndex: 195
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Validasi rekening'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'validasi_rekening'
            //     ,tabIndex: 196
            // },{
            //     xtype: 'datefield'
            //     ,fieldLabel: 'Last update'
            //     ,labelAlign: 'right'
            //     ,format: 'd/m/Y'
            //     ,maxValue: new Date()
            //     ,maxLength: 16
            //     ,enforceMaxLength: true
            //     ,name: 'last_update'
            //     ,tabIndex: 197
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Mapel id diajarkan'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'mapel_id_diajarkan'
            //     ,tabIndex: 198
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jjm linier di sekolah induk'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'JJM_Linier_di_sekolah_induk'
            //     ,tabIndex: 199
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jjm di sekolah induk'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'JJM_di_sekolah_induk'
            //     ,tabIndex: 200
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nama pengawas'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 256
            //     ,enforceMaxLength: true
            //     ,name: 'Nama_Pengawas'
            //     ,tabIndex: 201
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nrg'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 16
            //     ,enforceMaxLength: true
            //     ,name: 'NRG'
            //     ,tabIndex: 202
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jenis ptk id'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'jenis_ptk_id'
            //     ,tabIndex: 203
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Terkena dampak k13'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'terkena_dampak_k13'
            //     ,tabIndex: 204
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jam tugas tambahan ekuivalensi'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Jam_Tugas_Tambahan_Ekuivalensi'
            //     ,tabIndex: 205
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Kode bidang studi sertifikasi 2'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 3
            //     ,enforceMaxLength: true
            //     ,name: 'kode_bidang_studi_sertifikasi_2'
            //     ,tabIndex: 206
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Is sekolah induk flag3'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'is_sekolah_induk_flag3'
            //     ,tabIndex: 207
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nama kelurahan'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 256
            //     ,enforceMaxLength: true
            //     ,name: 'Nama_kelurahan'
            //     ,tabIndex: 208
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'No peserta ukg'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 16
            //     ,enforceMaxLength: true
            //     ,name: 'No_peserta_UKG'
            //     ,tabIndex: 209
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nama murni'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 128
            //     ,enforceMaxLength: true
            //     ,name: 'nama_murni'
            //     ,tabIndex: 210
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nama ibu murni'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 128
            //     ,enforceMaxLength: true
            //     ,name: 'nama_ibu_murni'
            //     ,tabIndex: 211
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tahun'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'tahun'
            //     ,tabIndex: 212
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Bulan'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'bulan'
            //     ,tabIndex: 213
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tanggal'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'tanggal'
            //     ,tabIndex: 214
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tempat tugas murni'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 128
            //     ,enforceMaxLength: true
            //     ,name: 'tempat_tugas_murni'
            //     ,tabIndex: 215
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nama kabupaten kota murni'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 128
            //     ,enforceMaxLength: true
            //     ,name: 'nama_kabupaten_kota_murni'
            //     ,tabIndex: 216
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nama fakultas s1'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 256
            //     ,enforceMaxLength: true
            //     ,name: 'nama_fakultas_s1'
            //     ,tabIndex: 217
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nama jurusan s1'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 256
            //     ,enforceMaxLength: true
            //     ,name: 'nama_jurusan_s1'
            //     ,tabIndex: 218
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nama pt s1'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 256
            //     ,enforceMaxLength: true
            //     ,name: 'nama_pt_s1'
            //     ,tabIndex: 219
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Npsn'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 30
            //     ,enforceMaxLength: true
            //     ,name: 'NPSN'
            //     ,tabIndex: 220
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tahun lulus s1'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'tahun_lulus_s1'
            //     ,tabIndex: 221
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Npwp'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 15
            //     ,enforceMaxLength: true
            //     ,name: 'npwp'
            //     ,tabIndex: 222
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Mapel group'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 255
            //     ,enforceMaxLength: true
            //     ,name: 'Mapel_Group'
            //     ,tabIndex: 223
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tahun masuk s1'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Tahun_masuk_S1'
            //     ,tabIndex: 224
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nip bkn'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 18
            //     ,enforceMaxLength: true
            //     ,name: 'NIP_BKN'
            //     ,tabIndex: 225
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nama bkn'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 80
            //     ,enforceMaxLength: true
            //     ,name: 'Nama_BKN'
            //     ,tabIndex: 226
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tgl lahir bkn'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 20
            //     ,enforceMaxLength: true
            //     ,name: 'Tgl_Lahir_BKN'
            //     ,tabIndex: 227
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Nik bkn'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 30
            //     ,enforceMaxLength: true
            //     ,name: 'NIK_BKN'
            //     ,tabIndex: 228
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Id bkn'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 32
            //     ,enforceMaxLength: true
            //     ,name: 'ID_BKN'
            //     ,tabIndex: 229
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Kedudukan hukum bkn'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 50
            //     ,enforceMaxLength: true
            //     ,name: 'Kedudukan_Hukum_BKN'
            //     ,tabIndex: 230
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Jabatan bkn'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 220
            //     ,enforceMaxLength: true
            //     ,name: 'Jabatan_BKN'
            //     ,tabIndex: 231
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tmt pns bkn'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 20
            //     ,enforceMaxLength: true
            //     ,name: 'TMT_PNS_BKN'
            //     ,tabIndex: 232
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Kode verval bkn'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Kode_verval_BKN'
            //     ,tabIndex: 233
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Verval mode'
            //     ,labelAlign: 'right'
            //     ,maxValue: 9999
            //     ,minValue: 0
            //     ,hideTrigger:true
            //     ,maxLength: 4
            //     ,enforceMaxLength: true
            //     ,name: 'Verval_mode'
            //     ,tabIndex: 234
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Tmt mengajar'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 20
            //     ,enforceMaxLength: true
            //     ,name: 'tmt_mengajar'
            //     ,tabIndex: 235
            // },{
            //     xtype: 'displayfield'
            //     ,fieldLabel: 'Karpeg'
            //     ,labelAlign: 'right'
            //     ,minValue: 0
            //     ,maxLength: 20
            //     ,enforceMaxLength: true
            //     ,name: 'karpeg'
            //     ,tabIndex: 236

            // }
        ];


        this.callParent(arguments);
    }
});
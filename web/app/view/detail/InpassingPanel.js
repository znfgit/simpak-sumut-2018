Ext.define('SimpegGundul.view.detail.InpassingPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'inpassingpanel',
    layout: 'fit',

    initComponent: function () {

        var me = this;

        me.items = {
            xtype: 'panel',
            autoScroll: true,
            html: '<iframe src="Ptk/getInpassing?nuptk='+ptk_selected.nuptk+'"  scrolling="yes" style=" width: 100%; height: 98%;  overflow: hidden;" ></iframe>'
        };

        this.callParent();

    }
});

Ext.define('SimpegGundul.view.detail.TProfesiPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'tprofesipanel',
    layout: 'fit',

    initComponent: function () {

        var me = this;

        me.items = {
            xtype: 'panel',
            autoScroll: true,
            layout: 'fit',
            html: '<iframe src="Ptk/getTProfesi?kabupaten_kota_id=' + ptk_selected.kabupaten_kota_sekolah_id + '&nuptk=' + ptk_selected.nuptk + '"  scrolling="yes" style=" width: 100%; height: 98%;  overflow: hidden;" ></iframe>',
        };

        this.callParent();

    }
});

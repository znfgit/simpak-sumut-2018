/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('SimpegGundul.view.penugasan.PenugasanController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.penugasan',

    onRenderGrid: function (grid) {
        var store = grid.getStore();

        store.on('beforeload', function (cmp) {
            if (['3', '4', '5', '6', '99'].indexOf(user.level) >= 0) {
                Ext.apply(store.proxy.extraParams, {
                    status: "[2,4,5]",
                });
            } else if (user.level == '2') {
                Ext.apply(store.proxy.extraParams, {
                    status: "[0,1,2,3,4]",
                });
            }
        });

        store.reload();
    },

    addRecord: function (btn) {
        // Dari button, 'look up'/lihat ke atas, cari container dengan xtype = 'form'
        var grid = btn.up('gridpanel');
        grid.rowEditing.cancelEdit();

        // Create a model instance
        var r = this.getNewRecord(grid);

        grid.store.insert(0, r);
        grid.rowEditing.startEdit(0, 0);
    },

    editRecord: function (btn) {
        // Defaults using row editor. Override this to use other method, such as form etc        
        var grid = btn.up('gridpanel');
        grid.rowEditing.cancelEdit();

        //var selections = grid.getSelectionModel().getSelection();
        var selections = grid.getSelection();
        var r = selections[0];
        if (!r) {
            Ext.toast('Mohon pilih salah satu baris', 'Error');
            return;
        }
        var startEditingColumnNumber = 0;
        for (var i = 0; i < grid.columns.length; i++) {
            if (grid.columns[i].isVisible()) {
                var startEditingColumnNumber = i;
                break;
            }
        }
        grid.rowEditing.startEdit(r, startEditingColumnNumber);
    },

    saveRecord: function (btn) {
        var grid = btn.up('gridpanel');

        var stores = grid.store;
        var arr = [];

        stores.each(function (record) {

            if (record.dirty) {
                // console.log(record);
                var arrRec = { penugasan_id: record.data.penugasan_id };

                for (var mod in record.modified) {
                    arrRec[mod] = record.data[mod];
                }

                arr.push(arrRec);
            }

        });

        if (Array.isArray(arr) !== true) {
            Ext.toast('Tidak ada yang diperbaharui', 'Info');
            return;
        }

        // console.log(JSO N.stringify(arr));
        Ext.getBody().mask('Tunggu sebentar...');
        Ext.Ajax.request({
            url: 'Penugasan/save',
            method: 'GET',
            params: {
                data: JSON.stringify(arr)
            },
            success: function (response) {
                var text = response.responseText;

                var obj = Ext.JSON.decode(response.responseText);

                Ext.toast('Menyimpan data penugsan (Berhasil: ' + obj.berhasil + ', Gagal: ' + obj.gagal + ')', 'Berhasil');
                Ext.getBody().unmask();
                stores.reload();
            }
        });
    },

    deleteRecord: function (btn) {
        // Defaults using row editor. Override this to use other method, such as form etc        
        var grid = btn.up('gridpanel');
        grid.rowEditing.cancelEdit();

        //var selections = grid.getSelectionModel().getSelection();
        var selections = grid.getSelection();
        var r = selections[0];
        if (!r) {
            Ext.toast('Mohon pilih salah satu baris', 'Error');
            return;
        }

        Ext.Msg.show({
            title: 'Konfirmasi',
            msg: 'Anda yakin? ',
            width: 300,
            closable: false,
            buttons: Ext.Msg.YESNO,
            multiline: false,
            fn: function (buttonValue, inputText, showConfig) {
                if (buttonValue === "yes") {
                    Ext.getBody().mask('Tunggu sebentar...');

                    Ext.Ajax.request({
                        url: 'Penugasan/delete',
                        method: 'GET',
                        params: {
                            id: r.data.penugasan_id
                        },
                        success: function (response) {
                            var text = response.responseText;

                            var obj = Ext.JSON.decode(response.responseText);

                            Ext.toast(obj.msg, 'Info');
                            Ext.getBody().unmask();
                            grid.getStore().reload();
                        },
                        failure: function () {
                            Ext.toast('Terjadi Kesalahan, hubungi admin');
                        }
                    });
                }
            },
            icon: Ext.Msg.QUESTION
        });
    },

    getNewRecord: function (grid) {
        var recordConfig = {
            penugasan_id: '',
            sekolah_induk: '',
            sekolah_tujuan: '',
            ptk_id: '',
            no_surat: '',
            tgl_surat: Ext.Date.clearTime(new Date()),
            soft_delete: 0
        };
        Ext.apply(recordConfig, grid.newRecordCfg);
        var r = new SimpegGundul.model.Penugasan(recordConfig);
        return r;
    },

    onApprove: function (btn) {
        var approved = btn.approved;

        var grid = btn.up('penugasangrid');

        if (!grid.getSelection()[0]) {
            Ext.Msg.alert('Error', "Mohon Pilih PTK Terlebih Dahulu");
            return false;
        }

        var alasan = '';
        if ([1, 3].indexOf(approved) >= 0) {
            Ext.Msg.prompt('Alasan Penolakan', 'Masukkan alasan penolakan', function (btn, text) {
                if (btn == 'ok') {
                    alasan = text;
                    Ext.Msg.show({
                        title: 'Konfirmasi',
                        msg: 'Sudah yakin? ',
                        width: 300,
                        closable: false,
                        buttons: Ext.Msg.YESNO,
                        multiline: false,
                        fn: function (buttonValue, inputText, showConfig) {
                            if (buttonValue === "yes") {
                                var jsonData = "[";
                                for (var i = 0; i < grid.getSelection().length; i++) {
                                    var rec = grid.getSelection()[i];

                                    jsonData += Ext.util.JSON.encode(rec.data) + ",";
                                }
                                jsonData = jsonData.substring(0, jsonData.length - 1) + "]";

                                grid.mask('Tunggu sebentar...');
                                Ext.Ajax.request({
                                    waitMsg: 'Menyimpan....',
                                    url: 'Penugasan/approve',
                                    method: 'GET',
                                    params: {
                                        approved: approved,
                                        alasan: alasan,
                                        rows: jsonData
                                    },
                                    failure: function (res, opt) {
                                        grid.unmask();
                                    },
                                    success: function (res, opt) {
                                        grid.unmask();
                                        Ext.ComponentQuery.query('grid[xtype=penugasangrid]')[0].getStore().reload();
                                    }
                                });
                            }
                        },
                        icon: Ext.Msg.QUESTION
                    });
                }
            });
        } else {
            Ext.Msg.show({
                title: 'Konfirmasi',
                msg: 'Sudah yakin? ',
                width: 300,
                closable: false,
                buttons: Ext.Msg.YESNO,
                multiline: false,
                fn: function (buttonValue, inputText, showConfig) {
                    if (buttonValue === "yes") {
                        var jsonData = "[";
                        for (var i = 0; i < grid.getSelection().length; i++) {
                            var rec = grid.getSelection()[i];

                            jsonData += Ext.util.JSON.encode(rec.data) + ",";
                        }
                        jsonData = jsonData.substring(0, jsonData.length - 1) + "]";

                        grid.mask('Tunggu sebentar...');
                        Ext.Ajax.request({
                            waitMsg: 'Menyimpan....',
                            url: 'Penugasan/approve',
                            method: 'GET',
                            params: {
                                approved: approved,
                                rows: jsonData
                            },
                            failure: function (res, opt) {
                                grid.unmask();
                            },
                            success: function (res, opt) {
                                grid.unmask();
                                Ext.ComponentQuery.query('grid[xtype=penugasangrid]')[0].getStore().reload();
                            }
                        });
                    }
                },
                icon: Ext.Msg.QUESTION
            });
        }
    },

    onCetakSurat: function(btn){
        var grid = btn.up('grid');
        var sel = grid.getSelection();
        var rec = sel[0];
        if (sel.length <= 0) {
            Ext.toast('Mohon pilih salah satu PTK terlebih dahulu', 'Error');
            return;
        }

        if (user.level == '1'){
            var printUrl = 'Penugasan/getCetakPermohonan?id=' + rec.data.penugasan_id;
        }else{
            var printUrl = 'Penugasan/getCetakRekomendasi?id=' + rec.data.penugasan_id;
        }


        var win = Ext.widget({
            xtype: 'window_print',
            title: 'Cetak Surat',
            src: printUrl
        });

        win.show();
    }
});

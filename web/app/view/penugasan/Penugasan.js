Ext.define('SimpegGundul.view.penugasan.Penugasan', {
    extend: 'Ext.panel.Panel',
    xtype: 'penugasan',

    initComponent: function () {

        var me = this;

        me.items = {
            id: 'gridpenugasan',
            xtype: 'penugasangrid',
            title: 'Daftar Penugasan',
            iconCls: 'x-fa fa-tasks',
            // selType: 'checkboxmodel',
            // forceFit: true
        };

        this.callParent();

    }
});

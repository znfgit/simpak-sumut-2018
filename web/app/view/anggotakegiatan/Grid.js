Ext.define('SimpegGundul.view.anggotakegiatan.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.anggotakegiatangrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,

    listeners: {
        afterrender: 'onRenderGrid'
    },

    viewConfig: {
        listeners: {
            refresh: function (dataview) {
                Ext.each(dataview.panel.columns, function (column) {
                    // if (column.autoSizeColumn === true)
                    column.autoSize();
                })
            }
        }
    },

    createStore: function () {
        return Ext.create('SimpegGundul.store.AnggotaKegiatan');
    },

    createEditing: function () {
        return Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2,
            pluginId: 'editing'
        });
    },

    createDockedItems: function () {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                text: 'Ubah',
                iconCls: 'fa fa-eraser fa-lg glyph-dark-orange glyph-shadow',
                itemId: 'edit',
                handler: 'onEdit'
            }, {
                xtype: 'button',
                text: 'Simpan',
                iconCls: 'fa fa-check fa-lg glyph-blue glyph-shadow',
                itemId: 'save',
                handler: 'onSave'
            },'->',{
                xtype: 'button',
                text: 'Cetak Sertifikat',
                iconCls: 'x-fa fa-newspaper-o',
                handler: 'onCetakSertifikat'
            }]
        }];
    },

    createBbar: function () {
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },

    initComponent: function () {

        var grid = this;

        this.store = this.createStore();

        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        // You can instaniate the component with basic parameters that affects filtering of the store
        // For example:
        // {
        //     xtype: 'yourgrid',
        //     baseParams: {
        //         area_id: 120,
        //         status_id: 1
        //     }
        // }
        if (this.initialConfig.baseParams) {

            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function (store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });

        }

        // this.getSelectionModel().setSelectionMode('SINGLE');

        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            listeners: {
                cancelEdit: function (rowEditing, context) {
                    // Canceling editing of a locally added, unsaved record: remove it
                    if (context.record.phantom) {
                        grid.store.remove(context.record);
                    }
                }
            }
        });

        var editing = this.createEditing();

        this.rowEditing = editing;
        this.plugins = [editing];


        this.columns = [{
            header: 'Id',
            tooltip: 'Id',
            width: 144,
            sortable: true,
            dataIndex: 'anggota_kegiatan_id',
            hideable: false,
            hidden: true
        }, {
            header: 'Kegiatan Id',
            tooltip: 'Kegiatan Id',
            width: 144,
            sortable: true,
            dataIndex: 'kegiatan_id',
            hideable: false,
            hidden: true
        }, {
            header: 'Peserta',
            tooltip: 'Nama & NIP',
            width: 144,
            sortable: true,
            dataIndex: 'nama',
            align: 'left',
            hideable: false,
            renderer: function (v, p, r) {
                return r.data.nama+"<br>"+r.data.nip;
            }
        }, {
            header: 'Instansi',
            tooltip: 'Nama Instansi',
            width: 144,
            sortable: true,
            dataIndex: 'instansi',
            align: 'left',
            hideable: false,
        }, {
            header: 'Peran',
            tooltip: 'Peran',
            width: 200,
            sortable: true,
            dataIndex: 'peran',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 200
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Soft Delete',
            tooltip: 'Soft Delete',
            width: 80,
            sortable: true,
            dataIndex: 'soft_delete',
            hideable: false,
            hidden: true,
            field: {
                xtype: 'numberfield'
            }

        }];

        this.dockedItems = this.createDockedItems();

        this.bbar = this.createBbar();

        this.callParent(arguments);
    }
});
Ext.define('SimpegGundul.view.anggotakegiatan.Window', {
    extend: 'Ext.window.Window',
    xtype: 'anggotakegiatanwindow',
    controller: 'anggotakegiatan',

    title: 'Daftar Peserta Kegiatan',
    maximizable: true,
    height: 478,
    width: 740,
    modal: true,
    layout: 'border',

    initComponent: function () {

        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        win = this;

        this.items = [{
            xtype: 'anggotakegiatangrid',
            title: 'Peserta Kegiatan',
            itemId: 'gridKiri',
            region: 'center',
            kegiatan_id: win.initialConfig.kegiatan_id,
            flex: 1,
            multiSelect: true,

            viewConfig: {
                plugins: {
                    ptype: 'gridviewdragdrop',
                    containerScroll: true,
                    dragGroup: 'dd-grid-to-grid-group1',
                    dropGroup: 'dd-grid-to-grid-group2'
                },
                listeners: {
                    drop: 'onDropGridAnggota',
                    refresh: function (dataview) {
                        Ext.each(dataview.panel.columns, function (column) {
                            // if (column.autoSizeColumn === true)
                            column.autoSize();
                        })
                    }
                }
            },
        }, {
            xtype: 'tabpanel',
            forceFit: false,
            title: 'Calon Peserta',
            region: 'east',
            split: true,
            collapsible: true,
            flex: 1,
            items: [{
                xtype: 'ptkgrid',
                itemId: 'gridKanan1',
                multiSelect: true,
                title: 'Guru/Tendik',
                tbar: ['Cari : ',{
                    xtype: "textfield",
                    emptyText: 'Nama/Nuptk/NIP...',
                    width: 200,
                    itemId: 'txtCariPtk',
                    clearIcon: true,

                    enableKeyEvents: true,
                    listeners: {
                        keypress: function (field, e) {
                            var grid = field.up('gridpanel');
                            if (e.getKey() == e.ENTER) {
                                grid.down('textfield[itemId=txtCariPtk]').setValue(field.getValue());

                                grid.getStore().load();
                            }
                        }
                    }
                }],
                kegiatan_id: win.initialConfig.kegiatan_id,

                viewConfig: {
                    plugins: {
                        ptype: 'gridviewdragdrop',
                        containerScroll: true,
                        dragGroup: 'dd-grid-to-grid-group2',
                        dropGroup: 'dd-grid-to-grid-group1',

                    },

                    listeners: {
                        drop: 'onDropGridPtk'
                    },

                    // The right hand drop zone gets special styling
                    // when dragging over it.
                    dropZone: {
                        overClass: 'dd-over-gridview'
                    }
                },
            }/* ,{
                xtype: 'pegawaigrid',
                itemId: 'gridKanan2',
                multiSelect: true,
                title: 'Pegawai',
                tbar: ['Cari : ',{
                    xtype: "textfield",
                    emptyText: 'Nama/NIP...',
                    width: 200,
                    itemId: 'txtCariPegawai',
                    clearIcon: true,

                    enableKeyEvents: true,
                    listeners: {
                        keypress: function (field, e) {
                            var grid = field.up('gridpanel');
                            if (e.getKey() == e.ENTER) {
                                grid.down('textfield[itemId=txtCari]').setValue(field.getValue());
                                
                                grid.getStore().reload();
                            }
                        }
                    }
                }],
                kegiatan_id: win.initialConfig.kegiatan_id,
                hidden: (['1', '2'].indexOf(user.level) < 0) ? false : true,

                viewConfig: {
                    plugins: {
                        ptype: 'gridviewdragdrop',
                        containerScroll: true,
                        dragGroup: 'dd-grid-to-grid-group2',
                        dropGroup: 'dd-grid-to-grid-group1',

                    },

                    listeners: {
                        drop: 'onDropGridPegawai',
                        refresh: function (dataview) {
                            Ext.each(dataview.panel.columns, function (column) {
                                // if (column.autoSizeColumn === true)
                                column.autoSize();
                            })
                        }
                    },

                    // The right hand drop zone gets special styling
                    // when dragging over it.
                    dropZone: {
                        overClass: 'dd-over-gridview'
                    }
                },
            } */]
        }];

        this.callParent(arguments);
    },
});
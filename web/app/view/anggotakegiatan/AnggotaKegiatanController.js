/**
 * Created by Ahmad on 12/25/2016.
 */

Ext.define('SimpegGundul.view.anggotakegiatan.AnggotaKegiatanController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.anggotakegiatan',

    onRenderGrid: function (grid) {
        var store = grid.getStore();

        store.on('beforeload', function (cmp) {
            Ext.apply(cmp.proxy.extraParams, {
                kegiatan_id: grid.kegiatan_id
            });
        });

        store.reload();
    },

    onEdit: function(btn){
        // Defaults using row editor. Override this to use other method, such as form etc        
        var grid = btn.up('gridpanel');
        grid.rowEditing.cancelEdit();

        //var selections = grid.getSelectionModel().getSelection();
        var selections = grid.getSelection();
        var r = selections[0];
        if (!r) {
            Ext.toast('Mohon pilih salah satu baris', 'Error');
            return;
        }
        var startEditingColumnNumber = 2;
        for (var i = 0; i < grid.columns.length; i++) {
            if (grid.columns[i].isVisible()) {
                var startEditingColumnNumber = i;
                break;
            }
        }
        grid.rowEditing.startEdit(r, startEditingColumnNumber);
    },

    onSave: function(btn){
        var grid = btn.up('gridpanel');

        var stores = grid.store;
        var arr = [];

        stores.each(function (record) {

            if (record.dirty) {
                // console.log(record);
                var arrRec = { anggota_kegiatan_id: record.data.anggota_kegiatan_id };

                for (var mod in record.modified) {
                    arrRec[mod] = record.data[mod];
                }

                arr.push(arrRec);
            }

        });

        if (Array.isArray(arr) !== true) {
            Ext.toast('Tidak ada yang diperbaharui', 'Info');
            return;
        }

        // console.log(JSO N.stringify(arr));
        grid.up('window').mask('Tunggu sebentar...');
        Ext.Ajax.request({
            url: 'AnggotaKegiatan/savePeran',
            method: 'GET',
            params: {
                data: JSON.stringify(arr)
            },
            success: function (response) {
                var text = response.responseText;

                var obj = Ext.JSON.decode(response.responseText);

                Ext.toast('Menyimpan data Anggota Kegiatan (Berhasil: ' + obj.berhasil + ', Gagal: ' + obj.gagal + ')', 'Berhasil');
                grid.up('window').unmask();
                stores.reload();
            }
        });
    },

    onDropGridAnggota: function (node, data, dropRec, dropPosition) {
        var gridKiri = Ext.ComponentQuery.query('anggotakegiatangrid[itemId=gridKiri]')[0];
        var gridKanan1 = Ext.ComponentQuery.query('ptkgrid[itemId=gridKanan1]')[0];
        var gridKanan2 = Ext.ComponentQuery.query('pegawaigrid[itemId=gridKanan2]')[0];

        var arrPtk = new Array();
        var arrPegawai = new Array();
        data.records.forEach(element => {
            if (element.data.pegawai_id){
                arrPegawai.push(element.data.pegawai_id);
            }else{
                arrPtk.push(element.data.ptk_id);
            }
        });

        if (Array.isArray(arrPtk) !== true || Array.isArray(arrPegawai) !== true) {
            Ext.toast('Tidak ada yang diperbaharui', 'Info');
            return;
        }
        
        gridKiri.up('window').mask('Tunggu sebentar...');
        Ext.Ajax.request({
            url: 'AnggotaKegiatan/save',
            method: 'GET',
            params: {
                ptk_id: JSON.stringify(arrPtk),
                pegawai_id: JSON.stringify(arrPegawai),
                kegiatan_id: gridKiri.kegiatan_id,
                peran: 'Peserta'
            },
            success: function (response) {
                var text = response.responseText;

                var obj = Ext.JSON.decode(response.responseText);

                Ext.toast('Menyimpan data Anggota (Berhasil: ' + obj.berhasil + ', Gagal: ' + obj.gagal + ')', 'Berhasil');
                gridKiri.up('window').unmask();
                gridKiri.getStore().reload();
                gridKanan1.getStore().reload();
                gridKanan2.getStore().reload();
            },
            failure: function (response) {
                gridKiri.up('window').unmask();
            }
        });
    },

    onCetakSertifikat: function(btn){
        var grid = btn.up('grid');
        var sel = grid.getSelection();
        var rec = sel[0];
        if (sel.length <= 0) {
            Ext.toast('Mohon pilih salah satu PTK terlebih dahulu', 'Error');
            return;
        }

        var printUrl = 'Kegiatan/getCetakSertifikat?id=' + rec.data.kegiatan_id + '&anggota_id=' + rec.data.anggota_kegiatan_id;

        var win = Ext.widget({
            xtype: 'window_print',
            title: 'Cetak Sertifikat',
            src: printUrl
        });

        win.show();
    }
});
Ext.define('SimpegGundul.view.periodepenilaian.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.periodepenilaiangrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    columnLines: true,
    controller: 'periodepenilaian',
    autoLoad: true,

    viewConfig: {
        listeners: {
            refresh: function (dataview) {
                Ext.each(dataview.panel.columns, function (column) {
                    // if (column.autoSizeColumn === true)
                    column.autoSize();
                })
            }
        }
    },

    createStore: function () {
        return Ext.create('SimpegGundul.store.PeriodePenilaian');
    },

    createEditing: function () {
        return Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2,
            pluginId: 'editing'
        });
    },

    createDockedItems: function () {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                itemId: 'btnTambah',
                text: 'Tambah',
                iconCls: 'x-fa fa-plus-circle',
                handler: 'addRecord'
            }, {
                xtype: 'button',
                itemId: 'btnUbah',
                text: 'Ubah',
                iconCls: 'x-fa fa-pencil-square',
                handler: 'editRecord'
            }, {
                xtype: 'button',
                text: 'Simpan',
                iconCls: 'x-fa fa-check fa-lg glyph-blue glyph-shadow',
                itemId: 'save',
                handler: 'saveRecord'
            }, {
                xtype: 'button',
                text: 'Hapus',
                iconCls: 'x-fa fa-remove fa-lg glyph-red glyph-shadow',
                itemId: 'delete',
                handler: 'deleteRecord'
            }]
        }];
    },

    createBbar: function () {
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    initComponent: function () {

        var grid = this;

        this.store = this.createStore();

        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        // You can instaniate the component with basic parameters that affects filtering of the store
        // For example:
        // {
        //     xtype: 'yourgrid',
        //     baseParams: {
        //         area_id: 120,
        //         status_id: 1
        //     }
        // }
        if (this.initialConfig.baseParams) {

            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function (store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });

        }

        // this.getSelectionModel().setSelectionMode('SINGLE');

        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            listeners: {
                cancelEdit: function (rowEditing, context) {
                    // Canceling editing of a locally added, unsaved record: remove it
                    if (context.record.phantom) {
                        grid.store.remove(context.record);
                    }
                }
            }
        });

        var editing = this.createEditing();

        this.rowEditing = editing;
        this.plugins = [editing];

        this.columns = [{
            header: 'ID',
            tooltip: 'ID',
            width: 80,
            sortable: true,
            dataIndex: 'periode_penilaian_id',
            hideable: false,
            field: {
                xtype: 'textfield'
                , allowBlank: false
            }
        }, {
            header: 'Nama',
            tooltip: 'Nama',
            width: 600,
            sortable: true,
            dataIndex: 'nama',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 150
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Tanggal Mulai',
            tooltip: 'Tanggal Mulai',
            width: 80,
            sortable: true,
            dataIndex: 'tanggal_mulai',
            hideable: false,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            field: {
                xtype: 'datefield'
                , format: 'd/m/Y'
                // , maxValue: new Date()
            }
        }, {
            header: 'Tanggal Akhir',
            tooltip: 'Tanggal Akhir',
            width: 80,
            sortable: true,
            dataIndex: 'tanggal_akhir',
            hideable: false,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            field: {
                xtype: 'datefield'
                , format: 'd/m/Y'
                // , maxValue: new Date()
            }
        }, {
            header: 'Aktif',
            tooltip: 'Aktif',
            width: 80,
            sortable: true,
            dataIndex: 'aktif',
            hideable: false,
            renderer: function (v, p, r) {
                switch (v) {
                    case 1: return 'Aktif'; break;
                    case 0: return 'Tidak Aktif'; break;
                    default: return '-'; break;
                }
            },
            field: {
                xtype: 'combobox',
                selectOnTab: true,
                valueField: 'id',
                hiddenName: 'id',
                displayField: 'data',
                store: {
                    data: [
                        { 'id': 1, 'data': 'Aktif' },
                        { 'id': 0, 'data': 'Tidak Aktif' }
                    ]
                },
                lazyRender: true
            },

        }];

        this.dockedItems = this.createDockedItems();

        this.bbar = this.createBbar();

        this.callParent(arguments);
    }
});
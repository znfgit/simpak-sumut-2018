Ext.define('SimpegGundul.view.kgb.Form', {
    extend: 'Ext.form.Panel',
    xtype: 'kgbform',
    url: '/Kgb/getDataForm',
    controller: 'kgb',
    waitMsgTarget: true,
    // trackResetOnLoad: true,
    autoScroll: true,
    bodyPadding: 10,
    defaults: {
        labelWidth: 185,
        anchor: '100%',
        fieldStyle: 'font-weight: bold;'
    },
    listeners: {
        afterrender: 'onRenderForm'
    },

    items: [{
        xtype: 'hidden',
        name: 'kgb_id',
    }, {
        xtype: 'hidden',
        name: 'ptk_id',
    }, {
        xtype: 'hidden',
        name: 'pegawai_id',
    }, {
        xtype: 'hidden',
        name: 'tmt_pangkat',
    }, {
        xtype: 'hidden',
        name: 'periode_pengusulan',
    }, {
        xtype: 'textfield',
        fieldLabel: 'No. Surat',
        name: 'no_surat',
        allowBlank: false,
    }, {
        xtype: 'displayfield',
        fieldLabel: '1. Nama',
        name: 'nama',
        margin: "0 0 -10 0"
    }, {
        xtype: 'displayfield',
        fieldLabel: '2. NIP / Karpeg',
        name: 'nip_karpeg',
        margin: "0 0 -10 0"
    }, {
        xtype: 'hidden',
        name: 'pangkat_golongan_id'
    }, {
        xtype: 'displayfield',
        fieldLabel: '3. Pangkat Golongan',
        name: 'nama_pangkat_golongan',
        margin: "0 0 -10 0"
    }, {
        xtype: 'displayfield',
        fieldLabel: 'TMT Pangkat',
        name: 'tmt_pangkat_display',
        hidden: true
    }, {
        xtype: 'displayfield',
        fieldLabel: '4. Jabatan',
        name: 'jabatan',
        value: '-',
        margin: "0 0 -10 0"
    }, {
        xtype: 'displayfield',
        fieldLabel: '5. Kantor/Instansi',
        name: 'instansi',
        value: '-',
        margin: "0 0 -10 0"
    }, {
        xtype: 'displayfield',
        fieldLabel: '6. Gaji Pokok Lama',
        name: 'gapok_lama',
        value: '-',
        margin: "0 0 -10 0"
    }, {
        xtype: 'textfield',
        fieldLabel: '7. a. Oleh Pejabat',
        name: 'pejabat',
        allowBlank: false,
    }, {
        xtype: 'datefield',
        fieldLabel: 'b. Tanggal',
        name: 'tgl_lama',
        padding: '0 0 0 15',
        labelWidth: 170,
        format: 'Y-m-d',
        allowBlank: false,
    }, {
        xtype: 'textfield',
        fieldLabel: 'Nomor',
        padding: '0 0 0 30',
        labelWidth: 155,
        name: 'no_surat_lama',
        allowBlank: false,
    }, {
        xtype: 'datefield',
        fieldLabel: 'c. Tanggal mulai berlaku',
        padding: '0 0 0 15',
        labelWidth: 170,
        name: 'tmt_lama',
        format: 'Y-m-d',
        allowBlank: false,
    }, {
        xtype: 'displayfield',
        fieldLabel: 'd. Masa Kerja Golongan pada tanggal tersebut',
        labelWidth: 170,
        padding: '0 0 -10 15',
        name: 'masa_kerja_lama',
        value: '-',
    }, {
        xtype: 'displayfield',
        fieldLabel: '8. Gaji Pokok Baru',
        labelWidth: 185,
        padding: '0 0 -10 0',
        name: 'gapok_baru',
        value: '-',
    }, {
        xtype: 'displayfield',
        fieldLabel: '9. Berdasarkan masa kerja',
        labelWidth: 185,
        padding: '0 0 -10 0',
        name: 'masa_kerja_baru',
        value: '-',
    }, {
        xtype: 'displayfield',
        fieldLabel: '10. Dalam Golongan Ruang',
        labelWidth: 185,
        padding: '0 0 -10 0',
        name: 'golongan',
        value: '-',
    }, {
        xtype: 'datefield',
        fieldLabel: '11. Mulai Tanggal',
        labelWidth: 185,
        name: 'tmt_kgb',
        format: 'Y-m-d',
        allowBlank: false,
    }, {
        xtype: 'penandatangancombo',
        fieldLabel: 'Penandatangan',
        name: 'penandatangan_id',
        hidden: true
    }],

    buttons: [{
        text: 'Simpan',
        handler: 'onSaveForm'
    }]
});
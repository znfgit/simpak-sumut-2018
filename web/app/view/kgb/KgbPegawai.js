Ext.define('SimpegGundul.view.kgb.KgbPegawai', {
    extend: 'Ext.panel.Panel',
    xtype: 'kgbpegawai',
    controller: 'kgb',

    initComponent: function () {

        var me = this;

        me.items = {
            xtype: 'kgbgrid',
            iconCls: 'x-fa fa-money',
            title: 'Kenaikan Gaji Berkala Pegawai',
            id: 'gridkgbpegawai',
            listeners: {
                afterrender: 'onRenderGridPegawai'
            },
            tbar: ['Waktu Kenaikan : ', {
                xtype: 'combo',
                store: Ext.create('Ext.data.Store', {
                    data: [
                        { waktu_id: '0', nama: '-- Semua --' },
                        { waktu_id: '1', nama: 'Kurang dari 1 tahun' },
                        { waktu_id: '2', nama: '1-2 tahun' }
                    ]
                }),
                emptyText: 'Pilih Waktu Kenaikan...',
                queryMode: 'local',
                displayField: 'nama',
                valueField: 'waktu_id',
                editable: false,
                itemId: 'waktuKenaikan',
                listeners: {
                    change: 'changeComboWaktu'
                }
            }, 'Cari : ', {
                    xtype: "textfield",
                    emptyText: 'Nama/NIP...',
                    width: 200,
                    itemId: 'txtCari',
                    clearIcon: true,

                    enableKeyEvents: true,
                    listeners: {
                        keypress: function (field, e) {
                            if (e.getKey() == e.ENTER) {
                                field.up('grid').getStore().load();
                            }
                        }
                    }
                }, '->', {
                    xtype: 'button',
                    iconCls: 'x-fa fa-plus-square',
                    text: 'Usulkan Kenaikan',
                    handler: 'onUsulkan',
                    tipe: 2,
                }, {
                    xtype: 'button',
                    iconCls: 'x-fa fa-minus-square',
                    text: 'Tolak Usulan',
                    handler: 'onUsulkan',
                    status: 5
                }, {
                    xtype: 'button',
                    iconCls: 'x-fa fa-print',
                    text: 'Cetak Surat',
                    handler: 'onCetakSurat',
                    tipe: 2
                }]
        };

        this.callParent();

    }
});

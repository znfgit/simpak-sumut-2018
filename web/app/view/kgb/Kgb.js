Ext.define('SimpegGundul.view.kgb.Kgb', {
    extend: 'Ext.panel.Panel',
    xtype: 'kgb',

    initComponent: function () {

        var me = this;

        me.items = {
            xtype: 'kgbgrid',
            iconCls: 'x-fa fa-money',
            title: 'Kenaikan Gaji Berkala Guru/Tendik',
            id: 'gridkgb',
        };

        this.callParent();

    }
});

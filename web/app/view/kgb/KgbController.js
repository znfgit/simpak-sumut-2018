/**
 * Created by Ahmad on 12/25/2016.
 */

Ext.define('SimpegGundul.view.kgb.KgbController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.kgb',

    onRenderGrid: function (grid) {
        var store = grid.getStore();

        store.on('beforeload', function (cmp) {
            Ext.apply(cmp.proxy.extraParams, {
                waktu_kenaikan: grid.down('combo[itemId=waktuKenaikan]').getValue(),
                txtCari: grid.down('textfield[itemId=txtCari]').getValue()
            });
        });
    },

    onRenderGridPegawai: function (grid) {
        // Update proxy
        grid.getStore().proxy.url = 'Kgb/getDataPegawai';

        grid.down('toolbar').destroy();
    },

    changeComboWaktu: function (cbo, val) {
        var grid = cbo.up('grid');

        grid.getStore().reload();
    },

    onUsulkan: function (btn) {
        var selected = this.getSelected(btn);
        var grid = btn.up('grid');

        if (selected) {
            if (ptk_selected.is_naik < 1) {
                Ext.toast('Guru / Pegawai yang dipilih belum memasuki periode kenaikan gaji berkala', 'Info');
                return;
            }

            if((user.level !== '1' && btn.tipe == 1) || (btn.status == 5)){
                this.getConfirmation(grid, btn.status);
            }else{
                Ext.create('Ext.window.Window', {
                    title: 'Kenaikan Gaji Berkala',
                    maximizable: true,
                    height: 620,
                    width: 780,
                    layout: 'fit',
                    modal: true,
                    items: Ext.create('SimpegGundul.view.kgb.Form', {
                        tipe: btn.tipe
                    })
                }).show();
            }
        }
    },

    getSelected: function (cmp) {
        var grid = cmp.up('grid');

        var selections = grid.getSelectionModel().getSelection();
        var r = selections[0];
        if (!r) {
            Ext.toast('Mohon pilih salah satu PTK!');

            return false;
        } else {
            ptk_selected = r.data;

            return true;
        }
    },

    getConfirmation: function (cmp, status) {
        if (ptk_selected.is_naik < 2){
            Ext.toast('Guru / Tendik / Pegawai yang dipilih belum diusulkan', 'Info');
            return;
        }
        Ext.Msg.show({
            title: 'Konfirmasi',
            msg: 'Anda yakin? ',
            width: 300,
            closable: false,
            buttons: Ext.Msg.YESNO,
            multiline: false,
            fn: function (buttonValue, inputText, showConfig) {
                if (buttonValue === "yes") {
                    Ext.getBody().mask('Tunggu sebentar...');

                    Ext.Ajax.request({
                        url: 'Kgb/setStatus',
                        method: 'GET',
                        params: {
                            kgb_id: ptk_selected.kgb_id,
                            status: status
                        },
                        success: function (response) {
                            var text = response.responseText;

                            var obj = Ext.JSON.decode(response.responseText);

                            Ext.toast(obj.msg, 'Info');
                            Ext.getBody().unmask();
                            cmp.getStore().reload();
                        },
                        failure: function () {
                            Ext.toast('Terjadi Kesalahan, hubungi admin');
                        }
                    });
                }
            },
            icon: Ext.Msg.QUESTION
        });
    },

    onCetakSurat: function (btn) {
        var selected = this.getSelected(btn);

        if (selected) {
            if (ptk_selected.is_naik < 2) {
                Ext.toast('Guru / Tendik / Pegawai yang dipilih belum memasuki periode Kenaikan Gaji Berkala / belum mengusulkan', 'Info');
                return;
            }
            
            if(btn.tipe == 2){
                var printUrl = 'Kgb/getCetakSuratPegawai?id='+ptk_selected.kgb_id;
            }else{
                var printUrl = 'Kgb/getCetakSurat?id='+ptk_selected.kgb_id;
            }

            var win = Ext.widget({
                xtype: 'window_print',
                title: 'Cetak Surat Pemberitahuan',
                src: printUrl
            });

            win.show();
            
        }

    },

    onRenderForm: function (form){
        if (['1', '2'].indexOf(user.level) < 0) {
            form.down('penandatangancombo').setHidden(false);
        }

        setTimeout(function () {
            form.getForm().load({
                url: 'Kgb/getDataForm',
                params: {
                    ptk_id: ptk_selected.ptk_id,
                    pegawai_id: ptk_selected.pegawai_id,
                    tipe: form.tipe
                },
                method: 'GET',
                waitMsg: 'Memuat data...',
                success: function (act) {
                    Ext.toast('Data berhasil dimuat');
                },
                failure: function (res) {
                    Ext.toast('Data gagal dimuat');
                }
            });
        }, 300);
    },

    onSaveForm: function (btn) {
        var form = btn.up('form');

        if ((form.isDirty()) && (form.isValid())) {
            Ext.MessageBox.show({
                title: 'konfirmasi',
                msg: 'Apakah anda sudah yakin ?',
                buttonText: { yes: "Ya", no: "Tidak" },
                fn: function (btn1) {
                    if (btn1 == "yes") {
                        form.submit({
                            method: 'GET',
                            url: '/Kgb/saveForm',
                            waitMsg: 'Menyimpan...',
                            success: function (frm, action) {
                                Ext.toast(action.result.msg, 'Info');
                                ptk_selected.kgb_id = action.result.kgb_id;

                                if (Ext.getCmp('gridkgb')){
                                    Ext.getCmp('gridkgb').getStore().reload();
                                }else{
                                    Ext.getCmp('gridkgbpegawai').getStore().reload();
                                }

                                form.getForm().load({
                                    url: 'Kgb/getDataForm',
                                    params: {
                                        ptk_id: ptk_selected.ptk_id,
                                        pegawai_id: ptk_selected.pegawai_id,
                                        tipe: form.tipe
                                    },
                                    method: 'GET',
                                    waitMsg: 'Memuat data...',
                                    success: function (act) {
                                        Ext.toast('Data berhasil dimuat');
                                    },
                                    failure: function (res) {
                                        Ext.toast('Data gagal dimuat');
                                    }
                                });
                            },
                            failure: function (form, action) {
                                Ext.toast(action.result.msg);
                            }
                        });
                    }
                }
            });
        } else {
            Ext.toast('Harap diisi semua', 'Info');
        }
    },
});
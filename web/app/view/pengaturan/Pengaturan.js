Ext.define('SimpegGundul.view.pengatura.Pengaturan', {
    extend: 'Ext.tab.Panel',
    xtype: 'pengaturan',

    initComponent: function () {

        var me = this;

        me.items = [{
            xtype: 'penandatangangrid',
            title: 'Penandatangan',
            iconCls: 'x-fa fa-pencil',
        },{
            xtype: 'periodepenilaiangrid',
            title: 'Periode Penilaian',
            iconCls: 'x-fa fa-calendar',
        },{
            xtype: 'penggunagrid',
            title: 'Pengguna',
            iconCls: 'x-fa fa-user',
        }];

        this.callParent();

    }
});

/**
 * Created by Ahmad on 12/25/2016.
 */

Ext.define('SimpegGundul.view.kepalasekolah.KepalaSekolahController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.kepalasekolah',

    onRenderGrid: function(grid){
        var store = grid.getStore();

        store.on('beforeload', function (cmp) {
            Ext.apply(cmp.proxy.extraParams, {
                txtCari: grid.down('textfield[itemId=txtCari]').getValue(),
                waktu_berakhir: grid.down('combo[itemId=waktuBerakhir]').getValue()
            });
        });
    },
    
    clickUnduh: function (btn) {
        window.location.assign('KepalaSekolah/getExcel');
    },

    changeComboWaktu: function (cbo, val) {
        var grid = cbo.up('grid');

        grid.getStore().reload();
    }
});
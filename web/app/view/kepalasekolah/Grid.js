Ext.define('SimpegGundul.view.kepalasekolah.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.kepalasekolahgrid',
    title: '',
    // selType: 'checkboxmodel',
    controller: 'kepalasekolah',
    autoScroll: true,
    autoLoad: true,

    listeners: {
        afterrender: 'onRenderGrid'
    },

    createStore: function () {
        return Ext.create('SimpegGundul.store.KepalaSekolah');
    },

    createDockedItems: function (grid) {
        return [{
            xtype: 'toolbar',
            items: ['Waktu Berakhir : ',{
                xtype: 'combo',
                store: Ext.create('Ext.data.Store', {
                    data: [
                        { waktu_id: '0', nama: '-- Semua --' },
                        { waktu_id: '1', nama: 'Kurang dari 1 tahun' },
                        { waktu_id: '2', nama: '1-2 tahun' },
                        { waktu_id: '3', nama: 'Lebih dari 2 tahun' }
                    ]
                }),
                emptyText: 'Pilih Waktu Berakhir...',
                queryMode: 'local',
                displayField: 'nama',
                valueField: 'waktu_id',
                editable: false,
                itemId: 'waktuBerakhir',
                listeners: {
                    change: 'changeComboWaktu'
                }
            },'-','Cari : ',{
                    xtype: "textfield",
                    emptyText: 'Nama/NIP...',
                    width: 200,
                    itemId: 'txtCari',
                    clearIcon: true,

                    enableKeyEvents: true,
                    listeners: {
                        keypress: function (field, e) {
                            if (e.getKey() == e.ENTER) {
                                grid.getStore().load();
                            }
                        }
                    }
                }, '->', {
                    xtype: 'button',
                    text: 'Unduh Daftar Kepsek',
                    iconCls: 'x-fa fa-file-excel-o',
                    itemId: 'cetak',
                    handler: 'clickUnduh'
                }]
        }];
    },

    createBbar: function () {
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    initComponent: function () {

        var grid = this;

        this.store = this.createStore();

        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        // You can instaniate the component with basic parameters that affects filtering of the store
        // For example:
        // {
        //     xtype: 'yourgrid',
        //     baseParams: {
        //         area_id: 120,
        //         status_id: 1
        //     }
        // }
        if (this.initialConfig.baseParams) {

            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function (store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });

        }

        this.columns = [{
            header: 'Id',
            tooltip: 'Id',
            width: 1020,
            sortable: true,
            dataIndex: 'ptk_id',
            hideable: false,
            hidden: true,
            field: {
                xtype: 'hidden'
                , maxLength: 255
                , enforceMaxLength: true
                , minValue: 0
                , editable: false
            }
        }, {
            header: 'NIP',
            tooltip: 'NIP',
            width: 175,
            sortable: true,
            dataIndex: 'nip',
            field: {
                xtype: 'textfield'
                , maxLength: 255
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'NUPTK',
            tooltip: 'NUPTK',
            width: 150,
            sortable: true,
            dataIndex: 'nuptk'
        }, {
            header: 'Nama',
            tooltip: 'Nama',
            width: 275,
            sortable: true,
            dataIndex: 'nama'
        }, {
            header: 'Unit Induk',
            tooltip: 'Unit Induk',
            width: 300,
            sortable: true,
            dataIndex: 'unit_induk'
        }, {
            header: 'TMT Kepsek',
            tooltip: 'Tanggal Mulai Tugas Kepala Sekolah',
            width: 100,
            sortable: true,
            dataIndex: 'tmt',
            renderer: Ext.util.Format.dateRenderer('Y-m-d')
        }, {
            header: 'Masa Kerja',
            tooltip: 'Masa Kerja',
            width: 175,
            sortable: true,
            dataIndex: 'masa_kerja'
        }, {
            header: 'Waktu Berakhir',
            tooltip: 'Waktu Berakhir Jabatan Kepala Sekolah',
            width: 175,
            sortable: true,
            dataIndex: 'warning'
        }];

        this.dockedItems = this.createDockedItems(grid);

        this.bbar = this.createBbar();

        this.callParent(arguments);
    }
});
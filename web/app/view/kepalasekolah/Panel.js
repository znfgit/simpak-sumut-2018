Ext.define('SimpegGundul.view.kepalasekolah.Panel', {
    extend: 'Ext.panel.Panel',
    xtype: 'kepalasekolah',

    initComponent: function () {

        var me = this;

        me.items = {
            xtype: 'kepalasekolahgrid',
            iconCls: 'x-fa fa-users',
            title: 'Kepala Sekolah',
        };

        this.callParent();

    }
});

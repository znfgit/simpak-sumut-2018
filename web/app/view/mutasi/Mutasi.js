Ext.define('SimpegGundul.view.mutasi.Mutasi', {
    extend: 'Ext.panel.Panel',
    xtype: 'mutasi',
    
    initComponent: function() {

        var me = this;
        
        me.items = {
            id: 'gridMutasiUsulan',
            xtype: 'mutasiusulangrid',
            title: 'Daftar Usulan Mutasi Guru/Tendik',
            iconCls: 'x-fa fa-list-ul',
            // selType: 'checkboxmodel',
            // forceFit: true
        };

        this.callParent();

    }
});

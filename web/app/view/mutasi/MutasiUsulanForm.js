Ext.define('SimpegGundul.view.mutasi.MutasiUsulanForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.mutasiusulanform',
    bodyPadding: 10,
    autoScroll: true,
    controller: 'mutasi',
    defaults: {
        labelWidth: 140,
        anchor: '100%'
    },
    initComponent: function() {

        var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record){
            this.listeners = {
                afterrender: function(form, options) {
                    form.loadRecord(record);
                }
            };
        }

        /*this.on('beforeadd', function(form, field){
            if (!field.allowBlank)
              field.labelSeparator += '<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>';
        });*/

        this.items = [{
            xtype: 'hidden'
            ,fieldLabel: 'Id'
            ,labelAlign: 'right'
            ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
            ,allowBlank: false
            ,minValue: 0
            ,maxLength: 255
            ,enforceMaxLength: true
            ,name: 'mutasi_usulan_id'
            ,tabIndex: 0
        },{
            xtype: 'textfield'
            ,fieldLabel: 'No surat'
            ,labelAlign: 'right'
            ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
            ,allowBlank: false
            ,minValue: 0
            ,maxLength: 50
            ,enforceMaxLength: true
            ,name: 'no_surat'
            ,tabIndex: 1
        },{
            xtype: 'ptkcombo'
            ,fieldLabel: 'Ptk'
            ,labelAlign: 'right'
            ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
            // ,allowBlank: false
            ,minValue: 0
            ,name: 'ptk_id'
            ,tabIndex: 2
            ,hidden: (user.level == '1') ? false : true,
        },{
            xtype: 'pegawaicombo'
            ,fieldLabel: 'Pegawai'
            ,labelAlign: 'right'
            ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
            // ,allowBlank: false
            ,minValue: 0
            ,name: 'pegawai_id'
            ,tabIndex: 2
            ,hidden: (user.level == '1') ? true : false,
        },{
            xtype: 'sekolahcombo'
            ,fieldLabel: 'Instansi Tujuan'
            ,labelAlign: 'right'
            ,allowBlank: false
            ,minValue: 0
            ,name: 'sekolah_id'
            ,tabIndex: 3
        },{
            xtype: 'jenisptkcombo'
            ,fieldLabel: 'Jabatan'
            ,labelAlign: 'right'
            ,allowBlank: false
            ,maxValue: 9999
            ,minValue: 0
            ,name: 'jenis_ptk_id'
            ,tabIndex: 4
        },{
            xtype: 'textareafield'
            ,fieldLabel: 'Alasan'
            ,labelAlign: 'right'
            ,allowBlank: false
            ,minValue: 0
            ,name: 'alasan'
            ,tabIndex: 5
        },{
            xtype: 'hidden'
            ,fieldLabel: 'Alasan penolakan'
            ,labelAlign: 'right'
            ,minValue: 0
            ,name: 'alasan_penolakan'
            ,tabIndex: 6
        },{
            xtype: 'hidden'
            ,fieldLabel: 'Proses'
            ,labelAlign: 'right'
            ,minValue: 0
            ,name: 'proses'
            ,tabIndex: 7
        },{
            xtype: 'hidden'
            ,fieldLabel: 'Create date'
            ,labelAlign: 'right'
            ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
            ,minValue: 0
            ,name: 'create_date'
            ,tabIndex: 8
        },{
            xtype: 'hidden'
            ,fieldLabel: 'Soft delete'
            ,labelAlign: 'right'
            ,minValue: 0
            ,name: 'soft_delete'
            ,tabIndex: 9
        
        }];

        this.buttons = [{
            text: 'Save',
            iconCls: 'fa fa-save fa-inverse fa-lg',
            handler: 'onSaveForm'
        }];

        this.callParent(arguments);
    }
});
Ext.define('SimpegGundul.view.mutasi.MutasiUsulanGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.mutasiusulangrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    columnLines: true,
    controller: 'mutasi',
    // forceFit: true,

    viewConfig: {
        listeners: {
            refresh: function (dataview) {
                Ext.each(dataview.panel.columns, function (column) {
                    // if (column.autoSizeColumn === true)
                    column.autoSize();
                })
            }
        }
    },

    listeners: {
        afterrender: 'onRenderGridMutasiUsulan'
    },

    createStore: function() {
        return Ext.create('SimpegGundul.store.MutasiUsulan');
    },

    createDockedItems: function(grid) {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                itemId: 'btnTambah',
                text: 'Tambah',
                iconCls: 'x-fa fa-plus-circle',
                hidden: (user.level == '1') ? false : true,
                handler: 'onTambahUsulan'
            },{
                xtype: 'button',
                itemId: 'btnUbah',
                text: 'Ubah',
                iconCls: 'x-fa fa-pencil-square',
                hidden: (user.level == '1') ? false : true,
                handler: 'onUbahUsulan'
            },'-','Cari : ',{
                xtype: "textfield",
                // emptyText: 'No. Surat... (Enter)',
                emptyText: 'Nip / Nama... (Enter)',
                width: 200,
                itemId: 'txtNoSurat',
                enableKeyEvents: true,
                name: 'no_surat',
                listeners: {
                    keypress: function (field, e) {
                        if (e.getKey() == e.ENTER){
                            grid.getStore().load();
                        }
                    }
                }
            },'->',{
                itemId: 'btnApproveUpt',
                iconCls: 'x-fa fa-check-square',
                text: 'Aprrove',
                hidden: (user.level == '2') ? false : true,
                approved: 2,
                handler: 'onApprove'
            },{
                itemId: 'btnRejectUpt',
                iconCls: 'x-fa fa-crosshairs',
                text: 'Tolak',
                hidden: (user.level == '2') ? false : true,
                approved: 1,
                handler: 'onApprove'
            },{
                itemId: 'btnProses',
                iconCls: 'x-fa fa-check-square',
                hidden: (['1','2'].indexOf(user.level) < 0) ? false : true,
                text: 'Proses Mutasi',
                handler: 'onProsesMutasi'
            },{
                itemId: 'btnRejectDinas',
                iconCls: 'x-fa fa-crosshairs',
                text: 'Tolak',
                hidden: (['1','2'].indexOf(user.level) < 0) ? false : true,
                approved: 3,
                handler: 'onApprove'
            },{
                itemId: 'btnCetakSurat',
                iconCls: 'x-fa fa-print',
                text: 'Cetak Surat Permohonan',
                hidden: (user.level == '1') ? false : true,
                handler: 'onCetakSuratPermohonan'
            }]
        }];
    },

    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },

    initComponent: function() {

        var grid = this;

        this.store = this.createStore();

        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        // You can instaniate the component with basic parameters that affects filtering of the store
        // For example:
        // {
        //     xtype: 'yourgrid',
        //     baseParams: {
        //         area_id: 120,
        //         status_id: 1
        //     }
        // }
        if (this.initialConfig.baseParams) {

            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });

        }

        // this.getSelectionModel().setSelectionMode('SINGLE');

        this.columns = [{
            header: 'Id',
            tooltip: 'Id',
            width: 1020,
            sortable: true,
            dataIndex: 'mutasi_usulan_id',
			hideable: false,
            hidden: true
        },{
            header: 'No Surat',
            tooltip: 'No Surat',
            width: 200,
            sortable: true,
            dataIndex: 'no_surat',
			hideable: false
        },{
            header: 'Nama',
            tooltip: 'Nama',
            width: 325,
            sortable: true,
            dataIndex: 'ptk_id',
			hideable: false,
            renderer: function(v,p,r) {
                return r.data.ptk_id_str;
            }
        },{
            header: 'NIP',
            tooltip: 'NIP',
            width: 144,
            sortable: true,
            dataIndex: 'nip',
			hideable: false
        },{
            header: 'Jabatan Lama',
            tooltip: 'Jabatan Lama',
            width: 144,
            sortable: true,
            dataIndex: 'jenis_ptk',
			hideable: false
        },{
            header: 'Instansi Asal',
            tooltip: 'Instansi Asal',
            width: 144,
            sortable: true,
            dataIndex: 'nama_sekolah',
			hideable: false
        },{
            header: 'Instansi Tujuan',
            tooltip: 'Instansi Tujuan',
            width: 144,
            sortable: true,
            dataIndex: 'sekolah_id',
			hideable: false,
            renderer: function(v,p,r) {
                return r.data.sekolah_id_str;
            }
        },{
            header: 'Jabatan Baru',
            tooltip: 'Jabatan Baru',
            width: 80,
            sortable: true,
            dataIndex: 'jenis_ptk_id',
			hideable: false,
            renderer: function(v,p,r) {
                return r.data.jenis_ptk_id_str;
            }
        },{
            header: 'Alasan Mutasi',
            tooltip: 'Alasan Mutasi',
            width: 300,
            sortable: true,
            dataIndex: 'alasan',
			hideable: false
        },{
            header: 'Alasan Penolakan',
            tooltip: 'Alasan Penolakan',
            width: 80,
            sortable: true,
            dataIndex: 'alasan_penolakan',
			hideable: false
        },{
            header: 'Proses',
            tooltip: 'Proses',
            width: 80,
            sortable: true,
            dataIndex: 'proses',
            hideable: false,
            renderer: function(v,p,r) {
                switch(v){
                    case 0 :
                        return 'Diusulkan Sekolah';
                    case 1 :
                        return 'Ditolak UPT';
                    case 2 :
                        return 'Disetujui UPT';
                    case 3 :
                        return 'Ditolak Dinas';
                    case 4 : 
                        return 'Disetujui Dinas';
                    case 5 :
                        return 'Diusulkan Sekolah';
                    case 6 :
                        return 'Diusulkan Dinas';
                }
            }
        },{
            header: 'Create Date',
            tooltip: 'Create Date',
            width: 80,
            sortable: true,
            dataIndex: 'create_date',
			hideable: false,
            hidden: true
        },{
            header: 'Soft Delete',
            tooltip: 'Soft Delete',
            width: 80,
            sortable: true,
            dataIndex: 'soft_delete',
			hideable: false,
            hidden: true
        
        }];

        this.dockedItems = this.createDockedItems(grid);

        this.bbar = this.createBbar();

        this.callParent(arguments);
    }
});
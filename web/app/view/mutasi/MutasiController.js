/**
 * Created by Ahmad on 12/25/2016.
 */

Ext.define('SimpegGundul.view.mutasi.MutasiController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.mutasi',

    onRenderGridMutasiUsulan: function(grid){
        var proses = 0;

        var grid = grid;

        grid.getStore().on('beforeload', function(store){
            if(['3','4','5','6','99'].indexOf(user.level) >= 0){
                // grid.setTitle('Daftar Usulan Mutasi Guru/Tendik/Pegawai');
                Ext.apply(store.proxy.extraParams, {
                    proses: "[2,3,4,5,6]",
                    no_surat: grid.down('textfield[itemId=txtNoSurat]').getValue()
                });
            }else if(user.level == '1'){
                Ext.apply(store.proxy.extraParams, {
                    no_surat: grid.down('textfield[itemId=txtNoSurat]').getValue()
                });
            }else if(user.level == '2')    {
                Ext.apply(store.proxy.extraParams, {
                    proses: "[0,1,2,3,4]",
                    no_surat: grid.down('textfield[itemId=txtNoSurat]').getValue()
                });
            }

            if (grid.sk) {
                grid.down('toolbar').hide();
                Ext.apply(store.proxy.extraParams, {
                    sk: 1
                });
            }
        });        

        grid.getStore().load();
    },

    onTambahUsulan: function(btn){
        Ext.create('Ext.window.Window', {
            title: 'Tambah / Rubah Mutasi Usulan',
            height: 386,
            width: 590,
            layout: 'fit',
            modal: true,
            items: {
                xtype: 'mutasiusulanform',
            }
        }).show();
    },

    onUbahUsulan: function(btn){
        // Defaults using row editor. Override this to use other method, such as form etc        
        var grid = btn.up('gridpanel');
        
        //var selections = grid.getSelectionModel().getSelection();
        var selections = grid.getSelection();
        var r = selections[0];
        if (!r) {
            Ext.toast('Mohon pilih salah satu baris', 'Error');
            return;
        }

        if([2,4].indexOf(r.get('proses')) >= 0 ){
            Ext.toast('PTK sudah disetujui UPT / Dinas', 'Error');
            return;
        }
        Ext.create('Ext.window.Window', {
            title: 'Tambah / Rubah Mutasi Usulan',
            height: 386,
            width: 590,
            layout: 'fit',
            modal: true,
            items: {
                xtype: 'mutasiusulanform',
                record: r
            }
        }).show();
    },

    onSaveForm: function(btn){
        var form = btn.up('form');

        if ((form.isDirty()) && (form.isValid())) {
            Ext.MessageBox.show({
                title: 'konfirmasi',
                msg: 'Apakah anda sudah yakin ?',
                buttonText: { yes: "Ya", no: "Tidak" },
                fn: function (btn1) {
                    if (btn1 == "yes") {
                        form.submit({
                            method: 'GET',
                            url: '/Mutasi/saveFormUsulan',
                            waitMsg: 'Menyimpan...',
                            success: function (frm, action) {
                                Ext.toast(action.result.msg, 'Info');

                                btn.up('window').close();
                                Ext.getCmp('gridMutasiUsulan').getStore().reload();
                            },
                            failure: function (form, action) {
                                Ext.toast("Terjadi kesalahan, hubungi admin");
                                // Ext.toast(action.result.msg);
                            }
                        });
                    }
                }
            });
        }else{
            Ext.toast('Harap diisi semua', 'Info');
        }
    },

    onApprove: function (btn) {
        var approved = btn.approved;

        var grid = btn.up('mutasiusulangrid');

        if (!grid.getSelection()[0]) {
            Ext.Msg.alert('Error', "Mohon Pilih PTK Terlebih Dahulu");
            return false;
        }

        var alasan = '';
        if ([1, 3].indexOf(approved) >= 0) {
            Ext.Msg.prompt('Alasan Penolakan', 'Masukkan alasan penolakan', function (btn, text) {
                if (btn == 'ok') {
                    alasan = text;
                    Ext.Msg.show({
                        title: 'Konfirmasi',
                        msg: 'Sudah yakin? ',
                        width: 300,
                        closable: false,
                        buttons: Ext.Msg.YESNO,
                        multiline: false,
                        fn: function (buttonValue, inputText, showConfig) {
                            if (buttonValue === "yes") {
                                var jsonData = "[";
                                for (var i = 0; i < grid.getSelection().length; i++) {
                                    var rec = grid.getSelection()[i];

                                    jsonData += Ext.util.JSON.encode(rec.data) + ",";
                                }
                                jsonData = jsonData.substring(0, jsonData.length - 1) + "]";

                                grid.mask('Tunggu sebentar...');
                                Ext.Ajax.request({
                                    waitMsg: 'Menyimpan....',
                                    url: 'Mutasi/approve',
                                    method: 'GET',
                                    params: {
                                        approved: approved,
                                        alasan: alasan,
                                        rows: jsonData
                                    },
                                    failure: function (res, opt) {
                                        grid.unmask();
                                    },
                                    success: function (res, opt) {
                                        grid.unmask();
                                        Ext.ComponentQuery.query('grid[xtype=mutasiusulangrid]')[0].getStore().reload();
                                    }
                                });
                            }
                        },
                        icon: Ext.Msg.QUESTION
                    });
                }
            });
        } else {
            Ext.Msg.show({
                title: 'Konfirmasi',
                msg: 'Sudah yakin? ',
                width: 300,
                closable: false,
                buttons: Ext.Msg.YESNO,
                multiline: false,
                fn: function (buttonValue, inputText, showConfig) {
                    if (buttonValue === "yes") {
                        var jsonData = "[";
                        for (var i = 0; i < grid.getSelection().length; i++) {
                            var rec = grid.getSelection()[i];

                            jsonData += Ext.util.JSON.encode(rec.data) + ",";
                        }
                        jsonData = jsonData.substring(0, jsonData.length - 1) + "]";
                        
                        grid.mask('Tunggu sebentar...');
                        Ext.Ajax.request({
                            waitMsg: 'Menyimpan....',
                            url: 'Mutasi/approve',
                            method: 'GET',
                            params: {
                                approved: approved,
                                rows: jsonData
                            },
                            failure: function (res, opt) {
                                grid.unmask();
                            },
                            success: function (res, opt) {
                                grid.unmask();
                                Ext.ComponentQuery.query('grid[xtype=mutasiusulangrid]')[0].getStore().reload();
                            }
                        });
                    }
                },
                icon: Ext.Msg.QUESTION
            });
        }
    },

    onProsesMutasi: function(btn){
        Ext.create('Ext.window.Window', {
            title: 'Proses Mutasi <i>(Pilih SK Mutasi)</i>',
            height: 400,
            width: 800,
            layout: 'fit',
            modal: true,
            items: {
                xtype: 'mutasiskgrid',
                itemId: 'gridProsesUsulan',
                forceFit: true,
            }
        }).show();
    },

    onDropGridUsulan: function (node, data, dropRec, dropPosition){
        var gridKiri = Ext.ComponentQuery.query('mutasianggotaskgrid[itemId=gridKiri]')[0];
        var gridKanan = Ext.ComponentQuery.query('mutasiusulangrid[itemId=gridKanan]')[0];

        var arr = new Array();
        data.records.forEach(element => {
            arr.push(element.data);
        });

        if (Array.isArray(arr) !== true) {
            Ext.toast('Tidak ada yang diperbaharui', 'Info');
            return;
        }

        gridKiri.up('window').mask('Tunggu sebentar...');
        Ext.Ajax.request({
            url: 'MutasiAnggotaSk/delete',
            method: 'GET',
            params: {
                data: JSON.stringify(arr),
                mutasi_sk_id: gridKiri.mutasi_sk_id
            },
            success: function (response) {
                var text = response.responseText;

                var obj = Ext.JSON.decode(response.responseText);

                Ext.toast('Menghapus data Anggota (Berhasil: ' + obj.berhasil + ', Gagal: ' + obj.gagal + ')', 'Berhasil');
                gridKiri.up('window').unmask();
                gridKiri.getStore().reload();
                gridKanan.getStore().reload();
            },
            failure: function (response) {
                gridKiri.up('window').unmask();
            }
        });
    },

    onCetakSuratPermohonan: function(btn){
        var grid = btn.up('grid');
        var sel = grid.getSelection();
        var rec = sel[0];
        if (sel.length <= 0) {
            Ext.toast('Mohon pilih salah satu PTK terlebih dahulu', 'Error');
            return;
        }

        var printUrl = 'Mutasi/getCetakSuratPermohonan?id=' + rec.data.mutasi_usulan_id;

        var win = Ext.widget({
            xtype: 'window_print',
            title: 'Cetak Surat Permohonan',
            src: printUrl
        });

        win.show();
    }
});
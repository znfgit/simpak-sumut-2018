Ext.define('SimpegGundul.view.kenaikanpangkat.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.kenaikanpangkatgrid',
    title: '',
    // selType: 'checkboxmodel',
    controller: 'kenaikanpangkat',
    autoScroll: true,
    autoLoad: true,

    listeners: {
        afterrender: 'onRenderGrid'
    },

    createStore: function () {
        return Ext.create('SimpegGundul.store.KenaikanPangkat');
    },

    createDockedItems: function (grid) {
        return [{
            xtype: 'toolbar',
            items: ['Waktu Kenaikan : ', {
                xtype: 'combo',
                store: Ext.create('Ext.data.Store', {
                    data: [
                        { waktu_id: '0', nama: '-- Semua --' },
                        { waktu_id: '1', nama: 'Kurang dari 1 tahun' },
                        { waktu_id: '2', nama: '1-2 tahun' },
                        { waktu_id: '3', nama: 'Lebih dari 2 tahun' }
                    ]
                }),
                emptyText: 'Pilih Waktu Kenaikan...',
                queryMode: 'local',
                displayField: 'nama',
                valueField: 'waktu_id',
                editable: false,
                itemId: 'waktuKenaikan',
                listeners: {
                    change: 'changeComboWaktu'
                }
            },'Cari : ',{
                xtype: "textfield",
                emptyText: 'Nama/NIP...',
                width: 200,
                itemId: 'txtCari',
                clearIcon: true,

                enableKeyEvents: true,
                listeners: {
                    keypress: function (field, e) {
                        if (e.getKey() == e.ENTER) {
                            grid.getStore().load();
                        }
                    }
                }
            },'->',{
                xtype: 'button',
                iconCls: 'x-fa fa-check-square',
                text: 'Setujui Usulan',
                handler: 'onUsulkan',
                status: 6,
                hidden: (user.level == 1) ? true : false
            },{
                xtype: 'button',
                iconCls: 'x-fa fa-minus-square',
                text: 'Tolak Usulan',
                handler: 'onUsulkan',
                status: 5,
                hidden: (user.level == 1) ? true : false
            },{
                xtype: 'button',
                iconCls: 'x-fa fa-plus-square',
                text: 'Usulkan Kenaikan',
                handler: 'onUsulkan',
                status: 2,
                hidden: (user.level == 1) ? false : true
            },{
                xtype: 'button',
                iconCls: 'x-fa fa-pencil-square',
                text: 'Penilaian',
                tipe: 1,
                handler: 'onNilai'
            },{
                xtype: 'button',
                iconCls: 'x-fa fa-print',
                text: 'Cetak Surat',
                handler: 'onCetakSurat',
                hidden: true
                // hidden: (['1','2'].indexOf(user.level) >= 0) ? true : false
            }]
        }];
    },

    createBbar: function () {
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    initComponent: function () {

        var grid = this;

        this.store = this.createStore();

        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        // You can instaniate the component with basic parameters that affects filtering of the store
        // For example:
        // {
        //     xtype: 'yourgrid',
        //     baseParams: {
        //         area_id: 120,
        //         status_id: 1
        //     }
        // }
        if (this.initialConfig.baseParams) {

            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function (store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });

        }

        this.columns = [{
            header: 'Id',
            tooltip: 'Id',
            width: 1020,
            sortable: true,
            dataIndex: 'ptk_id',
            hideable: false,
            hidden: true,
            field: {
                xtype: 'hidden'
                , maxLength: 255
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'NIP',
            tooltip: 'NIP',
            width: 175,
            sortable: true,
            dataIndex: 'nip'
        }, {
            header: 'No. Karpeg',
            tooltip: 'No. Karpeg',
            width: 100,
            sortable: true,
            dataIndex: 'karpeg'
        }, {
            header: 'Nama',
            tooltip: 'Nama',
            width: 250,
            sortable: true,
            dataIndex: 'nama'
        }, {
            header: 'Unit Induk',
            tooltip: 'Unit Induk',
            width: 300,
            sortable: true,
            dataIndex: 'unit_kerja'
        }, {
            header: 'Jenis PTK',
            tooltip: 'Jenis PTK',
            width: 125,
            sortable: true,
            dataIndex: 'Jenis_Guru'
        }, {
            header: 'Lama',
            columns: [{
                header: 'Gol',
                tooltip: 'Golongan',
                width: 75,
                sortable: true,
                dataIndex: 'gol_lama'
            }, {
                header: 'TMT',
                tooltip: 'Tanggal Mulai Tugas',
                width: 100,
                sortable: true,
                dataIndex: 'tmt_lama',
                renderer: Ext.util.Format.dateRenderer('Y-m-d')
            }]
        }, {
            header: 'Tujuan',
            columns: [{
                header: 'Gol',
                tooltip: 'Golongan',
                width: 75,
                sortable: true,
                dataIndex: 'gol_baru'
            }, {
                header: 'TMT',
                tooltip: 'Tanggal Mulai Tugas',
                width: 100,
                sortable: true,
                dataIndex: 'tmt_baru',
                renderer: Ext.util.Format.dateRenderer('Y-m-d')
            }]
        }, {
            header: 'Naik Pangkat',
            tooltip: 'Naik Pangkat',
            width: 150,
            sortable: true,
            dataIndex: 'naik_pangkat'
        }, {
            header: 'Status',
            tooltip: 'Status',
            width: 150,
            sortable: true,
            dataIndex: 'status'
        }];

        this.dockedItems = this.createDockedItems(grid);

        this.bbar = this.createBbar();

        this.callParent(arguments);
    }
});
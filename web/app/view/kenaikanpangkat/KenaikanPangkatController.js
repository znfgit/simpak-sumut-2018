/**
 * Created by Ahmad on 12/25/2016.
 */

Ext.define('SimpegGundul.view.kenaikanpangkat.KenaikanPangkatController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.kenaikanpangkat',

    onRenderGrid: function (grid) {
        var store = grid.getStore();

        store.on('beforeload', function (cmp) {
            Ext.apply(cmp.proxy.extraParams, {
                waktu_kenaikan: grid.down('combo[itemId=waktuKenaikan]').getValue(),
                txtCari: grid.down('textfield[itemId=txtCari]').getValue()
            });
        });
    },

    onRenderGridPegawai: function(grid){
        // Update proxy
        grid.getStore().proxy.url = 'KenaikanPangkat/getDataPegawai';

        grid.down('toolbar').destroy();
    },

    changeComboWaktu: function (cbo, val) {
        var grid = cbo.up('grid');

        grid.getStore().reload();
    },

    onUsulkan: function(btn){
        var selected = this.getSelected(btn);
        var grid = btn.up('grid');
        
        if (selected) {
            if ((ptk_selected.is_naik >= 1) && (ptk_selected.pak_id)){
                this.getConfirmation(grid, btn.status);
            }else{
                Ext.toast('Guru yang dipilih sudah diusulkan / belum menginputkan pengusulan PAK', 'Info');
            }
        }
    },

    onNilai: function(btn){
        var selected = this.getSelected(btn);

        if (selected) {
            if (ptk_selected.is_naik < 1) {
                Ext.toast('Guru yang dipilih belum memasuki periode kenaikan pangkat', 'Info');
                return;
            }

            Ext.create('Ext.window.Window', {
                title: 'Penilaian',
                maximizable: true,
                height: 540,
                width: 780,
                layout: 'fit',
                modal: true,
                items: Ext.create('SimpegGundul.view.penilaian.Panel',{
                    tipe: btn.tipe
                })
            }).show();
        }
    },

    getSelected: function (cmp) {
        var grid = cmp.up('grid');

        var selections = grid.getSelectionModel().getSelection();
        var r = selections[0];
        if (!r) {
            Ext.toast('Mohon pilih salah satu PTK!');

            return false;
        } else {
            ptk_selected = r.data;

            return true;
        }
    },

    getConfirmation: function(cmp, status){
        Ext.Msg.show({
            title: 'Konfirmasi',
            msg: 'Anda yakin? ',
            width: 300,
            closable: false,
            buttons: Ext.Msg.YESNO,
            multiline: false,
            fn: function (buttonValue, inputText, showConfig) {
                if (buttonValue === "yes") {
                    Ext.getBody().mask('Tunggu sebentar...');

                    Ext.Ajax.request({
                        url: 'KenaikanPangkat/setStatus',
                        method: 'GET',
                        params: {
                            pak_id: ptk_selected.pak_id,
                            status: status
                        },
                        success: function (response) {
                            var text = response.responseText;

                            var obj = Ext.JSON.decode(response.responseText);

                            Ext.toast(obj.msg, 'Info');
                            Ext.getBody().unmask();
                            cmp.getStore().reload();
                        },
                        failure: function () {
                            Ext.toast('Terjadi Kesalahan, hubungi admin');
                        }
                    });
                }
            },
            icon: Ext.Msg.QUESTION
        });
    },

    onCetakSurat: function(btn){
        var selected = this.getSelected(btn);

        if (selected) {
            if (ptk_selected.is_naik < 1) {
                Ext.toast('Guru yang dipilih belum memasuki periode kenaikan pangkat', 'Info');
                return;
            }
            /*
            var printUrl = 'Ptk/getTProfesi?nuptk=0233746649200013';

            var win = Ext.widget({
                xtype: 'window_print',
                title: 'Cetak Surat Permohonan',
                src: printUrl
            });

            win.show();
            */
        }
        
    }
});
Ext.define('SimpegGundul.view.kenaikanpangkat.PanelPegawai', {
    extend: 'Ext.panel.Panel',
    xtype: 'kenaikanpangkatpegawai',
    controller: 'kenaikanpangkat',

    initComponent: function () {

        var me = this;

        me.items = {
            xtype: 'kenaikanpangkatgrid',
            iconCls: 'x-fa fa-caret-square-o-up',
            title: 'Kenaikan Pangkat Pegawai',
            tbar: ['Waktu Kenaikan : ', {
                xtype: 'combo',
                store: Ext.create('Ext.data.Store', {
                    data: [
                        { waktu_id: '0', nama: '-- Semua --' },
                        { waktu_id: '1', nama: 'Kurang dari 1 tahun' },
                        { waktu_id: '2', nama: '1-2 tahun' },
                        { waktu_id: '3', nama: 'Lebih dari 2 tahun' }
                    ]
                }),
                emptyText: 'Pilih Waktu Kenaikan...',
                queryMode: 'local',
                displayField: 'nama',
                valueField: 'waktu_id',
                editable: false,
                itemId: 'waktuKenaikan',
                listeners: {
                    change: 'changeComboWaktu'
                }
            }, 'Cari : ', {
                xtype: "textfield",
                emptyText: 'Nama/NIP...',
                width: 200,
                itemId: 'txtCari',
                clearIcon: true,

                enableKeyEvents: true,
                listeners: {
                    keypress: function (field, e) {
                        if (e.getKey() == e.ENTER) {
                            field.up('grid').getStore().load();
                        }
                    }
                }
            },'->',{
                xtype: 'button',
                iconCls: 'x-fa fa-plus-square',
                text: 'Usulkan Kenaikan',
                handler: 'onUsulkan',
                status: 7,
            },{
                xtype: 'button',
                iconCls: 'x-fa fa-pencil-square',
                text: 'Penilaian',
                tipe: 2,
                handler: 'onNilai'
            }],
            listeners: {
                afterrender: 'onRenderGridPegawai'
            },
            id: 'gridKenaikanPangkatPegawai'
        };

        this.callParent();

    }
});

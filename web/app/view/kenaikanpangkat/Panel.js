Ext.define('SimpegGundul.view.kenaikanpangkat.Panel', {
    extend: 'Ext.panel.Panel',
    xtype: 'kenaikanpangkat',
    
    initComponent: function () {
        
        var me = this;
        
        me.items = {
            xtype: 'kenaikanpangkatgrid',
            iconCls: 'x-fa fa-caret-square-o-up',
            title: 'Kenaikan Pangkat PTK',
            id: 'gridKenaikanPangkat'
        };

        this.callParent();

    }
});

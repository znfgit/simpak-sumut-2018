/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('SimpegGundul.view.penilaian.PenilaianController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.penilaian',

    onRenderForm: function (form) {
        if(['1','2'].indexOf(user.level) < 0){
            form.down('displayfield[name=nomor_display]').setHidden(true);
            form.down('textfield[name=nomor]').setHidden(false);
            form.down('periodepenilaiancombo').setHidden(false);
            form.down('datefield[name=tgl_penetapan]').setHidden(false);
            form.down('penandatangancombo').setHidden(false);
        }


        setTimeout(function () {
            form.getForm().load({
                url: 'Penilaian/getDataForm',
                params: {
                    ptk_id: ptk_selected.ptk_id,
                    pegawai_id: ptk_selected.pegawai_id,
                    tipe: form.tipe
                },
                method: 'GET',
                waitMsg: 'Memuat data...',
                success: function (act) {
                    Ext.toast('Data berhasil dimuat');
                },
                failure: function(res){
                    Ext.toast('Data gagal dimuat');
                }
            });
        }, 300);
    },

    onSaveForm: function(btn){
        var form = btn.up('form');

        if ((form.isDirty()) && (form.isValid())) {
            Ext.MessageBox.show({
                title: 'konfirmasi',
                msg: 'Apakah anda sudah yakin ?',
                buttonText: { yes: "Ya", no: "Tidak" },
                fn: function (btn1) {
                    if (btn1 == "yes") {
                        form.submit({
                            method: 'GET',
                            url: '/Penilaian/saveForm',
                            waitMsg: 'Menyimpan...',
                            success: function (frm, action) {
                                Ext.toast(action.result.msg, 'Info');
                                ptk_selected.pak_id = action.result.pak_id;

                                Ext.getCmp('gridKenaikanPangkat').getStore().reload();
                            },
                            failure: function (form, action) {
                                Ext.toast(action.result.msg);
                            }
                        });
                    }
                }
            });
        }else{
            Ext.toast('Harap diisi semua', 'Info');
        }
    },

    onRenderTreeGrid: function(grid){
        var me = this;
        // grid.getStore().on('beforeload', function (store) {
        //     Ext.apply(store.proxy.extraParams, {
        //         ptk_id: ptk_selected.ptk_id
        //     });
        // });

        // grid.getStore().reload();
    },

    onRefreshTreeGrid: function(btn){
        btn.up('treepanel').getStore().reload();
    },

    onBeforeEdit: function (editor, e){
        if (e.column.dataIndex == 'ak_lama' && user.level !== '1') {
            e.cancel = true;
        }

        if (e.column.dataIndex == 'ak_usulan' && user.level !== '1') {
            e.cancel = true;
        }

        if (e.column.dataIndex == 'ak_penilai' && ['1','2'].indexOf(user.level) >= 0) {
            e.cancel = true;
        }

        if (!e.record.get('leaf')){
            e.cancel = true;
        }
    },

    onSaveTreeGrid: function(btn){
        var grid = btn.up('treepanel');
        var store = grid.getStore();
        var records = store.getRange();

        var count = 0;
        var jsonData = "[";

        for (var i = 0; i < records.length; i++) {
            var rec = records[i];

            if (!rec.data.leaf) {
                delete rec.data.children;
            }

            if (rec.dirty == true) {
                jsonData += Ext.util.JSON.encode(rec.data) + ",";
                count++;
            }
        }

        jsonData = jsonData.substring(0, jsonData.length - 1) + "]";

        if (!count > 0) {
            Ext.toast('Tidak ada yang diperbaharui.');
            return;
        }

        grid.mask('Tunggu sebentar...');
        Ext.Ajax.request({
            url: 'Penilaian/saveTreeGrid',
            method: 'GET',
            params: {
                ptk_id: ptk_selected.ptk_id,
                data: jsonData
            },
            failure: function (response, options) {
                Ext.toast('Ada Kesalahan dalam pemrosesan data.');
                grid.unmask();
            },
            success: function (response) {
                var json = Ext.JSON.decode(response.responseText);

                Ext.toast('Berhasil Menyimpan Data.');

                grid.unmask();
                store.reload();
            }
        });
    },

    onCetakPak: function(btn){
        var printUrl = 'Penilaian/getCetakPak?pak_id='+ptk_selected.pak_id;

        var win = Ext.widget({
            xtype: 'window_print',
            title: 'Cetak PAK',
            src: printUrl
        });

        win.show();
    }

});

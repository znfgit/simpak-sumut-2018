Ext.define('SimpegGundul.view.penilaian.PakForm', {
    extend: 'Ext.form.Panel',
    xtype: 'pakform',
    url: '/Penilaian/getDataForm',
    controller: 'penilaian',
    waitMsgTarget: true,
    // trackResetOnLoad: true,
    bodyPadding: 10,
    defaults: {
        labelWidth: 125,
        anchor: '100%',
        fieldStyle: 'font-weight: bold;'
    },
    listeners: {
        afterrender: 'onRenderForm'
    },

    items: [{
        xtype: 'hidden',
        name: 'pak_id',
    },{
        xtype: 'hidden',
        name: 'ptk_id',
    },{
        xtype: 'hidden',
        name: 'pegawai_id',
    },{
        xtype: 'hidden',
        name: 'tmt_pangkat',
    },{
        xtype: 'hidden',
        name: 'periode_pengusulan',
    },{
        xtype: 'displayfield',
        fieldLabel: 'No. PAK',
        name: 'nomor_display',
        value: '(Dibuatkan ketika usulan sudah diterima oleh Dinas)'
    },{
        xtype: 'textfield',
        fieldLabel: 'No. Pak',
        name: 'nomor',
        // allowBlank: false,
        hidden: true 
    },{
        xtype: 'periodepenilaiancombo',
        fieldLabel: 'Periode Penilaian',
        name: 'periode_penilaian_id',
        hidden: true
    },{
        xtype: 'displayfield',
        fieldLabel: 'Nama',
        name: 'nama'
    },{
        xtype: 'displayfield',
        fieldLabel: 'NIP',
        name: 'nip'
    },{
        xtype: 'hidden',
        name: 'pangkat_golongan_id'
    },{
        xtype: 'displayfield',
        fieldLabel: 'Pangkat Golongan',
        name: 'nama_pangkat_golongan'
    },{
        xtype: 'displayfield',
        fieldLabel: 'TMT Pangkat',
        name: 'tmt_pangkat_display'
    },{
        xtype: 'textfield',
        fieldLabel: 'Pendidikan Terakhir',
        name: 'pendidikan',
        // allowBlank: false,
        value: '-',
    },{
        xtype: 'textfield',
        fieldLabel: 'Jabatan',
        name: 'jabatan',
        // allowBlank: false,
        value: '-',
    },{
        xtype: 'datefield',
        fieldLabel: 'TMT Jabatan',
        name: 'tmt_jabatan',
        format: 'Y-m-d',
        // allowBlank: false,
    },{
        xtype: 'datefield',
        fieldLabel: 'Tgl Penetapan',
        name: 'tgl_penetapan',
        format: 'Y-m-d',
        // allowBlank: false,
        hidden: true
    },{
        xtype: 'penandatangancombo',
        fieldLabel: 'Penandateangan',
        name: 'penandatangan_id',
        hidden: true
    }],

    buttons: [{
        text: 'Simpan',
        handler: 'onSaveForm'
    }]
});
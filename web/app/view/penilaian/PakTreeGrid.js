Ext.define('SimpegGundul.view.penilaian.PakTreeGrid', {
    extend: 'Ext.tree.Panel',
    xtype: 'paktreegrid',
    title: 'Penilaian Angka Kredit',
    controller: 'penilaian',
    reserveScrollbar: true,
    collapsible: true,
    loadMask: true,
    useArrows: true,
    rootVisible: false,
    store: Ext.create('SimpegGundul.store.PakTree',{
        listeners: {
            beforeload: function (store) {
                Ext.apply(store.proxy.extraParams, {
                    pak_id: ptk_selected.pak_id
                });
            }
        }
    }),
    animate: true,

    listeners: {
        afterrender: 'onRenderTreeGrid',
        beforeedit: 'onBeforeEdit'
    },

    tbar: [{
        text: 'Bentangkan',
        glyph: 'xf196@FontAwesome',
        handler: function (btn) {
            btn.up('treepanel').expandAll();
        }
    },{
        text: 'Tutup',
        glyph: 'xf147@FontAwesome',
        handler: function (btn) {
            btn.up('treepanel').collapseAll();
        }
    },{
        text: 'Simpan',
        iconCls: 'x-fa fa-save',
        handler: 'onSaveTreeGrid'
    },'->',{
        text: 'Cetak PAK',
        iconCls: 'x-fa fa-print',
        handler: 'onCetakPak'
    },{
        text: 'Refresh',
        iconCls: 'x-fa fa-refresh',
        handler: 'onRefreshTreeGrid'
    }],

    plugins: {
        ptype: 'cellediting',
        clicksToEdit: 1
    },

    columns: [{
        xtype: 'treecolumn', //this is so we know which column will show the tree
        text: 'Uraian',
        sortable: true,
        dataIndex: 'uraian',
        align: 'left',
        flex: 3
    }, {
        xtype: 'hidden',
        text: 'Id',
        dataIndex: 'id',
        sortable: true
    }, {
        text: 'AK Lama',
        dataIndex: 'ak_lama',
        sortable: true,
        flex: 1,
        renderer: function(v, cmp, rec){
            if(v === 0){
                return null;
            }else{

                return Ext.util.Format.number(v, '0.000');
            }
        },
        field: {
            xtype: 'numberfield',
            editable: true,
            decimalPrecision: 3,
            decimalSeparator: ',',
            minValue: 0
        }
    }, {
        text: 'AK Usulan',
        dataIndex: 'ak_usulan',
        sortable: true,
        flex: 1,
        renderer: function(v){
            if(v === 0){
                return null;
            }else{
                return Ext.util.Format.number(v, '0.000');
            }
        },
        field: {
            xtype: 'numberfield',
            editable: true,
            decimalPrecision: 3,
            decimalSeparator: ',',
            minValue: 0
        }
    }, {
        text: 'AK Penilai',
        dataIndex: 'ak_penilai',
        sortable: true,
        flex: 1,
        renderer: function(v){
            if(v === 0){
                return null;
            }else{
                return Ext.util.Format.number(v, '0.000');
            }
        },
        field: {
            xtype: 'numberfield',
            editable: true,
            decimalPrecision: 3,
            decimalSeparator: ',',
            minValue: 0
        }
    }, {
        text: 'AK Final',
        dataIndex: 'ak_final',
        sortable: true,
        flex: 1,
        renderer: function(v){
            if(v === 0){
                return null;
            }else{
                return Ext.util.Format.number(v, '0.000');
            }
        }
    }]
});
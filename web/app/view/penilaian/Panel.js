Ext.define('SimpegGundul.view.penilaian.Panel', {
    extend: 'Ext.tab.Panel',
    xtype: 'penilaian',
    controller: 'penilaian',
    initComponent: function () {

        var me = this;

        me.items = [{
            itemId: 'pakForm',
            xtype: 'pakform', 
            title: 'Kelengkapan SK',
            tipe: me.tipe,
            autoScroll: true,
        }, {
            itemId: 'pakTree',
            xtype: 'paktreegrid',
            title: 'PAK',
            autoScroll: true,
        }];

        this.callParent();

    }
});
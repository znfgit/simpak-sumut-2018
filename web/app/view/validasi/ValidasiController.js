/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('SimpegGundul.view.validasi.ValidasiController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.validasi',

    onRenderGrid: function (grid) {
        var store = grid.getStore();

        store.on('beforeload', function(cmp){
            Ext.apply(cmp.proxy.extraParams, {
                ptk_id: ptk_selected.ptk_id
            });
        });

        store.reload();
    },

    onRefresh: function(btn){
        // console.log(btn.up('grid'));
        btn.up('grid').getStore().reload();
    }
});

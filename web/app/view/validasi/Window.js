Ext.define('SimpegGundul.view.validasi.Window', {
    extend: 'Ext.window.Window',
    xtype: 'validasiwindow',
    controller: 'validasi',

    title: 'Validasi Dengan Data BKN',
    maximizable: true,
    height: 378,
    width: 540,
    layout: 'fit',
    modal: true,

    initComponent: function(){

        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        this.items = [{
            xtype: 'grid',
            store: Ext.create('SimpegGundul.store.Validasi'),
            forceFit: true,
            listeners: {
                afterrender: 'onRenderGrid'
            },

            viewConfig: {
                listeners: {
                    refresh: function (dataview) {
                        Ext.each(dataview.panel.columns, function (column) {
                            // if (column.autoSizeColumn === true)
                            column.autoSize();
                        })
                    }
                }
            },

            tbar: [{
                xtype: 'label',
                text: 'Nama PTK : '
            }, {
                xtype: 'label',
                style: 'font-weight: bold;',
                text: ptk_selected.nama
            }, '->', {
                xtype: 'button',
                text: 'Refresh',
                iconCls: 'x-fa fa-refresh',
                handler: 'onRefresh'
            }],

            columns: [{
                header: 'Kolom',
                tooltip: 'Kolom',
                width: 120,
                sortable: true,
                dataIndex: 'kolom',
                style: 'text-align:center'
            }, {
                header: 'Validasi',
                columns: [{
                    header: 'Dapodik',
                    tooltip: 'Data Dapodik',
                    width: 250,
                    sortable: true,
                    dataIndex: 'dapodik',
                    style: 'text-align:center'
                }, {
                    header: 'BKN',
                    tooltip: 'Data BKN',
                    width: 250,
                    sortable: true,
                    dataIndex: 'bkn',
                    style: 'text-align:center'
                }]
            }, {
                header: '-',
                tooltip: 'Status Validasi',
                width: 25,
                sortable: true,
                dataIndex: 'status',
                renderer: function (v) {
                    if (v === 1) {
                        return '<img src="resources/icons/accept_button.png" width=16 height=16></img>';
                    } else if(v === 0){
                        return '<img src="resources/icons/warning.png" width=16 height=16></img>';
                    } else {
                        return '<img src="resources/icons/information.png" width=16 height=16></img>';
                    }
                }
            }]
        }];

        this.callParent(arguments);
    },
});
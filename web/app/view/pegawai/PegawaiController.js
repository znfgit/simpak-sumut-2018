/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('SimpegGundul.view.pegawai.PegawaiController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.pegawai',

    onRenderGrid: function (grid) {
        var store = grid.getStore();

        store.on('beforeload', function (cmp) {
            Ext.apply(cmp.proxy.extraParams, {
                txtCari: grid.down('textfield[itemId=txtCari]').getValue(),
            });

            if (grid.kegiatan_id) {
                grid.down('toolbar').hide();
                Ext.apply(cmp.proxy.extraParams, {
                    kegiatan: grid.kegiatan_id
                });
            }
        });

        store.reload();
    },

    addRecord: function(btn){
        // Dari button, 'look up'/lihat ke atas, cari container dengan xtype = 'form'
        var grid = btn.up('gridpanel');
        grid.rowEditing.cancelEdit();

        // Create a model instance
        var r = this.getNewRecord(grid);

        grid.store.insert(0, r);
        grid.rowEditing.startEdit(0, 0);
    },
    
    editRecord: function (btn) {
        // Defaults using row editor. Override this to use other method, such as form etc        
        var grid = btn.up('gridpanel');
        grid.rowEditing.cancelEdit();

        //var selections = grid.getSelectionModel().getSelection();
        var selections = grid.getSelection();
        var r = selections[0];
        if (!r) {
            Ext.toast('Mohon pilih salah satu baris', 'Error');
            return;
        }
        var startEditingColumnNumber = 0;
        for (var i = 0; i < grid.columns.length; i++) {
            if (grid.columns[i].isVisible()) {
                var startEditingColumnNumber = i;
                break;
            }
        }
        grid.rowEditing.startEdit(r, startEditingColumnNumber);
    },

    saveRecord: function (btn) {
        var grid = btn.up('gridpanel');

        var stores = grid.store;
        var arr = [];

        stores.each(function (record) {

            if (record.dirty) {
                // console.log(record);
                var arrRec = { pegawai_id: record.data.pegawai_id };

                for (var mod in record.modified) {
                    arrRec[mod] = record.data[mod];
                }

                arr.push(arrRec);
            }

        });

        if(Array.isArray(arr) !== true){
            Ext.toast('Tidak ada yang diperbaharui', 'Info');
            return;  
        } 

        // console.log(JSON.stringify(arr));
        Ext.getBody().mask('Tunggu sebentar...');
        Ext.Ajax.request({
            url: 'Pegawai/save',
            method: 'GET',
            params: {
                data: JSON.stringify(arr)
            },
            success: function (response) {
                var text = response.responseText;

                var obj = Ext.JSON.decode(response.responseText);

                Ext.toast('Menyimpan data pegawai (Berhasil: ' + obj.berhasil + ', Gagal: ' + obj.gagal + ')', 'Berhasil');
                Ext.getBody().unmask();
                stores.reload();
            }
        });
    },

    deleteRecord: function (btn) {
        // Defaults using row editor. Override this to use other method, such as form etc        
        var grid = btn.up('gridpanel');
        grid.rowEditing.cancelEdit();

        //var selections = grid.getSelectionModel().getSelection();
        var selections = grid.getSelection();
        var r = selections[0];
        if (!r) {
            Ext.toast('Mohon pilih salah satu baris', 'Error');
            return;
        }

        Ext.Msg.show({
            title: 'Konfirmasi',
            msg: 'Anda yakin? ',
            width: 300,
            closable: false,
            buttons: Ext.Msg.YESNO,
            multiline: false,
            fn: function (buttonValue, inputText, showConfig) {
                if (buttonValue === "yes") {
                    Ext.getBody().mask('Tunggu sebentar...');

                    Ext.Ajax.request({
                        url: 'Pegawai/delete',
                        method: 'GET',
                        params: {
                            id: r.data.pegawai_id
                        },
                        success: function (response) {
                            var text = response.responseText;

                            var obj = Ext.JSON.decode(response.responseText);

                            Ext.toast(obj.msg, 'Info');
                            Ext.getBody().unmask();
                            grid.getStore().reload();
                        },
                        failure: function(){
                            Ext.toast('Terjadi Kesalahan, hubungi admin');
                        }
                    });
                }
            },
            icon: Ext.Msg.QUESTION
        });
    },

    getNewRecord: function (grid) {
        var recordConfig = {
            pegawai_id: '',
            nama: '',
            nip: '',
            tempat_tugas: '',
            karpeg: '',
            tempat_lahir: '',
            tgl_lahir: Ext.Date.clearTime(new Date()),
            jenis_kelamin: '',
            jenjang_pendidikan: '',
            pangkat_golongan_id: '',
            tmt_pangkat: '',
            tmt_pengangkatan: '',
            jabatan: '',
            last_update: '',
            soft_delete: 0
        };
        Ext.apply(recordConfig, grid.newRecordCfg);
        var r = new SimpegGundul.model.Pegawai(recordConfig);
        return r;
    },

    getExcel: function (grid) {
        window.location.assign('Pegawai/getExcel');
    },

    onDropGridPtk: function (node, data, dropRec, dropPosition) {
        var gridKiri = Ext.ComponentQuery.query('anggotakegiatangrid[itemId=gridKiri]')[0];
        var gridKanan1 = Ext.ComponentQuery.query('ptkgrid[itemId=gridKanan1]')[0];
        var gridKanan2 = Ext.ComponentQuery.query('pegawaigrid[itemId=gridKanan2]')[0];

        var arr = new Array();
        data.records.forEach(element => {
            arr.push(element.data);
        });

        if (Array.isArray(arr) !== true) {
            Ext.toast('Tidak ada yang diperbaharui', 'Info');
            return;
        }

        gridKiri.up('window').mask('Tunggu sebentar...');
        Ext.Ajax.request({
            url: 'AnggotaKegiatan/delete',
            method: 'GET',
            params: {
                data: JSON.stringify(arr),
                kegiatan_id: gridKiri.kegiatan_id
            },
            success: function (response) {
                var text = response.responseText;

                var obj = Ext.JSON.decode(response.responseText);

                Ext.toast('Menghapus data Anggota (Berhasil: ' + obj.berhasil + ', Gagal: ' + obj.gagal + ')', 'Berhasil');
                gridKiri.up('window').unmask();
                gridKiri.getStore().reload();
                gridKanan1.getStore().reload();
                gridKanan2.getStore().reload();
            },
            failure: function (response) {
                gridKiri.up('window').unmask();
            }
        });
    }
});

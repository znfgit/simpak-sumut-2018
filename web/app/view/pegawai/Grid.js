Ext.define('SimpegGundul.view.pegawai.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pegawaigrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    controller: 'pegawai',
    columnLines: true,

    viewConfig: {
        listeners: {
            refresh: function (dataview) {
                Ext.each(dataview.panel.columns, function (column) {
                    // if (column.autoSizeColumn === true)
                    column.autoSize();
                })
            }
        }
    },

    listeners: {
        afterrender: 'onRenderGrid'
    },

    createStore: function () {
        return Ext.create('SimpegGundul.store.Pegawai');
    },
    createEditing: function () {
        return Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2,
            pluginId: 'editing'
        });
    },
    createDockedItems: function (grid) {
        return [{
            xtype: 'toolbar',
            // controller: 'pegawai',
            items: [{
                xtype: 'button',
                text: 'Tambah',
                iconCls: 'x-fa fa-plus-circle',
                itemId: 'add',
                handler: 'addRecord'
            }, {
                xtype: 'button',
                text: 'Ubah',
                iconCls: 'x-fa fa-edit',
                itemId: 'edit',
                handler: 'editRecord'
            }, {
                xtype: 'button',
                text: 'Simpan',
                iconCls: 'x-fa fa-save',
                itemId: 'save',
                handler: 'saveRecord'
            }, {
                xtype: 'button',
                text: 'Hapus',
                iconCls: 'x-fa fa-minus-circle',
                itemId: 'delete',
                handler: 'deleteRecord'
            },'-',{
                xtype: 'label',
                text: 'Cari : '
            },{
                xtype: "textfield",
                emptyText: 'Nama/NIP...',
                width: 200,
                itemId: 'txtCari',
                clearIcon: true,

                enableKeyEvents: true,
                listeners: {
                    keypress: function (field, e) {
                        if (e.getKey() == e.ENTER) {
                            grid.getStore().load();
                        }
                    }
                }
            },'->',{
                itemId: 'btndwnloadexcel',
                iconCls: 'x-fa fa-file-excel-o',
                text: 'Download Excel',
                handler: 'getExcel'
            }]
        }];
    },
    createBbar: function () {
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    initComponent: function () {

        var grid = this;

        this.store = this.createStore();

        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        // You can instaniate the component with basic parameters that affects filtering of the store
        // For example:
        // {
        //     xtype: 'yourgrid',
        //     baseParams: {
        //         area_id: 120,
        //         status_id: 1
        //     }
        // }
        if (this.initialConfig.baseParams) {

            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function (store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });

        }

        // this.getSelectionModel().setSelectionMode('SINGLE');

        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            listeners: {
                cancelEdit: function (rowEditing, context) {
                    // Canceling editing of a locally added, unsaved record: remove it
                    if (context.record.phantom) {
                        grid.store.remove(context.record);
                    }
                }
            }
        });

        var editing = this.createEditing();

        this.rowEditing = editing;
        this.plugins = [editing];

        this.lookupStorePangkatGolongan = new SimpegGundul.store.PangkatGolongan({
            autoLoad: true
        });

        this.columns = [{
            header: 'Id',
            tooltip: 'Id',
            width: 2040,
            sortable: true,
            dataIndex: 'pegawai_id',
            hideable: false,
            hidden: true,
            field: {
                xtype: 'hidden'
                , maxLength: 510
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Nama',
            tooltip: 'Nama',
            width: 2040,
            sortable: true,
            dataIndex: 'nama',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 510
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'NIK',
            tooltip: 'NIK',
            width: 2040,
            sortable: true,
            dataIndex: 'nik',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 510
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'NPWP',
            tooltip: 'NPWP',
            width: 2040,
            sortable: true,
            dataIndex: 'npwp',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 510
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'NIP',
            tooltip: 'NIP',
            width: 2040,
            sortable: true,
            dataIndex: 'nip',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 510
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Tempat Tugas',
            tooltip: 'Tempat Tugas',
            width: 2040,
            sortable: true,
            dataIndex: 'tempat_tugas',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 510
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Karpeg',
            tooltip: 'Karpeg',
            width: 2040,
            sortable: true,
            dataIndex: 'karpeg',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 510
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Tempat Lahir',
            tooltip: 'Tempat Lahir',
            width: 2040,
            sortable: true,
            dataIndex: 'tempat_lahir',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 510
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Tgl Lahir',
            tooltip: 'Tgl Lahir',
            width: 80,
            sortable: true,
            dataIndex: 'tgl_lahir',
            hideable: false,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            field: {
                xtype: 'datefield'
                , format: 'd/m/Y'
                , maxValue: new Date()
            }
        }, {
            header: 'JK',
            tooltip: 'JK',
            width: 2040,
            sortable: true,
            dataIndex: 'jenis_kelamin',
            hideable: false,
            field: {
                xtype: 'combobox',
                selectOnTab: true,
                valueField: 'id',
                hiddenName: 'id',
                displayField: 'data',
                store: {
                    fields: [{ name: 'id' }, { name: 'data' }],
                    data: [
                        { 'id': 'Laki - laki', 'data': 'Laki - laki' },
                        { 'id': 'Perempuan', 'data': 'Perempuan' }
                    ]
                },
                lazyRender: true
            },
            renderer: function (v, p, r) {
                switch (v) {
                    case 'Laki - laki': return 'Laki - laki'; break;
                    case 'Perempuan': return 'Perempuan'; break;
                    default: return '-'; break;
                }
            }
        }, {
            header: 'Jenjang Pendidikan',
            tooltip: 'Jenjang Pendidikan',
            width: 2040,
            sortable: true,
            dataIndex: 'jenjang_pendidikan',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 510
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Pangkat Golongan',
            tooltip: 'Pangkat Golongan',
            width: 80,
            sortable: true,
            dataIndex: 'pangkat_golongan_id',
            hideable: false,
            renderer: function (v, p, r) {
                var record = this.lookupStorePangkatGolongan.getById(v);
                if (record) {
                    return record.get('nama');
                } else {
                    return v;
                }
            },
            field: {
                displayField: 'nama',
                xtype: 'combo',
                valueField: 'pangkat_golongan_id',
                displayField: 'nama',
                queryMode: 'local',
                store: Ext.create('SimpegGundul.store.PangkatGolongan', {
                    sorters: ['pangkat_golongan_id']
                })
            }
        }, {
            header: 'Tmt Pangkat',
            tooltip: 'Tmt Pangkat',
            width: 80,
            sortable: true,
            dataIndex: 'tmt_pangkat',
            hideable: false,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            field: {
                xtype: 'datefield'
                , maxLength: 20
                , enforceMaxLength: true
                , minValue: 0
                , format: 'd/m/Y'
                , maxValue: new Date()
            }
        }, {
            header: 'Tmt Pengangkatan',
            tooltip: 'Tmt Pengangkatan',
            width: 80,
            sortable: true,
            dataIndex: 'tmt_pengangkatan',
            hideable: false,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            field: {
                xtype: 'datefield'
                , maxLength: 20
                , enforceMaxLength: true
                , minValue: 0
                , format: 'd/m/Y'
                , maxValue: new Date()
            }
        }, {
            header: 'Jabatan',
            tooltip: 'Jabatan',
            width: 2040,
            sortable: true,
            dataIndex: 'jabatan',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 510
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Last Update',
            tooltip: 'Last Update',
            width: 80,
            sortable: true,
            dataIndex: 'last_update',
            hideable: false,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            field: {
                xtype: 'datefield'
                , maxLength: 16
                , enforceMaxLength: true
                , minValue: 0
                , format: 'd/m/Y'
                , maxValue: new Date()
            }
        }, {
            header: 'Soft Delete',
            tooltip: 'Soft Delete',
            width: 80,
            sortable: true,
            dataIndex: 'soft_delete',
            hideable: false,
            field: {
                xtype: 'numberfield'
                , maxLength: 2
                , enforceMaxLength: true
                , minValue: 0
            }

        }];

        this.dockedItems = this.createDockedItems(grid);

        this.bbar = this.createBbar();

        this.callParent(arguments);
    }
});
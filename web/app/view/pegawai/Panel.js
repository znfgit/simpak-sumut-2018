Ext.define('SimpegGundul.view.pegawai.Panel', {
    extend: 'Ext.panel.Panel',
    xtype: 'pegawai',

    initComponent: function () {

        var me = this;

        me.items = {
            xtype: 'pegawaigrid',
            iconCls: 'x-fa fa-users',
            title: 'Pegawai',
        };

        this.callParent();

    }
});

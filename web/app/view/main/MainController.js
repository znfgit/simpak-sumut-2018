/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('SimpegGundul.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onMenuClick: function(btn){
        var me = this;

        me.switchMenu(btn.routeId, btn.text);
    },

    switchMenu: function (menuName, title) {

        var me = this;

        if (menuName == 'keluar_aplikasi') {
            Ext.Msg.show({
                title: 'Konfirmasi',
                msg: 'Apakah yakin anda mau keluar? ',
                width: 300,
                closable: false,
                buttons: Ext.Msg.YESNO,
                buttonText:
                    {
                        yes: 'Ya dan Keluar',
                        no: 'Tidak dan Kembali'
                    },
                multiline: false,
                fn: function (buttonValue, inputText, showConfig) {
                    if (buttonValue === "yes") {
                        window.location.assign("logout");
                        // window.location.assign(window.location.pathname + "logout");
                    }
                },
                icon: Ext.Msg.QUESTION
            });
        } else if (menuName == 'simrasio') {
            var win = window.open('', 'form');

            win.document.write("<html><title>Sim Rasio</title><body><form id='form' method='POST' action='http://simrasio.gtk.kemdikbud.go.id:2018/login_check'><input type='hidden' name='_username' value='simrasio_kota_palembang'><input type='hidden' name='_password' value='1273468'></form></body></html>");
            win.document.close();
            win.document.getElementById('form').submit();
        } else if (menuName == 'informasi_pengguna') {

            Ext.create('Ext.window.Window', {
                title: 'Informasi Pengguna',
                height: 280,
                width: 490,
                layout: 'vbox',
                modal: true,
                bodyPadding: 10,
                items: [{
                    xtype: 'displayfield',
                    fieldLabel: 'Username',
                    value: user.username
                }, {
                    xtype: 'displayfield',
                    fieldLabel: 'Nama',
                    value: user.nama
                }, {
                    xtype: 'displayfield',
                    fieldLabel: 'Peran',
                    value: user.peran
                }, {
                    xtype: 'displayfield',
                    fieldLabel: 'Alamat',
                    value: user.alamat
                }]
            }).show();
        } else {
            me.searchAddActivateTab(menuName, title);
        }

    },

    searchAddActivateTab: function (xtypeName, title) {

        var me = this;

        var tabpanel = Ext.getCmp('main-tabpanel');
        var cnt = {};

        cnt = tabpanel.down("panel[xtype='" + xtypeName + "']");

        if (cnt) {

            // Do nothin

        } else {

            cnt = Ext.create({
                xtype: xtypeName,
                layout: 'fit',
                closable: false,
                title: title
            });

            tabpanel.add(cnt);

        }
        tabpanel.setActiveTab(cnt);
    },

    onRenderMenu: function(pnl){
        // Get Pengguna Info
        var query = document.querySelectorAll('input[type=hidden]');
        query.forEach(u => {
            user[u.name] = u.value;
        });

        if (user.level == 1) {
            // Ext.getCmp('pegawaiGrid').hide();
            Ext.getCmp('pengawasGrid').hide();
            // Ext.getCmp('kepsekGrid').hide();

            pnl.down('button[routeId=pengaturan]').hide();
            // Ext.getCmp('kenaikangPangkatPegawai').hide();
            // Ext.getCmp('kgbPegawai').hide();
        }

        if (['3', '99'].indexOf(user.level) < 0) {
            pnl.down('button[routeId=pengaturan]').hide();
        }
        
    }
});

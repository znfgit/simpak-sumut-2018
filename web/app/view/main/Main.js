/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('SimpegGundul.view.main.Main', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.dashboard.Dashboard'
    ],

    xtype: 'app-main',

    controller: 'main',

    layout: {
        type: 'border'
    },

    initComponent: function () {
        var me = this;
        var uri = window.location.pathname;

        me.dashboard = Ext.create('Ext.dashboard.Dashboard', {

            // Tab Pertama - Beranda
            title: 'Beranda',
            xtype: 'dashboard',
            region: 'center',
            layout: 'fit',
            // autoScroll: true,
            html: '<iframe src="dashboard/dashboardv2"  scrolling="yes" style=" width: 100%; height: 98%;  overflow: hidden;" ></iframe>'
        });

        // Create The Buttoned Tab Panel
        me.tabPanel = {
            region: 'center',
            xtype: 'tabpanel',
            id: 'main-tabpanel',
            tabPosition: 'bottom',
            tabBar: {
                hidden: true
            },
            dockedItems: [{
                dock: 'top',       // Alternatives: 'left', 'top'
                xtype: 'toolbar',
                listeners: {
                    afterrender: 'onRenderMenu'
                },
                items: [{
                    iconCls: 'x-fa fa-home',
                    xtype: 'button',
                    tooltip: 'Beranda',
                    // text: 'Beranda',
                    iconAlign: 'left',   // Alternatives: 'top', 'left'
                    routeId: 'dashboard',
                    handler: 'onMenuClick'
                }, {
                    xtype: 'button',
                    text: 'Data Master',
                    iconCls: 'x-fa fa-users',
                    itemId: 'dataMaster',
                    menu: [{
                        iconCls: 'x-fa fa-users',
                        // xtype: 'button',
                        id:'pnsGrid',
                        text: 'Guru PNS',
                        iconAlign: 'left',   // Alternatives: 'top', 'left'
                        routeId: 'gurupns',
                        handler: 'onMenuClick'
                    }, {
                        iconCls: 'x-fa fa-users',
                        // xtype: 'button',
                        id: 'nonPnsGrid',
                        text: 'Guru Non PNS',
                        iconAlign: 'left',   // Alternatives: 'top', 'left'
                        routeId: 'gurunonpns',
                        handler: 'onMenuClick'
                    }, {
                        iconCls: 'x-fa fa-users',
                        // xtype: 'button',
                        id: 'tendikGrid',
                        text: 'Tendik',
                        iconAlign: 'left',   // Alternatives: 'top', 'left'
                        routeId: 'tendik',
                        handler: 'onMenuClick'
                    }, {
                        iconCls: 'x-fa fa-users',
                        // xtype: 'button',
                        id: 'pengawasGrid',
                        text: 'Pengawas',
                        iconAlign: 'left',   // Alternatives: 'top', 'left'
                        routeId: 'pengawas',
                        handler: 'onMenuClick',
                    } /*, {
                        iconCls: 'x-fa fa-users',
                        // xtype: 'button',
                        id: 'pegawaiGrid',
                        text: 'Pegawai',
                        iconAlign: 'left',   // Alternatives: 'top', 'left'
                        routeId: 'pegawai',
                        handler: 'onMenuClick',
                    }*/, {
                        iconCls: 'x-fa fa-users',
                        text: 'Kepala Sekolah',
                        id: 'kepsekGrid',
                        iconAlign: 'left',   // Alternatives: 'top', 'left'
                        routeId: 'kepalasekolah',
                        handler: 'onMenuClick',
                    }]
                }, {
                    iconCls: 'x-fa fa-caret-square-o-up',
                    xtype: 'button',
                    text: 'Kenaikan Pangkat',
                    // routeId: 'kenaikanpangkat',
                    // handler: 'onMenuClick',
                    menu: [{
                        iconCls: 'x-fa fa-caret-square-o-up',
                        text: 'PTK',
                        iconAlign: 'left',   // Alternatives: 'top', 'left'
                        routeId: 'kenaikanpangkat',
                        handler: 'onMenuClick'
                    }/*, {
                        iconCls: 'x-fa fa-caret-square-o-up',
                        text: 'Pegawai',
                        id: 'kenaikangPangkatPegawai',
                        // hidden: true,
                        iconAlign: 'left',   // Alternatives: 'top', 'left'
                        routeId: 'kenaikanpangkatpegawai',
                        handler: 'onMenuClick'
                    }*/]
                }, {
                    iconCls: 'x-fa fa-money',
                    xtype: 'button',
                    text: 'KGB',
                    hidden: true,
                    menu: [{
                        iconCls: 'x-fa fa-money',
                        text: 'PTK',
                        iconAlign: 'left',   // Alternatives: 'top', 'left'
                        routeId: 'kgb',
                        handler: 'onMenuClick'
                    }/*, {
                        iconCls: 'x-fa fa-money',
                        text: 'Pegawai',
                        id: 'kgbPegawai',
                        // hidden: true,
                        iconAlign: 'left',   // Alternatives: 'top', 'left'
                        routeId: 'kgbpegawai',
                        handler: 'onMenuClick'
                    }*/]
                }, {
                    iconCls: 'x-fa fa-exchange',
                    xtype: 'button',
                    text: 'Mutasi',
                    itemId: 'mutasi',
                    menu: [{
                        iconCls: 'x-fa fa-exchange',
                        text: 'Simulasi (SIM Rasio Pusat)',
                        iconAlign: 'left',   // Alternatives: 'top', 'left'
                        routeId: 'simrasio',
                        handler: 'onMenuClick',
                        hidden: true
                    }, {
                        iconCls: 'x-fa fa-exchange',
                        text: 'Usulan',
                        iconAlign: 'left',   // Alternatives: 'top', 'left',
                        routeId: 'mutasi',
                        handler: 'onMenuClick'
                    }]
                }, {
                    iconCls: 'x-fa fa-tasks',
                    xtype: 'button',
                    text: 'Penugasan',
                    iconAlign: 'left',   // Alternatives: 'top', 'left'
                    routeId: 'penugasan',
                    handler: 'onMenuClick',
                    hidden: true
                }, {
                    iconCls: 'x-fa fa-calendar',
                    xtype: 'button',
                    text: 'Kegiatan',
                    iconAlign: 'left',   // Alternatives: 'top', 'left'
                    routeId: 'kegiatan',
                    handler: 'onMenuClick',
                    hidden: true
                }, {
                    // iconCls: 'x-fa fa-id-card',
                    xtype: 'button',
                    glyph: 'xf09d@FontAwesome',
                    text: 'Pengajuan Kartu',
                    iconAlign: 'left',   // Alternatives: 'top', 'left'
                    routeId: 'pengajuankartu',
                    handler: 'onMenuClick',
                    hidden: true
                    // menu: [{
                    //     glyph: 'xf09d@FontAwesome',
                    //     text: 'PTK',
                    //     iconAlign: 'left',   // Alternatives: 'top', 'left'
                    //     routeId: 'pengajuankartu',
                    //     handler: 'onMenuClick'
                    // }, {
                    //     glyph: 'xf09d@FontAwesome',
                    //     text: 'Pegawai',
                    //     iconAlign: 'left',   // Alternatives: 'top', 'left',
                    //     routeId: 'pengajuankartupegawai',
                    //     handler: 'onMenuClick'
                    // }]
                }, {
                    iconCls: 'x-fa fa-gears',
                    xtype: 'button',
                    text: 'Pengaturan',
                    iconAlign: 'left',   // Alternatives: 'top', 'left'
                    routeId: 'pengaturan',
                    handler: 'onMenuClick',
                }, '->', {
                    iconCls: 'x-fa fa-info-circle',
                    xtype: 'button',
                    text: 'Informasi Pengguna',
                    iconAlign: 'left',   // Alternatives: 'top', 'left',
                    routeId: 'informasi_pengguna',
                    handler: 'onMenuClick'
                }, {
                    iconCls: 'x-fa fa-sign-out',
                    xtype: 'button',
                    cls: 'button-ico',
                    style: {
                        backgroundColor: '#B22222'
                    },
                    tooltip: 'Keluar Aplikasi',
                    // text: 'Keluar Aplikasi',
                    iconAlign: 'left',   // Alternatives: 'top', 'left',
                    routeId: 'keluar_aplikasi',
                    handler: 'onMenuClick'
                }]
            }],
            items: [me.dashboard]
        };

        me.items = [{
            region: 'north',
            xtype: 'component',
            // padding: 15,
            height: 60,
            style: 'background-image: url(resources/images/dark-exa.png), linear-gradient(to right, #ff8008, #ffc837);margin: 0px; width: 800px; height: 160px; padding-left: 15px; padding-top: 5px;',
            html: '<img style="float:left;padding-bottom: 19px;width: 2.5%;/* height: 126%; */" src="resources/images/logo-sumut.png"><div class="appheader"><b>SIMPAK Provinsi Sumatera Utara</b></div>'
        }, me.tabPanel];

        this.callParent();
    }

});

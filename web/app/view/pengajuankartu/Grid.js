Ext.define('SimpegGundul.view.pengajuankartu.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pengajuankartugrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    columnLines: true,
    controller: 'pengajuankartu',
    // forceFit: true,

    viewConfig: {
        listeners: {
            refresh: function (dataview) {
                Ext.each(dataview.panel.columns, function (column) {
                    // if (column.autoSizeColumn === true)
                    column.autoSize();
                })
            }
        }
    },

    listeners: {
        afterrender: 'onRenderGrid'
    },

    createStore: function () {
        return Ext.create('SimpegGundul.store.PengajuanKartu');
    },

    createEditing: function () {
        return Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2,
            pluginId: 'editing'
        });
    },

    createDockedItems: function (grid) {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                itemId: 'btnTambah',
                text: 'Tambah',
                iconCls: 'x-fa fa-plus-circle',
                hidden: (user.level == '1') ? false : true,
                handler: 'onTambahPengajuan'
            }, {
                xtype: 'button',
                itemId: 'btnUbah',
                text: 'Ubah',
                iconCls: 'x-fa fa-pencil-square',
                hidden: (user.level == '1') ? false : true,
                handler: 'onUbahPengajuan'
            }, {
                xtype: 'button',
                text: 'Hapus',
                iconCls: 'x-fa fa-remove fa-lg glyph-red glyph-shadow',
                itemId: 'delete',
                hidden: (user.level == '1') ? false : true,
                handler: 'onHapusPengajuan'
            },'-','No. Surat : ',{
                xtype: "textfield",
                emptyText: 'No. Surat... (Enter)',
                width: 200,
                itemId: 'txtNoSurat',
                enableKeyEvents: true,
                name: 'no_surat',
                listeners: {
                    keypress: function (field, e) {
                        if (e.getKey() == e.ENTER){
                            grid.getStore().load();
                        }
                    }
                }
            }, '->', {
                itemId: 'btnApproveUpt',
                iconCls: 'x-fa fa-check-square',
                text: 'Aprrove',
                hidden: (user.level == '2') ? false : true,
                approved: 2,
                handler: 'onApprove'
            }, {
                itemId: 'btnRejectUpt',
                iconCls: 'x-fa fa-crosshairs',
                text: 'Tolak',
                hidden: (user.level == '2') ? false : true,
                approved: 1,
                handler: 'onApprove'
            }, {
                itemId: 'btnProses',
                iconCls: 'x-fa fa-check-square',
                hidden: (['1', '2'].indexOf(user.level) < 0) ? false : true,
                text: 'Approve',
                approved: 4,
                handler: 'onApprove'
            }, {
                itemId: 'btnRejectDinas',
                iconCls: 'x-fa fa-crosshairs',
                text: 'Tolak',
                hidden: (['1', '2'].indexOf(user.level) < 0) ? false : true,
                approved: 3,
                handler: 'onApprove'
            }, {
                itemId: 'btnCetakSuratPermohonan',
                iconCls: 'x-fa fa-print',
                text: 'Cetak Surat Permohonan',
                hidden: (user.level == '1') ? false : true,
                // hidden: (user.level == '1') ? false : true,
                handler: 'onCetakSurat'
            }]
        }];
    },

    createBbar: function () {
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },

    initComponent: function () {

        var grid = this;

        this.store = this.createStore();

        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        // You can instaniate the component with basic parameters that affects filtering of the store
        // For example:
        // {
        //     xtype: 'yourgrid',
        //     baseParams: {
        //         area_id: 120,
        //         status_id: 1
        //     }
        // }
        if (this.initialConfig.baseParams) {

            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function (store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });

        }

        // this.getSelectionModel().setSelectionMode('SINGLE');

        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            listeners: {
                cancelEdit: function (rowEditing, context) {
                    // Canceling editing of a locally added, unsaved record: remove it
                    if (context.record.phantom) {
                        grid.store.remove(context.record);
                    }
                }
            }
        });

        var editing = this.createEditing();

        this.rowEditing = editing;
        this.plugins = [editing];

        // this.lookupStorePtk = new SimpegGundul.store.Ptk({
        //     autoLoad: true
        // });

        // this.lookupStoreSekolah = new SimpegGundul.store.Sekolah({
        //     autoLoad: true
        // });

        this.columns = [{
            header: 'Id',
            tooltip: 'Id',
            width: 1020,
            sortable: true,
            dataIndex: 'pengajuan_kartu_id',
            hideable: false,
            hidden: true
        }, {
            header: 'No Surat',
            tooltip: 'No Surat',
            width: 100,
            sortable: true,
            dataIndex: 'no_surat',
            align: 'left',
            hideable: false,
        }, {
            header: 'Tgl Surat',
            tooltip: 'Tgl Surat',
            width: 50,
            sortable: true,
            dataIndex: 'tgl_surat',
            align: 'left',
            hideable: false,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
        }, {
            header: 'NIP',
            tooltip: 'NIP',
            width: 144,
            sortable: true,
            dataIndex: 'nip',
            align: 'left',
            hideable: false
        }, {
            header: 'Nama',
            tooltip: 'Nama',
            width: 325,
            sortable: true,
            dataIndex: 'ptk_id',
            align: 'left',
            hideable: false,
            renderer: function (v, p, r) {
                return r.data.ptk_id_str;
            }
        }, {
            header: 'Instansi',
            tooltip: 'Instansi',
            width: 144,
            sortable: true,
            dataIndex: 'instansi',
            align: 'left',
            hideable: false
        }, {
            header: 'Jenis Pengajuan',
            tooltip: 'Jenis Pengajuan',
            width: 144,
            sortable: true,
            dataIndex: 'jenis_kartu',
            align: 'left',
            hideable: false,
            renderer: function (v, p, r) {
                switch (v) {
                    case 1:
                        return 'KARPEG';
                    case 2:
                        return 'KARSU / KARIS';
                    case 3:
                        return 'TASPEN';
                }
            }
        }, {
            header: 'Banyaknya',
            tooltip: 'Banyaknya',
            width: 144,
            sortable: true,
            dataIndex: 'banyaknya',
            align: 'left',
            hideable: false,
        }, {
            header: 'Keterangan',
            tooltip: 'Keterangan',
            width: 144,
            sortable: true,
            dataIndex: 'keterangan',
            align: 'left',
            hideable: false,
        }, {
            header: 'Status',
            tooltip: 'Status',
            width: 80,
            sortable: true,
            dataIndex: 'status',
            align: 'left',
            hideable: false,
            renderer: function (v, p, r) {
                switch (v) {
                    case 0:
                        return 'Diusulkan Sekolah';
                    case 1:
                        return 'Ditolak UPT';
                    case 2:
                        return 'Disetujui UPT';
                    case 3:
                        return 'Ditolak Dinas';
                    case 4:
                        return 'Disetujui Dinas';
                    case 5:
                        return 'Diusulkan Sekolah';
                    case 6:
                        return 'Diusulkan Dinas';
                }
            }
        }, {
            header: 'Alasan Penolakan',
            tooltip: 'Alasan Penolakan',
            width: 150,
            sortable: true,
            dataIndex: 'alasan_penolakan',
            hideable: false,
        }];

        this.dockedItems = this.createDockedItems(grid);

        this.bbar = this.createBbar();

        this.callParent(arguments);
    }
});
Ext.define('SimpegGundul.view.pengajuankartu.PengajuanKartu', {
    extend: 'Ext.panel.Panel',
    xtype: 'pengajuankartu',

    initComponent: function () {

        var me = this;

        me.items = {
            id: 'gridPengajuanKartu',
            xtype: 'pengajuankartugrid',
            title: 'Daftar Pengajuan Kartu - Kartu',
            glyph: 'xf09d@FontAwesome',
        };

        this.callParent();

    }
});

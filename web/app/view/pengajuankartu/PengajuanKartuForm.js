Ext.define('SimpegGundul.view.pengajuankartu.PengajuanKartuForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.pengajuankartuform',
    bodyPadding: 10,
    autoScroll: true,
    controller: 'pengajuankartu',
    defaults: {
        labelWidth: 140,
        anchor: '100%'
    },
    initComponent: function () {

        var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record) {
            this.listeners = {
                afterrender: function (form, options) {
                    form.loadRecord(record);
                }
            };
        }

        /*this.on('beforeadd', function(form, field){
            if (!field.allowBlank)
              field.labelSeparator += '<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>';
        });*/

        this.items = [{
            xtype: 'hidden'
            , fieldLabel: 'Id'
            , labelAlign: 'right'
            , labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
            , allowBlank: false
            , minValue: 0
            , maxLength: 255
            , enforceMaxLength: true
            , name: 'pengajuan_kartu_id'
            , tabIndex: 0
        }, {
            xtype: 'textfield'
            , fieldLabel: 'No surat'
            , labelAlign: 'right'
            , labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
            , allowBlank: false
            , minValue: 0
            , maxLength: 50
            , enforceMaxLength: true
            , name: 'no_surat'
            , tabIndex: 1
        }, {
            xtype: 'datefield'
            , fieldLabel: 'Tanggal Surat'
            , labelAlign: 'right'
            , labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
            , allowBlank: false
            , name: 'tgl_surat'
            , tabIndex: 2
        }, {
            xtype: 'ptkcombo'
            , fieldLabel: 'PTK'
            , labelAlign: 'right'
            , labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
            // ,allowBlank: false
            , minValue: 0
            , name: 'ptk_id'
            , tabIndex: 3
            , hidden: (user.level == '1') ? false : true,
        }, {
            xtype: 'pegawaicombo'
            , fieldLabel: 'Pegawai'
            , labelAlign: 'right'
            , labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
            // ,allowBlank: false
            , minValue: 0
            , name: 'pegawai_id'
            , tabIndex: 4
            , hidden: (user.level == '1') ? true : false,
        }, {
            xtype: 'combobox',
            fieldLabel: 'Jenis Kartu',
            selectOnTab: true,
            allowBlank: false,
            labelAlign: 'right',
            valueField: 'jenis_kartu',
            displayField: 'data',
            name: 'jenis_kartu',
            store: {
                data: [
                    { 'jenis_kartu': 1, 'data': 'KARPEG' },
                    { 'jenis_kartu': 2, 'data': 'KARSU / KARIS' },
                    { 'jenis_kartu': 3, 'data': 'TASPEN' },
                ]
            },
            lazyRender: true,
            tabIndex: 5
        }, {
            xtype: 'textfield'
            , fieldLabel: 'Banyaknya'
            , labelAlign: 'right'
            , labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
            , allowBlank: false
            , minValue: 0
            , maxLength: 50
            , enforceMaxLength: true
            , name: 'banyaknya'
            , tabIndex: 6
        }, {
            xtype: 'textareafield'
            , fieldLabel: 'Keterangan'
            , labelAlign: 'right'
            , allowBlank: false
            , minValue: 0
            , name: 'keterangan'
            , tabIndex: 7
        }];

        this.buttons = [{
            text: 'Save',
            iconCls: 'fa fa-save fa-inverse fa-lg',
            handler: 'onSaveForm'
        }];

        this.callParent(arguments);
    }
});
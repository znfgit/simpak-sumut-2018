/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('SimpegGundul.view.pengajuankartu.PengajuanKartuController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.pengajuankartu',

    onRenderGrid: function (grid) {
        var store = grid.getStore();

        store.on('beforeload', function (cmp) {
            if (['3', '4', '5', '6', '99'].indexOf(user.level) >= 0) {
                Ext.apply(store.proxy.extraParams, {
                    status: "[2,3,4,5,6]",
                });
            } else if (user.level == '2') {
                Ext.apply(store.proxy.extraParams, {
                    status: "[0,1,2,3,4]",
                });
            }

            Ext.apply(store.proxy.extraParams, {
                no_surat: grid.down('textfield[itemId=txtNoSurat]').getValue()
            });
        });

        store.reload();
    },

    onTambahPengajuan: function (btn) {
        Ext.create('Ext.window.Window', {
            title: 'Tambah / Rubah Pengajuan',
            height: 486,
            width: 590,
            layout: 'fit',
            modal: true,
            items: {
                xtype: 'pengajuankartuform',
            }
        }).show();
    },

    onUbahPengajuan: function (btn) {
        // Defaults using row editor. Override this to use other method, such as form etc        
        var grid = btn.up('gridpanel');

        //var selections = grid.getSelectionModel().getSelection();
        var selections = grid.getSelection();
        var r = selections[0];
        if (!r) {
            Ext.toast('Mohon pilih salah satu baris', 'Error');
            return;
        }

        if ([2, 4].indexOf(r.get('proses')) >= 0) {
            Ext.toast('PTK sudah disetujui UPT / Dinas', 'Error');
            return;
        }
        Ext.create('Ext.window.Window', {
            title: 'Tambah / Rubah Pengajuan',
            height: 486,
            width: 590,
            layout: 'fit',
            modal: true,
            items: {
                xtype: 'pengajuankartuform',
                record: r
            }
        }).show();
    },

    onHapusPengajuan: function (btn) {
        // Defaults using row editor. Override this to use other method, such as form etc        
        var grid = btn.up('gridpanel');
        grid.rowEditing.cancelEdit();

        //var selections = grid.getSelectionModel().getSelection();
        var selections = grid.getSelection();
        var r = selections[0];
        if (!r) {
            Ext.toast('Mohon pilih salah satu baris', 'Error');
            return;
        }

        Ext.Msg.show({
            title: 'Konfirmasi',
            msg: 'Anda yakin? ',
            width: 300,
            closable: false,
            buttons: Ext.Msg.YESNO,
            multiline: false,
            fn: function (buttonValue, inputText, showConfig) {
                if (buttonValue === "yes") {
                    Ext.getBody().mask('Tunggu sebentar...');

                    Ext.Ajax.request({
                        url: 'PengajuanKartu/delete',
                        method: 'GET',
                        params: {
                            id: r.data.pengajuan_kartu_id
                        },
                        success: function (response) {
                            var text = response.responseText;

                            var obj = Ext.JSON.decode(response.responseText);

                            Ext.toast(obj.msg, 'Info');
                            Ext.getBody().unmask();
                            grid.getStore().reload();
                        },
                        failure: function () {
                            Ext.toast('Terjadi Kesalahan, hubungi admin');
                        }
                    });
                }
            },
            icon: Ext.Msg.QUESTION
        });
    },

    onSaveForm: function (btn) {
        var form = btn.up('form');

        if ((form.isDirty()) && (form.isValid())) {
            Ext.MessageBox.show({
                title: 'konfirmasi',
                msg: 'Apakah anda sudah yakin ?',
                buttonText: { yes: "Ya", no: "Tidak" },
                fn: function (btn1) {
                    if (btn1 == "yes") {
                        form.submit({
                            method: 'GET',
                            url: '/PengajuanKartu/saveForm',
                            waitMsg: 'Menyimpan...',
                            success: function (frm, action) {
                                Ext.toast(action.result.msg, 'Info');

                                btn.up('window').close();
                                Ext.getCmp('gridPengajuanKartu').getStore().reload();
                            },
                            failure: function (form, action) {
                                Ext.toast("Terjadi kesalahan, hubungi admin");
                                // Ext.toast(action.result.msg);
                            }
                        });
                    }
                }
            });
        } else {
            Ext.toast('Harap diisi semua', 'Info');
        }
    },

    onApprove: function (btn) {
        var approved = btn.approved;

        var grid = btn.up('pengajuankartugrid');

        if (!grid.getSelection()[0]) {
            Ext.Msg.alert('Error', "Mohon Pilih PTK Terlebih Dahulu");
            return false;
        }

        var alasan = '';
        if ([1, 3].indexOf(approved) >= 0) {
            Ext.Msg.prompt('Alasan Penolakan', 'Masukkan alasan penolakan', function (btn, text) {
                if (btn == 'ok') {
                    alasan = text;
                    Ext.Msg.show({
                        title: 'Konfirmasi',
                        msg: 'Sudah yakin? ',
                        width: 300,
                        closable: false,
                        buttons: Ext.Msg.YESNO,
                        multiline: false,
                        fn: function (buttonValue, inputText, showConfig) {
                            if (buttonValue === "yes") {
                                var jsonData = "[";
                                for (var i = 0; i < grid.getSelection().length; i++) {
                                    var rec = grid.getSelection()[i];

                                    jsonData += Ext.util.JSON.encode(rec.data) + ",";
                                }
                                jsonData = jsonData.substring(0, jsonData.length - 1) + "]";

                                grid.mask('Tunggu sebentar...');
                                Ext.Ajax.request({
                                    waitMsg: 'Menyimpan....',
                                    url: 'PengajuanKartu/approve',
                                    method: 'GET',
                                    params: {
                                        approved: approved,
                                        alasan: alasan,
                                        rows: jsonData
                                    },
                                    failure: function (res, opt) {
                                        grid.unmask();
                                    },
                                    success: function (res, opt) {
                                        grid.unmask();
                                        Ext.ComponentQuery.query('grid[xtype=pengajuankartugrid]')[0].getStore().reload();
                                    }
                                });
                            }
                        },
                        icon: Ext.Msg.QUESTION
                    });
                }
            });
        } else {
            Ext.Msg.show({
                title: 'Konfirmasi',
                msg: 'Sudah yakin? ',
                width: 300,
                closable: false,
                buttons: Ext.Msg.YESNO,
                multiline: false,
                fn: function (buttonValue, inputText, showConfig) {
                    if (buttonValue === "yes") {
                        var jsonData = "[";
                        for (var i = 0; i < grid.getSelection().length; i++) {
                            var rec = grid.getSelection()[i];

                            jsonData += Ext.util.JSON.encode(rec.data) + ",";
                        }
                        jsonData = jsonData.substring(0, jsonData.length - 1) + "]";

                        grid.mask('Tunggu sebentar...');
                        Ext.Ajax.request({
                            waitMsg: 'Menyimpan....',
                            url: 'PengajuanKartu/approve',
                            method: 'GET',
                            params: {
                                approved: approved,
                                rows: jsonData
                            },
                            failure: function (res, opt) {
                                grid.unmask();
                            },
                            success: function (res, opt) {
                                grid.unmask();
                                Ext.ComponentQuery.query('grid[xtype=pengajuankartugrid]')[0].getStore().reload();
                            }
                        });
                    }
                },
                icon: Ext.Msg.QUESTION
            });
        }
    },

    onCetakSurat: function (btn) {
        var grid = btn.up('grid');
        var sel = grid.getSelection();
        var rec = sel[0];
        if (sel.length <= 0) {
            Ext.toast('Mohon pilih salah satu baris terlebih dahulu', 'Error');
            return;
        }

        if (user.level == '1') {
            var printUrl = 'PengajuanKartu/getCetakSurat?id=' + rec.data.pengajuan_kartu_id;
        } else {
            var printUrl = 'PengajuanKartu/getCetakSuratPegawai?id=' + rec.data.pengajuan_kartu_id;
        }


        var win = Ext.widget({
            xtype: 'window_print',
            title: 'Cetak Surat Pengantar',
            src: printUrl
        });

        win.show();
    }
});

Ext.define('SimpegGundul.view.mutasianggotask.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.mutasianggotaskgrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    controller: 'mutasianggotask',
    listeners: {
        afterrender: 'onRenderGrid'
    },

    createStore: function () {
        return Ext.create('SimpegGundul.store.MutasiAnggotaSk');
    },

    createDockedItems: function () {
        return [{
            xtype: 'toolbar',
            items: ['Daftar PTK yang sudah masuk SK']
        }];
    },

    createBbar: function () {
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },

    initComponent: function () {

        var grid = this;

        this.store = this.createStore();

        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        // You can instaniate the component with basic parameters that affects filtering of the store
        // For example:
        // {
        //     xtype: 'yourgrid',
        //     baseParams: {
        //         area_id: 120,
        //         status_id: 1
        //     }
        // }
        if (this.initialConfig.baseParams) {

            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function (store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });

        }

        this.columns = [{
            header: 'Id',
            tooltip: 'Id',
            width: 1020,
            sortable: true,
            dataIndex: 'mutasi_anggota_sk_id',
            hideable: false,
            hidden: true,
            field: {
                xtype: 'hidden'
                , maxLength: 255
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'PTK',
            tooltip: 'Nama & NIP',
            width: 200,
            sortable: true,
            dataIndex: 'ptk_id_str',
            align: 'left',
            hideable: false,
            renderer: function(v, cmp, rcd){
                return rcd.get('ptk_id_str') + "<br>" + rcd.get('nip');
            }
        }, {
            header: 'Sekolah Asal',
            tooltip: 'Sekolah Asal',
            align: 'left',
            width: 200,
            sortable: true,
            dataIndex: 'sekolah_id_str',
            hideable: false
        }, {
            header: 'Sekolah Tujuan',
            tooltip: 'Sekolah Tujuan',
            align: 'left',
            width: 200,
            sortable: true,
            dataIndex: 'nama_sekolah',
            hideable: false
        }, {
            header: 'Alasan',
            tooltip: 'Alasan',
            align: 'left',
            width: 200,
            sortable: true,
            dataIndex: 'alasan',
            hideable: false
        }];
        
        this.dockedItems = this.createDockedItems();
        
        this.bbar = this.createBbar();

        this.callParent(arguments);
    }
});
Ext.define('SimpegGundul.view.mutasianggotask.Window', {
    extend: 'Ext.window.Window',
    xtype: 'mutasianggotaskwindow',
    controller: 'mutasianggotask',

    title: 'Daftar PTK di SK',
    maximizable: true,
    height: 478,
    width: 740,
    modal: true,
    layout: 'border',

    initComponent: function () {

        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        this.items = [{
            xtype: 'mutasianggotaskgrid',
            title: 'Anggota SK',
            itemId: 'gridKiri',
            region: 'center',
            mutasi_sk_id: this.initialConfig.mutasi_sk_id,
            flex: 1,
            multiSelect: true,

            viewConfig: {
                plugins: {
                    ptype: 'gridviewdragdrop',
                    containerScroll: true,
                    dragGroup: 'dd-grid-to-grid-group1',
                    dropGroup: 'dd-grid-to-grid-group2'
                },
                listeners: {
                    drop: 'onDropGridAnggota'
                }
            },
        },{
            xtype: 'mutasiusulangrid',
            title: 'Usulan Mutasi',
            itemId: 'gridKanan',
            forceFit: false,
            region: 'east',
            split: true,
            collapsible: true,
            flex: 1,
            multiSelect: true,
            tbar: ["Daftar Guru/Tendik/Pegawai yang sudah diusulkan UPT/Sekolah"],
            sk: true,

            viewConfig: {
                plugins: {
                    ptype: 'gridviewdragdrop',
                    containerScroll: true,
                    dragGroup: 'dd-grid-to-grid-group2',
                    dropGroup: 'dd-grid-to-grid-group1',

                },

                listeners: {
                    drop: 'onDropGridUsulan'
                },

                // The right hand drop zone gets special styling
                // when dragging over it.
                dropZone: {
                    overClass: 'dd-over-gridview'
                }
            },
        }];

        this.callParent(arguments);
    },
});
/**
 * Created by Ahmad on 12/25/2016.
 */

Ext.define('SimpegGundul.view.mutasianggotask.MutasiAnggotaSkController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.mutasianggotask',

    onRenderGrid: function (grid) {
        var store = grid.getStore();

        store.on('beforeload', function (cmp) {
            Ext.apply(cmp.proxy.extraParams, {
                mutasi_sk_id: grid.mutasi_sk_id
            });
        });

        store.reload();
    },

    onDropGridAnggota: function (node, data, dropRec, dropPosition){
        var gridKiri = Ext.ComponentQuery.query('mutasianggotaskgrid[itemId=gridKiri]')[0];
        var gridKanan = Ext.ComponentQuery.query('mutasianggotaskgrid[itemId=gridKanan]')[0];

        var arr = new Array();
        data.records.forEach(element => {
            arr.push(element.data); 
        });

        if (Array.isArray(arr) !== true) {
            Ext.toast('Tidak ada yang diperbaharui', 'Info');
            return;
        } 

        gridKiri.up('window').mask('Tunggu sebentar...');
        Ext.Ajax.request({
            url: 'MutasiAnggotaSk/save',
            method: 'GET',
            params: {
                data: JSON.stringify(arr),
                mutasi_sk_id: gridKiri.mutasi_sk_id
            },
            success: function (response) {
                var text = response.responseText;

                var obj = Ext.JSON.decode(response.responseText);

                Ext.toast('Menyimpan data Anggota (Berhasil: ' + obj.berhasil + ', Gagal: ' + obj.gagal + ')', 'Berhasil');
                gridKiri.up('window').unmask();
                gridKiri.getStore().reload();
                gridKanan.getStore().reload();
            },
            failure: function(response){
                gridKiri.up('window').unmask();
            }
        });
    },

    
});
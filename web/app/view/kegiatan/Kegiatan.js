Ext.define('SimpegGundul.view.kegiatan.Kegiatan', {
    extend: 'Ext.panel.Panel',
    xtype: 'kegiatan',

    initComponent: function () {

        var me = this;

        me.items = {
            id: 'gridKegiatan',
            xtype: 'kegiatangrid',
            title: 'Daftar Kegiatan',
            iconCls: 'x-fa fa-calendar',
        };

        this.callParent();

    }
});

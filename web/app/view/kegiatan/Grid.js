Ext.define('SimpegGundul.view.kegiatan.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.kegiatangrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    controller: 'kegiatan',

    viewConfig: {
        listeners: {
            refresh: function (dataview) {
                Ext.each(dataview.panel.columns, function (column) {
                    // if (column.autoSizeColumn === true)
                    column.autoSize();
                })
            }
        }
    },

    listeners: {
        afterrender: 'onRenderGrid',
        beforeedit: 'onBeforeEdit'
    },

    createStore: function () {
        return Ext.create('SimpegGundul.store.Kegiatan');
    },
    createEditing: function () {
        return Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2,
            pluginId: 'editing'
        });
    },
    createDockedItems: function (grid) {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                itemId: 'btnTambah',
                text: 'Tambah',
                iconCls: 'x-fa fa-plus-circle',
                hidden: (['1', '2'].indexOf(user.level) < 0) ? false : true,
                handler: 'addRecord'
            }, {
                xtype: 'button',
                itemId: 'btnUbah',
                text: 'Ubah',
                iconCls: 'x-fa fa-pencil-square',
                hidden: (['1', '2'].indexOf(user.level) < 0) ? false : true,
                handler: 'editRecord'
            }, {
                xtype: 'button',
                text: 'Simpan',
                iconCls: 'x-fa fa-check fa-lg glyph-blue glyph-shadow',
                itemId: 'save',
                hidden: (['1', '2'].indexOf(user.level) < 0) ? false : true,
                handler: 'saveRecord'
            }, {
                xtype: 'button',
                text: 'Hapus',
                iconCls: 'x-fa fa-remove fa-lg glyph-red glyph-shadow',
                itemId: 'delete',
                hidden: (['1', '2'].indexOf(user.level) < 0) ? false : true,
                handler: 'deleteRecord'
            },'No. Surat',{
                xtype: "textfield",
                emptyText: 'No. Surat...',
                width: 200,
                itemId: 'txtCari',
                clearIcon: true,

                enableKeyEvents: true,
                listeners: {
                    keypress: function (field, e) {
                        if (e.getKey() == e.ENTER) {
                            grid.getStore().load();
                        }
                    }
                }
            },'->',{
                xtype: 'button',
                text: 'Peserta Kegiatan',
                iconCls: 'x-fa fa-users',
                itemId: 'peserta',
                handler: 'onPeserta'
            },{
                xtype: 'button',
                text: 'Cetak Sertifikat',
                iconCls: 'x-fa fa-newspaper-o',
                all: true,
                hidden: ((['1', '2'].indexOf(user.level) < 0)) ? false : true,
                handler: 'onCetakSertifikat'
            }]
        }];
    },
    createBbar: function () {
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    initComponent: function () {

        var grid = this;

        this.store = this.createStore();

        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        // You can instaniate the component with basic parameters that affects filtering of the store
        // For example:
        // {
        //     xtype: 'yourgrid',
        //     baseParams: {
        //         area_id: 120,
        //         status_id: 1
        //     }
        // }
        if (this.initialConfig.baseParams) {

            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function (store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });

        }

        // this.getSelectionModel().setSelectionMode('SINGLE');

        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            listeners: {
                cancelEdit: function (rowEditing, context) {
                    // Canceling editing of a locally added, unsaved record: remove it
                    if (context.record.phantom) {
                        grid.store.remove(context.record);
                    }
                }
            }
        });

        var editing = this.createEditing();

        this.rowEditing = editing;
        this.plugins = [editing];

        this.lookupStorePenandatangan = new SimpegGundul.store.Penandatangan({
            autoLoad: true
        });

        this.columns = [{
            header: 'Id',
            tooltip: 'Id',
            width: 144,
            sortable: true,
            dataIndex: 'kegiatan_id',
            hideable: false,
            hidden: true,
            field: {
                xtype: 'hidden'
                , maxLength: 36
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Nama',
            tooltip: 'Nama',
            width: 800,
            sortable: true,
            dataIndex: 'nama',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 200
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Tempat',
            tooltip: 'Tempat',
            width: 800,
            sortable: true,
            dataIndex: 'tempat',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 200
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Tgl Surat',
            tooltip: 'Tgl Surat',
            width: 80,
            sortable: true,
            dataIndex: 'tgl_surat',
            hideable: false,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            field: {
                xtype: 'datefield'
                , format: 'd/m/Y'
                , maxValue: new Date()
            }
        }, {
            header: 'No Surat',
            tooltip: 'No Surat',
            width: 200,
            sortable: true,
            dataIndex: 'no_surat',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 50
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Tgl Mulai',
            tooltip: 'Tgl Mulai',
            width: 80,
            sortable: true,
            dataIndex: 'tgl_mulai',
            hideable: false,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            field: {
                xtype: 'datefield'
                , format: 'd/m/Y'
                , maxValue: new Date()
            }
        }, {
            header: 'Tgl Selesai',
            tooltip: 'Tgl Selesai',
            width: 80,
            sortable: true,
            dataIndex: 'tgl_selesai',
            hideable: false,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            field: {
                xtype: 'datefield'
                , format: 'd/m/Y'
            }
        }, {
            header: 'Penandatangan',
            tooltip: 'Penandatangan',
            width: 144,
            sortable: true,
            dataIndex: 'penandatangan_id',
            hideable: false,
            renderer: function (v, p, r) {
                var record = this.lookupStorePenandatangan.getById(v);
                if (record) {
                    return record.get('nama');
                } else {
                    return v;
                }
            },
            field: {
                displayField: 'nama',
                xtype: 'penandatangancombo'
            }
        }, {
            header: 'Jumlah Peserta',
            tooltip: 'Jumlah Peserta Kegiatan',
            width: 80,
            sortable: true,
            dataIndex: 'max_peserta',
            hideable: false,
            field: {
                xtype: 'numberfield',
                minValue: 1,
            }
        }, {
            header: 'Soft Delete',
            tooltip: 'Soft Delete',
            width: 80,
            sortable: true,
            dataIndex: 'soft_delete',
            hideable: false,
            hidden: true,

        }];

        this.dockedItems = this.createDockedItems(grid);

        this.bbar = this.createBbar();

        this.callParent(arguments);
    }
});
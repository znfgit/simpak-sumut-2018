Ext.define('SimpegGundul.view._components.combo.Peran', {
    extend: 'Ext.form.field.ComboBox',
    // queryMode: 'local',
    alias: 'widget.perancombo',
    valueField: 'peran_id',
    displayField: 'nama',
    label: 'Peran',
    // editable: false,
    initComponent: function () {
        this.store = Ext.create('SimpegGundul.store.Peran', {
            model: 'SimpegGundul.model.Peran',
            sorters: ['peran_id'],
            autoLoad: true
        });
        this.callParent(arguments);
    }
});
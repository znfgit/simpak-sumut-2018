Ext.define('SimpegGundul.view._components.combo.MstWilayah', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'remote',
    alias: 'widget.mstwilayahcombo',
    valueField: 'kode_wilayah',
    displayField: 'nama',
    label: 'Kode Wilayah',
    // editable: false,
    initComponent: function () {
        this.store = Ext.create('SimpegGundul.store.MstWilayah', {
            model: 'SimpegGundul.model.MstWilayah',
            sorters: ['kode_wilayah'],
            autoLoad: true
        });
        this.callParent(arguments);
    }
});
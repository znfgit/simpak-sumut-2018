Ext.define('SimpegGundul.view._components.combo.Penandatangan', {
    extend: 'Ext.form.field.ComboBox',
    // queryMode: 'local',
    alias: 'widget.penandatangancombo',
    valueField: 'penandatangan_id',
    displayField: 'nama',
    label: 'Penandatangan',
    // editable: false,
    initComponent: function () {
        this.store = Ext.create('SimpegGundul.store.Penandatangan', {
            model: 'SimpegGundul.model.Penandatangan',
            sorters: ['penandatangan_id'],
            autoLoad: true
        });
        this.callParent(arguments);
    }
});
Ext.define('SimpegGundul.view._components.combo.Sekolah', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'remote',
    pageSize: 20,
    valueField: 'sekolah_id',
    displayField: 'nama',
    label: 'Sekolah',    
    alias: 'widget.sekolahcombo',
    //hideTrigger: false,
    listeners: {
        beforerender: function(combo, options) {
            this.store.load();
        }
    },
    initComponent: function() {
        this.store = Ext.create('SimpegGundul.store.Sekolah', {
            model: 'SimpegGundul.model.Sekolah',
            sorters: ['sekolah_id']
        });
        this.callParent(arguments); 
    }
});
Ext.define('SimpegGundul.view._components.combo.JenisPtk', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'local',
    pageSize: 20,
    valueField: 'jenis_ptk_id',
    displayField: 'jenis_ptk',
    label: 'Jenis ptk',    
    alias: 'widget.jenisptkcombo',
    //hideTrigger: false,
    listeners: {
        beforerender: function(combo, options) {
            this.store.load();
        }
    },
    initComponent: function() {
        this.store = Ext.create('SimpegGundul.store.JenisPtk', {
            model: 'SimpegGundul.model.JenisPtk',
            sorters: ['jenis_ptk_id']
        });
        this.callParent(arguments); 
    }
});
Ext.define('SimpegGundul.view._components.combo.PeriodePenilaian', {
    extend: 'Ext.form.field.ComboBox',
    // queryMode: 'local',
    alias: 'widget.periodepenilaiancombo',
    valueField: 'periode_penilaian_id',
    displayField: 'nama',
    label: 'Periode penilaian',
    // editable: false,
    initComponent: function () {
        this.store = Ext.create('SimpegGundul.store.PeriodePenilaian', {
            model: 'SimpegGundul.model.PeriodePenilaian',
            sorters: ['periode_penilaian_id'],
            autoLoad: true
        });
        this.callParent(arguments);
    }
});
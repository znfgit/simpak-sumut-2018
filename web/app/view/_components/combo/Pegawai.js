Ext.define('SimpegGundul.view._components.combo.Pegawai', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'remote',
    pageSize: 20,
    valueField: 'pegawai_id',
    displayField: 'nama',
    label: 'Pegawai',
    alias: 'widget.pegawaicombo',
    //hideTrigger: false,
    tpl: Ext.create('Ext.XTemplate',
        '<ul class="x-list-plain"><tpl for=".">',
        '<li role="option" class="x-boundlist-item"><b>- {nama}</b><br/>NIP: {nip}</li>',
        '</tpl></ul>'
    ),
    // template for the content inside text field
    displayTpl: Ext.create('Ext.XTemplate',
        '<tpl for=".">',
        '{nama}',
        '</tpl>'
    ),
    listeners: {
        beforerender: function (combo, options) {
            this.store.load();
        }
    },
    initComponent: function () {
        this.store = Ext.create('SimpegGundul.store.Pegawai', {
            model: 'SimpegGundul.model.Pegawai',
            sorters: ['pegawai_id']
        });
        this.callParent(arguments);
    }
});
Ext.define('SimpegGundul.view.ptk.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ptkgrid',
    controller: 'ptk',

    selType: 'rowmodel',
    autoScroll: true,
    columnLines: true,

    listeners: {
        afterrender: 'onRender'
    },

    viewConfig: {
        listeners: {
            refresh: function (dataview) {
                Ext.each(dataview.panel.columns, function (column) {
                    // if (column.autoSizeColumn === true)
                    column.autoSize();
                })
            }
        }
    },

    createStore: function () {
        return Ext.create('SimpegGundul.store.Ptk');
    },
    createDockedItems: function (grid) {
        var jenisSekolah = Ext.create('Ext.data.Store', {
            data: [
                { "bentuk_pendidikan_id": null, "nama": "-- Semua --" },
                { "bentuk_pendidikan_id": 7, "nama": "SDLB" },
                { "bentuk_pendidikan_id": 8, "nama": "SMPLB" },
                { "bentuk_pendidikan_id": 13, "nama": "SMA" },
                { "bentuk_pendidikan_id": 14, "nama": "SMLB" },
                { "bentuk_pendidikan_id": 15, "nama": "SMK" },
                { "bentuk_pendidikan_id": 29, "nama": "SLB" },
            ]
        });

        switch(grid.jenis){
            case 1:
                var jenisPtk = Ext.create('Ext.data.Store', {
                    data: [
                        { "jenis_ptk_id": null, "jenis_ptk": "-- Semua --" },
                        { "jenis_ptk_id": 3, "jenis_ptk": "Guru Kelas" },
                        { "jenis_ptk_id": 4, "jenis_ptk": "Guru Mapel" },
                        { "jenis_ptk_id": 5, "jenis_ptk": "Guru BK" },
                        { "jenis_ptk_id": 6, "jenis_ptk": "Guru Inklusi" },
                        { "jenis_ptk_id": 14, "jenis_ptk": "Guru TIK" },
                    ]
                });

                break;
            case 2:
                var jenisPtk = Ext.create('Ext.data.Store', {
                    data: [
                        { "jenis_ptk_id": null, "jenis_ptk": "-- Semua --" },
                        { "jenis_ptk_id": 11, "jenis_ptk": "Tenaga Administrasi Sekolah" },
                        { "jenis_ptk_id": 30, "jenis_ptk": "Laboran" },
                        { "jenis_ptk_id": 40, "jenis_ptk": "Tenaga Perpustakaan" },
                        { "jenis_ptk_id": 41, "jenis_ptk": "Tukang Kebun" },
                        { "jenis_ptk_id": 42, "jenis_ptk": "Penjaga Sekolah" },
                        { "jenis_ptk_id": 43, "jenis_ptk": "Petugas Keamanan" },
                        { "jenis_ptk_id": 44, "jenis_ptk": "Pesuruh/Office Boy" },
                    ]
                });
                break;
            case 3:
                var jenisPtk = Ext.create('Ext.data.Store', {
                    data: [
                        { "jenis_ptk_id": null, "jenis_ptk": "-- Semua --" },
                        { "jenis_ptk_id": 7, "jenis_ptk": "Pengawas Satuan Pendidikan" },
                        { "jenis_ptk_id": 8, "jenis_ptk": "Pengawas PLB" },
                        { "jenis_ptk_id": 9, "jenis_ptk": "Pengawas Matpel/Rumpun" },
                        { "jenis_ptk_id": 10, "jenis_ptk": "Pengawas Bidang" },
                        { "jenis_ptk_id": 25, "jenis_ptk": "Pengawas BK" },
                        { "jenis_ptk_id": 26, "jenis_ptk": "Pengawas SD" },
                    ]
                });
                break;
        }

        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'combo',
                store: jenisSekolah,
                emptyText: 'Jenis Sekolah...',
                queryMode: 'local',
                displayField: 'nama',
                valueField: 'bentuk_pendidikan_id',
                editable: false,
                itemId: 'bentuk_pendidikan',
                listeners: {
                    select: function (combo, newValue) {
                        grid.getStore().load();
                    }
                }
            }, {
                xtype: 'combo',
                store: jenisPtk,
                emptyText: 'Jenis PTK...',
                queryMode: 'local',
                displayField: 'jenis_ptk',
                valueField: 'jenis_ptk_id',
                // editable: false,
                itemId: 'jenis_ptk',
                listeners: {
                    select: function (combo, newValue) {
                        grid.getStore().load();
                    }
                }
            }, {
                xtype: 'combo',
                store: Ext.create('Ext.data.Store', {
                    data: grid.statusKepegawaian
                }),
                emptyText: 'Status Kepegawaian...',
                queryMode: 'local',
                displayField: 'nama',
                valueField: 'status_kepegawaian_id',
                // editable: false,
                itemId: 'status_kepegawaian',
                listeners: {
                    select: function (combo, newValue) {
                        grid.getStore().load();
                    }
                }
            }, {
                xtype: "textfield",
                emptyText: 'Nama/Nuptk/NIP...',
                width: 200,
                itemId: 'txtCariPtk',
                clearIcon: true,

                enableKeyEvents: true,
                listeners: {
                    keypress: function (field, e) {
                        if (e.getKey() == e.ENTER) {
                            grid.getStore().load();
                        }
                    }
                }
            }, '->', {
                itemId: 'btnValidasiBkn',
                iconCls: 'x-fa fa-check-circle',
                text: 'Validasi BKN',
                hidden: (grid.type == 'gurunonpns') ? true : false,
                listeners: {
                    click: 'getBkn'
                }
            },{
                itemId: 'btnPtkDetail',
                iconCls: 'x-fa fa-list-alt',
                text: 'PTK Detail',
                listeners: {
                    click: 'getDetail'
                }
            }, {
                itemId: 'btndwnloadexcel',
                iconCls: 'x-fa fa-file-excel-o',
                text: 'Download Excel',
                listeners: {
                    click: 'getExcel'
                }
            }]
        }];
    },
    createBbar: function () {
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    initComponent: function () {

        var grid = this;

        this.store = this.createStore();

        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        // You can instaniate the component with basic parameters that affects filtering of the store
        // For example:
        // {
        //     xtype: 'yourgrid',
        //     baseParams: {
        //         area_id: 120,
        //         status_id: 1
        //     }
        // }
        if (this.initialConfig.baseParams) {

            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function (store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });

        }

        // this.getSelectionModel().setSelectionMode('SINGLE');

        this.lookupStorePangkatGolongan = new SimpegGundul.store.PangkatGolongan({
            autoLoad: true
        });

        var lookupStoreStatusDokumen = new SimpegGundul.store.StatusDokumen();
        var lookupStoreStatusDokumenInpassing = new SimpegGundul.store.StatusDokumenInpassing();

        this.columns = [{
            header: 'Nama',
            tooltip: 'Nama',
            width: 400,
            sortable: true,
            dataIndex: 'nama',
            hideable: false,
        }, {
            header: 'Pangkat Golongan',
            tooltip: 'Pangkat Golongan',
            width: 80,
            sortable: true,
            dataIndex: 'pangkat_golongan_id',
            hideable: false,
            renderer: function (v, p, r) {
                var record = this.lookupStorePangkatGolongan.getById(v);
                if (record) {
                    return record.get('nama');
                } else {
                    return v;
                }
            }
        }, {
            header: 'NIP',
            tooltip: 'NIP',
            width: 120,
            sortable: true,
            dataIndex: 'nip',
            hideable: false,
            hidden: (grid.type == 'gurunonpns') ? true : false
        }, {
            header: 'NUPTK',
            tooltip: 'NUPTK',
            width: 80,
            sortable: true,
            dataIndex: 'nuptk',
            hideable: false,
        }, {
            header: 'NIK',
            tooltip: 'NIK',
            width: 120,
            sortable: true,
            dataIndex: 'nik',
            hideable: false,
        }, {
            header: 'Jenis Kelamin',
            tooltip: 'Jenis Kelamin',
            width: 80,
            sortable: true,
            dataIndex: 'jenis_kelamin',
            hideable: false,
            renderer: function (v, p, r) {
                switch (v) {
                    case 1 : return 'Laki - laki'; break;
                    case 2 : return 'Perempuan'; break;
                    default: return '-'; break;
                }
            }
        }, {
            header: 'Tempat Lahir',
            tooltip: 'Tempat Lahir',
            width: 400,
            sortable: true,
            dataIndex: 'tempat_lahir',
            hideable: false,
        }, {
            header: 'Tgl Lahir',
            tooltip: 'Tgl Lahir',
            width: 80,
            sortable: true,
            dataIndex: 'tgl_lahir',
            hideable: false,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
        }, {
            header: 'Nama Ibu Kandung',
            tooltip: 'Nama Ibu Kandung',
            width: 400,
            sortable: true,
            dataIndex: 'nama_ibu_kandung',
            hideable: false,
        }, {
            header: (grid.type == 'tendik') ? 'Jenis Tendik' : 'Jenis Guru',
            // tooltip: 'Jenis Guru',
            width: 128,
            sortable: true,
            dataIndex: 'Jenis_Guru',
            hideable: false,
            hidden: (grid.type == 'pengawas') ? true : false
        }, {
            header: 'Jenis Pengawas',
            tooltip: 'Jenis Pengawas',
            width: 1020,
            sortable: true,
            dataIndex: 'Jenis_Pengawas',
            hideable: false,
            hidden: (grid.type == 'pengawas') ? false : true
        }, {
            header: 'Mapel Diawasi',
            tooltip: 'Mapel Diawasi',
            width: 1020,
            sortable: true,
            dataIndex: 'mapel_diawasi',
            hideable: false,
            hidden: (grid.type == 'pengawas') ? false : true
        }, {
            header: 'Jenjang Kepengawasan',
            tooltip: 'Jenjang Kepengawasan',
            width: 80,
            sortable: true,
            dataIndex: 'jenjang_kepengawasan_id',
            hideable: false,
            hidden: (grid.type == 'pengawas') ? false : true
        }, {
            header: 'Jumlah Guru Diawasi',
            tooltip: 'Jumlah Guru Diawasi',
            width: 80,
            sortable: true,
            dataIndex: 'jumlah_guru_diawasi_dk',
            hideable: false,
            hidden: (grid.type == 'pengawas') ? false : true
        }, {
            header: 'Jumlah Sekolah Diawasi',
            tooltip: 'Jumlah Sekolah Diawasi',
            width: 80,
            sortable: true,
            dataIndex: 'jumlah_sekolah_diawasi_dk',
            hideable: false,
            hidden: (grid.type == 'pengawas') ? false : true
        }, {
            header: 'Nama Status Kepegawaian',
            tooltip: 'Nama Status Kepegawaian',
            width: 240,
            sortable: true,
            dataIndex: 'nama_status_kepegawaian',
            hideable: false,
        }, {
            header: 'Nama Sekolah',
            tooltip: 'Nama Sekolah',
            width: 1020,
            sortable: true,
            dataIndex: 'nama_sekolah',
            hideable: false,
            hidden: (grid.type == 'pengawas') ? true : false
        }, {
            header: 'Lulus Sertifikasi',
            tooltip: 'Lulus Sertifikasi',
            width: 80,
            sortable: true,
            dataIndex: 'lulus_sertifikasi',
            hideable: false,
            renderer: function (v, p, r) {
                switch (v) {
                    case 1 : return 'Ya'; break;
                    default: return 'Belum'; break;
                }
            }
        }, {
            header: 'Status Dokumen SKTP',
            tooltip: 'Status Dokumen Sktp',
            width: 80,
            sortable: true,
            dataIndex: 'Status_dokumen_SKTP',
            hideable: false,
            hidden: (grid.type == 'tendik') ? true : false,
            renderer: function(v, p, r) {
                var record = lookupStoreStatusDokumen.getById(v);
                if (record) {
                    return record.get('nama');
                } else {
                    return '-';
                }
            }
        }, {
            header: 'Status Dokumen Inpassing',
            tooltip: 'Status Dokumen Inpassing',
            width: 80,
            sortable: true,
            dataIndex: 'status_dokumen_inpassing',
            hideable: false,
            hidden: (grid.type == 'gurunonpns') ? false : true,
            renderer: function(v, p, r) {
                var record = lookupStoreStatusDokumenInpassing.getById(v);
                if (record) {
                    return record.get('nama');
                } else {
                    return '-';
                }
            }
        }, {
            header: 'Tgl Pensiun',
            tooltip: 'Tgl Pensiun',
            width: 80,
            sortable: true,
            dataIndex: 'tgl_pensiun',
            hideable: false,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
        }];

        this.dockedItems = this.createDockedItems(grid);

        this.bbar = this.createBbar();

        this.callParent(arguments);
    }
});
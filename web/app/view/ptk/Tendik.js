Ext.define('SimpegGundul.view.ptk.Tendik', {
    extend: 'Ext.panel.Panel',
    xtype: 'tendik',

    initComponent: function () {

        var me = this;

        me.items = {
            xtype: 'ptkgrid',
            type: 'tendik',
            iconCls: 'x-fa fa-users',
            title: 'Tendik (<i>Update Data per ' + user.lastUpdate + '</i>)',
            jenis: 2,
            statusKepegawaian: [
                { "status_kepegawaian_id": null, "nama": "-- Semua --" },
                { "status_kepegawaian_id": 1, "nama": "PNS" },
                { "status_kepegawaian_id": 2, "nama": "PNS Diperbantukan" },
                { "status_kepegawaian_id": 3, "nama": "PNS Kemenag" },
                { "status_kepegawaian_id": 4, "nama": "GTY/PTY" },
                { "status_kepegawaian_id": 5, "nama": "GTT/PTT Provinsi" },
                { "status_kepegawaian_id": 6, "nama": "GTT/PTT Kab/Kota" },
                { "status_kepegawaian_id": 7, "nama": "Guru Bantu Pusat" },
                { "status_kepegawaian_id": 8, "nama": "Guru Honor Sekolah" },
                { "status_kepegawaian_id": 10, "nama": "CPNS" },
                { "status_kepegawaian_id": 9, "nama": "Tenaga Honor Sekolah" },
                { "status_kepegawaian_id": 51, "nama": "Kontrak Kerja WNA" },
            ]
        };

        this.callParent();

    }
});

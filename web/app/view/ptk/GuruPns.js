Ext.define('SimpegGundul.view.ptk.GuruPns', {
    extend: 'Ext.panel.Panel',
    xtype: 'gurupns',

    initComponent: function () {

        var me = this;

        me.items = {
            xtype: 'ptkgrid',
            type: 'gurupns',
            iconCls: 'x-fa fa-users',
            title: 'Guru PNS (<i>Update Data per ' + user.lastUpdate + '</i>)',
            jenis: 1,
            statusKepegawaian: [
                { "status_kepegawaian_id": null, "nama": "-- Semua --" },
                { "status_kepegawaian_id": 1, "nama": "PNS" },
                { "status_kepegawaian_id": 2, "nama": "PNS Diperbantukan" },
                { "status_kepegawaian_id": 3, "nama": "PNS Kemenag" },
                { "status_kepegawaian_id": 10, "nama": "CPNS" },
            ]
        };

        this.callParent();

    }
});

Ext.define('SimpegGundul.view.ptk.Pengawas', {
    extend: 'Ext.panel.Panel',
    xtype: 'pengawas',

    initComponent: function () {

        var me = this;

        me.items = {
            xtype: 'ptkgrid',
            type: 'pengawas',
            iconCls: 'x-fa fa-users',
            title: 'Pengawas (<i>Update Data per ' + user.lastUpdate + '</i>)',
            jenis: 3,
            statusKepegawaian: [
                { "status_kepegawaian_id": null, "nama": "-- Semua --" },
                { "status_kepegawaian_id": 1, "nama": "PNS" },
                { "status_kepegawaian_id": 2, "nama": "PNS Diperbantukan" },
                { "status_kepegawaian_id": 3, "nama": "PNS Kemenag" },
                { "status_kepegawaian_id": 10, "nama": "CPNS" },
            ]
        };

        this.callParent();

    }
});

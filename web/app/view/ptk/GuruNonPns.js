Ext.define('SimpegGundul.view.ptk.GuruNonPns', {
    extend: 'Ext.panel.Panel',
    xtype: 'gurunonpns',

    initComponent: function () {

        var me = this;

        me.items = {
            xtype: 'ptkgrid',
            type: 'gurunonpns',
            iconCls: 'x-fa fa-users',
            title: 'Guru Non PNS (<i>Update Data per ' + user.lastUpdate + '</i>)',
            jenis: 1,
            statusKepegawaian: [
                { "status_kepegawaian_id": null, "nama": "-- Semua --" },
                { "status_kepegawaian_id": 4, "nama": "GTY/PTY" },
                { "status_kepegawaian_id": 5, "nama": "GTT/PTT Provinsi" },
                { "status_kepegawaian_id": 6, "nama": "GTT/PTT Kab/Kota" },
                { "status_kepegawaian_id": 7, "nama": "Guru Bantu Pusat" },
                { "status_kepegawaian_id": 8, "nama": "Guru Honor Sekolah" },
                { "status_kepegawaian_id": 9, "nama": "Tenaga Honor Sekolah" },
                { "status_kepegawaian_id": 51, "nama": "Kontrak Kerja WNA" },
            ]
        };

        this.callParent();

    }
});

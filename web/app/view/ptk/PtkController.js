/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('SimpegGundul.view.ptk.PtkController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.ptk',

    onRender: function(grid){
        grid.down('toolbar[dock=bottom]').add(0, {
            xtype: 'label',
            text: 'Size per Page : '
        }, {
            xtype: 'combo',
            store: Ext.create('Ext.data.Store', {
                data: [
                    { 'size': 10 },
                    { 'size': 20 },
                    { 'size': 50 },
                    { 'size': 100 }
                ]
            }),
            width: 75,
            emptyText: 'Record per page..',
            queryMode: 'local',
            displayField: 'size',
            valueField: 'size',
            editable: false,
            itemId: 'page_size',
            value: 50,
            listeners: {
                change: function (combo, newValue) {
                    grid.getStore().reload();
                }
            }
        },'-');

        // grid.down('toolbar[dock=bottom]').add('-', '<b>Update data per 10/01/2017</b>');

        grid.getStore().on('beforeload', function (store){
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: grid.down('combo[itemId=bentuk_pendidikan]').getValue(),
                jenis_ptk_id: grid.down('combo[itemId=jenis_ptk]').getValue(),
                status_kepegawaian_id: grid.down('combo[itemId=status_kepegawaian]').getValue(),
                txtCari: grid.down('textfield[itemId=txtCariPtk]').getValue(),
                limit: grid.down('combo[itemId=page_size]').getValue(),
                jenis: grid.up('panel').xtype
            });

            if (grid.kegiatan_id) {
                grid.down('toolbar').hide();
                Ext.apply(store.proxy.extraParams, {
                    kegiatan_id: grid.kegiatan_id
                });
            }
        });

        grid.getStore().load();
    },

    getExcel: function(btn){
        var grid = btn.up('panel');
        var title = btn.up('gridpanel').title;
        var url = window.location.href;
        
        var query = 'title=' + title;

        if (grid.type == 'tendik') {
            query += '&jenis_ptk_id=[11,30,40,41,42,43,44,99]';
        } else if (grid.type == 'gurupns') {
            query += '&jenis_ptk_id=[3,4,5,6,12,13,14]&is_pns=1';
        } else if (grid.type == 'gurunonpns') {
            query += '&jenis_ptk_id=[3,4,5,6,12,13,14]&is_pns=0';
        } else if (grid.type == 'pengawas') {
            query += '&jenis_ptk_id=[7,8,9,10,25,26]';
        } else {
            query += '&jenis_ptk_id=[7,8,9,10,11,20,30,40,99,3,4,5,6,12,13,14]';
        }

        switch (user.level) {
            case '1':
                query += '&sekolah_id=' + user.sekolahId;
                break;
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
                query += '&kabupaten_kota_sekolah_id=' + user.kodeWilayah;
                break;
            default:
                break;
        }

        var fileUrl = 'Ptk/getExcel?' + query;
        // window.location.assign(fileUrl);
        window.open(fileUrl);
    },

    getDetail: function(btn){
        var selected = this.getSelected(btn);

        
        if (selected) {
            var selections = btn.up('grid').getSelectionModel().getSelection();
            var r = selections[0];

            Ext.create('Ext.window.Window', {
                title: 'PTK Detail',
                maximizable: true,
                height: 540,
                width: 811,
                layout: 'fit',
                modal: true,
                items: [{
                    xtype: 'tabpanel',
                    items: [{
                        xtype: 'detailpanel',
                        title: 'Dapodik',
                        autoScroll: true,
                        record: r
                    }, {
                        xtype: 'tprofesipanel',
                        title: 'Tunjangan Profesi',
                        autoScroll: true,
                        record: r,
                    }, {
                        xtype: 'inpassingpanel',
                        title: 'Inpassing',
                        autoScroll: true,
                        record: r,
                        hidden: (ptk_selected.is_pns == 1) ? true : false
                    }]
                }]
            }).show();
        }
    },

    getBkn: function(btn){
        var selected = this.getSelected(btn);

        if (selected) {
            Ext.create('SimpegGundul.view.validasi.Window').show();
        }
    },

    getSelected: function (cmp) {
        var grid = cmp.up('grid');

        var selections = grid.getSelectionModel().getSelection();
        var r = selections[0];
        if (!r) {
            Ext.toast('Mohon pilih salah satu PTK!');

            return false;
        } else {
            ptk_selected = r.data;

            return true;
        }
    },

    onDropGridPtk: function (node, data, dropRec, dropPosition) {
        var gridKiri = Ext.ComponentQuery.query('anggotakegiatangrid[itemId=gridKiri]')[0];
        var gridKanan1 = Ext.ComponentQuery.query('ptkgrid[itemId=gridKanan1]')[0];
        var gridKanan2 = Ext.ComponentQuery.query('pegawaigrid[itemId=gridKanan2]')[0];

        var arr = new Array();
        data.records.forEach(element => {
            arr.push(element.data);
        });

        if (Array.isArray(arr) !== true) {
            Ext.toast('Tidak ada yang diperbaharui', 'Info');
            return;
        }

        gridKiri.up('window').mask('Tunggu sebentar...');
        Ext.Ajax.request({
            url: 'AnggotaKegiatan/delete',
            method: 'GET',
            params: {
                data: JSON.stringify(arr),
                kegiatan_id: gridKiri.kegiatan_id
            },
            success: function (response) {
                var text = response.responseText;

                var obj = Ext.JSON.decode(response.responseText);

                Ext.toast('Menghapus data Anggota (Berhasil: ' + obj.berhasil + ', Gagal: ' + obj.gagal + ')', 'Berhasil');
                gridKiri.up('window').unmask();
                gridKiri.getStore().reload();
                gridKanan1.getStore().reload();
                gridKanan2.getStore().reload();
            },
            failure: function (response) {
                gridKiri.up('window').unmask();
            }
        });
    }
});

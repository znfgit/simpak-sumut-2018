/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('SimpegGundul.view.mutasisk.MutasiSkController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.mutasisk',

    onRenderGrid: function (grid) {
        var store = grid.getStore();

        // store.on('beforeload', function (cmp) {
        //     Ext.apply(cmp.proxy.extraParams, {
                
        //     });
        // });

        store.reload();
    },

    addRecord: function (btn) {
        // Dari button, 'look up'/lihat ke atas, cari container dengan xtype = 'form'
        var grid = btn.up('gridpanel');
        grid.rowEditing.cancelEdit();

        // Create a model instance
        var r = this.getNewRecord(grid);

        grid.store.insert(0, r);
        grid.rowEditing.startEdit(0, 0);
    },

    editRecord: function (btn) {
        // Defaults using row editor. Override this to use other method, such as form etc        
        var grid = btn.up('gridpanel');
        grid.rowEditing.cancelEdit();

        //var selections = grid.getSelectionModel().getSelection();
        var selections = grid.getSelection();
        var r = selections[0];
        if (!r) {
            Ext.toast('Mohon pilih salah satu baris', 'Error');
            return;
        }
        var startEditingColumnNumber = 0;
        for (var i = 0; i < grid.columns.length; i++) {
            if (grid.columns[i].isVisible()) {
                var startEditingColumnNumber = i;
                break;
            }
        }
        grid.rowEditing.startEdit(r, startEditingColumnNumber);
    },

    saveRecord: function (btn) {
        var grid = btn.up('gridpanel');

        var stores = grid.store;
        var arr = new Array();

        stores.each(function (record) {

            if (record.dirty) {
                // console.log(record);
                var arrRec = { mutasi_sk_id: record.data.mutasi_sk_id };

                for (var mod in record.modified) {
                    arrRec[mod] = record.data[mod];
                }

                arr.push(arrRec);
            }

        });

        console.log(Array.isArray(arr));

        if (Array.isArray(arr) !== true) {
            Ext.toast('Tidak ada yang diperbaharui', 'Info');
            return;
        }

        // console.log(JSON.stringify(arr));
        grid.mask('Tunggu sebentar...');
        Ext.Ajax.request({
            url: 'MutasiSk/save',
            method: 'GET',
            params: {
                data: JSON.stringify(arr)
            },
            success: function (response) {
                var text = response.responseText;

                var obj = Ext.JSON.decode(response.responseText);

                Ext.toast('Menyimpan data Mutasi Sk (Berhasil: ' + obj.berhasil + ', Gagal: ' + obj.gagal + ')', 'Berhasil');
                grid.unmask();
                stores.reload();
            }
        });
    },

    deleteRecord: function (btn) {
        // Defaults using row editor. Override this to use other method, such as form etc        
        var grid = btn.up('gridpanel');
        grid.rowEditing.cancelEdit();

        //var selections = grid.getSelectionModel().getSelection();
        var selections = grid.getSelection();
        var r = selections[0];
        if (!r) {
            Ext.toast('Mohon pilih salah satu baris', 'Error');
            return;
        }

        Ext.Msg.show({
            title: 'Konfirmasi',
            msg: 'Anda yakin? ',
            width: 300,
            closable: false,
            buttons: Ext.Msg.YESNO,
            multiline: false,
            fn: function (buttonValue, inputText, showConfig) {
                if (buttonValue === "yes") {
                    grid.mask('Tunggu sebentar...');

                    Ext.Ajax.request({
                        url: 'MutasiSk/delete',
                        method: 'GET',
                        params: {
                            id: r.data.mutasi_sk_id
                        },
                        success: function (response) {
                            var text = response.responseText;

                            var obj = Ext.JSON.decode(response.responseText);

                            Ext.toast(obj.msg, 'Info');
                            grid.unmask();
                            grid.getStore().reload();
                        },
                        failure: function () {
                            Ext.toast('Terjadi Kesalahan, hubungi admin');
                        }
                    });
                }
            },
            icon: Ext.Msg.QUESTION
        });
    },

    getNewRecord: function (grid) {
        var recordConfig = {
            mutasi_sk_id: '',
            no_sk: '',
            tgl_sk: Ext.Date.clearTime(new Date()),
            penandatangan_id: '',
            soft_delete: 0
        };
        Ext.apply(recordConfig, grid.newRecordCfg);
        var r = new SimpegGundul.model.MutasiSk(recordConfig);
        return r;
    },

    listPtk: function(btn){
        var grid = btn.up('gridpanel');

        var selections = grid.getSelection();
        var r = selections[0];
        if (!r) {
            Ext.toast('Mohon pilih salah satu baris', 'Error');
            return;
        }

        Ext.create('SimpegGundul.view.mutasianggotask.Window',{
            mutasi_sk_id: r.data.mutasi_sk_id
        }).show();
    },

    onPrintSuratPengantar: function(btn){
        var grid = btn.up('grid');
        var sel = grid.getSelection();
        var rec = sel[0];
        if (sel.length <= 0) {
            Ext.toast('Mohon pilih salah satu PTK terlebih dahulu', 'Error');
            return;
        }

        var printUrl = 'MutasiSk/getCetakSuratPengantar?id=' + rec.data.mutasi_sk_id;

        var win = Ext.widget({
            xtype: 'window_print',
            title: 'Cetak Surat Pengantar Mutasi',
            src: printUrl
        });

        win.show();
    },
    
    onLampiran: function(btn){
        var grid = btn.up('grid');
        var sel = grid.getSelection();
        var rec = sel[0];
        if (sel.length <= 0) {
            Ext.toast('Mohon pilih salah satu PTK terlebih dahulu', 'Error');
            return;
        }

        var printUrl = 'MutasiSk/getCetakLampiran?id=' + rec.data.mutasi_sk_id;

        var win = Ext.widget({
            xtype: 'window_print',
            title: 'Cetak Lampiran',
            src: printUrl
        });

        win.show();
    }
});

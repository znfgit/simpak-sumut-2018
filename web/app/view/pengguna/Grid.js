Ext.define('SimpegGundul.view.pengguna.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.penggunagrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    columnLines: true,
    controller: 'pengguna',
    autoLoad: true,

    listeners: {
        afterrender: 'onRenderGrid',
        beforeedit: 'onBeforeEdit'
    },

    viewConfig: {
        listeners: {
            refresh: function (dataview) {
                Ext.each(dataview.panel.columns, function (column) {
                    // if (column.autoSizeColumn === true)
                    column.autoSize();
                })
            }
        }
    },

    createStore: function () {
        return Ext.create('SimpegGundul.store.Pengguna');
    },

    createEditing: function () {
        return Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2,
            pluginId: 'editing'
        });
    },

    createDockedItems: function (grid) {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                itemId: 'btnTambah',
                text: 'Tambah',
                iconCls: 'x-fa fa-plus-circle',
                handler: 'addRecord'
            }, {
                xtype: 'button',
                itemId: 'btnUbah',
                text: 'Ubah',
                iconCls: 'x-fa fa-pencil-square',
                handler: 'editRecord'
            }, {
                xtype: 'button',
                text: 'Simpan',
                iconCls: 'x-fa fa-check fa-lg glyph-blue glyph-shadow',
                itemId: 'save',
                handler: 'saveRecord'
            }, {
                xtype: 'button',
                text: 'Hapus',
                iconCls: 'x-fa fa-remove fa-lg glyph-red glyph-shadow',
                itemId: 'delete',
                handler: 'deleteRecord'
            },'Cari: ',{
                xtype: "textfield",
                emptyText: 'Username..',
                width: 200,
                itemId: 'txtCari',
                clearIcon: true,

                enableKeyEvents: true,
                listeners: {
                    keypress: function (field, e) {
                        if (e.getKey() == e.ENTER) {
                            grid.getStore().load();
                        }
                    }
                }
            },'->',{
                type: 'button',
                text: 'Reset Password',
                iconCls: 'x-fa fa-lock',
                itemId: 'reset',
                handler: 'resetPassword'
            }]
        }];
    },

    createBbar: function () {
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    initComponent: function () {

        var grid = this;

        this.store = this.createStore();

        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        // You can instaniate the component with basic parameters that affects filtering of the store
        // For example:
        // {
        //     xtype: 'yourgrid',
        //     baseParams: {
        //         area_id: 120,
        //         status_id: 1
        //     }
        // }
        if (this.initialConfig.baseParams) {

            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function (store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });

        }

        // this.getSelectionModel().setSelectionMode('SINGLE');

        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            listeners: {
                cancelEdit: function (rowEditing, context) {
                    // Canceling editing of a locally added, unsaved record: remove it
                    if (context.record.phantom) {
                        grid.store.remove(context.record);
                    }
                }
            }
        });

        var editing = this.createEditing();

        this.rowEditing = editing;
        this.plugins = [editing];

        this.columns = [{
            header: 'Id',
            tooltip: 'Id',
            width: 144,
            sortable: true,
            dataIndex: 'pengguna_id',
            hideable: false,
            hidden: true,
            field: {
                xtype: 'hidden'
                , maxLength: 36
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Username',
            tooltip: 'Username',
            width: 240,
            align: 'left',
            sortable: true,
            dataIndex: 'username',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 60
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Password',
            tooltip: 'Password',
            width: 200,
            sortable: true,
            dataIndex: 'password',
            hidden: true,
            field: {
                xtype: 'textfield'
                , maxLength: 50
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Nama',
            tooltip: 'Nama',
            width: 400,
            align: 'left',
            sortable: true,
            dataIndex: 'nama',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 100
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Peran',
            tooltip: 'Peran',
            width: 80,
            sortable: true,
            dataIndex: 'peran_id',
            hideable: false,
            renderer: function (v, p, r) {
                return r.data.peran_id_str;
            },
            field: {
                xtype: 'perancombo',
                listeners: {
                    change: function (combo, newValue, oldValue, eOpts) {
                        var grid = combo.up('gridpanel');
                        var selections = grid.getSelectionModel().getSelection();
                        var r = selections[0];

                        r.set('peran_id_str', combo.getRawValue());
                    }
                }
            }
        }, {
            header: 'NIP',
            tooltip: 'NIP',
            width: 80,
            sortable: true,
            dataIndex: 'nip_nim',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 18
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Jabatan Lembaga',
            tooltip: 'Jabatan Lembaga',
            width: 100,
            sortable: true,
            dataIndex: 'jabatan_lembaga',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 25
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Ym',
            tooltip: 'Ym',
            width: 80,
            sortable: true,
            dataIndex: 'ym',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 20
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Skype',
            tooltip: 'Skype',
            width: 80,
            sortable: true,
            dataIndex: 'skype',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 20
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Alamat',
            tooltip: 'Alamat',
            width: 320,
            sortable: true,
            dataIndex: 'alamat',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 80
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Kecamatan',
            tooltip: 'Kecamatan',
            width: 80,
            sortable: true,
            dataIndex: 'kode_wilayah',
            hideable: false,
            renderer: function(v,p,r){
                return r.data.kode_wilayah_str;
            },
            field: {
                xtype: 'mstwilayahcombo',
                listeners: {
                    change: function (combo, newValue, oldValue, eOpts) {
                        var grid = combo.up('gridpanel');
                        var selections = grid.getSelectionModel().getSelection();
                        var r = selections[0];

                        r.set('kode_wilayah_str', combo.getRawValue());
                    }
                }
            }
        }, {
            header: 'No Telepon',
            tooltip: 'No Telepon',
            width: 80,
            sortable: true,
            dataIndex: 'no_telepon',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 20
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'No Hp',
            tooltip: 'No Hp',
            width: 80,
            sortable: true,
            dataIndex: 'no_hp',
            hideable: false,
            field: {
                xtype: 'textfield'
                , maxLength: 20
                , enforceMaxLength: true
                , minValue: 0
            }
        }, {
            header: 'Aktif',
            tooltip: 'Aktif',
            width: 80,
            sortable: true,
            dataIndex: 'aktif',
            hideable: false,
            renderer: function (v, p, r) {
                switch (v) {
                    case 1: return 'Aktif'; break;
                    case 0: return 'Tidak Aktif'; break;
                    default: return '-'; break;
                }
            },
            field: {
                xtype: 'combobox',
                selectOnTab: true,
                valueField: 'id',
                hiddenName: 'id',
                displayField: 'data',
                store: {
                    data: [
                        { 'id': 1, 'data': 'Aktif' },
                        { 'id': 0, 'data': 'Tidak Aktif' }
                    ]
                },
                lazyRender: true
            },
        }];

        this.dockedItems = this.createDockedItems(grid);

        this.bbar = this.createBbar();

        this.callParent(arguments);
    }
});
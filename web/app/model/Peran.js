Ext.define('SimpegGundul.model.Peran', {
    extend: 'Ext.data.Model',
    idProperty: 'peran_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'peran_id', type: 'string' },
        { name: 'nama', type: 'string', useNull: true }
    ]
});
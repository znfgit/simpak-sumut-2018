Ext.define('SimpegGundul.model.Validasi', {
    extend: 'Ext.data.Model',
    idProperty: 'validasi_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'validas_id', type: 'int' },
        { name: 'kolom', type: 'string' },
        { name: 'dapodik', type: 'string' },
        { name: 'bkn', type: 'string' },
        { name: 'status', type: 'int' },
    ]
});
Ext.define('SimpegGundul.model.AnggotaKegiatan', {
    extend: 'Ext.data.Model',
    idProperty: 'anggota_kegiatan_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'anggota_kegiatan_id', type: 'string' },
        { name: 'kegiatan_id', type: 'string' },
        { name: 'ptk_id', type: 'string', useNull: true },
        { name: 'ptk_id_str', type: 'string' },
        { name: 'pegawai_id', type: 'string', useNull: true },
        { name: 'pegawai_id_str', type: 'string' },
        { name: 'peran', type: 'string' },
        { name: 'soft_delete', type: 'int' }
    ]
});
Ext.define('SimpegGundul.model.Penugasan', {
    extend: 'Ext.data.Model',
    idProperty: 'penugasan_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'penugasan_id', type: 'string' },
        { name: 'sekolah_induk', type: 'string', useNull: true },
        { name: 'sekolah_induk_str', type: 'string' },
        { name: 'sekolah_tujuan', type: 'string', useNull: true },
        { name: 'sekolah_tujuan_str', type: 'string' },
        { name: 'ptk_id', type: 'string', useNull: true },
        { name: 'ptk_id_str', type: 'string' },
        { name: 'no_surat', type: 'string' },
        { name: 'tgl_surat', type: 'date', dateFormat: 'Y-m-d' },
        { name: 'status', type: 'int' },
        { name: 'alasan_penolakan', type: 'string' },
        { name: 'soft_delete', type: 'int' },
        { name: 'create_date', type: 'date', dateFormat: 'Y-m-d H:i:s' },
    ]
});
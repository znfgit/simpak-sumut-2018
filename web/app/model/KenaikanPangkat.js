Ext.define('SimpegGundul.model.KenaikanPangkat', {
    extend: 'Ext.data.Model',
    idProperty: 'ptk_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'ptk_id', type: 'string' },
        { name: 'nip', type: 'string' },
        { name: 'karpeg', type: 'string' },
        { name: 'nama', type: 'string' },
        { name: 'unit_kerja', type: 'string' },
        { name: 'gol_lama', type: 'string' },
        { name: 'tmt_lama', type: 'date', dateFormat: 'Y-m-d' },
        { name: 'gol_baru', type: 'string' },
        { name: 'tmt_baru', type: 'date', dateFormat: 'Y-m-d' },
        { name: 'status', type: 'string' }
    ]
});
Ext.define('SimpegGundul.model.PakTree', {
    extend: 'Ext.data.TreeModel',
    idProperty: 'id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'id', type: 'string' },
        { name: 'pak_id', type: 'string' },
        { name: 'uraian', type: 'string' },
        { name: 'ak_lama', type: 'float' }, 
        { name: 'ak_usulan', type: 'float' }, 
        { name: 'ak_penilai', type: 'float' }, 
        { name: 'ak_final', type: 'float' }, 
    ]
});
Ext.define('SimpegGundul.model.PangkatGolongan', {
    extend: 'Ext.data.Model',
    idProperty: 'pangkat_golongan_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'pangkat_golongan_id', type: 'int' },
        { name: 'nama', type: 'string' },
        { name: 'golongan', type: 'string' },
        { name: 'pangkat', type: 'string' },
        { name: 'jabatan_guru', type: 'string' },
        { name: 'jabatan_pengawas', type: 'string' },
        { name: 'akk_min', type: 'float' },
        { name: 'pd_min', type: 'float' },
        { name: 'piki_min', type: 'float' },
        { name: 'akp_min', type: 'float' },
        { name: 'kumulatif_min', type: 'float' },
        { name: 'pangkat_golongan_kenaikan', type: 'int', useNull: true },
        { name: 'pangkat_golongan_kenaikan_str', type: 'string' }
    ]
});
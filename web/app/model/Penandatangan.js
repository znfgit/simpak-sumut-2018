Ext.define('SimpegGundul.model.Penandatangan', {
    extend: 'Ext.data.Model',
    idProperty: 'penandatangan_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'penandatangan_id', type: 'string' },
        { name: 'atas_nama', type: 'string' },
        { name: 'jabatan', type: 'string' },
        { name: 'instansi', type: 'string' },
        { name: 'nama', type: 'string' },
        { name: 'nip', type: 'string' },
        { name: 'pangkat_golongan_id', type: 'int', useNull: true },
        { name: 'pangkat_golongan_id_str', type: 'string' }
    ]
});
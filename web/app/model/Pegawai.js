Ext.define('SimpegGundul.model.Pegawai', {
    extend: 'Ext.data.Model',
    idProperty: 'pegawai_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'pegawai_id', type: 'string' },
        { name: 'nama', type: 'string' },
        { name: 'nip', type: 'string' },
        { name: 'nik', type: 'string' },
        { name: 'npwp', type: 'string' },
        { name: 'tempat_tugas', type: 'string' },
        { name: 'karpeg', type: 'string' },
        { name: 'tempat_lahir', type: 'string' },
        { name: 'tgl_lahir', type: 'date', dateFormat: 'Y-m-d' },
        { name: 'jenis_kelamin', type: 'string' },
        { name: 'jenjang_pendidikan', type: 'string' },
        { name: 'pangkat_golongan_id', type: 'string', useNull: true },
        { name: 'pangkat_golongan_id_str', type: 'string' },
        { name: 'tmt_pangkat', type: 'string' },
        { name: 'tmt_pengangkatan', type: 'string' },
        { name: 'jabatan', type: 'string' },
        { name: 'last_update', type: 'date', dateFormat: 'Y-m-d H:i:s' },
        { name: 'soft_delete', type: 'int' }
    ]
});
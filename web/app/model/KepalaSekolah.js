Ext.define('SimpegGundul.model.KepalaSekolah', {
    extend: 'Ext.data.Model',
    idProperty: 'ptk_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'ptk_id', type: 'string' },
        { name: 'nip', type: 'string' },
        { name: 'nuptk', type: 'string' },
        { name: 'nama', type: 'string' },
        { name: 'unit_induk', type: 'string' },
        { name: 'tmt_lama', type: 'date', dateFormat: 'Y-m-d' },
        { name: 'warning', type: 'string' }
    ]
});
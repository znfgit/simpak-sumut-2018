Ext.define('SimpegGundul.model.JenisPtk', {
    extend: 'Ext.data.Model',
    idProperty: 'jenis_ptk_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'jenis_ptk_id', type: 'float'  },
        { name: 'jenis_ptk', type: 'string'  },
        { name: 'guru_kelas', type: 'float'  },
        { name: 'guru_matpel', type: 'float'  },
        { name: 'guru_bk', type: 'float'  },
        { name: 'guru_inklusi', type: 'float'  },
        { name: 'pengawas_satdik', type: 'float'  },
        { name: 'pengawas_plb', type: 'float'  },
        { name: 'pengawas_matpel', type: 'float'  },
        { name: 'pengawas_bidang', type: 'float'  },
        { name: 'tas', type: 'float'  },
        { name: 'formal', type: 'float'  },
        { name: 'create_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'last_update', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'expired_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'last_sync', type: 'date',  dateFormat: 'Y-m-d H:i:s'}
    ]
});
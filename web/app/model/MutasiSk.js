Ext.define('SimpegGundul.model.MutasiSk', {
    extend: 'Ext.data.Model',
    idProperty: 'mutasi_sk_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'mutasi_sk_id', type: 'string' },
        { name: 'no_sk', type: 'string' },
        { name: 'tgl_sk', type: 'date', dateFormat: 'Y-m-d' },
        { name: 'penandatangan_id', type: 'string', useNull: true },
        { name: 'penandatangan_id_str', type: 'string' },
        { name: 'soft_delete', type: 'int' }
    ]
});
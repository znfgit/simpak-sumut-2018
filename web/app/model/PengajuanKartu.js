Ext.define('SimpegGundul.model.PengajuanKartu', {
    extend: 'Ext.data.Model',
    idProperty: 'pengajuan_kartu_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'pengajuan_kartu_id', type: 'string' },
        { name: 'ptk_id', type: 'string', useNull: true },
        { name: 'ptk_id_str', type: 'string', useNull: true },
        { name: 'pegawai_id', type: 'string' },
        { name: 'pegawai_id_str', type: 'string' },
        { name: 'no_surat', type: 'string', useNull: true },
        { name: 'jenis_kartu', type: 'int' },
        { name: 'tgl_surat', type: 'date', dateFormat: 'Y-m-d' },
        { name: 'banyaknya', type: 'string' },
        { name: 'keterangan', type: 'string' },
        { name: 'status', type: 'int' },
        { name: 'alasan_penolakan', type: 'string' },
        { name: 'soft_delete', type: 'int' },
        { name: 'create_date', type: 'date', dateFormat: 'Y-m-d H:i:s' },
    ]
});
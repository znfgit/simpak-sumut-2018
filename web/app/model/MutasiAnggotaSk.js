Ext.define('SimpegGundul.model.MutasiAnggotaSk', {
    extend: 'Ext.data.Model',
    idProperty: 'mutasi_anggota_sk_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'mutasi_anggota_sk_id', type: 'string' },
        { name: 'mutasi_sk_id', type: 'string', useNull: true },
        { name: 'mutasi_usulan_id', type: 'string', useNull: true },
        { name: 'ptk_id_str', type: 'string' },
        { name: 'nip', type: 'string' },
        { name: 'sekolah_id_str', type: 'string' },
        { name: 'nama_sekolah', type: 'string' },
        { name: 'alasan', type: 'string' },
        { name: 'soft_delete', type: 'int' }
    ]
});
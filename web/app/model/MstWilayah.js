Ext.define('SimpegGundul.model.MstWilayah', {
    extend: 'Ext.data.Model',
    idProperty: 'kode_wilayah',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'kode_wilayah', type: 'string' },
        { name: 'nama', type: 'string' },
        { name: 'id_level_wilayah', type: 'int', useNull: true },
        { name: 'id_level_wilayah_str', type: 'string' },
        { name: 'mst_kode_wilayah', type: 'string', useNull: true },
        { name: 'mst_kode_wilayah_str', type: 'string' },
        { name: 'negara_id', type: 'string', useNull: true },
        { name: 'negara_id_str', type: 'string' },
        { name: 'asal_wilayah', type: 'string' },
        { name: 'kode_bps', type: 'string' },
        { name: 'kode_dagri', type: 'string' },
        { name: 'kode_keu', type: 'string' },
        { name: 'create_date', type: 'date', dateFormat: 'Y-m-d H:i:s' },
        { name: 'last_update', type: 'date', dateFormat: 'Y-m-d H:i:s' },
        { name: 'expired_date', type: 'date', dateFormat: 'Y-m-d H:i:s' },
        { name: 'last_sync', type: 'date', dateFormat: 'Y-m-d H:i:s' }
    ]
});
Ext.define('SimpegGundul.model.Kegiatan', {
    extend: 'Ext.data.Model',
    idProperty: 'kegiatan_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'kegiatan_id', type: 'string' },
        { name: 'nama', type: 'string' },
        { name: 'Tempat', type: 'string' },
        { name: 'tgl_surat', type: 'date', dateFormat: 'Y-m-d' },
        { name: 'no_surat', type: 'string' },
        { name: 'tgl_mulai', type: 'date', dateFormat: 'Y-m-d' },
        { name: 'tgl_selesai', type: 'date', dateFormat: 'Y-m-d' },
        { name: 'penandatangan_id', type: 'string', useNull: true },
        { name: 'penandatangan_id_str', type: 'string' },
        { name: 'max_peserta', type: 'int' },
        { name: 'soft_delete', type: 'int' }
    ]
});
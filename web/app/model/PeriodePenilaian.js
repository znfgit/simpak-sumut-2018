Ext.define('SimpegGundul.model.PeriodePenilaian', {
    extend: 'Ext.data.Model',
    idProperty: 'periode_penilaian_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'periode_penilaian_id', type: 'int' },
        { name: 'nama', type: 'string' },
        { name: 'tanggal_mulai', type: 'date', dateFormat: 'Y-m-d' },
        { name: 'tanggal_akhir', type: 'date', dateFormat: 'Y-m-d' },
        { name: 'aktif', type: 'float' }
    ]
});
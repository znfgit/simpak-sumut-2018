Ext.define('SimpegGundul.model.Kgb', {
    extend: 'Ext.data.Model',
    idProperty: 'ptk_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'ptk_id', type: 'string' },
        { name: 'nip', type: 'string' },
        { name: 'karpeg', type: 'string' },
        { name: 'nama', type: 'string' },
        { name: 'unit_kerja', type: 'string' },
        { name: 'pangkat_golongan_id', type: 'int' },
        { name: 'pangkat_golongan_id_str', type: 'string' },
        { name: 'tmt_pangkat', type: 'date', dateFormat: 'Y-m-d' },
        { name: 'kenaikan', type: 'string' },
        { name: 'status', type: 'string' }
    ]
});
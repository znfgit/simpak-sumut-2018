Ext.define('SimpegGundul.model.MutasiUsulan', {
    extend: 'Ext.data.Model',
    idProperty: 'mutasi_usulan_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'mutasi_usulan_id', type: 'string'  },
        { name: 'no_surat', type: 'string'  },
        { name: 'ptk_id', type: 'string', useNull: true },
        { name: 'ptk_id_str', type: 'string'  },
        { name: 'pegawai_id', type: 'string', useNull: true },
        { name: 'pegawai_id_str', type: 'string'  },
        { name: 'nip', type: 'string'  },
        { name: 'jenis_ptk', type: 'string'  },
        { name: 'sekolah_id', type: 'string', useNull: true },
        { name: 'sekolah_id_str', type: 'string'  },
        { name: 'jenis_ptk_id', type: 'float', useNull: true },
        { name: 'jenis_ptk_id_str', type: 'string'  },
        { name: 'alasan', type: 'string'  },
        { name: 'alasan_penolakan', type: 'string'  },
        { name: 'proses', type: 'int'  },
        { name: 'create_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'soft_delete', type: 'int'  }
    ]
});
<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace MstWilayah;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of MstWilayah
 *
 * @author faisaluje
 */
class MstWilayah {
    public function getData(Application $app, Request $request){
        $start = ($request->get('start')) ? $request->get('start') : 0;
        $limit = ($request->get('limit')) ? $request->get('limit') : 50;
        
        $query = \Model\MstWilayahQuery::create()
                ->filterByMstKodeWilayah('040300  ');
        
        if($request->get('query')) $query->filterByNama('%'.$request->get('query')."%", \Propel\Runtime\ActiveQuery\Criteria::LIKE);
        
        $query->offset($start)->limit($limit);
        
        $count = $query->count();
        $data = $query->find();
        
        if(is_null($data)) return tableJson(array(), 0, array('id'));
        
        $outArr = $data->toArray(null, false, \Propel\Runtime\Map\TableMap::TYPE_FIELDNAME);
        
        return tableJson($outArr, $count, array('kode_wilayah'), $start, $limit);
    }
}

<?php
namespace Auth;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\User;
use Model\PenggunaQuery;

class UserProvider implements UserProviderInterface
{
    public function __construct($app)
    {
        $this->app = $app;
    }
    
    public function loadUserByUsername($username){
//         $stmt = $this->conn->executeQuery('SELECT * FROM users WHERE username = ?', array(strtolower($username)));
        $stmt = PenggunaQuery::create()->filterByUsername($username)->filterByAktif(1)->findOne();
        
        if (!is_object($stmt)) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }
        
        $alamat = "Dinas Pendidikan Provinsi Sumatera Utara";
        $level = 0;
        $peran = null;
        
        if(in_array($stmt->getPeranId(), [8,9,53])){
            $level = 1;  //Dinas
            $peran = "Operator Dinas";
            
            if(!is_null($stmt->getSekolahId())){
                $sekolah = \Model\SekolahQuery::create()->findOneBySekolahId($stmt->getSekolahId());
                
                if(is_object($sekolah)){
                    $alamat = $sekolah->getNama();
                }
            }
        }elseif($stmt->getPeranId() == 57){
            $level = 2; //UPTD
            $peran = "Operator UPT";
        }elseif($stmt->getPeranId() == 1){
            $level = 3; //Dinas
            $peran = "Admin Dinas";
        }elseif($stmt->getPeranId() == 91){
            $level = 4;  //Dinas Paud
            $peran = "Operator Dinas (PAUD)";
        }elseif($stmt->getPeranId() == 92){
            $level = 5;  //Dinas SD
            $peran = "Operator Dinas (SD)";
        }elseif($stmt->getPeranId() == 93){
            $level = 6;  //Dinas SMP
            $peran = "Operator Dinas (SMP)";
        }else{
            $level = 99;
            $peran = "Super Admin";
        }
        
        $this->app['session']->set('user', array(
            'id' => $stmt->getPenggunaId(),
            'username' => $stmt->getUsername(),
            'nama' => $stmt->getNama(),
            'peranId' => $stmt->getPeranId(),
            'peran' => $peran,
            'alamat' => $alamat,
            'noHp' => $stmt->getNoHp(),
            'ptkId' => $stmt->getPtkId(),
            'sekolahId' => $stmt->getSekolahId(),
            'kodeWilayah' => $stmt->getKodeWilayah(),
            'level' => $level
        ));
        
        $this->app['session']->set('login_gundul', true);
        
        return new User($stmt->getUsername(), $stmt->getPassword(), array('ROLE_USER'), true, true, true, true);
    }
    
    public function refreshUser(UserInterface $user)    
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }
    
        return $this->loadUserByUsername($user->getUsername());
    }
    
    public function supportsClass($class)
    {
        return $class === 'Symfony\Component\Security\Core\User\User';
    }
}
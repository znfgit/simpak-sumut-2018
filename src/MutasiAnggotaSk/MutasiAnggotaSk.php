<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace MutasiAnggotaSk;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of MutasiAnggota
 *
 * @author faisaluje
 */
class MutasiAnggotaSk {
    public function getData(Application $app, Request $request){
        $mutasiSkId = $request->get('mutasi_sk_id');
        $start = ($request->get('start')) ? $request->get('start') : 0;
        $limit = ($request->get('limit')) ? $request->get('limit') : 50;
        
        $query = \Model\MutasiAnggotaSkQuery::create()
                ->filterBySoftDelete(0)
                ->filterByMutasiSkId($mutasiSkId);
        
        $count = $query->count();
        
        $query->offset($start)
              ->limit($limit);
        
        $mutasiAnggotaSks = $query->find();
        
        $outArr = array();
        foreach ($mutasiAnggotaSks as $anggota){
            $arr = $anggota->toArray();
            $mutasi = $anggota->getMutasiUsulan();
            $ptk = $mutasi->getPtk();
            $pegawai = $mutasi->getPegawai();
            
            $arr["ptk_id_str"] = ($ptk) ? $ptk->getNama() : $pegawai->getNama();
            $arr["nip"] = ($ptk) ? $ptk->getNip() : $pegawai->getNip();
            $arr["sekolah_id_str"] = ($ptk) ? $ptk->getNamaSekolah() : $pegawai->getTempatTugas();
            $arr["nama_sekolah"] = $mutasi->getSekolah()->getNama();
            $arr["alasan"] = $mutasi->getAlasan();
            
            $outArr[] = $arr;
        }
        
        return tableJson($outArr, $count, array('mutasi_anggota_sk_id'), $start, $limit);
    }
    
    public function save(Application $app, Request $request){
        $data = $request->get('data');
        $mutasiSkId = $request->get('mutasi_sk_id');
        
        $objs = json_decode($data, true);
        
        $recordUpdated = 0;
        $recordFailed = 0;
        
        foreach ($objs as $obj){
            if(array_key_exists("mutasi_anggota_sk_id", $obj)){
                $anggota = \Model\MutasiAnggotaSkQuery::create()->findOneByMutasiAnggotaSkId($obj["mutasi_anggota_sk_id"]);
            }else{
                $anggota = new \Model\MutasiAnggotaSk();
                $anggota->setMutasiAnggotaSkId(getGUID())
                        ->setSoftDelete(0);
            }
            
            $anggota->setMutasiUsulanId($obj["mutasi_usulan_id"]);
            $anggota->setMutasiSkId($mutasiSkId);
            
            $mutasiUsulan = $anggota->getMutasiUsulan();
            $mutasiUsulan->setProses(4);
            $mutasiUsulan->save();

            if($anggota->save()){
                $recordUpdated++;
            }else{
                $recordFailed++;
            }
            
        }
        
        $return = array();
        $return['berhasil'] = $recordUpdated;
        $return['gagal'] = $recordFailed;

        return json_encode($return);
    }
    
    public function delete(Application $app, Request $request){
        $data = $request->get('data');
        $mutasiSkId = $request->get('mutasi_sk_id');
        
        $objs = json_decode($data, true);
        
        $recordDeleted = 0;
        $recordFailed = 0;
        
        foreach ($objs as $obj){
            if(array_key_exists("mutasi_anggota_sk_id", $obj)){
                $anggota = \Model\MutasiAnggotaSkQuery::create()->findOneByMutasiAnggotaSkId($obj["mutasi_anggota_sk_id"]);
            }else{
                continue;
            }
            
            $mutasiUsulan = $anggota->getMutasiUsulan();
            $mutasiUsulan->setProses(2);
            $mutasiUsulan->save();
            
            $anggota->delete();
            $recordDeleted++;
        }
        
        $return = array();
        $return['berhasil'] = $recordDeleted;
        $return['gagal'] = $recordFailed;

        return json_encode($return);
    }
}

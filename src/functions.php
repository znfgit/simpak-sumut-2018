<?php

function getConnection(){
    $serverName = DBHOST;
    $connectionInfo = array( "Database"=>DBNAME, "UID"=>DBUSER, "PWD"=>DBPASS,'ReturnDatesAsStrings'=>true);
    
    $conn = sqlsrv_connect( $serverName, $connectionInfo);

    if( $conn ) {
        return $conn;
    }else{
        echo "Connection could not be established.<br />";
        die( print_r( sqlsrv_errors(), true));
    }
}

function getDataBySql($sql){
    $con = getConnection();
    $return = array();

    $stmt = sqlsrv_query( $con, $sql );
    while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
            array_push($return, $row);
    }

    return $return;
}

function executeSql($sql){
    $con = getConnection();
    
    $stmt = sqlsrv_query( $con, $sql);
    if( $stmt === false ) {
        return sqlsrv_errors();
    }else{
        return true;
    }
}

function tableJson($array, $rownum, $id, $start="0", $limit="20") {

    $rows = sizeof($array) > 0 ? json_encode($array) : "[]";
    $result = sprintf("{ 'results' : %s, 'id' : '%s', 'start': %s, 'limit': %s, 'rows' : %s }",
    $rownum, $id[0], $start, $limit, $rows);

    return $result;
}

function getGUID(){
    mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
    $charid = strtoupper(md5(uniqid(rand(), true)));
    $hyphen = chr(45);// "-"
    $uuid = substr($charid, 0, 8).$hyphen
        .substr($charid, 8, 4).$hyphen
        .substr($charid,12, 4).$hyphen
        .substr($charid,16, 4).$hyphen
        .substr($charid,20,12);
    return $uuid;
}

/**
 * Splitter for incoming json formatted array
 *
 * @param string $jsonText
 * @return array
 */
function splitJsonArray($jsonText) {

    $j = $jsonText;
    $arr = explode("},{", $j);
    $size = sizeof($arr);
    for ($i=0; $i<$size; $i++) {
        if (($i==0) || ($i == ($size-1))) {
            $arr[$i] = str_replace('[{','{', $arr[$i]);
            $arr[$i] = str_replace('}]','}', $arr[$i]);
        }
        if (($arr[$i][0] != "{")) {
            $arr[$i] = "{".$arr[$i];
        }
        $lastOffset = strlen($arr[$i])-1;
        if ($arr[$i][$lastOffset] != "}") {
            $arr[$i] = $arr[$i]."}";
        }

    }
    return $arr;
}

function getbulan($number) {
    $bulan = array (
        '1' => 'Januari',
        '2' => 'Februari',
        '3' => 'Maret',
        '4' => 'April',
        '5' => 'Mei',
        '6' => 'Juni',
        '7' => 'Juli',
        '8' => 'Agustus',
        '9' => 'September',
        '10' => 'Oktober',
        '11' => 'November',
        '12' => 'Desember'
    );

    $bul = $bulan[$number];
    return $bul;
}

function date_to_bahasa($date) {
    //date('Y-m-d')
    $date = date_parse($date);
    $tanggal = $date['day']." ".getbulan($date['month'])." ".$date['year'];
    return $tanggal;
}

function getArray($objArray, $type= Propel\Runtime\Map\TableMap::TYPE_PHPNAME) {
        $outArr = array();	
        foreach ($objArray as $o) {
                array_push($outArr, $o->toArray($type));
        }
        return $outArr;
}

function getKeyArray($products, $field, $value)
{
   foreach($products as $key => $product)
   {
      if ( $product[$field] === $value )
         return $key;
   }
   return false;
}
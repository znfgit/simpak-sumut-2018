<?php

namespace Epak;

use Model\Pak;
use Model\PakQuery;
use Model\PtkQuery;
use Propel\Runtime\Map\TableMap;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Model\KenaikanPangkatQuery;
use Model\KenaikanPangkat;

class Epak
{
    public function getPage(Request $request, Application $app) {
        $user = $app['session']->get('user');

        if ($user['peranId'] !== 53) {
            return $app->redirect('/');
        }

        $guru = $this->getDataGuru($user['ptkId']);
        $showData = $this->getDataAll($guru);

        return $app['twig']->render('e-pak/index.html', [
            'dir'=> 'e-pak',
            'guru' => $showData,
            'save' => false
        ]);
    }

    private function getDataGuru($ptkId) {
        $guru = PtkQuery::create()->findOneByPtkId($ptkId);
        $arrGuru = $guru->toArray();

        if (is_null($guru)) {
            return null;
        }
//        print_r($arrGuru);die;
        $pak = ($guru->getPaks()) ? $guru->getPaks()->toArray(null, null, TableMap::TYPE_FIELDNAME) : [];

        $arrGuru = array_merge($arrGuru, ['pak' => $pak[0]]);

        return $arrGuru;
    }

    private function getDataAll($arrGuru) {
        if(is_array($arrGuru['pak'])) {
            $arrGuru['pak']['nama'] = $arrGuru['nama'];
            $arrGuru['pak']['nip'] = $arrGuru['nip'];
            $arrGuru['pak']['tempat_lahir'] = $arrGuru['tempat_lahir'];
            $arrGuru['pak']['tgl_lahir'] = $arrGuru['tgl_lahir'];
            $arrGuru['pak']['jenis_kelamin'] = $arrGuru['jenis_kelamin'];
            $arrGuru['pak']['nama_pangkat_golongan'] = $arrGuru['nama_pangkat_golongan'];
            $arrGuru['pak']['nama_sekolah'] = $arrGuru['nama_sekolah'];
            $arrGuru['pak']['mapel_diajarkan'] = $arrGuru['mapel_diajarkan'];
            return $arrGuru['pak'];
        }

        $arr['pak_id'] = getGUID();
        $arr['nomor'] = null;
        $arr['nama'] = $arrGuru['nama'];
        $arr['nip'] = $arrGuru['nip'];
        $arr['tempat_lahir'] = $arrGuru['tempat_lahir'];
        $arr['tgl_lahir'] = $arrGuru['tgl_lahir'];
        $arr['jenis_kelamin'] = $arrGuru['jenis_kelamin'];
        $arr['nama_pangkat_golongan'] = $arrGuru['nama_pangkat_golongan'];
        $arr['nama_sekolah'] = $arrGuru['nama_sekolah'];
        $arr['mapel_diajarkan'] = $arrGuru['mapel_diajarkan'];
        $arr['ptk_id'] = $arrGuru['ptk_id'];
        $arr['pegawai_id'] = null;
        $arr['periode_pengusulan'] = null;
        $arr['periode_penilaian_id'] = null;
        $arr['penandatangan_id'] = null;
        $arr['tgl_penetapan'] = null;
        $arr['pangkat_golongan_id'] = $arrGuru['pangkat_golongan_id'];
        $arr['tmt_pangkat'] = $arrGuru['tmt_pangkat_gol'];
        $arr['pendidikan'] = $arrGuru['nama_ijazah_terakhir'] . ' ' . $arrGuru['nama_jurusan_s1'];
        $arr['jabatan'] = $arrGuru['Jenis_Guru'];
        $arr['tmt_jabatan'] = $arrGuru['tmt_pengangkatan'];
        $arr['status_pengusulan'] = 2;
        $arr['soft_delete'] = 0;

        $arr['utama_a1_lama'] = null;
        $arr['utama_a2_lama'] = null;
        $arr['utama_b1_lama'] = null;
        $arr['utama_b2_lama'] = null;
        $arr['utama_b3_lama'] = null;
        $arr['utama_c1_lama'] = null;
        $arr['utama_c2_lama'] = null;
        $arr['utama_c3_lama'] = null;
        $arr['penunjang_a_lama'] = null;
        $arr['penunjang_b_lama'] = null;
        $arr['penunjang_c_lama'] = null;
        $arr['utama_a1_usulan'] = null;
        $arr['utama_a2_usulan'] = null;
        $arr['utama_b1_usulan'] = null;
        $arr['utama_b2_usulan'] = null;
        $arr['utama_b3_usulan'] = null;
        $arr['utama_c1_usulan'] = null;
        $arr['utama_c2_usulan'] = null;
        $arr['utama_c3_usulan'] = null;
        $arr['penunjang_a_usulan'] = null;
        $arr['penunjang_b_usulan'] = null;
        $arr['penunjang_c_usulan'] = null;
        $arr['utama_a1_penilai'] = null;
        $arr['utama_a2_penilai'] = null;
        $arr['utama_b1_penilai'] = null;
        $arr['utama_b2_penilai'] = null;
        $arr['utama_b3_penilai'] = null;
        $arr['utama_c1_penilai'] = null;
        $arr['utama_c2_penilai'] = null;
        $arr['utama_c3_penilai'] = null;
        $arr['penunjang_a_penilai'] = null;
        $arr['penunjang_b_penilai'] = null;
        $arr['penunjang_c_penilai'] = null;
        $arr['utama_a1_final'] = null;
        $arr['utama_a2_final'] = null;
        $arr['utama_b1_final'] = null;
        $arr['utama_b2_final'] = null;
        $arr['utama_b3_final'] = null;
        $arr['utama_c1_final'] = null;
        $arr['utama_c2_final'] = null;
        $arr['utama_c3_final'] = null;
        $arr['penunjang_a_final'] = null;
        $arr['penunjang_b_final'] = null;
        $arr['penunjang_c_final'] = null;

        return $arr;
    }

    public function save(Request $request, Application $app){
//        print_r($request->request->all());die;
        $user = $app['session']->get('user');
        $pak_id = $request->get('pak_id');
        $kenaikanPangkat = KenaikanPangkatQuery::create()->findOneByPtkId($user['ptkId']);
        $pak = PakQuery::create()->findOneByPakId($pak_id);

        if (!$kenaikanPangkat) {
            $kenaikanPangkat = new KenaikanPangkat();
        }

        if (!$pak) {
            $pak = new Pak();
        }

        $kenaikanPangkat->setKenaikanPengkatId(getGUID())
                        ->setPtkId($user['ptkId'])
                        ->setSoftDelete(0)
                        ->setPeriodeUsulan(getbulan(date('n')) . ' ' . date('Y'))
        ->save();

        $pak->fromArray($request->request->all());
        if (in_array($pak->getStatusPengusulan(), [1,3,5])) {
            $pak->setStatusPengusulan(2);
        }
        $pak->setSoftDelete(0);
        $pak->setPeriodePengusulan(getbulan(date('n')) . ' ' . date('Y'));

        $pak->save();

        $guru = $this->getDataGuru($pak->getPtkId());
        $showData = $this->getDataAll($guru);

        return $app['twig']->render('e-pak/index.html', [
            'dir'=> 'e-pak',
            'guru' => $showData,
            'save' => true
        ]);
    }
}
<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Penandatangan;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of Penandatangan
 *
 * @author faisaluje
 */
class Penandatangan {
    public function getData(Application $app, Request $request){
        $start = ($request->get('start')) ? $request->get('start') : 0;
        $limit = ($request->get('limit')) ? $request->get('limit') : 50;
        
        $query = \Model\PenandatanganQuery::create();
        
        if($request->get('query')) $query->filterByNama('%'.$request->get('query')."%", \Propel\Runtime\ActiveQuery\Criteria::LIKE);
        
        $query->offset($start)->limit($limit);
        
        $count = $query->count();
        $data = $query->find();
        $outArr = array();
        
        if(is_null($data)) return tableJson(array(), 0, array('id'));
        
        foreach($data as $d){
            $arr = $d->toArray();
            $arr["pangkat_golongan_id_str"] = ($d->getPangkatGolongan()) ? $d->getPangkatGolongan()->getNama() : null;
            
            $outArr[] = $arr;
        }
        
        return tableJson($outArr, $count, array('penandatangan_id'), $start, $limit);
    }
    
    public function save(Application $app, Request $request){
        $data = $request->get('data');
        
        $objs = json_decode($data, true);
        
        $recordUpdated = 0;
        $recordFailed = 0;
        
        foreach ($objs as $obj){
            
            if(strpos($obj["penandatangan_id"], "SimpegGundul.model.Penandatangan") !== false){
                $obj["penandatangan_id"] = getGUID();
                
                $penandatangan = new \Model\Penandatangan();
            }else{
                $penandatangan = \Model\PenandatanganQuery::create()->findOneByPenandatanganId($obj["penandatangan_id"]);
            }
            
            $penandatangan->fromArray($obj, \Propel\Runtime\Map\TableMap::TYPE_FIELDNAME);
            
            if($penandatangan->save()){
                $recordUpdated++;
            }else{
                $recordFailed++;
            }
            
        }
        
        $return = array();
        $return['berhasil'] = $recordUpdated;
        $return['gagal'] = $recordFailed;

        return json_encode($return);
    }
    
    public function delete(Application $app, Request $request){
        $id = $request->get('id');
        
        try{
            $penandatangan = \Model\PenandatanganQuery::create()->findOneByPenandatanganId($id);
        
            if(is_null($penandatangan)) return new Response('Data tidak ditemukan', 400);
            
            $penandatangan->delete();
        } catch (Propel\Runtime\Exception\PropelException $ex) {
            return " { 'success' : false, 'msg' : 'Data gagal dihapus' } ";
        }
        
        return " { 'success' : true, 'msg' : 'Data berhasil dihapus' } ";
    }
}

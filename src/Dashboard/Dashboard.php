<?php 
namespace Dashboard;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Acl\Exception\Exception;
use Symfony\Component\EventDispatcher\Event;

class Dashboard
{

    public function fetchDashboard(Application $app, Request $request){

        // Set source template directory
        //diganti dari pakai absolute path jadi relative path (agar linux friendly)
        $sourceTplDir = '.';
        // $sourceTplDir = $this->config["nama_folder_penyimpanan"] . "/" . $this->config["nama_folder"] . "/web";

        // Create the twig object
        $loader = new \Twig_Loader_Filesystem($sourceTplDir);
        $twig = new \Twig_Environment($loader, array(
            'debug' => true
        ));

//        return 'uhuy';
        // Enable var_dump {{ dump(var) }}
        $twig->addExtension(new \Twig_Extension_Debug());

        // Get module and attach to object
        $module = $request->get("module");
        $module = $module ? $module : "dashboard";
        // $this->module = $module;
		
        $sql1 = "
                SELECT COUNT(*) FROM ptk
                ";
        $datas1 = getDataBySql($sql1);

        $sql2 = "
                SELECT COUNT(*) FROM mutasi_anggota_sk
                ";
        $datas2 = getDataBySql($sql2);

        $sql3 = "
                SELECT COUNT(*) FROM kenaikan_pangkat
                ";
        $datas3 = getDataBySql($sql3);


        $sql4 = "
                SELECT COUNT(*) FROM pak where periode_penilaian_id = '2015001'
                ";
        $datas4 = getDataBySql($sql4);

        $guruss = $datas1[0];
        $gurus = $guruss[''];
        $guru = $gurus;

        $mutasiss = $datas2[0];
        $mutasis = $mutasiss[''];
        $mutasi = $mutasis;

        $kenaikanss = $datas3[0];
        $kenaikans = $kenaikanss[''];
        $kenaikan = $kenaikans;

        $penilaianpakss = $datas4[0];
        $penilaianpaks = $penilaianpakss[''];
        $penilaianpak = $penilaianpaks;

        $token = $app["session"]->get('login_gundul');
        
        $data = array(
            "guru" => $guru,
            "mutasi" => $mutasi,
            "kenaikan" => $kenaikan,
            "penilaianpak" => $penilaianpak,
            "login" => $token,
            "uri" => uri,
            "url_info" => url_info
        );

        // Render it
        $outStr = $twig->render($module . '.twig', $data);

        // Return to browser
        return $outStr;

    }    

}  


 ?>
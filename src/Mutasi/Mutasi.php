<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mutasi;

use Model\Map\MutasiAnggotaSkTableMap;
use Model\Map\MutasiUsulanTableMap;
use Model\Map\PtkTableMap;
use Model\MutasiUsulan;
use Model\MutasiUsulanQuery;
use Model\PtkQuery;
use Model\SekolahQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Twig_Environment;
use Twig_Extension_Debug;
use Twig_Loader_Filesystem;

/**
 * Description of Mutasi
 *
 * @author faisaluje
 */
class Mutasi {
    public function getDataUsulan(Application $app, Request $request){
        $outArr = array();
        $start = ($request->get('start')) ? $request->get('start') : 0;
        $limit = ($request->get('limit')) ? $request->get('limit') : 100;
        $proses = $request->get('proses');
        $ascending = $request->get('ascending');
        $noSurat = $request->get('no_surat');
        
        $query = MutasiUsulanQuery::create()
                ->addJoin(MutasiUsulanTableMap::COL_PTK_ID, PtkTableMap::COL_PTK_ID, Criteria::LEFT_JOIN)
                ->filterBySoftDelete(0);
        
//        if($app["session"]->get('user')['level'] == 1) $query->add(\Model\Map\PtkTableMap::COL_SEKOLAH_ID, $app["session"]->get('user')['sekolahId']);
//        if($app["session"]->get('user')['level'] == 2) $query->add(\Model\Map\PtkTableMap::COL_KECAMATAN_ID, $app["session"]->get('user')['kodeWilayah']);
        if($proses) $query->filterByProses(json_decode ($proses), Criteria::IN);
        if($noSurat) {
            $query->addAnd(MutasiUsulanTableMap::COL_NO_SURAT, "%".$noSurat."%", Criteria::LIKE);
            $query->addOr(PtkTableMap::COL_NAMA, "%".$noSurat."%", Criteria::LIKE);
            $query->addOr(PtkTableMap::COL_NIP, "%".$noSurat."%", Criteria::LIKE);
        }
        if($request->get('sk') == 1){
            $query->addJoin(MutasiUsulanTableMap::COL_MUTASI_USULAN_ID, MutasiAnggotaSkTableMap::COL_MUTASI_USULAN_ID, Criteria::LEFT_JOIN)
                    ->add(MutasiAnggotaSkTableMap::COL_MUTASI_USULAN_ID, null, Criteria::ISNULL);
        }
        
        $query->orderByCreateDate();
        $count = $query->count();
        
        $query->offset($start)->limit($limit);
        $usulans = $query->find();
        
        foreach ($usulans as $usulan){
            $arr = $usulan->toArray();
            $arr["ptk_id_str"] = ($usulan->getPtkId()) ? $usulan->getPtk()->getNama() : $usulan->getPegawai()->getNama();
            $arr["nip"] = ($usulan->getPtkId()) ? $usulan->getPtk()->getNip() : $usulan->getPegawai()->getNip();
            $arr["jenis_ptk"] = ($usulan->getPtkId()) ? $usulan->getPtk()->getJenisGuru() : $usulan->getPegawai()->getJabatan();
            $arr["sekolah_id_str"] = $usulan->getSekolah()->getNama();
            $arr["jenis_ptk_id_str"] = $usulan->getJenisPtk()->getJenisPtk();
            $arr["nama_sekolah"] = ($usulan->getPtkId()) ? $usulan->getPtk()->getNamaSekolah() : $usulan->getPegawai()->getTempatTugas();
            
            $outArr[] = $arr;
        }
        
        return tableJson($outArr, $count, array("mutasi_usulan_id"), $start, $limit);
        
    }
    
    public function saveFormUsulan(Application $app, Request $request){
        $data = $_REQUEST;
        $mutasiUsulanId = $request->get('mutasi_usulan_id');
        
        $mutasiUsulan = MutasiUsulanQuery::create()->findOneByMutasiUsulanId($mutasiUsulanId);
        
        if(is_null($mutasiUsulan)){
            $mutasiUsulan = new MutasiUsulan();
            $data["mutasi_usulan_id"] = getGUID();
            $data["soft_delete"] = 0;
            $data["proses"] = 0;
        }
        
        unset($data["create_date"]);
        $mutasiUsulan->fromArray($data);
        if($app["session"]->get('user')['level'] == 1){
//            if($mutasiUsulan->getPtk()->getJenisSekolahId() == 6){
                $mutasiUsulan->setProses(5);
//            }
            $mutasiUsulan->setPegawaiId(null);
        }else{
            $mutasiUsulan->setProses(6);
            $mutasiUsulan->setPtkId(null);
        }
        
        if($mutasiUsulan->save()){
            return "{ 'success' : true, 'msg' : 'Berhasil Menyimpan' }";
        }else{
            return '{ "success" : false, "msg" : "Gagal Menyimpan" }';
        }
    }
    
    public function approve(Application $app, Request $request){
        $data = splitJsonArray($request->get('rows'));
        
        try {
            foreach ($data as $d) {
                $row = json_decode(stripslashes($d));
                $mutasiUsulanId = $row->mutasi_usulan_id;

                $mutasiUsulan = MutasiUsulanQuery::create()->findOneByMutasiUsulanId($mutasiUsulanId);

                if(in_array($mutasiUsulan->getProses(), array(3,4))){
                    continue;
                }

                $mutasiUsulan->setProses($request->get('approved'))
                    ->setAlasanPenolakan($request->get('alasan'));
                $mutasiUsulan->save();
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            $success = 'false';
            $result = sprintf("{'success' : $s, 'message' : '$s' }",$success,$message);
            die($result);
        }
        $result = sprintf("{'success' : true}");

        return $result;
    }
    
    public function getCetakSuratPermohonan(Application $app, Request $request){
        $id = $request->get('id');
        $mutasiUsulan = MutasiUsulanQuery::create()->findOneByMutasiUsulanId($id);

        if(is_null($mutasiUsulan)) return "<h1>Data Tidak Ditemukan!</h1>";

        $ptk = $mutasiUsulan->getPtk();
        $tglLahir = $ptk->getTglLahir('d ').getbulan($ptk->getTglLahir('n')).$ptk->getTglLahir(' Y');

        $pangkat = ($ptk->getPangkatGolonganId()) ? $ptk->getPangkatGolongan()->getNama() : null;

        $sekolahInduk = SekolahQuery::create()->findOneBySekolahId($ptk->getSekolahId());
//        return print_r($sekolahInduk->toArray());
        $sekolahTujuan = $mutasiUsulan->getSekolah();

        $ptk = $ptk->toArray();
        $ptk["golongan"] = $pangkat;

        $kepsek = PtkQuery::create()
                ->filterByTugasTambahanId(2)
                ->filterBySekolahId($sekolahInduk->getSekolahId())
                ->findOne();
        $kepsek = ($kepsek) ? $kepsek->toArray() : null;

        // Apply custom placed template
        $sourceTplDir = dirname(__FILE__) . "/../Templates";
        $fileName = "surat_permohonan_mutasi.twig";

        $loader = new Twig_Loader_Filesystem($sourceTplDir);
        $twig = new Twig_Environment($loader, array(
            "debug" => true
        ));
        $twig->addExtension(new Twig_Extension_Debug());

        $outStr = $twig->render($fileName, array(
            'mutasi' => $mutasiUsulan->toArray(),
            'ptk' => $ptk,
            'kepsek' => $kepsek,
            'tglLahir' => $tglLahir,
            'sekolahInduk' => $sekolahInduk->toArray(),
            'sekolahTujuan' => $sekolahTujuan->toArray()
        ));

        return $outStr;
    }
   
}

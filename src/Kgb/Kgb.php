<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Kgb;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of Kgb
 *
 * @author faisaluje
 */
class Kgb {
    public function getData(Application $app, Request $request){
        $start = ($request->get('start')) ? $request->get('start') : 0;
        $limit = ($request->get('limit')) ? $request->get('limit') : 100;
        
        $query = \Model\PtkQuery::create()
                ->filterByTglPensiun(date('Y-m-d'), \Propel\Runtime\ActiveQuery\Criteria::GREATER_THAN)
                ->filterByTmtPangkatGol(date('Y-m-d'), \Propel\Runtime\ActiveQuery\Criteria::LESS_EQUAL)
                ->filterByJenisSekolahId(array(7,8,13,14,15,29), \Propel\Runtime\ActiveQuery\Criteria::IN)
                ->filterByJenisPtkId(array(3,4,5,6,12,13,14), \Propel\Runtime\ActiveQuery\Criteria::IN)
                ->orderByTmtPangkatGol();
        
        if($request->get('pangkat_golongan_id')) $query->filterByPangkatGolonganId();        
        if($app["session"]->get('user')['sekolahId']) $query->filterBySekolahId($app["session"]->get('user')['sekolahId']);
        if($app["session"]->get('user')['level'] == 2){
            $query->filterByKecamatanId($app["session"]->get('user')['kodeWilayah'])
                    ->filterByJenisSekolahId(array(7,8,13,14,15,29), \Propel\Runtime\ActiveQuery\Criteria::IN);
        }
        
        if($request->get('txtCari')){
            $query->addAnd(\Model\Map\PtkTableMap::COL_NAMA, '%'.$request->get('txtCari').'%', \Propel\Runtime\ActiveQuery\Criteria::LIKE);
            $query->addOr(\Model\Map\PtkTableMap::COL_NIP, '%'.$request->get('txtCari').'%', \Propel\Runtime\ActiveQuery\Criteria::LIKE);
        }
        
        if($request->get('waktu_kenaikan')){
            switch ($request->get('waktu_kenaikan')){
                case 1 : $val = "< 1"; break;
                case 2 : $val = "in(1,2)"; break;
                case 3 : $val = "> 2"; break;
            }
            
            $query->where("DATEDIFF(
		YEAR,
		GETDATE(),
		DATEADD(YEAR, 2, ptk.tmt_pangkat_gol)
            ) {$val}");
        }
        
        $count = $query->count();
        
        $query->offset($start);
        $query->limit($limit);
        
        $ptks = $query->find();
        
        if(is_null($ptks)) return tableJson(array(), 0, array('id'));
        
        $ptkId = array();
        foreach($ptks as $ptk){
            $tmtGolLama = strtotime($ptk->getTmtPangkatGol('Y-m-d'));
            $t2 = strtotime('2 years', $tmtGolLama);
            $tmtGolLama = new \DateTime(date('Y-m-d', $t2));
            $tmtGolBaru = new \DateTime('now');
            $strNaik = null;
            $isNaik = null;
            
            if ($tmtGolBaru >= $tmtGolLama) {
                $kenaikanY=0;
            }else{
                $kenaikanY = $tmtGolBaru->diff($tmtGolLama)->format('%y');
                $kenaikanY = $kenaikanY+intval($tmtGolBaru->diff($tmtGolLama)->format('%m'))/12;
            }
            
            if ($tmtGolBaru >= $tmtGolLama) {
                $strNaik = "Sekarang";
                $isNaik = 1;
            } else {
                $strNaik = $tmtGolBaru->diff($tmtGolLama)->format('%y tahun %m bulan %d hari');
                $isNaik = 0;
            }
            
            $arr = $ptk->toArray();
            $arr["unit_kerja"] = $ptk->getNamaSekolah();
            $arr["kenaikan_y"] = $kenaikanY;
            $arr["pangkat_golongan_id"] = $ptk->getPangkatGolonganId();
            $arr["pangkat_golongan_id_str"] = ($ptk->getPangkatGolonganId()) ? $ptk->getPangkatGolongan()->getNama() : null;
            $arr["tmt_pangkat"] = $ptk->getTmtPangkatGol('Y-m-d');
            $arr["kenaikan"] = $strNaik;
            $arr["is_naik"] = $isNaik;
            $arr["status"] = \KenaikanPangkat\KenaikanPangkat::getStatus($isNaik);
            $arr["kgb_id"] = null;
            
            $ptkId[] = $ptk->getPtkId();

            $outArr[] = $arr;
        }
        
        $kgbs = \Model\KgbQuery::create()
                ->filterBySoftDelete(0)
                ->filterByPtkId($ptkId, \Propel\Runtime\ActiveQuery\Criteria::IN)
                ->find();
//        
        foreach($kgbs as $kgb){
            $key = array_search($kgb->getPtkId(), array_column($outArr, "ptk_id"));
            
            if($key === false){
                continue;
            }
            
            $outArr[$key]["is_naik"] = $kgb->getStatusPengusulan();
            $outArr[$key]["status"] = \KenaikanPangkat\KenaikanPangkat::getStatus($kgb->getStatusPengusulan());
            $outArr[$key]["kgb_id"] = $kgb->getKgbId();
        }
        
        return tableJson($outArr, $count, array('ptk_id'));
    }
    
    public function getDataPegawai(Application $app, Request $request){
        $start = ($request->get('start')) ? $request->get('start') : 0;
        $limit = ($request->get('limit')) ? $request->get('limit') : 100;
        
        $query = \Model\PegawaiQuery::create()
                ->filterByTmtPangkat(date('Y-m-d'), \Propel\Runtime\ActiveQuery\Criteria::LESS_EQUAL)
                ->orderByTmtPangkat();
        
        if($request->get('pangkat_golongan_id')) $query->filterByPangkatGolonganId();
        
        if($request->get('txtCari')){
            $query->addAnd(\Model\Map\PegawaiTableMap::COL_NAMA, '%'.$request->get('txtCari').'%', \Propel\Runtime\ActiveQuery\Criteria::LIKE);
            $query->addOr(\Model\Map\PegawaiTableMap::COL_NIP, '%'.$request->get('txtCari').'%', \Propel\Runtime\ActiveQuery\Criteria::LIKE);
        }
        
        if($request->get('waktu_kenaikan')){
            switch ($request->get('waktu_kenaikan')){
                case 1 : $val = "< 1"; break;
                case 2 : $val = "in(1,2)"; break;
                case 3 : $val = "> 2"; break;
            }
            
            $query->where("DATEDIFF(
		YEAR,
		GETDATE(),
		DATEADD(YEAR, 2, pegawai.tmt_pangkat)
            ) {$val}");
        }
        
        $count = $query->count();
        
        $query->offset($start);
        $query->limit($limit);
        
        $pegawais = $query->find();
        
        if(is_null($pegawais)) return tableJson(array(), 0, array('id'));
        
        $pegawaiId = array();
        foreach($pegawais as $pegawai){
            $tmtGolLama = strtotime($pegawai->getTmtPangkat('Y-m-d'));
            $t2 = strtotime('2 years', $tmtGolLama);
            $tmtGolLama = new \DateTime(date('Y-m-d', $t2));
            $tmtGolBaru = new \DateTime('now');
            $strNaik = null;
            $isNaik = null;
            
            if ($tmtGolBaru >= $tmtGolLama) {
                $kenaikanY=0;
            }else{
                $kenaikanY = $tmtGolBaru->diff($tmtGolLama)->format('%y');
                $kenaikanY = $kenaikanY+intval($tmtGolBaru->diff($tmtGolLama)->format('%m'))/12;
            }
            
            if ($tmtGolBaru >= $tmtGolLama) {
                $strNaik = "Sekarang";
                $isNaik = 1;
            } else {
                $strNaik = $tmtGolBaru->diff($tmtGolLama)->format('%y tahun %m bulan %d hari');
                $isNaik = 0;
            }
            
            $arr = $pegawai->toArray();
            $arr["unit_kerja"] = $pegawai->getTempatTugas();
            $arr["kenaikan_y"] = $kenaikanY;
            $arr["pangkat_golongan_id"] = $pegawai->getPangkatGolonganId();
            $arr["pangkat_golongan_id_str"] = ($pegawai->getPangkatGolonganId()) ? $pegawai->getPangkatGolongan()->getNama() : null;
            $arr["tmt_pangkat"] = $pegawai->getTmtPangkat('Y-m-d');
            $arr["kenaikan"] = $strNaik;
            $arr["is_naik"] = $isNaik;
            $arr["status"] = \KenaikanPangkat\KenaikanPangkat::getStatus($isNaik);
            $arr["kgb_id"] = null;
            
            $pegawaiId[] = $pegawai->getPegawaiId();

            $outArr[] = $arr;
        }
        
        $kgbs = \Model\KgbQuery::create()
                ->filterBySoftDelete(0)
                ->filterByPegawaiId($pegawaiId, \Propel\Runtime\ActiveQuery\Criteria::IN)
                ->find();
//        
        foreach($kgbs as $kgb){
            $key = array_search($kgb->getPegawaiId(), array_column($outArr, "pegawai_id"));
            
            if($key === false){
                continue;
            }
            
            $outArr[$key]["is_naik"] = $kgb->getStatusPengusulan();
            $outArr[$key]["status"] = \KenaikanPangkat\KenaikanPangkat::getStatus($kgb->getStatusPengusulan());
            $outArr[$key]["kgb_id"] = $kgb->getKgbId();
        }
        
        return tableJson($outArr, $count, array('pegawai_id'));
    }
    
    public function getDataForm(Application $app, Request $request){
        $tipe  = $request->get('tipe');
        $data = array();
        
        if(is_null($tipe)) return '{ "success" : false, "msg" : "Gagal Mengambil data" }';
                
        if($tipe == 1){
            $ptkId = $request->get('ptk_id');
            $kgb = \Model\KgbQuery::create()->findOneByPtkId($ptkId);
            $ptk = \Model\PtkQuery::create()->findOneByPtkId($ptkId);
            $pangkatGolonganId = $ptk->getPangkatGolonganId();
        }else{
            $pegawaiId = $request->get('pegawai_id');
            $kgb = \Model\KgbQuery::create()->findOneByPegawaiId($pegawaiId);
            $pegawai = \Model\PegawaiQuery::create()->findOneByPegawaiId($pegawaiId);
            $pangkatGolonganId = $pegawai->getPangkatGolonganId();
        }
        
        if(is_object($kgb)){
            $arr = $kgb->toArray();
            
            $tmtLama = new \DateTime($kgb->getTmtLama('Y-m-d'));
            $tmtKgb = new \DateTime($kgb->getTmtKgb('Y-m-d'));
            $tmtPangkat = new \DateTime($kgb->getTmtPangkat('Y-m-d'));
            $masaKerjaLama = $tmtLama->diff($tmtPangkat)->format('%y tahun %m bulan');
            $masaKerjaBaru = $tmtKgb->diff($tmtPangkat)->format('%y tahun %m bulan');
            
            $mkgLama = $tmtLama->diff($tmtPangkat)->format('%y');
            $gapokLama = \Model\GajiPokokQuery::create()
                    ->filterByPangkatGolonganId($pangkatGolonganId)
                    ->filterByMkg($mkgLama)
                    ->findOne();
            
            $mkgBaru = $tmtKgb->diff($tmtPangkat)->format('%y');
            $gapokBaru = \Model\GajiPokokQuery::create()
                    ->filterByPangkatGolonganId($pangkatGolonganId)
                    ->filterByMkg($mkgBaru)
                    ->findOne();
            
            $arr["gapok_lama"] = ($gapokLama) ? "Rp. ".number_format($gapokLama->getJumlah(), 0) : null;
            $arr["gapok_baru"] = ($gapokBaru) ? "Rp. ".number_format($gapokBaru->getJumlah(), 0) : null;
            $arr["masa_kerja_lama"] = $masaKerjaLama;
            $arr["masa_kerja_baru"] = $masaKerjaBaru;
        }else{
            $arr["kgb_id"] = null;
            $arr["no_surat"] = "-";
            $arr["soft_delete"] = 0;
            $arr["periode_pengusulan"] = date('Y');
            if($tipe == 1){
                $arr["ptk_id"] = $ptk->getPtkId();
                $arr["pangkat_golongan_id"] = $ptk->getPangkatGolonganId();
                $arr["tmt_pangkat"] = $ptk->getTmtPangkatGol('Y-m-d');
                $arr["jabatan"] = $ptk->getJenisGuru();
                $arr["status_pengusulan"] = 1;
            }else{
                $arr["pegawai_id"] = $pegawai->getPegawaiId();
                $arr["pangkat_golongan_id"] = $pegawai->getPangkatGolonganId();
                $arr["tmt_pangkat"] = $pegawai->getTmtPangkat('Y-m-d');
                $arr["jabatan"] = $pegawai->getJabatan();
                $arr["status_pengusulan"] = 7;
            }
        }
        
        if($tipe == 1){
            $arr["nama"] = $ptk->getNama();
            $arr["nip_karpeg"] = $ptk->getNip()." / ".$ptk->getKarpeg();
            $arr["nama_pangkat_golongan"] = $ptk->getPangkatGolongan()->getNama();
            $arr["instansi"] = $ptk->getNamaSekolah();
            $arr["golongan"] = $ptk->getPangkatGolongan()->getGolongan();
        }else{
            $arr["nama"] = $pegawai->getNama();
            $arr["nip_karpeg"] = $pegawai->getNip()." / ".$pegawai->getKarpeg();
            $arr["nama_pangkat_golongan"] = $pegawai->getPangkatGolongan()->getNama();
            $arr["instansi"] = $pegawai->getTempatTugas();
            $arr["golongan"] = $pegawai->getPangkatGolongan()->getGolongan();
        }
        $arr["tmt_pangkat_display"] = $arr["tmt_pangkat"];
        
        $data["success"] = true;
        $data["data"] = $arr;
        
        return json_encode($data);
    }
    
    public function saveForm(Application $app, Request $request){
        $data = $_REQUEST;
        $kgbId = $request->get('kgb_id');
        
        $kgb = \Model\KgbQuery::create()->findOneByKgbId($kgbId);
        
        if(is_null($kgb)){
            $kgb = new \Model\Kgb();
            $data["kgb_id"] = getGUID();
            $data["status_pengusulan"] = ($data["ptk_id"] == "") ? 7 : 2;
            $data["periode_kenaikan"] = date('Y');
        }
        
        if($data["no_surat"] == "") $data["no_surat"] = "- Dibuatkan Dinas -";
        if($data["penandatangan_id"] == "") $data["penandatangan_id"] = null;
        
        $data["soft_delete"] = 0;
        if($data["ptk_id"] == "") unset ($data["ptk_id"]);
        if($data["pegawai_id"] == "") unset ($data["pegawai_id"]);
        
        $kgb->fromArray($data);
        
//        print_r($pak->toArray());die;
        
        if($kgb->save()){
            return "{ 'success' : true, 'msg' : 'Berhasil Menyimpan', 'kgb_id' : '".$kgb->getKgbId()."' }";
        }else{
            return '{ "success" : false, "msg" : "Gagal Menyimpan" }';
        }
    }
    
    public function setStatus(Application $app, Request $request){
        $kgbId = $request->get('kgb_id');
        
        $kgb = \Model\KgbQuery::create()->findOneByKgbId($kgbId);
        
        if(is_null($kgb)) return '{ "success" : false, "msg" : "Gagal Menyimpan, PTK tidak ditemukan" }';
        
        $kgb->setStatusPengusulan($request->get('status'));
        
        if($kgb->save()){
            return "{ 'success' : true, 'msg' : 'Berhasil Menyimpan' }";
        }else{
            return '{ "success" : false, "msg" : "Gagal Menyimpan, Hubungi Admin" }';
        }
    }
    
    public function getCetakSurat(Application $app, Request $request){
        $kgbId = $request->get('id');

        $kgb = \Model\KgbQuery::create()->findOneByKgbId($kgbId);

        if(is_null($kgb)) return "<h1>Data Tidak Ditemukan!</h1>";
        
        $ptk = $kgb->getPtk();

        $tmtLama = new \DateTime($kgb->getTmtLama('Y-m-d'));
        $tmtKgb = new \DateTime($kgb->getTmtKgb('Y-m-d'));
        $tmtPangkat = new \DateTime($kgb->getTmtPangkat('Y-m-d'));
        $masaKerjaLama = $tmtLama->diff($tmtPangkat)->format('%y tahun %m bulan');
        $masaKerjaBaru = $tmtKgb->diff($tmtPangkat)->format('%y tahun %m bulan');

        $pangkatGolonganId = ($ptk->getPangkatGolonganId()) ? $ptk->getPangkatGolongan()->getPangkatGolonganId() : 0;
        $mkgLama = $tmtLama->diff($tmtPangkat)->format('%y');
        $gapokLama = \Model\GajiPokokQuery::create()
                ->filterByPangkatGolonganId($pangkatGolonganId)
                ->filterByMkg($mkgLama)
                ->findOne();

        $mkgBaru = $tmtKgb->diff($tmtPangkat)->format('%y');
        $gapokBaru = \Model\GajiPokokQuery::create()
                ->filterByPangkatGolonganId($pangkatGolonganId)
                ->filterByMkg($mkgBaru)
                ->findOne();
        
        $kgb = $kgb->toArray();
        
        $kgb["masa_kerja_lama"] = $masaKerjaLama;
        $kgb["masa_kerja_baru"] = $masaKerjaBaru;
        $kgb["gapok_lama"] = ($gapokLama) ? "Rp. ".number_format($gapokLama->getJumlah(), 0) : null;
        $kgb["gapok_baru"] = ($gapokBaru) ? "Rp. ".number_format($gapokBaru->getJumlah(), 0) : null;
        
        $pangkat = ($ptk->getPangkatGolonganId()) ? $ptk->getPangkatGolongan()->getNama() : null;
        $golongan = ($ptk->getPangkatGolonganId()) ? $ptk->getPangkatGolongan()->getGolongan() : null;
        $sekolah = \Model\SekolahQuery::create()->findOneBySekolahId($ptk->getSekolahId());
        
        $kepsek = \Model\PtkQuery::create()
                ->filterByTugasTambahanId(2)
                ->filterBySekolahId($ptk->getSekolahId())
                ->findOne();
        
        $kepsek = ($kepsek) ? $kepsek->toArray() : null;
        
        $ptk = $ptk->toArray();
        $ptk["pangkat"] = $pangkat;
        $ptk["golongan"] = $golongan;
        
        $kgb["tgl_lama"] = date_to_bahasa($kgb["tgl_lama"]);
        $kgb["tmt_lama"] = date_to_bahasa($kgb["tmt_lama"]);
        $kgb["tmt_kgb"] = date_to_bahasa($kgb["tmt_kgb"]);

        // Apply custom placed template
        $sourceTplDir = dirname(__FILE__) . "/../Templates";
        $fileName = "kgb.twig";

        $loader = new \Twig_Loader_Filesystem($sourceTplDir);
        $twig = new \Twig_Environment($loader, array(
            "debug" => true
        ));
        $twig->addExtension(new \Twig_Extension_Debug());

        $outStr = $twig->render($fileName, array(
            'kgb' => $kgb,
            'tglSurat' => date_to_bahasa(date('Y-m-d')),
            'ptk' => $ptk,
            'kepsek' => $kepsek,
            'sekolah' => $sekolah->toArray(),
        ));

        return $outStr;
    }
    
    public function getCetakSuratPegawai(Application $app, Request $request){
        $kgbId = $request->get('id');

        $kgb = \Model\KgbQuery::create()->findOneByKgbId($kgbId);

        if(is_null($kgb)) return "<h1>Data Tidak Ditemukan!</h1>";
        
        $pegawai = $kgb->getPegawai();

        $tmtLama = new \DateTime($kgb->getTmtLama('Y-m-d'));
        $tmtKgb = new \DateTime($kgb->getTmtKgb('Y-m-d'));
        $tmtPangkat = new \DateTime($kgb->getTmtPangkat('Y-m-d'));
        $masaKerjaLama = $tmtLama->diff($tmtPangkat)->format('%y tahun %m bulan');
        $masaKerjaBaru = $tmtKgb->diff($tmtPangkat)->format('%y tahun %m bulan');

        $pangkatGolonganId = $pegawai->getPangkatGolonganId();
        $mkgLama = $tmtLama->diff($tmtPangkat)->format('%y');
        $gapokLama = \Model\GajiPokokQuery::create()
                ->filterByPangkatGolonganId($pangkatGolonganId)
                ->filterByMkg($mkgLama)
                ->findOne();

        $mkgBaru = $tmtKgb->diff($tmtPangkat)->format('%y');
        $gapokBaru = \Model\GajiPokokQuery::create()
                ->filterByPangkatGolonganId($pangkatGolonganId)
                ->filterByMkg($mkgBaru)
                ->findOne();
        
        $pejabat = $kgb->getPenandatangan()->toArray();
        $kgb = $kgb->toArray();
        
        $kgb["masa_kerja_lama"] = $masaKerjaLama;
        $kgb["masa_kerja_baru"] = $masaKerjaBaru;
        $kgb["gapok_lama"] = ($gapokLama) ? "Rp. ".number_format($gapokLama->getJumlah(), 0) : null;
        $kgb["gapok_baru"] = ($gapokBaru) ? "Rp. ".number_format($gapokBaru->getJumlah(), 0) : null;
        
        $pangkat = ($pegawai->getPangkatGolonganId()) ? $pegawai->getPangkatGolongan()->getNama() : null;
        $golongan = ($pegawai->getPangkatGolonganId()) ? $pegawai->getPangkatGolongan()->getGolongan() : null;
        
        $pegawai = $pegawai->toArray();
        $pegawai["pangkat"] = $pangkat;
        $pegawai["golongan"] = $golongan;
        
        $kgb["tgl_lama"] = date_to_bahasa($kgb["tgl_lama"]);
        $kgb["tmt_lama"] = date_to_bahasa($kgb["tmt_lama"]);
        $kgb["tmt_kgb"] = date_to_bahasa($kgb["tmt_kgb"]);

        // Apply custom placed template
        $sourceTplDir = dirname(__FILE__) . "/../Templates";
        $fileName = "kgb_pegawai.twig";

        $loader = new \Twig_Loader_Filesystem($sourceTplDir);
        $twig = new \Twig_Environment($loader, array(
            "debug" => true
        ));
        $twig->addExtension(new \Twig_Extension_Debug());

        $outStr = $twig->render($fileName, array(
            'kgb' => $kgb,
            'tglSurat' => date_to_bahasa(date('Y-m-d')),
            'pegawai' => $pegawai,
            'pejabat' => $pejabat
        ));

        return $outStr;
    }
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Sekolah;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of Sekolah
 *
 * @author faisaluje
 */
class Sekolah {
    public function getData(Application $app, Request $request){
        $start = ($request->get('start')) ? $request->get('start') : 0;
        $limit = ($request->get('limit')) ? $request->get('limit') : 100;
        
        $query = \Model\SekolahQuery::create();
        
        if($request->get('query')) $query->filterByNama("%".$request->get('query')."%", \Propel\Runtime\ActiveQuery\Criteria::LIKE);
        
        $count = $query->count();
        
        $query->offset($start)->limit($limit);
        $sekolahs = $query->find();
        
        return tableJson($sekolahs->toArray(null, false, \Propel\Runtime\Map\TableMap::TYPE_FIELDNAME), $count, array("sekolah_id"), $start, $limit);
    }
}

<?php

use JDesrosiers\Silex\Provider\CorsServiceProvider;
use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\SecurityServiceProvider;
use Silex\Provider\SessionServiceProvider;

require_once __DIR__.'/../src/functions.php';
require_once __DIR__.'/../config/config.php';

$app = new Application();
$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app->register(new SessionServiceProvider());
$app['twig'] = $app->extend('twig', function ($twig, $app) {
    // add custom globals, filters, tags, ...

    return $twig;
});
$app->register(new Propel\Silex\PropelServiceProvider(), array(
    'propel.config_file' => __DIR__.'/../config/database/generated-conf/config.php'
));
$app->register(new SecurityServiceProvider(), array(
    'security.encoder.digest' => function($app){
        return new Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder('md5', false, 1);
    },
    
   'security.firewalls' => array(
       'login_path' => array(
           'pattern' => '^/login$',
           'anonymous' => true
       ),
       'test' => array(
           'pattern' => '^/test$',
           'anonymous' => true
       ),
       'passgen' => array(
           'pattern' => '^/passgen/.*$',
           'anonymous' => true
       ),
       'secured' => array(
           'pattern' => '^/.*$',
           'form' => array(
               'login_path' => uri.'login', 
               'check_path' => '/login_check',
               'always_use_default_target_path' => true,
               'default_target_path' => uri
           ),
           'logout' => array(
               'logout_path' => '/logout', 
               'invalidate_session' => true,
               'always_use_default_target_path' => true,
               'default_target_path' => uri
           ),
               'users' => function() use ($app){
               return new Auth\UserProvider($app);
           }
       ),
   )
 ));
 $app['security.default_encoder'] = function ($app) {
     return $app['security.encoder.digest'];
 };
 $app['security.access_rules'] = array(
     array('^/login$', 'IS_AUTHENTICATED_ANONYMOUSLY'),
     array('^/test$', 'IS_AUTHENTICATED_ANONYMOUSLY'),
     array('^/passgen/.*$', 'IS_AUTHENTICATED_ANONYMOUSLY'),
     array('^/.+$', 'ROLE_USER')
     // array('^/.+$', 'ROLE_USER')
 );

$app->register(new CorsServiceProvider(), [
    'cors.allowCredentials' => true
]);

return $app;

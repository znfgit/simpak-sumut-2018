<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace KenaikanPangkat;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of KenaikanPangkat
 *
 * @author faisaluje
 */
class KenaikanPangkat {
    public function getData(Application $app, Request $request){
        $start = ($request->get('start')) ? $request->get('start') : 0;
        $limit = ($request->get('limit')) ? $request->get('limit') : 100;
        
        
        $query = \Model\PtkQuery::create()
                ->filterByTglPensiun(date('Y-m-d'), \Propel\Runtime\ActiveQuery\Criteria::GREATER_THAN)
                ->filterByTmtPangkatGol(date('Y-m-d'), \Propel\Runtime\ActiveQuery\Criteria::LESS_EQUAL)
                ->filterByJenisSekolahId(array(7,8,13,14,15,29), \Propel\Runtime\ActiveQuery\Criteria::IN)
                ->filterByJenisPtkId(array(3,4,5,6,12,13,14), \Propel\Runtime\ActiveQuery\Criteria::IN)
                ->orderByTmtPangkatGol();
        
        if($request->get('pangkat_golongan_id')) $query->filterByPangkatGolonganId();        
        if($app["session"]->get('user')['sekolahId']) $query->filterBySekolahId($app["session"]->get('user')['sekolahId']);
        if($app["session"]->get('user')['level'] == 2){
            $query->filterByKecamatanId($app["session"]->get('user')['kodeWilayah'])
                    ->filterByJenisSekolahId(array(7,8,13,14,15,29), \Propel\Runtime\ActiveQuery\Criteria::IN);
        }
        
        if($request->get('txtCari')){
            $query->addAnd(\Model\Map\PtkTableMap::COL_NAMA, '%'.$request->get('txtCari').'%', \Propel\Runtime\ActiveQuery\Criteria::LIKE);
            $query->addOr(\Model\Map\PtkTableMap::COL_NIP, '%'.$request->get('txtCari').'%', \Propel\Runtime\ActiveQuery\Criteria::LIKE);
        }
        
        if($request->get('waktu_kenaikan')){
            switch ($request->get('waktu_kenaikan')){
                case 1 : $val = "< 1"; break;
                case 2 : $val = "in(1,2)"; break;
                case 3 : $val = "> 2"; break;
            }
            
            $query->where("DATEDIFF(
            YEAR,
            GETDATE(),
            DATEADD(YEAR, 4, ptk.tmt_pangkat_gol)
                ) {$val}");
        }
        
        $count = $query->count();
        
        $query->offset($start);
        $query->limit($limit);
        
        $ptks = $query->find();
        
        if(is_null($ptks)) return tableJson(array(), 0, array('id'));
        
        $ptkId = array();
        foreach($ptks as $ptk){
            $tmtGolLama = strtotime($ptk->getTmtPangkatGol('Y-m-d'));
            $t2 = strtotime('4 years', $tmtGolLama);
            $tmtGolLama = new \DateTime(date('Y-m-d', $t2));
            $tmtGolBaru = new \DateTime('now');
            $strNaik = null;
            $isNaik = null;
            
            if ($tmtGolBaru >= $tmtGolLama) {
                $kenaikanY=0;
            }else{
                $kenaikanY = $tmtGolBaru->diff($tmtGolLama)->format('%y');
                $kenaikanY = $kenaikanY+intval($tmtGolBaru->diff($tmtGolLama)->format('%m'))/12;
            }
            
            if ($tmtGolBaru >= $tmtGolLama) {
                $strNaik = "Sekarang";
                $isNaik = 1;
            } else {
                $strNaik = $tmtGolBaru->diff($tmtGolLama)->format('%y tahun %m bulan %d hari');
                $isNaik = 0;
            }
            
            $arr = $ptk->toArray();
            $arr["unit_kerja"] = $ptk->getNamaSekolah();
            $arr["kenaikan_y"] = $kenaikanY;
            $arr["gol_lama"] = $ptk->getNamaPangkatGolongan();
            $arr["tmt_lama"] = $ptk->getTmtPangkatGol('Y-m-d');
            $arr["gol_baru"] = $ptk->getNamaPangkatGolongan();
            $arr["tmt_baru"] = date('Y-m-d', $t2);
            $arr["naik_pangkat"] = $strNaik;
            $arr["is_naik"] = $isNaik;
            $arr["status"] = KenaikanPangkat::getStatus($isNaik);
            $arr["pak_id"] = null;
            
            $ptkId[] = $ptk->getPtkId();

            $outArr[] = $arr;
        }
        
        $paks = \Model\PakQuery::create()
                ->filterBySoftDelete(0)
                ->filterByPtkId($ptkId, \Propel\Runtime\ActiveQuery\Criteria::IN)
                ->find();
        
        foreach($paks as $pak){
            $key = array_search($pak->getPtkId(), array_column($outArr, "ptk_id"));
            
            if($key === false){
                continue;
            }
            
            $outArr[$key]["is_naik"] = $pak->getStatusPengusulan();
            $outArr[$key]["status"] = KenaikanPangkat::getStatus($pak->getStatusPengusulan());
            $outArr[$key]["pak_id"] = $pak->getPakId();
        }
        
        return tableJson($outArr, $count, array('ptk_id'));
    }
    
    public function setStatus(Application $app, Request $request){
        $pakId = $request->get('pak_id');
        
        $pak = \Model\PakQuery::create()->findOneByPakId($pakId);
        
        if(is_null($pak)) return '{ "success" : false, "msg" : "Gagal Menyimpan, PTK tidak ditemukan" }';
        
        $pak->setStatusPengusulan($request->get('status'));
        
        if($pak->save()){
            return "{ 'success' : true, 'msg' : 'Berhasil Menyimpan' }";
        }else{
            return '{ "success" : false, "msg" : "Gagal Menyimpan, Hubungi Admin" }';
        }
    }
    
    public function getDataPegawai(Application $app, Request $request){
        $start = ($request->get('start')) ? $request->get('start') : 0;
        $limit = ($request->get('limit')) ? $request->get('limit') : 100;
        
        $query = \Model\PegawaiQuery::create()
                ->filterByTmtPangkat(date('Y-m-d'), \Propel\Runtime\ActiveQuery\Criteria::LESS_EQUAL)
                ->filterBySoftDelete(0)
                ->orderByTmtPangkat();
        
        if($request->get('pangkat_golongan_id')) $query->filterByPangkatGolonganId();
        
        if($request->get('txtCari')){
            $query->addAnd(\Model\Map\PegawaiTableMap::COL_NAMA, '%'.$request->get('txtCari').'%', \Propel\Runtime\ActiveQuery\Criteria::LIKE);
            $query->addOr(\Model\Map\PegawaiTableMap::COL_NIP, '%'.$request->get('txtCari').'%', \Propel\Runtime\ActiveQuery\Criteria::LIKE);
        }
        
        if($request->get('waktu_kenaikan')){
            switch ($request->get('waktu_kenaikan')){
                case 1 : $val = "< 1"; break;
                case 2 : $val = "in(1,2)"; break;
                case 3 : $val = "> 2"; break;
            }
            
            $query->where("DATEDIFF(
		YEAR,
		GETDATE(),
		DATEADD(YEAR, 4, pegawai.tmt_pangkat)
            ) {$val}");
        }
        
        $count = $query->count();
        
        $query->offset($start);
        $query->limit($limit);
        
        $pegawais = $query->find();
        
        if(is_null($pegawais)) return tableJson(array(), 0, array('id'));
        
        $pegawaiId = array();
        foreach($pegawais as $pegawai){
            $tmtGolLama = strtotime($pegawai->getTmtPangkat('Y-m-d'));
            $t2 = strtotime('4 years', $tmtGolLama);
            $tmtGolLama = new \DateTime(date('Y-m-d', $t2));
            $tmtGolBaru = new \DateTime('now');
            $strNaik = null;
            $isNaik = null;
            
            if ($tmtGolBaru >= $tmtGolLama) {
                $kenaikanY=0;
            }else{
                $kenaikanY = $tmtGolBaru->diff($tmtGolLama)->format('%y');
                $kenaikanY = $kenaikanY+intval($tmtGolBaru->diff($tmtGolLama)->format('%m'))/12;
            }
            
            if ($tmtGolBaru >= $tmtGolLama) {
                $strNaik = "Sekarang";
                $isNaik = 1;
            } else {
                $strNaik = $tmtGolBaru->diff($tmtGolLama)->format('%y tahun %m bulan %d hari');
                $isNaik = 0;
            }
            
            $arr = $pegawai->toArray();
            $arr["unit_kerja"] = $pegawai->getTempatTugas();
            $arr["kenaikan_y"] = $kenaikanY;
            $arr["gol_lama"] = $pegawai->getPangkatGolongan()->getGolongan();
            $arr["tmt_lama"] = $pegawai->getTmtPangkat('Y-m-d');
            $arr["gol_baru"] = $pegawai->getPangkatGolongan()->getGolongan();
            $arr["tmt_baru"] = date('Y-m-d', $t2);
            $arr["naik_pangkat"] = $strNaik;
            $arr["is_naik"] = $isNaik;
            $arr["status"] = KenaikanPangkat::getStatus($isNaik);
            $arr["pak_id"] = null;
            
            $pegawaiId[] = $pegawai->getPegawaiId();

            $outArr[] = $arr;
        }
        
        $paks = \Model\PakQuery::create()
                ->filterBySoftDelete(0)
                ->filterByPegawaiId($pegawaiId, \Propel\Runtime\ActiveQuery\Criteria::IN)
                ->find();
        
        foreach($paks as $pak){
            $key = array_search($pak->getPegawaiId(), array_column($outArr, "pegawai_id"));
            
            if($key === false){
                continue;
            }
            
            $outArr[$key]["is_naik"] = $pak->getStatusPengusulan();
            $outArr[$key]["status"] = KenaikanPangkat::getStatus($pak->getStatusPengusulan());
            $outArr[$key]["pak_id"] = $pak->getPakId();
        }
        
        return tableJson($outArr, $count, array('pegawai_id'));
    }
    
    public static function getStatus($statusId){
        $str = "";
        
        switch ($statusId){
            case 1 : $str = "Belum mengusulkan"; break;
            case 2 : $str = "Diusulkan Sekolah"; break;
            case 3 : $str = "Ditolak UPT"; break;
            case 4 : $str = "Disetujui UPT"; break;
            case 5 : $str = "Ditolak Dinas"; break;
            case 6 : $str = "Disetujui Dinas"; break;
            case 7 : $str = "Diusulkan Dinas"; break;
            default: $str = ""; break;
        }
        
        return $str;
    }
}
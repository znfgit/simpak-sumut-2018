<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AnggotaKegiatan;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of AnggotaKegiatan
 *
 * @author faisaluje
 */
class AnggotaKegiatan {
    public function getData(Application $app, Request $request){
        $kegiatanId = $request->get('kegiatan_id');
        $start = ($request->get('start')) ? $request->get('start') : 0;
        $limit = ($request->get('limit')) ? $request->get('limit') : 50;
        
        $query = \Model\AnggotaKegiatanQuery::create()
                ->filterBySoftDelete(0)
                ->filterByKegiatanId($kegiatanId);
        
        if($app["session"]->get('user')['sekolahId']){
            $query->addJoin(\Model\Map\AnggotaKegiatanTableMap::COL_PTK_ID, \Model\Map\PtkTableMap::COL_PTK_ID, \Propel\Runtime\ActiveQuery\Criteria::INNER_JOIN);
            $query->add(\Model\Map\PtkTableMap::COL_SEKOLAH_ID, $app["session"]->get('user')['sekolahId']);
        }
        if($app["session"]->get('user')['level'] == 2){
            $query->addJoin(\Model\Map\AnggotaKegiatanTableMap::COL_PTK_ID, \Model\Map\PtkTableMap::COL_PTK_ID, \Propel\Runtime\ActiveQuery\Criteria::INNER_JOIN);
            $query->add(\Model\Map\PtkTableMap::COL_KECAMATAN_ID, $app["session"]->get('user')['kodeWilayah']);
        }
        
        $count = $query->count();
        $anggotas = $query->offset($start)->limit($limit)->find();
        
        $outArr = array();
        foreach ($anggotas as $anggota){
            $arr = $anggota->toArray();
            $arr["nama"] = ($anggota->getPegawai()) ? $anggota->getPegawai()->getNama() : $anggota->getPtk()->getNama();
            $arr["nip"] = ($anggota->getPegawai()) ? $anggota->getPegawai()->getNip() : $anggota->getPtk()->getNip();
            $arr["instansi"] = ($anggota->getPegawai()) ? $anggota->getPegawai()->getTempatTugas() : $anggota->getPtk()->getNamaSekolah();
            
            $outArr[] = $arr;
        }
        
        return tableJson($outArr, $count, array("anggota_kegiatan_id"));
    }
    
    public function save(Application $app, Request $request){
        $ptkIds = $request->get('ptk_id');
        $pegawaiIds = $request->get('pegawai_id');
        $kegiatanId = $request->get('kegiatan_id');
        
        $ptkIds = json_decode($ptkIds);
        $pegawaiIds = json_decode($pegawaiIds);
        
        $recordUpdated = 0;
        $recordFailed = 0;
        
        $kegiatan = \Model\KegiatanQuery::create()->findOneByKegiatanId($kegiatanId);
        $max = $kegiatan->getMaxPeserta();
        
        $jmlAnggota = \Model\AnggotaKegiatanQuery::create()
                ->filterBySoftDelete(0)
                ->filterByKegiatanId($kegiatanId)
                ->count();
        
        foreach ($ptkIds as $ptkId){
            if($max <= ($jmlAnggota+$recordUpdated)){
                continue;
            }
            $anggota = \Model\AnggotaKegiatanQuery::create()
                    ->filterByKegiatanId($kegiatanId)
                    ->filterByPtkId($ptkId)
                    ->findOne();
            
            if(is_null($anggota)){
                $anggota = new \Model\AnggotaKegiatan();
                $anggota->setAnggotaKegiatanId(getGUID());
            }
            
            $anggota->setKegiatanId($kegiatanId)
                    ->setPtkId($ptkId)
                    ->setSoftDelete(0)
                    ->setPeran($request->get('peran'));
            
            if($anggota->save()){
                $recordUpdated++;
            }else{
                $recordFailed++;
            }
        }
        
        foreach ($pegawaiIds as $pegawaiId){
            if($max <= ($jmlAnggota+$recordUpdated)){
                continue;
            }
            $anggota = \Model\AnggotaKegiatanQuery::create()
                    ->filterByKegiatanId($kegiatanId)
                    ->filterByPegawaiId($pegawaiId)
                    ->findOne();
            
            if(is_null($anggota)){
                $anggota = new \Model\AnggotaKegiatan();
                $anggota->setAnggotaKegiatanId(getGUID());
            }
            
            $anggota->setKegiatanId($kegiatanId)
                    ->setPegawaiId($pegawaiId)
                    ->setSoftDelete(0)
                    ->setPeran($request->get('peran'));
            
            if($anggota->save()){
                $recordUpdated++;
            }else{
                $recordFailed++;
            }
            
        }
        
        $return = array();
        $return['berhasil'] = $recordUpdated;
        $return['gagal'] = $recordFailed;

        return json_encode($return);
    }
    
    public function savePeran(Application $app, Request $request){
        $data = $request->get('data');
        
        $objs = json_decode($data, true);
        
        $recordUpdated = 0;
        $recordFailed = 0;
        
        foreach ($objs as $obj){
            
            if(strpos($obj["anggota_kegiatan_id"], "SimpegGundul.model.AnggotaKegiatan") !== false){
                $obj["anggota_kegiatan_id"] = getGUID();
                
                $anggotaKegiatan = new \Model\AnggotaKegiatan();
            }else{
                $anggotaKegiatan = \Model\AnggotaKegiatanQuery::create()->findOneByAnggotaKegiatanId($obj["anggota_kegiatan_id"]);
            }
            
            $obj["soft_delete"] = 0;
            
            $anggotaKegiatan->fromArray($obj, \Propel\Runtime\Map\TableMap::TYPE_FIELDNAME);
            
            if($anggotaKegiatan->save()){
                $recordUpdated++;
            }else{
                $recordFailed++;
            }
            
        }
        
        $return = array();
        $return['berhasil'] = $recordUpdated;
        $return['gagal'] = $recordFailed;

        return json_encode($return);
    }
    
    public function delete(Application $app, Request $request){
        $data = $request->get('data');
        $kegiatanId = $request->get('kegiatan_id');
        
        $objs = json_decode($data, true);
        
        $recordDeleted = 0;
        $recordFailed = 0;
        
        foreach ($objs as $obj){
            if(array_key_exists("anggota_kegiatan_id", $obj)){
                $anggota = \Model\AnggotaKegiatanQuery::create()->findOneByAnggotaKegiatanId($obj["anggota_kegiatan_id"]);
            }else{
                continue;
            }
            
            $anggota->delete();
            $recordDeleted++;
        }
        
        $return = array();
        $return['berhasil'] = $recordDeleted;
        $return['gagal'] = $recordFailed;

        return json_encode($return);
    }
}

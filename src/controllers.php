<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

//Request::setTrustedProxies(array('127.0.0.1'));

$app['debug'] = true;
$app->get('', function () use ($app) {
    $user = $app['session']->get('user');
    if($user['peranId'] == 53){
        return $app['twig']->render('landing-page/index.html', [
            'dir' => 'landing-page'
        ]);
    }

    $query = \Model\SyncLogQuery::create()
            ->filterByTableName('ptk')
            ->orderByCreateDate(\Propel\Runtime\ActiveQuery\Criteria::DESC)
            ->findOne();
    
    $lastUpdate = null;
    if($query) $lastUpdate = date_to_bahasa($query->getCreateDate ('Y-m-d'));
    
    return $app['twig']->render('index.html', array(
        'user' => $app['session']->get('user'),
        'lastUpdate' => $lastUpdate
    ));
})
->bind('homepage')
;

$app->get('emutasi', function () use ($app) {
    return $app->redirect('emutasi/index.html');
});

$app->get('epak', 'Epak\Epak::getPage');
$app->get('epak/cetak/{pak_id}', 'Penilaian\Penilaian::getCetakDraft');
$app->post('epak', 'Epak\Epak::save');

$app->get('login', function(Request $request) use ($app) {
//    if(is_null($app['session']->get('user'))){
        return $app['twig']->render('login/index.html', array(
            'error' => $app['security.last_error']($request),
            'last_username' => $app['session']->get('_security.last_username')
        ));
//    }else{
//        return $app->redirect('index.php');
//    }
});

$app->get('dashboard', 'Dashboard\Dashboard::fetchDashboard');
$app->get('dashboard/{module}', 'Dashboard\Dashboard::fetchDashboard');

$app->get('test', function(Request $request) use ($app) {
    $pengguna = Model\PenggunaQuery::create()->findOne();
    
    return print_r($pengguna);
});

$app->get('passgen/{pass}', function(Request $request) use ($app) {
    $password = $request->get('pass');
    return $app['security.encoder.digest']->encodePassword($password, '');
});

$app->get('{class}/{method}', function(Request $request) use ($app) {
    $class = $request->get('class');
    $method = $request->get('method');
    
    $class = "{$class}\\{$class}::{$method}";
    
    $class =  call_user_func($class, $app, $request);
    
    if(is_null($class)){
        return new Response('Bad Request', 400);
    }
    
    return $class;
});

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html.twig',
        'errors/'.substr($code, 0, 2).'x.html.twig',
        'errors/'.substr($code, 0, 1).'xx.html.twig',
        'errors/default.html.twig',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});

$app["cors-enabled"]($app);
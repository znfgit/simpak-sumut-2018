<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ptk;

use Model\MapelGroupQuery;
use Model\PtkQuery;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of Ptk
 *
 * @author faisaluje
 */
class Ptk {
    public function getData(Application $app, Request $request){
        $start = ($request->get('start')) ? $request->get('start') : 0;
        $limit = ($request->get('limit')) ? $request->get('limit') : 50;
        
        $query = \Model\PtkQuery::create();
        
        switch ($request->get('jenis')){
            case 'gurupns' :
                $query->filterByIsPns(1);
                $query->filterByJenisPtkId(array(3,4,5,6,12,13,14), \Propel\Runtime\ActiveQuery\Criteria::IN);
                $query->filterByJenisSekolahId(array(7,8,13,14,15,29), \Propel\Runtime\ActiveQuery\Criteria::IN);
                break;
            case 'gurunonpns' :
                $query->filterByIsPns(null, \Propel\Runtime\ActiveQuery\Criteria::ISNULL);
                $query->filterByJenisPtkId(array(3,4,5,6,12,13,14), \Propel\Runtime\ActiveQuery\Criteria::IN);
                $query->filterByJenisSekolahId(array(7,8,13,14,15,29), \Propel\Runtime\ActiveQuery\Criteria::IN);
                break;
            case 'tendik' :
                $query->filterByJenisPtkId(array(11,30,40,41,42,43,44), \Propel\Runtime\ActiveQuery\Criteria::IN);
                $query->filterByJenisSekolahId(array(7,8,13,14,15,29), \Propel\Runtime\ActiveQuery\Criteria::IN);
                break;
            case 'pengawas' :
                $query->filterByJenisPtkId(array(7,8,9,10,25,26), \Propel\Runtime\ActiveQuery\Criteria::IN);
                break;
            default : $query->filterByIsPns(1); break;
        }

        // Filter
        if($request->get('bentuk_pendidikan_id')) $query->filterByJenisSekolahId($request->get('bentuk_pendidikan_id'));
        if($request->get('jenis_ptk_id')) $query->filterByJenisPtkId($request->get('jenis_ptk_id'));
        if($request->get('status_kepegawaian_id')) $query->filterByStatusKepegawaianId($request->get('status_kepegawaian_id'));
        if($request->get('txtCari')){
            $query->addAnd(\Model\Map\PtkTableMap::COL_NAMA, "%".$request->get('txtCari')."%", \Propel\Runtime\ActiveQuery\Criteria::LIKE);
            $query->addOr(\Model\Map\PtkTableMap::COL_NUPTK, "%".$request->get('txtCari')."%", \Propel\Runtime\ActiveQuery\Criteria::LIKE);
            $query->addOr(\Model\Map\PtkTableMap::COL_NIP, "%".$request->get('txtCari')."%", \Propel\Runtime\ActiveQuery\Criteria::LIKE);
        }
        if($request->get('query')) $query->add(\Model\Map\PtkTableMap::COL_NAMA, "%".$request->get('query')."%", \Propel\Runtime\ActiveQuery\Criteria::LIKE);
        
        if($app["session"]->get('user')['sekolahId']) $query->filterBySekolahId($app["session"]->get('user')['sekolahId']);
        if($app["session"]->get('user')['level'] == 2){
            $query->filterByKecamatanId($app["session"]->get('user')['kodeWilayah']);
        }
        
        if($request->get('kegiatan_id')){
            $query->addJoin(\Model\Map\PtkTableMap::COL_PTK_ID, \Model\Map\AnggotaKegiatanTableMap::COL_PTK_ID, \Propel\Runtime\ActiveQuery\Criteria::LEFT_JOIN)
                    ->add(\Model\Map\AnggotaKegiatanTableMap::COL_ANGGOTA_KEGIATAN_ID, null, \Propel\Runtime\ActiveQuery\Criteria::ISNULL);
//                    ->add(\Model\Map\AnggotaKegiatanTableMap::COL_KEGIATAN_ID, $request->get('kegiatan_id'));
        }
        
        $count = $query->count();
        
        $query->setOffset($start);
        $query->setLimit($limit);
        
        $ptks = $query->find();
        
        return tableJson($ptks->toArray(null, false, \Propel\Runtime\Map\TableMap::TYPE_FIELDNAME), $count, array('ptk_id'));
    }
    
    public function getValidasiBkn(Application $app, Request $request){
        $ptkId = $request->get('ptk_id');
        
        $ptk = \Model\PtkQuery::create()->findOneByPtkId($ptkId);
        
        if(is_null($ptk)) return "No Data";
        
        $data = array();
        
        $data[] = array( //NIP
            "validas_id" => 1,
            "kolom" => "NIP",
            "dapodik" => $ptk->getNip(),
            "bkn" => $ptk->getNipBkn(),
            "status" => (trim($ptk->getNip()) == trim($ptk->getNipBkn())) ? 1 : 0
        );
        
        $data[] = array( //Nama
            "validas_id" => 2,
            "kolom" => "Nama",
            "dapodik" => $ptk->getNama(),
            "bkn" => $ptk->getNamaBkn(),
            "status" => (trim(strtolower($ptk->getNama())) == trim(strtolower($ptk->getNamaBkn()))) ? 1 : 0
        );
        
        $data[] = array( //Tgl Lahir
            "validas_id" => 3,
            "kolom" => "Tgl Lahir",
            "dapodik" => $ptk->getTglLahir('Y-m-d'),
            "bkn" => $ptk->getTglLahirBkn('Y-m-d'),
            "status" => ($ptk->getTglLahir('Y-m-d') == $ptk->getTglLahirBkn('Y-m-d')) ? 1 : 0
        );
        
        $data[] = array( //NIK
            "validas_id" => 4,
            "kolom" => "NIK",
            "dapodik" => $ptk->getNik(),
            "bkn" => $ptk->getNikBkn(),
            "status" => (trim($ptk->getNik()) == trim($ptk->getNikBkn())) ? 1 : 0
        );
        
        $jabatan = \Model\PangkatGolonganQuery::create()->findOneByPangkatGolonganId($ptk->getPangkatGolonganId());
        $jabatan = ($jabatan) ? $jabatan->getJabatanGuru() : '-';
        $data[] = array( //Jabatan
            "validas_id" => 5,
            "kolom" => "Jabatan",
            "dapodik" => $jabatan,
            "bkn" => $ptk->getJabatanBkn(),
            "status" => (trim(strtolower($ptk->getJabatanBkn())) == trim(strtolower($jabatan))) ? 1 : 0
        );
        
        $data[] = array( //TMT PNS
            "validas_id" => 1,
            "kolom" => "TMT PNS",
            "dapodik" => $ptk->getTmtPns('Y-m-d'),
            "bkn" => $ptk->getTmtPnsBkn('Y-m-d'),
            "status" => ($ptk->getTmtPns('Y-m-d') == $ptk->getTmtPnsBkn('Y-m-d')) ? 1 : 0
        );
        
        return tableJson($data, count($data), $id);
    }
    
    public function getExcel(Application $app, Request $request){
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 1000);

        $sekolahId = $request->get('sekolah_id');
        $title = $request->get('title');
        $isPns = $request->get('is_pns');
        $kabupatenKotaSekolahId= $request->get('kabupaten_kota_sekolah_id');
        $jenisPtkId = $request->get('jenis_ptk_id');

        $dataHeaderArr =array(
            'nama'=>'Nama',
            'nama_pangkat_golongan'=>'Nama Pangkat Golongan',
            'tmt_pangkat_gol'=>'Tmt Pangkat Gol',
            'nip'=>'Nip',
            'nuptk'=>'Nuptk',
            'karpeg'=>'Karpeg',
            'NRG'=>'NRG',
            'nik'=>'Nik',
            'No_peserta_UKG'=>'No Peserta UKG',
            'jenis_kelamin'=>'Jenis Kelamin',
            'tempat_lahir'=>'Tempat Lahir',
            'tgl_lahir'=>'Tgl Lahir',
            'nama_ibu_kandung'=>'Nama Ibu Kandung',
            'Jenis_Guru'=>'Jenis Guru',
            'nama_status_kepegawaian'=>'Nama Status Kepegawaian',
            'nama_sekolah'=>'Nama Sekolah',
            'lulus_sertifikasi'=>'Lulus Sertifikasi',
            'status_dokumen_sktp'=>'Status Dokumen Sktp',
            'tgl_pensiun'=>'Tgl Pensiun',
            'tahun_ijazah_terakhir'=>'Tahun Ijazah Terakhir',
            'alamat_jalan'=>'Alamat Jalan',
            'nama_desa_kelurahan'=>'Nama Desa Kelurahan',
            'rt'=>'Rt',
            'rw'=>'Rw',
            'kode_pos'=>'Kode Pos',
            'no_hp'=>'No Hp',
            'email'=>'Email',
            'tmt_pns'=>'Tmt Pns',
            'nomor_sk_pengangkatan'=>'Nomor Sk Pengangkatan',
            'tmt_pengangkatan'=>'Tmt Pengangkatan',
            'nama_ijazah_terakhir'=>'Nama Ijazah Terakhir',
            'nama_jenis_sekolah'=>'Nama Jenis Sekolah',
            'Nama_Kecamatan'=>'Nama Kecamatan',
            'nama_kabupaten_kota'=>'Nama Kabupaten Kota',
            'nama_propinsi'=>'Nama Propinsi',
            'nama_tugas_tambahan'=>'Nama Tugas Tambahan',
            'tmt_tugas_tambahan'=>'Tmt Tugas Tambahan',
            'sk_tugas_tambahan'=>'Sk Tugas Tambahan',
            'Gaji_Pokok'=>'Gaji Pokok',
            'Dasar_penetapan'=>'Dasar Penetapan',
            'Nama_Sumber_Gaji'=>'Nama Sumber Gaji',
            'satuan_pendidikan_formal'=>'Satuan Pendidikan Formal',
            'NIM'=>'NIM',
            'fakultas'=>'Fakultas',
            'mapel_diajarkan'=>'Mapel Diajarkan');

        $sql = "SELECT
                                nama,
                                CASE
                            WHEN jenis_kelamin = 1 THEN
                                'Laki-Laki'
                            ELSE
                                'Perempuan'
                            END as 'jenis_kelamin'
                            ";
        foreach ($dataHeaderArr as $key => $val) {
            if($key!="nama" || $key!="jenis_kelamin"){
                $sql.=",$key";
            }
        }

        $sql.=" from ptk WHERE kabupaten_kota_sekolah_id='116000' ";

        if(!empty($sekolahId)){
            $sql.=" and sekolah_id='$sekolahId'";
        }
        if($kabupatenKotaSekolahId){
            $sql.=" and kabupaten_kota_sekolah_id=".$kabupatenKotaSekolahId;
        }
        
        if($isPns == 1){
            $sql.=" and is_pns=".$isPns;
        }else if($isPns == 0 && !is_null($isPns)){
            $sql.=" and is_pns is null";
        }
        
        if($jenisPtkId){
            $jenisPtkId = json_decode($jenisPtkId);
                        
            $sql .= " and jenis_ptk_id in(".implode(',', $jenisPtkId).")";
        }
        
//        print_r($sql);die;
        $data = getDataBySql($sql);
        $fileName = "Data ".$title." per ".date('ymdHis').".xls";
        header('Content-Description: File Transfer');
        header('Content-Type: application/excel');
        header('Content-Disposition: inline; filename='.$fileName);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        echo "<h3>Data ".$title."</h3>Per Tanggal : ".date_to_bahasa(date('Y-m-d'))."<br><br>";
        echo "<table border=1><thead><tr>";
        echo "<th> <b>No.</b> </th>";
            foreach($dataHeaderArr as $key => $val){
                echo "<th> <b>$val</b> </th>";
            }
        echo "</tr></thead><tbody>";

        $no = 1;
        foreach($data as $row){
            $row= (object)$row;
            echo "<tr>";
            echo "<td> &nbsp;".htmlspecialchars($no, ENT_QUOTES)."</td>";
            foreach($dataHeaderArr as $key => $val){
                echo "<td> &nbsp;".htmlspecialchars($row->$key, ENT_QUOTES)."</td>";
            }
            echo "</tr>";
            $no++;
        }
        echo "</tbody></table>";
        return exit;
    }
    
    public function getTProfesi(Application $app, Request $request){
        $kabKotaId = $request->get('kabupaten_kota_id');
        $nuptk = $request->get('nuptk');

        $data = file_get_contents("http://223.27.144.198:8081/Aplikasi/getTNew.php/{$kabKotaId}/{$nuptk}");
        $data = str_replace("==9USUsiO==", "", $data);
        $data = base64_decode($data);
        $data = json_decode($data, true);
        
        if(is_null($data)) return new Response('Data tidak ditemukan', 400);
        
        
        // Apply custom placed template
        $sourceTplDir = dirname(__FILE__) . "/../Templates";
        $fileName = "tprofesi.twig";

        $loader = new \Twig_Loader_Filesystem($sourceTplDir);
        $twig = new \Twig_Environment($loader, array(
            "debug" => true
        ));
        $twig->addExtension(new \Twig_Extension_Debug());

        $outStr = $twig->render($fileName, $data[0]);

        return $outStr;
    }
    
    public function getInpassing(Application $app, Request $request){
        $nuptk = $request->get('nuptk');

        $data = file_get_contents('http://223.27.144.204/data/'.$nuptk);
        $data = json_decode($data, true);

        // return print_r($data["guru"]);

        if(is_null($data)){
            return "Datai tidak ditemukan.";
        }

        // Apply custom placed template
        $sourceTplDir = dirname(__FILE__) . "/../Templates";
        $fileName = "inpassing.twig";

        $loader = new \Twig_Loader_Filesystem($sourceTplDir);
        $twig = new \Twig_Environment($loader, array(
            "debug" => true
        ));
        $twig->addExtension(new \Twig_Extension_Debug());

        $outStr = $twig->render($fileName, $data["guru"]);

        return $outStr;
    }

    public function getPtk(Application $app) {
        if (is_null($app["session"]->get('user')['ptkId'])) {
            return new Response('', 400);
        }

        $ptk = PtkQuery::create()->findOneByPtkId($app["session"]->get('user')['ptkId']);
        if (!$ptk) {
            return new Response('', 400);
        }

        return new JsonResponse($ptk->toArray());
    }
}

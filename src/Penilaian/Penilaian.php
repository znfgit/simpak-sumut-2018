<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Penilaian;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of Penilaian
 *
 * @author faisaluje
 */
class Penilaian {
    public function getDataForm(Application $app, Request $request){
        $tipe  = $request->get('tipe');
        $data = array();
        
        if(is_null($tipe)) return '{ "success" : false, "msg" : "Gagal Mengambil data" }';
                
        if($tipe == 1){
            $ptkId = $request->get('ptk_id');
            $pak = \Model\PakQuery::create()->findOneByPtkId($ptkId);
            $ptk = \Model\PtkQuery::create()->findOneByPtkId($ptkId);
        }else{
            $pegawaiId = $request->get('pegawai_id');
            $pak = \Model\PakQuery::create()->findOneByPegawaiId($pegawaiId);
            $pegawai = \Model\PegawaiQuery::create()->findOneByPegawaiId($pegawaiId);
        }
        
        if(is_object($pak)){
            $arr = $pak->toArray();
        }else{
            $arr["pak_id"] = null;
            $arr["nomor"] = "-";
            $arr["soft_delete"] = 0;
            if($tipe == 1){
                $arr["ptk_id"] = $ptk->getPtkId();
                $arr["pangkat_golongan_id"] = $ptk->getPangkatGolonganId();
                $arr["tmt_pangkat"] = $ptk->getTmtPangkatGol('Y-m-d');
                $arr["jabatan"] = $ptk->getJenisGuru();
                $arr["tmt_jabatan"] = $ptk->getTmtPengangkatan('Y-m-d');
                $arr["pendidikan"] = $ptk->getNamaIjazahTerakhir();
                $arr["status_pengusulan"] = 1;
                $arr["periode_pengusulan"] = getbulan(date('n'))." ".date('Y');
            }else{
                $arr["pegawai_id"] = $pegawai->getPegawaiId();
                $arr["pangkat_golongan_id"] = $pegawai->getPangkatGolonganId();
                $arr["tmt_pangkat"] = $pegawai->getTmtPangkat('Y-m-d');
                $arr["jabatan"] = $pegawai->getJabatan();
                $arr["tmt_jabatan"] = null;
                $arr["pendidikan"] = $pegawai->getJenjangPendidikan();
                $arr["status_pengusulan"] = 7;
                $arr["periode_pengusulan"] = getbulan(date('n'))." ".date('Y');
            }
        }
        
        if($tipe == 1){
            $arr["nama"] = $ptk->getNama();
            $arr["nip"] = $ptk->getNip();
            $arr["nama_pangkat_golongan"] = $ptk->getPangkatGolongan()->getNama();
        }else{
            $arr["nama"] = $pegawai->getNama();
            $arr["nip"] = $pegawai->getNip();
            $arr["nama_pangkat_golongan"] = $pegawai->getPangkatGolongan()->getNama();
        }
        $arr["tmt_pangkat_display"] = $arr["tmt_pangkat"];
        $arr["nomor_display"] = $arr["nomor"];
        
        $data["success"] = true;
        $data["data"] = $arr;
        
        return json_encode($data);
    }
    
    public function saveForm(Application $app, Request $request){
        $data = $_REQUEST;
        $pakId = $request->get('pak_id');
        
        $pak = \Model\PakQuery::create()->findOneByPakId($pakId);
        
        if(is_null($pak)){
            $pak = new \Model\Pak();
            $data["pak_id"] = getGUID();
            $data["status_pengusulan"] = ($data["ptk_id"] == "") ? 7 : 1;
        }
        
        if($data["nomor"] == "") $data["nomor"] = "- Dibuatkan Dinas -";
        if($data["periode_penilaian_id"] == "") $data["periode_penilaian_id"] = null;
        if($data["penandatangan_id"] == "") $data["penandatangan_id"] = null;
        
        $data["soft_delete"] = 0;
        if($data["ptk_id"] == "") unset ($data["ptk_id"]);
        if($data["pegawai_id"] == "") unset ($data["pegawai_id"]);
        
        $pak->fromArray($data);
        
//        print_r($pak->toArray());die;
        
        if($pak->save()){
            return "{ 'success' : true, 'msg' : 'Berhasil Menyimpan', 'pak_id' : '".$pak->getPakId()."' }";
        }else{
            return '{ "success" : false, "msg" : "Gagal Menyimpan" }';
        }
    }
    
    public function getDataPakTree(Application $app, Request $request){
        $pakId = $request->get('pak_id');
        
        $pak = \Model\PakQuery::create()->findOneByPakId($pakId);
        
        if(is_null($pak)) return new \Symfony\Component\HttpFoundation\Response(" { 'success' : false, 'msg' : 'Kelengkapan SK belum disimpan'} ", 400);
        
        $penunjangLama = $pak->getPenunjangALama()+$pak->getPenunjangBLama()+$pak->getPenunjangCLama();
        $penunjangUsulan = $pak->getPenunjangAUsulan()+$pak->getPenunjangBUsulan()+$pak->getPenunjangCUsulan();
        $penunjangPenilai = $pak->getPenunjangAPenilai()+$pak->getPenunjangBPenilai()+$pak->getPenunjangCPenilai();
        $penunjangFinal = $pak->getPenunjangAFinal()+$pak->getPenunjangBFinal()+$pak->getPenunjangCFinal();
        
        $utamaCLama = $pak->getUtamaC1Lama()+$pak->getUtamaC2Lama()+$pak->getUtamaC3Lama();
        $utamaCUsulan = $pak->getUtamaC1Usulan()+$pak->getUtamaC2Usulan()+$pak->getUtamaC3Usulan();
        $utamaCPenilai = $pak->getUtamaC1Penilai()+$pak->getUtamaC2Penilai()+$pak->getUtamaC3Penilai();
        $utamaCFinal = $pak->getUtamaC1Final()+$pak->getUtamaC2Final()+$pak->getUtamaC3Final();
        
        $utamaBLama = $pak->getUtamaB1Lama()+$pak->getUtamaB2Lama()+$pak->getUtamaB3Lama();
        $utamaBUsulan = $pak->getUtamaB1Usulan()+$pak->getUtamaB2Usulan()+$pak->getUtamaB3Usulan();
        $utamaBPenilai = $pak->getUtamaB1Penilai()+$pak->getUtamaB2Penilai()+$pak->getUtamaB3Penilai();
        $utamaBFinal = $pak->getUtamaB1Final()+$pak->getUtamaB2Final()+$pak->getUtamaB3Final();
        
        $utamaALama = $pak->getUtamaA1Lama()+$pak->getUtamaA2Lama();
        $utamaAUsulan = $pak->getUtamaA1Usulan()+$pak->getUtamaA2Usulan();
        $utamaAPenilai = $pak->getUtamaA1Penilai()+$pak->getUtamaA2Penilai();
        $utamaAFinal = $pak->getUtamaA1Final()+$pak->getUtamaA2Final();
        
        $utamaLama = $utamaALama+$utamaBLama+$utamaCLama;
        $utamaUsulan = $utamaAUsulan+$utamaBUsulan+$utamaCUsulan;
        $utamaPenilai = $utamaAPenilai+$utamaBPenilai+$utamaCPenilai;
        $utamaFinal = $utamaAFinal+$utamaBFinal+$utamaCFinal;
        
        $unsurLama = $utamaLama+$penunjangLama;
        $unsurUsulan= $utamaUsulan+$penunjangUsulan;
        $unsurPenilai = $utamaPenilai+$penunjangPenilai;
        $unsurFinal = $utamaFinal+$penunjangFinal;
        
        $uraian = '{
            "id": "root",
            "pak_id": "'.$pak->getPakId().'",
            "uraian": "root",
            "parentId": null,
            "leaf": false,
            "children": [
                {
                    "id": "unsur",
                    "pak_id": "'.$pak->getPakId().'",
                    "uraian": "Unsur Yang Dinilai",
                    "ak_lama": '.$unsurLama.',
                    "ak_usulan": '.$unsurUsulan.',
                    "ak_penilai": '.$unsurPenilai.',
                    "ak_final": '.$unsurFinal.',
                    "parentId": "root",
                    "leaf": false,
                    "children": [
                        {
                            "id": "utama",
                            "pak_id": "'.$pak->getPakId().'",
                            "uraian": "Unsur Utama",
                            "ak_lama": '.$utamaLama.',
                            "ak_usulan": '.$utamaUsulan.',
                            "ak_penilai": '.$utamaPenilai.',
                            "ak_final": '.$utamaFinal.',
                            "parentId": "unsur",
                            "leaf": false,
                            "children": [
                                {
                                    "id": "utama_a",
                                    "pak_id": "'.$pak->getPakId().'",
                                    "uraian": "Pendidikan",
                                    "ak_lama": '.$utamaALama.',
                                    "ak_usulan": '.$utamaAUsulan.',
                                    "ak_penilai": '.$utamaAPenilai.',
                                    "ak_final": '.$utamaAFinal.',
                                    "parentId": "utama",
                                    "leaf": false,
                                    "children": [
                                        {
                                            "id": "utama_a1",
                                            "pak_id": "'.$pak->getPakId().'",
                                            "uraian": "Pendidikan Sekolah",
                                            "ak_lama": '.$pak->getUtamaA1Lama().',
                                            "ak_usulan": '.$pak->getUtamaA1Usulan().',
                                            "ak_penilai": '.$pak->getUtamaA1Penilai().',
                                            "ak_final": '.$pak->getUtamaA1Final().',
                                            "parentId": "utama_a",
                                            "leaf": true
                                        },
                                        {
                                            "id": "utama_a2",
                                            "pak_id": "'.$pak->getPakId().'",
                                            "uraian": "Pelatihan Prajabatan",
                                            "ak_lama": '.$pak->getUtamaA2Lama().',
                                            "ak_usulan": '.$pak->getUtamaA2Usulan().',
                                            "ak_penilai": '.$pak->getUtamaA2Penilai().',
                                            "ak_final": '.$pak->getUtamaA2Final().',
                                            "parentId": "utama_a",
                                            "leaf": true
                                        }
                                    ]
                                },
                                {
                                    "id": "utama_b",
                                    "pak_id": "'.$pak->getPakId().'",
                                    "uraian": "Pembelajaran / bimbingan dan tugas tertentu",
                                    "ak_lama": '.$utamaBLama.',
                                    "ak_usulan": '.$utamaBUsulan.',
                                    "ak_penilai": '.$utamaBPenilai.',
                                    "ak_final": '.$utamaBFinal.',
                                    "parentId": "utama",
                                    "leaf": false,
                                    "children": [
                                        {
                                            "id": "utama_b1",
                                            "pak_id": "'.$pak->getPakId().'",
                                            "uraian": "Proses Pembelajaran",
                                            "ak_lama": '.$pak->getUtamaB1Lama().',
                                            "ak_usulan": '.$pak->getUtamaB1Usulan().',
                                            "ak_penilai": '.$pak->getUtamaB1Penilai().',
                                            "ak_final": '.$pak->getUtamaB1Final().',
                                            "parentId": "utama_b",
                                            "leaf": true
                                        },
                                        {
                                            "id": "utama_b2",
                                            "pak_id": "'.$pak->getPakId().'",
                                            "uraian": "Proses Bimbingan",
                                            "ak_lama": '.$pak->getUtamaB2Lama().',
                                            "ak_usulan": '.$pak->getUtamaB2Usulan().',
                                            "ak_penilai": '.$pak->getUtamaB2Penilai().',
                                            "ak_final": '.$pak->getUtamaB2Final().',
                                            "parentId": "utama_b",
                                            "leaf": true
                                        },
                                        {
                                            "id": "utama_b3",
                                            "pak_id": "'.$pak->getPakId().'",
                                            "uraian": "Tugas Lainnya",
                                            "ak_lama": '.$pak->getUtamaB3Lama().',
                                            "ak_usulan": '.$pak->getUtamaB3Usulan().',
                                            "ak_penilai": '.$pak->getUtamaB3Penilai().',
                                            "ak_final": '.$pak->getUtamaB3Final().',
                                            "parentId": "utama_b",
                                            "leaf": true
                                        }
                                    ]
                                },
                                {
                                    "id": "utama_c",
                                    "pak_id": "'.$pak->getPakId().'",
                                    "uraian": "Pengembangan Keprofesian Berkelanjutan",
                                    "ak_lama": '.$utamaCLama.',
                                    "ak_usulan": '.$utamaCUsulan.',
                                    "ak_penilai": '.$utamaCPenilai.',
                                    "ak_final": '.$utamaCFinal.',
                                    "parentId": "utama",
                                    "leaf": false,
                                    "children": [
                                        {
                                            "id": "utama_c1",
                                            "pak_id": "'.$pak->getPakId().'",
                                            "uraian": "Pengembangan diri",
                                            "ak_lama": '.$pak->getUtamaC1Lama().',
                                            "ak_usulan": '.$pak->getUtamaC1Usulan().',
                                            "ak_penilai": '.$pak->getUtamaC1Penilai().',
                                            "ak_final": '.$pak->getUtamaC1Final().',
                                            "parentId": "utama_c",
                                            "leaf": true
                                        },
                                        {
                                            "id": "utama_c2",
                                            "pak_id": "'.$pak->getPakId().'",
                                            "uraian": "Publikasi ilmiah",
                                            "ak_lama": '.$pak->getUtamaC2Lama().',
                                            "ak_usulan": '.$pak->getUtamaC2Usulan().',
                                            "ak_penilai": '.$pak->getUtamaC2Penilai().',
                                            "ak_final": '.$pak->getUtamaC2Final().',
                                            "parentId": "utama_c",
                                            "leaf": true
                                        },
                                        {
                                            "id": "utama_c3",
                                            "pak_id": "'.$pak->getPakId().'",
                                            "uraian": "Karya inovatif",
                                            "ak_lama": '.$pak->getUtamaC3Lama().',
                                            "ak_usulan": '.$pak->getUtamaC3Usulan().',
                                            "ak_penilai": '.$pak->getUtamaC3Penilai().',
                                            "ak_final": '.$pak->getUtamaC3Final().',
                                            "parentId": "utama_c",
                                            "leaf": true
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "penunjang",
                            "pak_id": "'.$pak->getPakId().'",
                            "uraian": "Unsur Penunjang",
                            "ak_lama": '.$penunjangLama.',
                            "ak_usulan": '.$penunjangUsulan.',
                            "ak_penilai": '.$penunjangPenilai.',
                            "ak_final": '.$penunjangFinal.',
                            "parentId": "unsur",
                            "leaf": false,
                            "children": [
                                {
                                    "id": "penunjang_a",
                                    "pak_id": "'.$pak->getPakId().'",
                                    "uraian": "Ijazah yang tidak sesuai",
                                    "ak_lama": '.$pak->getPenunjangALama().',
                                    "ak_usulan": '.$pak->getPenunjangAUsulan().',
                                    "ak_penilai": '.$pak->getPenunjangAPenilai().',
                                    "ak_final": '.$pak->getPenunjangAFinal().',
                                    "parentId": "penunjang",
                                    "leaf": true
                                },
                                {
                                    "id": "penunjang_b",
                                    "pak_id": "'.$pak->getPakId().'",
                                    "uraian": "Pendukung tugas guru",
                                    "ak_lama": '.$pak->getPenunjangBLama().',
                                    "ak_usulan": '.$pak->getPenunjangBUsulan().',
                                    "ak_penilai": '.$pak->getPenunjangBPenilai().',
                                    "ak_final": '.$pak->getPenunjangBFinal().',
                                    "parentId": "penunjang",
                                    "leaf": true
                                },
                                {
                                    "id": "penunjang_c",
                                    "pak_id": "'.$pak->getPakId().'",
                                    "uraian": "Memperoleh penghargaan",
                                    "ak_lama": '.$pak->getPenunjangCLama().',
                                    "ak_usulan": '.$pak->getPenunjangCUsulan().',
                                    "ak_penilai": '.$pak->getPenunjangCPenilai().',
                                    "ak_final": '.$pak->getPenunjangCFinal().',
                                    "parentId": "penunjang",
                                    "leaf": true
                                }
                            ]
                        }
                    ]
                }
            ]
        }';
        
        return $uraian;
    }
    
    public function saveTreeGrid(Application $app, Request $request){
        $datas = $request->get('data');
        if(is_null($datas)) return '{ "success" : false, "msg" : "Gagal Menyimpan, PTK tidak ditemukan" }';
        
        $datas = json_decode($datas, true);
        $pakId = $datas[0]["pak_id"];
        
        $pak = \Model\PakQuery::create()->findOneByPakId($pakId);
        if(is_null($pak)) return '{ "success" : false, "msg" : "Gagal Menyimpan, Kelengkapan data tidak ditemukan" }';
        
        $arr = array();
        foreach($datas as $data){
            $id = $data["id"];
            
            $arr[$id."_lama"] = $data["ak_lama"];
            $arr[$id."_usulan"] = $data["ak_usulan"];
            $arr[$id."_penilai"] = ($pak->getStatusPengusulan() > 1) ? $data["ak_penilai"] : $data["ak_usulan"];
            $arr[$id."_final"] = $arr[$id."_lama"]+$arr[$id."_penilai"];
        }
        
        $pak->fromArray($arr);
        
        if($pak->save()){
            return "{ 'success' : true, 'msg' : 'Berhasil Menyimpan' }";
        }else{
            return '{ "success" : false, "msg" : "Gagal Menyimpan, Hubungi Admin" }';
        }
    }
    
    public function getCetakPak(Application $app, Request $request){
        if(in_array($app["session"]->get('user')['level'], array(1, 2))){
            return Penilaian::getCetakDraft($app, $request);
        }else{
            return Penilaian::getCetakSk($app, $request);
        }
    }
    
    public function getCetakSk(Application $app, Request $request){
        $pakId = $request->get('pak_id');

        $pak = \Model\PakQuery::create()->findOneByPakId($pakId);
        if($pak->getPtkId()){
            $ptk = $pak->getPtk();
        }else{
            $pegawai = $pak->getPegawai();
        }
        

        // Apply custom placed template
        $sourceTplDir = dirname(__FILE__) . "/../Templates";
        $fileName = "pak.twig";

        $loader = new \Twig_Loader_Filesystem($sourceTplDir);
        $twig = new \Twig_Environment($loader, array(
            "debug" => true
        ));
        $twig->addExtension(new \Twig_Extension_Debug());

        $arrData=  ($ptk) ? $ptk->toArray() : $pegawai->toArray();
        $arrPak =  $pak->toArray();
        $arrperbaikan['nama_pangkat_golongan_full'] = "aaaa";// $pak->getPangkatGolongan()->getNama();
        $arrperbaikan['nama_jabatan'] = $pak->getJabatan();
        $arrperbaikan['nama_pejabat'] = $pak->getPenandatangan()->getNama();
        $arrperbaikan['nip_pejabat'] = $pak->getPenandatangan()->getNip();
        $arrperbaikan['nama_jabatan'] = $pak->getJabatan();

        $arrperbaikan['tanggal_penetapan'] = date_to_bahasa($pak->getTglPenetapan());

        $arrperbaikan['masa_penilaian_awal'] = date_to_bahasa($pak->getPeriodePenilaian()->getTanggalMulai());
        $arrperbaikan['masa_penilaian_akhir'] = date_to_bahasa($pak->getPeriodePenilaian()->getTanggalAkhir());

        $tmtPangkat = new \DateTime($pak->getTmtPangkat());
        $tglPenetapan = new \DateTime($pak->getTglPenetapan());

        $arrperbaikan['masa_kerja_golongan_lama']= $tmtPangkat->diff($tglPenetapan)->format('%y tahun %m bulan');
        $arrperbaikan['masa_kerja_golongan_baru']= $tmtPangkat->diff($tglPenetapan)->format('%y tahun %m bulan');


        // Attach data and render
        $data = $arrData+$arrPak+$arrperbaikan;
        $data["tmt_jabatan"] = date_to_bahasa($pak->getTmtJabatan());
        $data["tmt_pangkat"] = date_to_bahasa($pak->getTmtPangkat());
        $data["tgl_lahir"] = date_to_bahasa($data["tgl_lahir"]);
//                print_r($data);die;

        $outStr = $twig->render($fileName, $data);

        return $outStr;
    }
    
    public function getCetakDraft(Application $app, Request $request){
        $pakId = $request->get('pak_id');

        $pak = \Model\PakQuery::create()->findOneByPakId($pakId);
        $ptk = $pak->getPtk();
        
        $kepsek = \Model\PtkQuery::create()
                ->filterByTugasTambahanId(2)
                ->filterBySekolahId($ptk->getSekolahId())
                ->findOne();

        // Apply custom placed template
        $sourceTplDir = dirname(__FILE__) . "/../Templates";
        $fileName = "pak_draft.twig";

        $loader = new \Twig_Loader_Filesystem($sourceTplDir);
        $twig = new \Twig_Environment($loader, array(
            "debug" => true
        ));
        $twig->addExtension(new \Twig_Extension_Debug());

        $arrPtk=  $ptk->toArray();
        $arrPak =  $pak->toArray();
        $arrperbaikan['nama_pangkat_golongan_full'] = "-";// $pak->getPangkatGolongan()->getNama();
        $arrperbaikan['nama_jabatan'] = $pak->getJabatan();
        $arrperbaikan['nama_pejabat'] = $kepsek->getNama();
        $arrperbaikan['nip_pejabat'] = $kepsek->getNip();

        $arrperbaikan['tanggal_penetapan'] = date_to_bahasa(date('Y-m-d'));

        $tmtPangkat = new \DateTime($pak->getTmtPangkat());
        $tglPenetapan = new \DateTime($pak->getTglPenetapan());

        $arrperbaikan['masa_kerja_golongan_lama']= $tmtPangkat->diff($tglPenetapan)->format('%y tahun %m bulan');
        $arrperbaikan['masa_kerja_golongan_baru']= $tmtPangkat->diff($tglPenetapan)->format('%y tahun %m bulan');


        // Attach data and render
        $data = $arrPtk+$arrPak+$arrperbaikan;
        $data["tmt_jabatan"] = date_to_bahasa($pak->getTmtJabatan());
        $data["tmt_pangkat"] = date_to_bahasa($pak->getTmtPangkat());
        $data["tgl_lahir"] = date_to_bahasa($ptk->getTglLahir());
//                print_r($data);die;

        $outStr = $twig->render($fileName, $data);

        return $outStr;
    }
}

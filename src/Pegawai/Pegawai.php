<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pegawai;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of Pegawai
 *
 * @author faisaluje
 */
class Pegawai {
    public function getData(Application $app, Request $request){
        $start = ($request->get('start')) ? $request->get('start') : 0;
        $limit = ($request->get('limit')) ? $request->get('limit') : 50;
        
        $query = \Model\PegawaiQuery::create()
                ->filterBySoftDelete(0)
                ->addAscendingOrderByColumn(\Model\Map\PegawaiTableMap::COL_NAMA);
        
        if($request->get('txtCari')){
            $query->addAnd(\Model\Map\PegawaiTableMap::COL_NAMA, '%'.$request->get('txtCari').'%', \Propel\Runtime\ActiveQuery\Criteria::LIKE)
                    ->addOr(\Model\Map\PegawaiTableMap::COL_NIP, '%'.$request->get('txtCari').'%', \Propel\Runtime\ActiveQuery\Criteria::LIKE);
        }
        if($request->get('query')) $query->add(\Model\Map\PegawaiTableMap::COL_NAMA, "%".$request->get('query')."%", \Propel\Runtime\ActiveQuery\Criteria::LIKE);
        
        $count = $query->count();
        
        $query->offset($start)
              ->limit($limit);
        
        $pegawai = $query->find()->toArray(null, false, \Propel\Runtime\Map\TableMap::TYPE_FIELDNAME);
        
        return tableJson($pegawai, $count, array('pegawai_id'), $start, $limit);
    }
    
    public function save(Application $app, Request $request){
        $data = $request->get('data');
        
        $objs = json_decode($data, true);
        
        $recordUpdated = 0;
        $recordFailed = 0;
        
        foreach ($objs as $obj){
            
            if(strpos($obj["pegawai_id"], "SimpegGundul.model.Pegawai") !== false){
                $obj["pegawai_id"] = getGUID();
                
                $pegawai = new \Model\Pegawai();
                                
            }else{
                $pegawai = \Model\PegawaiQuery::create()->findOneByPegawaiId($obj["pegawai_id"]);
            }
            
            $pegawai->fromArray($obj, \Propel\Runtime\Map\TableMap::TYPE_FIELDNAME);

            if($pegawai->save()){
                $recordUpdated++;
            }else{
                $recordFailed++;
            }
            
        }
        
        $return = array();
        $return['berhasil'] = $recordUpdated;
        $return['gagal'] = $recordFailed;

        return json_encode($return);
    }
    
    public function delete(Application $app, Request $request){
        $id = $request->get('id');
        
        $pegawai = \Model\PegawaiQuery::create()->findOneByPegawaiId($id);
        
        if(is_null($pegawai)) return new Response('Data tidak ditemukan', 400);
        
        $pegawai->setSoftDelete(1);
        
        if($pegawai->save()){
            return " { 'success' : true, 'msg' : 'Data berhasil dihapus' } ";
        }else{
            return " { 'success' : false, 'msg' : 'Data gagal dihapus' } ";
        }
    }
    
    public function getExcel(Application $app, Request $request){
        $query = \Model\PegawaiQuery::create()
                ->filterBySoftDelete(0)
                ->orderByNama();
        
        $pegawais = $query->find();
        $columns = \Model\Map\PegawaiTableMap::getFieldNames(\Propel\Runtime\Map\TableMap::TYPE_FIELDNAME);
        $columns[11] = "pangkat / golongan";
        unset($columns[0]);
        unset($columns[15]);
        unset($columns[16]);
        
        
        $fileName = "Data Pegawai per ".date('ymdHis').".xls";
        header('Content-Description: File Transfer');
        header('Content-Type: application/excel');
        header('Content-Disposition: inline; filename='.$fileName);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        echo "<h3>Data Pegawai</h3>Per Tanggal : ".date_to_bahasa(date('Y-m-d'))."<br><br>";
        echo "<table border=1><thead><tr>";
        echo "<th> <b>No.</b> </th>";
        
        foreach($columns as $column){
            $column = str_replace("_", " ", $column);
            $column = ucwords($column);
            echo "<th> <b>$column</b> </th>";
        }
        
        echo "</tr></thead><tbody>";

        $no = 1;
        foreach($pegawais as $pegawai){
            $row = $pegawai->toArray();
            $pangkatGolongan = ($pegawai->getPangkatGolonganId()) ? $pegawai->getPangkatGolongan()->getNama() : null;
            
            echo "<tr>";
            echo "<td> &nbsp;".htmlspecialchars($no, ENT_QUOTES)."</td>";
            foreach($columns as $key => $val){
                if($key == 11){
                    echo "<td> &nbsp;".htmlspecialchars($pangkatGolongan, ENT_QUOTES)."</td>";
                }else{
                    echo "<td> &nbsp;".htmlspecialchars($row[$val], ENT_QUOTES)."</td>";
                }
                
            }
            echo "</tr>";
            $no++;
        }
        echo "</tbody></table>";
        return exit;
    }
}

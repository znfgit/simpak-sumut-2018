<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Peran;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of Peran
 *
 * @author faisaluje
 */
class Peran {
    public function getData(Application $app, Request $request){
        $start = ($request->get('start')) ? $request->get('start') : 0;
        $limit = ($request->get('limit')) ? $request->get('limit') : 50;
        
        $query = \Model\PeranQuery::create()
                ->filterByPeranId(array(1,57,91,92,93,94,95,96), \Propel\Runtime\ActiveQuery\Criteria::IN);
        
        if($request->get('query')) $query->filterByNama('%'.$request->get('query')."%", \Propel\Runtime\ActiveQuery\Criteria::LIKE);
        
        $query->offset($start)->limit($limit);
        
        $count = $query->count();
        $data = $query->find();
        
        if(is_null($data)) return tableJson(array(), 0, array('id'));
        
        $outArr = $data->toArray(null, false, \Propel\Runtime\Map\TableMap::TYPE_FIELDNAME);
        
        return tableJson($outArr, $count, array('peran_id'), $start, $limit);
    }
}

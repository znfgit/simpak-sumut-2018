<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PeriodePenilaian;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of PeriodePenilaian
 *
 * @author faisaluje
 */
class PeriodePenilaian {
    public function getData(Application $app, Request $request){
        $start = ($request->get('start')) ? $request->get('start') : 0;
        $limit = ($request->get('limit')) ? $request->get('limit') : 50;
        
        $query = \Model\PeriodePenilaianQuery::create();
        
        if($request->get('query')) $query->filterByNama('%'.$request->get('query')."%", \Propel\Runtime\ActiveQuery\Criteria::LIKE);
        
        $query->offset($start)->limit($limit);
        
        $count = $query->count();
        $data = $query->find();
        
        return tableJson($data->toArray(null, false, \Propel\Runtime\Map\TableMap::TYPE_FIELDNAME), $count, array('periode_penilaian_id'), $start, $limit);
    }
    
    public function save(Application $app, Request $request){
        $data = $request->get('data');
        
        $objs = json_decode($data, true);
        
        $recordUpdated = 0;
        $recordFailed = 0;
        
        foreach ($objs as $obj){
            $periode = \Model\PeriodePenilaianQuery::create()->findOneByPeriodePenilaianId($obj["periode_penilaian_id"]);
            if(is_null($periode)){
                $periode = new \Model\PeriodePenilaian();
            }
            
            $periode->fromArray($obj, \Propel\Runtime\Map\TableMap::TYPE_FIELDNAME);
            
            if($periode->save()){
                $recordUpdated++;
            }else{
                $recordFailed++;
            }
            
        }
        
        $return = array();
        $return['berhasil'] = $recordUpdated;
        $return['gagal'] = $recordFailed;

        return json_encode($return);
    }
    
    public function delete(Application $app, Request $request){
        $id = $request->get('id');
        
        try{
            $periode = \Model\PeriodePenilaianQuery::create()->findOneByPeriodePenilaianId($id);
        
            if(is_null($periode)) return new Response('Data tidak ditemukan', 400);
            
            $periode->delete();
        } catch (Propel\Runtime\Exception\PropelException $ex) {
            return " { 'success' : false, 'msg' : 'Data gagal dihapus' } ";
        }
        
        return " { 'success' : true, 'msg' : 'Data berhasil dihapus' } ";
    }
}

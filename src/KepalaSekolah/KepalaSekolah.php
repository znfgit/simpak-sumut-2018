<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace KepalaSekolah;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of KepalaSekolah
 *
 * @author faisaluje
 */
class KepalaSekolah {
    public function getData(Application $app, Request $request){
        $outArr = array();
        $start = ($request->get('start')) ? $request->get('start') : 0;
        $limit = ($request->get('limit')) ? $request->get('limit') : 100;
        if($start<0){
            $start=0;
        }
        $waktuBerakhir = $request->get('waktu_berakhir');
        
        $query = \Model\PtkQuery::create()
                ->filterByTugasTambahanId(2)
                ->filterByStatusSekolah(1)
                ->orderByTmtTugasTambahan(\Propel\Runtime\ActiveQuery\Criteria::ASC)
                ->filterByJenisSekolahId(array(7,8,13,14,15,29), \Propel\Runtime\ActiveQuery\Criteria::IN);
        
        if($request->get('jenis_sekolah_id')){
            $query->filterByJenisSekolahId($request->get('jenis_sekolah_id'));
        }
        
        if($app["session"]->get('user')['sekolahId']) $query->filterBySekolahId($app["session"]->get('user')['sekolahId']);
        if($app["session"]->get('user')['level'] == 2){
            $query->filterByKecamatanId($app["session"]->get('user')['kodeWilayah']);
        }
        
        if($request->get('txtCari')){
            $query->addAnd(\Model\Map\PtkTableMap::COL_NAMA, '%'.$request->get('txtCari').'%', \Propel\Runtime\ActiveQuery\Criteria::LIKE);
            $query->addOr(\Model\Map\PtkTableMap::COL_NIP, '%'.$request->get('txtCari').'%', \Propel\Runtime\ActiveQuery\Criteria::LIKE);
        }
        
        if($waktuBerakhir){
            switch ($waktuBerakhir){
                case 1 : $val = "< 1"; break;
                case 2 : $val = "in(1,2)"; break;
                case 3 : $val = "> 2"; break;
            }
            
            $query->where("DATEDIFF(
		YEAR,
		GETDATE(),
		DATEADD(YEAR, 4, ptk.tmt_tugas_tambahan)
            ) {$val}");
        }
        
        $count = $query->count();
        
        $query->setLimit($limit);
        $query->setOffset($start);
        $kepsek = $query->find();
        
        if(is_null($kepsek)) return tableJson($outArr, 0, array('id'));
        
        foreach ($kepsek as $k){
            $arr['ptk_id'] = $k->getPtkId();
            $arr['nip'] = $k->getNip();
            $arr['nuptk'] = $k->getNuptk();
            $arr['nama'] = $k->getNama();
            $arr['unit_induk'] = $k->getNamaSekolah();
            $arr['tmt'] = $k->getTmtTugasTambahan();
            
            $tmt = strtotime($arr['tmt']);
            $tmtOri = new \DateTime($arr['tmt']);
            $t2 = strtotime('4 years', $tmt);

            $tmt = new \DateTime(date('Y-m-d', $t2));
            $now = new \DateTime('now');


            if ($now >= $tmt) {
                $kenaikanY=0;
            }else{
                $kenaikanY = $now->diff($tmt)->format('%y');
                $kenaikanY = $kenaikanY+intval($now->diff($tmt)->format('%m'))/12;
            }

//            if($waktuBerakhir){
//                if(($waktuBerakhir==1&&(!($kenaikanY<1)))){
//                    continue;
//                }elseif(($waktuBerakhir==2 &&(!($kenaikanY>=1  && $kenaikanY<=2)))){
//                    continue;
//                }elseif( ($waktuBerakhir==3 && (!($kenaikanY>2)))){
//                    continue;
//                }
//            }


            if ($tmt <= $now) {
                $arr['warning'] = "<font color=red><b>Sekarang</b></font>";
            } else {
                $arr['warning'] = $tmt->diff($now, true)->format('%y tahun %m bulan %d hari');
            }

            $arr['masa_kerja']=$tmtOri->diff($now, true)->format('%y tahun %m bulan %d hari');

            $outArr[] = $arr;
        }
        
        return tableJson($outArr, $count, array('ptk_id'));
    }
    
    public function getExcel(Application $app, Request $request){
        $outArr = array();
        
        $kepsek = \Model\PtkQuery::create()
                ->filterByTugasTambahanId(2)
                ->filterByStatusSekolah(1)
                ->orderByTmtTugasTambahan(\Propel\Runtime\ActiveQuery\Criteria::ASC)
                ->find();
        
        if(is_null($kepsek)) return 'Failed!';
        
        foreach ($kepsek as $k){
            $arr['ptk_id'] = $k->getPtkId();
            $arr['nip'] = $k->getNip();
            $arr['nuptk'] = $k->getNuptk();
            $arr['nama'] = $k->getNama();
            $arr['unit_induk'] = $k->getNamaSekolah();
            $arr['tmt'] = $k->getTmtTugasTambahan();
        
            $tmt = strtotime($arr['tmt']);
            $t2 = strtotime('4 years', $tmt);
        
            $tmt = new \DateTime(date('Y-m-d', $t2));
            $now = new \DateTime('now');
        
            if ($tmt <= $now) {
                $arr['warning'] = "Sekarang";
            } else {
                $arr['warning'] = $tmt->diff($now, true)->format('%y tahun %m bulan %d hari');
            }
        
            //             echo "TMT : ".$tmt->format('Y-m-d')." sd. ".$now->format('Y-m-d')." | ".$arr['warning']."<br/>";
        
            $outArr[] = $arr;
        }
        
        $sourceTplDir = dirname(__FILE__)."/../Templates";
        $fileName = "kepsek.xml.twig";
        
        $loader = new \Twig_Loader_Filesystem($sourceTplDir);
        $twig = new \Twig_Environment($loader, array(
            "debug" => true
        ));
        $twig->addExtension(new \Twig_Extension_Debug());
        
        $data = array(
            "datas" => $outArr,
            "jumlah" => count($outArr)
        );
        // Render stringnya
        $outStr = $twig->render($fileName, $data);
        
        // Buat response
        $response = new \Symfony\Component\HttpFoundation\Response();
        $response->setContent($outStr);
        $response->headers->set('Content-Type', 'application/vnd.ms-excel');
        $response->headers->set('Content-Disposition', 'attachment; filename="Daftar Kepala Sekolah.xls"');
        
        return $response;
    }
}

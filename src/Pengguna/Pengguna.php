<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pengguna;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of Pengguna
 *
 * @author faisaluje
 */
class Pengguna {
    public function getData(Application $app, Request $request){
        $start = ($request->get('start')) ? $request->get('start') : 0;
        $limit = ($request->get('limit')) ? $request->get('limit') : 50;
        
        $query = \Model\PenggunaQuery::create();
        
        if($request->get('query')) $query->filterByUsername('%'.$request->get('query')."%", \Propel\Runtime\ActiveQuery\Criteria::LIKE);
        
       
        
        $count = $query->count();
        $query->offset($start)->limit($limit);
        
        $data = $query->find();
        $outArr = array();
        
        if(is_null($data)) return tableJson(array(), 0, array('id'));
        
        foreach($data as $d){
            $arr = $d->toArray();
            $arr["peran_id_str"] = ($d->getPeranId()) ? $d->getPeran()->getNama() : null;
            $arr["kode_wilayah_str"] = ($d->getKodeWilayah()) ? $d->getMstWilayah()->getNama() : null;
            
            $outArr[] = $arr;
        }
        
        return tableJson($outArr, $count, array('pengguna_id'), $start, $limit);
    }
    
    public function save(Application $app, Request $request){
        $data = $request->get('data');
        
        $objs = json_decode($data, true);
        
        $recordUpdated = 0;
        $recordFailed = 0;
        
        foreach ($objs as $obj){
            
            if(strpos($obj["pengguna_id"], "SimpegGundul.model.Pengguna") !== false){
                $obj["pengguna_id"] = getGUID();
                
                $pengguna = new \Model\Pengguna();
                $obj["password"] = md5('1234');
            }else{
                $pengguna = \Model\PenggunaQuery::create()->findOneByPenggunaId($obj["pengguna_id"]);
            }
            
            $pengguna->fromArray($obj, \Propel\Runtime\Map\TableMap::TYPE_FIELDNAME);
            
            if($pengguna->save()){
                $recordUpdated++;
            }else{
                $recordFailed++;
            }
            
        }
        
        $return = array();
        $return['berhasil'] = $recordUpdated;
        $return['gagal'] = $recordFailed;

        return json_encode($return);
    }
    
    public function delete(Application $app, Request $request){
        $id = $request->get('id');
        
        try{
            $pengguna = \Model\PenggunaQuery::create()->findOneByPenggunaId($id);
        
            if(is_null($pengguna)) return new Response('Data tidak ditemukan', 400);
            
            $pengguna->delete();
        } catch (Propel\Runtime\Exception\PropelException $ex) {
            return " { 'success' : false, 'msg' : 'Data gagal dihapus' } ";
        }
        
        return " { 'success' : true, 'msg' : 'Data berhasil dihapus' } ";
    }
    
    public function resetPassword(Application $app, Request $request){
        $id = $request->get('id');
        $pass = $request->get('pass');
        
        try{
            $pengguna = \Model\PenggunaQuery::create()->findOneByPenggunaId($id);
        
            if(is_null($pengguna)) return new Response('Data tidak ditemukan', 400);
            
            $pengguna->setPassword(md5($pass));
            
            if($pengguna->save()){
                return " { 'success' : true, 'msg' : 'Password berhasil direset' } ";
            }else{
                return " { 'success' : false, 'msg' : 'Password gagal direset' } ";
            }
        } catch (Propel\Runtime\Exception\PropelException $ex) {
            return " { 'success' : false, 'msg' : 'Terjadi Kesalahan' } ";
        }
        
        
    }
}

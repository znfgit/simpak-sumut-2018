<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PengajuanKartu;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of PengajuanKartu
 *
 * @author faisaluje
 */
class PengajuanKartu {
    public function getData(Application $app, Request $request){
        $outArr = array();
        $start = ($request->get('start')) ? $request->get('start') : 0;
        $limit = ($request->get('limit')) ? $request->get('limit') : 100;
        $status = $request->get('status');
        $noSurat = $request->get('no_surat');
        
        $query = \Model\PengajuanKartuQuery::create()
                ->addJoin(\Model\Map\PengajuanKartuTableMap::COL_PTK_ID, \Model\Map\PtkTableMap::COL_PTK_ID, \Propel\Runtime\ActiveQuery\Criteria::LEFT_JOIN)
                ->filterBySoftDelete(0);
        
        
//        if($app["session"]->get('user')['level'] == 1) $query->add(\Model\Map\PtkTableMap::COL_SEKOLAH_ID, $app["session"]->get('user')['sekolahId']);
//        if($app["session"]->get('user')['level'] == 2) $query->add(\Model\Map\PtkTableMap::COL_KECAMATAN_ID, $app["session"]->get('user')['kodeWilayah']);
        if($status) $query->filterByStatus(json_decode ($status), \Propel\Runtime\ActiveQuery\Criteria::IN);
        if($noSurat) $query->filterByNoSurat("%".$noSurat."%", \Propel\Runtime\ActiveQuery\Criteria::LIKE);
        
        $query->orderByCreateDate();
        $count = $query->count();
        
        $query->offset($start)->limit($limit);
        $usulans = $query->find();
        
        foreach ($usulans as $usulan){
            $arr = $usulan->toArray();
            $arr["ptk_id_str"] = ($usulan->getPtkId()) ? $usulan->getPtk()->getNama() : $usulan->getPegawai()->getNama();
            $arr["nip"] = ($usulan->getPtkId()) ? $usulan->getPtk()->getNip() : $usulan->getPegawai()->getNip();
            $arr["instansi"] = ($usulan->getPtkId()) ? $usulan->getPtk()->getNamaSekolah() : $usulan->getPegawai()->getTempatTugas();
            
            $outArr[] = $arr;
        }
        
        return tableJson($outArr, $count, array("pengajuan_kartu_id"), $start, $limit);
    }
    
    public function saveForm(Application $app, Request $request){
        $data = $_REQUEST;
        $pengajuanKartuId = $request->get('pengajuan_kartu_id');
        
        $pengajuanKartu = \Model\PengajuanKartuQuery::create()->findOneByPengajuanKartuId($pengajuanKartuId);
                
        if(is_null($pengajuanKartu)){
            $pengajuanKartu = new \Model\PengajuanKartu();
            $data["pengajuan_kartu_id"] = getGUID();
            $data["soft_delete"] = 0;
            $data["status"] = 0;
        }
        
        unset($data["create_date"]);
        $pengajuanKartu->fromArray($data);
        if($app["session"]->get('user')['level'] == 1){
//            if($pengajuanKartu->getPtk()->getJenisSekolahId() == 6){
                $pengajuanKartu->setStatus(5);
//            }
            $pengajuanKartu->setPegawaiId(null);
        }else{
            $pengajuanKartu->setStatus(6);
            $pengajuanKartu->setPtkId(null);
        }
        
        if($pengajuanKartu->save()){
            return "{ 'success' : true, 'msg' : 'Berhasil Menyimpan' }";
        }else{
            return '{ "success" : false, "msg" : "Gagal Menyimpan" }';
        }
    }
    
    public function delete(Application $app, Request $request){
        $id = $request->get('id');
        
        $pengajuanKartu = \Model\PengajuanKartuQuery::create()->findOneByPengajuanKartuId($id);
        
        if(is_null($pengajuanKartu)) return new Response('Data tidak ditemukan', 400);
        
        $pengajuanKartu->setSoftDelete(1);
        
        if($pengajuanKartu->save()){
            return " { 'success' : true, 'msg' : 'Data berhasil dihapus' } ";
        }else{
            return " { 'success' : false, 'msg' : 'Data gagal dihapus' } ";
        }
    }
    
    public function approve(Application $app, Request $request){
        $data = splitJsonArray($request->get('rows'));
        
        try {
            foreach ($data as $d) {
                $row = json_decode(stripslashes($d));
                $pengajuanKartuId = $row->pengajuan_kartu_id;

                $pengajuanKartu = \Model\PengajuanKartuQuery::create()->findOneByPengajuanKartuId($pengajuanKartuId);

                if(in_array($pengajuanKartu->getStatus(), array(3,4))){
                    continue;
                }

                $pengajuanKartu->setStatus($request->get('approved'))
                    ->setAlasanPenolakan($request->get('alasan'));
                $pengajuanKartu->save();
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            $success = 'false';
            $result = sprintf("{'success' : $s, 'message' : '$s' }",$success,$message);
            die($result);
        }
        $result = sprintf("{'success' : true}");

        return $result;
    }
    
    public function getCetakSurat(Application $app, Request $request){
        $pengajuanKartuId = $request->get('id');

        $pengajuanKartu = \Model\PengajuanKartuQuery::create()->findOneByPengajuanKartuId($pengajuanKartuId);

        if(is_null($pengajuanKartu)) return "<h1>Data Tidak Ditemukan!</h1>";

        $ptk = $pengajuanKartu->getPtk();
        
        $sekolah = \Model\SekolahQuery::create()->findOneBySekolahId($ptk->getSekolahId());
        
        $kepsek = \Model\PtkQuery::create()
                ->filterByTugasTambahanId(2)
                ->filterBySekolahId($ptk->getSekolahId())
                ->findOne();
        
        $kepsek = ($kepsek) ? $kepsek->toArray() : null;
        $ptk = $ptk->toArray();

        // Apply custom placed template
        $sourceTplDir = dirname(__FILE__) . "/../Templates";
        $fileName = "surat_pengantar.twig";

        $loader = new \Twig_Loader_Filesystem($sourceTplDir);
        $twig = new \Twig_Environment($loader, array(
            "debug" => true
        ));
        $twig->addExtension(new \Twig_Extension_Debug());

        $outStr = $twig->render($fileName, array(
            'pengajuanKartu' => $pengajuanKartu->toArray(),
            'tglSurat' => date_to_bahasa($pengajuanKartu->getTglSurat('Y-m-d')),
            'ptk' => $ptk,
            'sekolah' => $sekolah->toArray(),
            'kepsek' => $kepsek
        ));

        return $outStr;
    }
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace MutasiSk;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of MutasiSk
 *
 * @author faisaluje
 */
class MutasiSk {
    public function getData(Application $app, Request $request){
        $start = ($request->get('start')) ? $request->get('start') : 0;
        $limit = ($request->get('limit')) ? $request->get('limit') : 50;
        
        $query = \Model\MutasiSkQuery::create()
                ->filterBySoftDelete(0)
                ->orderByTglSk();
        
        $count = $query->count();
        
        $query->offset($start)
              ->limit($limit);
        
        $mutasiSk = $query->find()->toArray(null, false, \Propel\Runtime\Map\TableMap::TYPE_FIELDNAME);
        
        return tableJson($mutasiSk, $count, array('mutasi_sk_id'), $start, $limit);
    }
    
    public function save(Application $app, Request $request){
        $data = $request->get('data');
        
        $objs = json_decode($data, true);
        
        $recordUpdated = 0;
        $recordFailed = 0;
        
        foreach ($objs as $obj){
            
            if(strpos($obj["mutasi_sk_id"], "SimpegGundul.model.MutasiSk") !== false){
                $obj["mutasi_sk_id"] = getGUID();
                
                $mutasiSk = new \Model\MutasiSk();                   
            }else{
                $mutasiSk = \Model\MutasiSkQuery::create()->findOneByMutasiSkId($obj["mutasi_sk_id"]);
            }
            
            $obj["soft_delete"] = 0;            
            $mutasiSk->fromArray($obj);

            if($mutasiSk->save()){
                $recordUpdated++;
            }else{
                $recordFailed++;
            }
            
        }
        
        $return = array();
        $return['berhasil'] = $recordUpdated;
        $return['gagal'] = $recordFailed;

        return json_encode($return);
    }
    
    public function delete(Application $app, Request $request){
        $id = $request->get('id');
        
        $mutasiSk = \Model\MutasiSkQuery::create()->findOneByMutasiSkId($id);
        
        if(is_null($mutasiSk)) return new Response('Data tidak ditemukan', 400);
        
        $mutasiSk->setSoftDelete(1);
        
        if($mutasiSk->save()){
            return " { 'success' : true, 'msg' : 'Data berhasil dihapus' } ";
        }else{
            return " { 'success' : false, 'msg' : 'Data gagal dihapus' } ";
        }
    }
    
    public function getCetakSuratPengantar(Application $app, Request $request){
        $mutasiSkId = $request->get('id');
        
        $sk = \Model\MutasiSkQuery::create()->findOneByMutasiSkId($mutasiSkId);
        if(!$sk){
            die();
        }

        $jumlahOrang = \Model\MutasiAnggotaSkQuery::create()
                ->filterByMutasiSkId($mutasiSkId)
                ->count();
        if ($jumlahOrang > 0) {
            $ptk = \Model\MutasiAnggotaSkQuery::create()->findOneByMutasiSkId($mutasiSkId)->getMutasiUsulan()->getPtk()->getNama();
        }

        // Apply custom placed template
        $sourceTplDir = dirname(__FILE__) . "/../Templates";
        $fileName = "pengantar_surat_mutasi.twig";

        $loader = new \Twig_Loader_Filesystem($sourceTplDir);
        $twig = new \Twig_Environment($loader, array(
            "debug" => true
        ));
        $twig->addExtension(new \Twig_Extension_Debug());

        // Attach data and render
        if (isset($ptk)) {
            $data = array(
                "title" => "Surat Pengantar Mutasi",
                "jumlah_orang" => $jumlahOrang,
                "nama" => $ptk,
                "no_sk" => $sk->getNoSk(),
                "tahun" => $sk->getTglSk("Y"),
                "tanggal" => $sk->getTglSk("d ").getbulan($sk->getTglSk("n")).$sk->getTglSk(" Y"),
                "ttd_jabatan" => $sk->getPenandatangan()->getJabatan(),
                "ttd_instansi" => $sk->getPenandatangan()->getInstansi(),
                "ttd_nama" => $sk->getPenandatangan()->getNama(),
                "ttd_pangkat" => $sk->getPenandatangan()->getPangkatGolongan()->getNama(),
                "ttd_nip" => $sk->getPenandatangan()->getNip()
            );
        }else{
            $data = array(
                "title" => "Surat Pengantar Mutasi",
                "jumlah_orang" => $jumlahOrang,
                "no_sk" => $sk->getNoSk(),
                "tahun" => $sk->getTglSk("Y"),
                "tanggal" => $sk->getTglSk("d ").getbulan($sk->getTglSk("n")).$sk->getTglSk(" Y"),
                "ttd_jabatan" => $sk->getPenandatangan()->getJabatan(),
                "ttd_instansi" => $sk->getPenandatangan()->getInstansi(),
                "ttd_nama" => $sk->getPenandatangan()->getNama(),
                "ttd_pangkat" => $sk->getPenandatangan()->getPangkatGolongan()->getNama(),
                "ttd_nip" => $sk->getPenandatangan()->getNip()
            );
        }

        $outStr = $twig->render($fileName, $data);

        return $outStr;
    }
    
    public function getCetakLampiran(Application $app, Request $request){
        $mutasiSkId = $request->get('id');

        $sk = \Model\MutasiSkQuery::create()->findOneByMutasiSkId($mutasiSkId);
        
        $ptks = \Model\MutasiAnggotaSkQuery::create()->findByMutasiSkId($mutasiSkId);

        foreach ($ptks as $p){

            $arr["nama"] = ($p->getMutasiUsulan()->getPtkId()) ? $p->getMutasiUsulan()->getPtk()->getNama() : $p->getMutasiUsulan()->getPegawai()->getNama();
            $arr["tempat_lahir"] = ($p->getMutasiUsulan()->getPtkId()) ? $p->getMutasiUsulan()->getPtk()->getTempatLahir() : $p->getMutasiUsulan()->getPegawai()->getTempatLahir();
            $arr["tanggal_lahir"] = ($p->getMutasiUsulan()->getPtkId()) ? date_to_bahasa($p->getMutasiUsulan()->getPtk()->getTglLahir('Y-m-d')) : date_to_bahasa($p->getMutasiUsulan()->getPegawai()->getTglLahir('Y-m-d'));
            $arr["nip"] = ($p->getMutasiUsulan()->getPtkId()) ? $p->getMutasiUsulan()->getPtk()->getNip() : $p->getMutasiUsulan()->getPegawai()->getNip();
            $arr["ijasah"] = ($p->getMutasiUsulan()->getPtkId()) ? $p->getMutasiUsulan()->getPtk()->getNamaIjazahTerakhir() : $p->getMutasiUsulan()->getPegawai()->getJenjangPendidikan();
            $arr["jurusan"] = null;
            $arr["gol"] = ($p->getMutasiUsulan()->getPtkId()) ? $p->getMutasiUsulan()->getPtk()->getNamaPangkatGolongan() : $p->getMutasiUsulan()->getPegawai()->getPangkatGolongan()->getGolongan();
            $arr["jabatan_lama"] = ($p->getMutasiUsulan()->getPtkId()) ? $p->getMutasiUsulan()->getPtk()->getJenisGuru() : $p->getMutasiUsulan()->getPegawai()->getJabatan();
            $arr["jabatan_baru"] = $p->getMutasiUsulan()->getJenisPtk()->getJenisPtk();
            $arr["alamat"] = ($p->getMutasiUsulan()->getPtkId()) ? $p->getMutasiUsulan()->getPtk()->getAlamatJalan() : null;
            $arr["alasan"] = $p->getMutasiUsulan()->getAlasan();

            $outArr[] = $arr;
        }

        $sourceTplDir = dirname(__FILE__) . "/../Templates";
        $fileName = "lampiran_mutasi.twig";

        $loader = new \Twig_Loader_Filesystem($sourceTplDir);
        $twig = new \Twig_Environment($loader, array(
            "debug" => true
        ));
        $twig->addExtension(new \Twig_Extension_Debug());
        if (!empty($sk)) {
            $data = array(
                "no_sk" => $sk->getNoSk(),
                "datas" => $outArr,
                "tahun" => $sk->getTglSk("Y"),
                "tanggal" => date_to_bahasa(date('Y-m-d')),
                "ttd_jabatan" => $sk->getPenandatangan()->getJabatan(),
                "ttd_instansi" => $sk->getPenandatangan()->getInstansi(),
                "ttd_nama" => $sk->getPenandatangan()->getNama(),
                "ttd_pangkat" => $sk->getPenandatangan()->getPangkatGolongan()->getNama(),
                "ttd_nip" => $sk->getPenandatangan()->getNip()
            );
        }

        // Render stringnya
        $outStr = $twig->render($fileName, $data);

        return $outStr;
    }
}

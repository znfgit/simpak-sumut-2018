<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Kegiatan;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of Kegiatan
 *
 * @author faisaluje
 */
class Kegiatan {
    public function getData(Application $app, Request $request){
        $outArr = array();
        $start = ($request->get('start')) ? $request->get('start') : 0;
        $limit = ($request->get('limit')) ? $request->get('limit') : 100;
        
        $query = \Model\KegiatanQuery::create()
                ->filterBySoftDelete(0);
        
        if($request->get('txtCari')) $query->filterByNoSurat($request->get('txtCari'));
        
        $count = $query->count();
        
        $query->offset($start)->limit($limit);
        $kegiatans = $query->find();
        
        foreach ($kegiatans as $kegiatan){
            $arr = $kegiatan->toArray();
            $arr["penandatangan_id_str"] = ($kegiatan->getPenandatangan()) ? $kegiatan->getPenandatangan()->getNama() : null;
            
            $outArr[] = $arr;
        }
        
        return tableJson($outArr, $count, array("kegiatan_id"), $start, $limit);
    }
    
    public function save(Application $app, Request $request){
        $data = $request->get('data');
        
        $objs = json_decode($data, true);
        
        $recordUpdated = 0;
        $recordFailed = 0;
        
        foreach ($objs as $obj){
            
            if(strpos($obj["kegiatan_id"], "SimpegGundul.model.Kegiatan") !== false){
                $obj["kegiatan_id"] = getGUID();
                
                $kegiatan = new \Model\Kegiatan();
            }else{
                $kegiatan = \Model\KegiatanQuery::create()->findOneByKegiatanId($obj["kegiatan_id"]);
            }
            
            $obj["soft_delete"] = 0;
            
            $kegiatan->fromArray($obj, \Propel\Runtime\Map\TableMap::TYPE_FIELDNAME);
            
            if($kegiatan->save()){
                $recordUpdated++;
            }else{
                $recordFailed++;
            }
            
        }
        
        $return = array();
        $return['berhasil'] = $recordUpdated;
        $return['gagal'] = $recordFailed;

        return json_encode($return);
    }
    
    public function delete(Application $app, Request $request){
        $id = $request->get('id');
        
        $kegiatan = \Model\KegiatanQuery::create()->findOneByKegiatanId($id);
        
        if(is_null($kegiatan)) return new Response('Data tidak ditemukan', 400);
        
        $kegiatan->setSoftDelete(1);
        
        if($kegiatan->save()){
            return " { 'success' : true, 'msg' : 'Data berhasil dihapus' } ";
        }else{
            return " { 'success' : false, 'msg' : 'Data gagal dihapus' } ";
        }
    }
    
    public function getCetakSertifikat(Application $app, Request $request){
        $id = $request->get('id');
        
        $kegiatan = \Model\KegiatanQuery::create()->findOneByKegiatanId($id);
        
        if(is_null($kegiatan)) return new Response('Data tidak ditemukan', 400);
        
        $query = \Model\AnggotaKegiatanQuery::create()
                ->filterByKegiatanId($id)
                ->filterBySoftDelete(0);
        
        if($request->get('anggota_id')){
            $query->filterByAnggotaKegiatanId($request->get('anggota_id'));
        }
        
        $anggotas = $query->find();
        
        $outArr = array();
        foreach($anggotas as $anggota ){
            $arr["peran"] = $anggota->getPeran();
            if($anggota->getPtkId()){
                $arr["nama"] = $anggota->getPtk()->getNama();
                $arr["tempat_lahir"] = $anggota->getPtk()->getTempatLahir();
                $arr["tanggal_lahir"] = date_to_bahasa($anggota->getPtk()->getTglLahir('Y-m-d'));
                $arr["instansi"] = $anggota->getPtk()->getNamaSekolah();
            }else{
                $arr["nama"] = $anggota->getPegawai()->getNama();
                $arr["tempat_lahir"] = $anggota->getPegawai()->getTempatLahir();
                $arr["tanggal_lahir"] = date_to_bahasa($anggota->getPegawai()->getTglLahir('Y-m-d'));
                $arr["instansi"] = $anggota->getPegawai()->getTempatTugas();
            }
            
            $outArr[] = $arr;
        }
        
        $pejabat = $kegiatan->getPenandatangan();
        
        // Apply custom placed template
        $sourceTplDir = dirname(__FILE__) . "/../Templates";
        $fileName = "sertifikat.twig";

        $loader = new \Twig_Loader_Filesystem($sourceTplDir);
        $twig = new \Twig_Environment($loader, array(
            "debug" => true
        ));
        $twig->addExtension(new \Twig_Extension_Debug());

        $outStr = $twig->render($fileName, array(
            "title" => "sertifikat ".$kegiatan->getNama(),
            "kegiatan" => $kegiatan->toArray(),
            "pejabat" => $pejabat->toArray(),
            "tgl_mulai" => date_to_bahasa($kegiatan->getTglMulai('Y-m-d')),
            "tgl_selesai" => date_to_bahasa($kegiatan->getTglSelesai('Y-m-d')),
            "peserta" => $outArr
        ));

        return $outStr;
    }
}

<?php

error_reporting(E_ERROR);

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Model\PenggunaQuery;

require_once __DIR__.'/../config/database/generated-conf/config.php';

$console = new Application('Data Console', '0.2');
$console->getDefinition()->addOption(new InputOption('--env', '-e', InputOption::VALUE_REQUIRED, 'The Environment name.', 'dev'));
$console->setDispatcher($app['dispatcher']);
    
    $console
    ->register('get-data')
    ->setDefinition(array(
        // new InputOption('some-option', null, InputOption::VALUE_NONE, 'Some help'),
    ))
    ->addArgument('name', InputArgument::REQUIRED, 'Jenis Data')
    ->addArgument('kode', InputArgument::OPTIONAL, 'Kode Wilayah')
    ->setDescription('Get data from GTK by region.')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 6000);
       $kodeWilayah = ($input->getArgument('kode')) ? $input->getArgument('kode') : '350800';
       $jenis = ($input->getArgument('name')) ? strtolower($input->getArgument('name')) : 'ptk';
       $waktuAwal = date('H:i:s');
       
       // Get data from GTK
       print("Get data from GTK. => ".date('H:i:s')."\n");
       switch ($jenis){
           case 'ptk' : 
               $table = "ptk"; 
               $content = file_get_contents('http://223.27.144.198:8081/Aplikasi/getTPtk.php/'.$kodeWilayah); // 40300
               break;
           case 'pengawas' : 
               $table = "ptk"; 
               $content = file_get_contents('http://223.27.144.198:8081/Aplikasi/getTPtkPengawas.php/'.$kodeWilayah); // 40300
               break;
           case 'pengguna' :
               $table = "pengguna";
               $content = file_get_contents('http://simtara.gtk.kemdikbud.go.id:8000/getPengguna/'.$kodeWilayah); // 0403
               break;
           case 'sekolah' :
               $table = "sekolah";
               $content = file_get_contents('http://223.27.144.198:8081/Aplikasi/getSekolah.php/'.$kodeWilayah); // 0403
               break;
           default: return "Table not found.";
       }
       
       // Drop and Create table Temp for saving data from GTK
       print("Drop and Create table Temp for saving data from GTK. => ".date('H:i:s')."\n");
       $sql = "SELECT TABLE_NAME, COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE TABLE_NAME = '{$table}'
        ORDER BY TABLE_NAME";
       
       $data = getDataBySql($sql);
       
       // Get PK
       $pk = getDataBySql("EXEC sp_pkeys '{$table}'");
       $primariKey = "";
       if(is_array($pk)){
            $primariKey = $pk[0]["COLUMN_NAME"];
       }else{
           return print_r("No have primary key, please set primary key for the table.");
       }
       
       $query = "IF (
                OBJECT_ID('temp') IS NOT NULL
        )
        BEGIN
                DROP TABLE temp
        END ; ";
       $query .= "CREATE TABLE temp(";
       $columns = array();
       foreach ($data as $d){
           $columns[] = $d['COLUMN_NAME'];
           $isPk = ($primariKey == $d["COLUMN_NAME"]) ? "primary key" : null;
           $length = ($d['CHARACTER_MAXIMUM_LENGTH']) ? "({$d['CHARACTER_MAXIMUM_LENGTH']})" : null; 
           $query .= "{$d['COLUMN_NAME']} {$d['DATA_TYPE']}{$length} {$isPk},";
       }
       $query = rtrim($query, ",");
       $query .=");";
       
       $stmt = executeSql($query);
       if($stmt !== true){
           return print_r($stmt);
       }
       
       // $content = file_get_contents(__DIR__.'/../web/t_ptk.txt');
       $content = str_replace("==9USUsiO==", "", $content);
       $decode = base64_decode($content);
       $datas = json_decode($decode, true);  
       
       
       /* Begin the transaction. */
       print("Begin transaction input data fetched to Temp table. => ".date('H:i:s')."\n");
       $conn = getConnection();
        if ( sqlsrv_begin_transaction( $conn ) === false ) {
             die( print_r( sqlsrv_errors(), true ));
        }
        
       $sql = "";
       $total = count($datas);
       $count = 0;
       $columnTarget = "";
       $columnSource = "";
       $columnUpdate = "";
       foreach ($datas as $data){           
           $column = "";
           $values = "";
           
           if(array_key_exists("t_ptk_id", $data)) $data["ptk_id"] = $data["t_ptk_id"];
           if(array_key_exists("t_sekolah_id", $data)) $data["sekolah_id"] = $data["t_sekolah_id"];
           
           foreach ($data as $key => $val){
               if(array_search($key, $columns) === false){
                   continue;
               }else{
                   $val = str_replace("'", "''", $val);
                   $column .= "[{$key}],";
                   $values .= ($val) ? "'{$val}'," : "null,";
                   
                   if($count < 1){
                    $columnTarget .= "target.[{$key}],";
                    $columnSource .= "source.[{$key}],";
                    $columnUpdate .= "target.[{$key}] = source.[{$key}],";
                   }
               }
           }
           $column = rtrim($column, ",");
           $values = rtrim($values, ",");
           
           $columnTarget = rtrim($columnTarget, ",");
           $columnSource = rtrim($columnSource, ",");
           $columnUpdate = rtrim($columnUpdate, ",");
           
           $sql = "INSERT INTO temp({$column}) values({$values});";
           $stmt = sqlsrv_query( $conn, $sql );
           if($stmt){
               $count++;
               print("Data Inserted from {$count} to {$total}\n");
           }else{
               sqlsrv_rollback( $conn );
               print("Transaction rolled back.\n");
               die();
           }
//           print_r($sql."\n");
       }
       
       print("Commit the transaction. => ".date('H:i:s')."\n");
       sqlsrv_commit( $conn );
       
       print("Merging between data fetched and data existing. => ".date('H:i:s')."\n");
       $query = "MERGE {$table} AS target
        USING (SELECT * from temp) AS source
        ON target.{$primariKey} = source.{$primariKey}
        WHEN MATCHED THEN UPDATE SET
            {$columnUpdate}
        WHEN NOT MATCHED THEN INSERT(
            {$column}
        ) VALUES(
            {$columnSource}
        );";
       
        $stmt = executeSql($query);
        if($stmt !== true){
            return print_r($stmt);
        }
       
        // Generate Log for last sync
        $sync = new Model\SyncLog();
        $sync->setSyncLogId(getGUID())
                ->setTableName($table)
                ->save();
                
       return print("Finish {$count} records from {$waktuAwal} to ".date('H:i:s')."\n");
    });

return $console;

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Penugasan;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of Penugasan
 *
 * @author faisaluje
 */
class Penugasan {
    public function getData(Application $app, Request $request){
        $outArr = array();
        $start = ($request->get('start')) ? $request->get('start') : 0;
        $limit = ($request->get('limit')) ? $request->get('limit') : 100;
        $status = $request->get('status');
        
        $query = \Model\PenugasanQuery::create()
                ->addJoin(\Model\Map\PenugasanTableMap::COL_PTK_ID, \Model\Map\PtkTableMap::COL_PTK_ID, \Propel\Runtime\ActiveQuery\Criteria::INNER_JOIN)
                ->filterBySoftDelete(0);
        
//        if($app["session"]->get('user')['level'] == 1) $query->add(\Model\Map\PtkTableMap::COL_SEKOLAH_ID, $app["session"]->get('user')['sekolahId']);
//        if($app["session"]->get('user')['level'] == 2) $query->add(\Model\Map\PtkTableMap::COL_KECAMATAN_ID, $app["session"]->get('user')['kodeWilayah']);
        if($status) $query->filterByStatus(json_decode ($status), \Propel\Runtime\ActiveQuery\Criteria::IN);
        
        $query->orderByCreateDate();
        $count = $query->count();
        
        $query->offset($start)->limit($limit);
        $penugasans = $query->find();
        
        foreach ($penugasans as $penugasan){
            $arr = $penugasan->toArray();
            $arr["ptk_id_str"] = $penugasan->getPtk()->getNama();
            $arr["nip"] = $penugasan->getPtk()->getNip();
            $arr["sekolah_induk_str"] = $penugasan->getSekolahRelatedBySekolahInduk()->getNama();
            $arr["sekolah_tujuan_str"] = $penugasan->getSekolahRelatedBySekolahTujuan()->getNama();
            
            $outArr[] = $arr;
        }
        
        return tableJson($outArr, $count, array("mutasi_usulan_id"), $start, $limit);
    }
    
    public function save(Application $app, Request $request){
        $data = $request->get('data');
        
        $objs = json_decode($data, true);
        
        $recordUpdated = 0;
        $recordFailed = 0;
        
        foreach ($objs as $obj){
            
            if(strpos($obj["penugasan_id"], "SimpegGundul.model.Penugasan") !== false){
                $obj["penugasan_id"] = getGUID();
                
                $penugasan = new \Model\Penugasan();
                $obj["status"] = 0;
                $obj["sekolah_induk"] = \Model\PtkQuery::create()->findOneByPtkId($obj["ptk_id"])->getSekolahId();
            }else{
                $penugasan = \Model\PenugasanQuery::create()->findOneByPenugasanId($obj["penugasan_id"]);
            }
            
            $obj["soft_delete"] = 0;
            unset($obj["create_date"]);
            $penugasan->fromArray($obj, \Propel\Runtime\Map\TableMap::TYPE_FIELDNAME);
//            print_r($penugasan->toArray());die;
//            if($penugasan->getPtk()->getJenisSekolahId() == 6){
                $penugasan->setStatus(5);
//            }

            if($penugasan->save()){
                $recordUpdated++;
            }else{
                $recordFailed++;
            }
            
        }
        
        $return = array();
        $return['berhasil'] = $recordUpdated;
        $return['gagal'] = $recordFailed;

        return json_encode($return);
    }
    
    public function delete(Application $app, Request $request){
        $id = $request->get('id');
        
        $penugasan = \Model\PenugasanQuery::create()->findOneByPenugasanId($id);
        
        if(is_null($penugasan)) return new Response('Data tidak ditemukan', 400);
        
        $penugasan->setSoftDelete(1);
        
        if($penugasan->save()){
            return " { 'success' : true, 'msg' : 'Data berhasil dihapus' } ";
        }else{
            return " { 'success' : false, 'msg' : 'Data gagal dihapus' } ";
        }
    }
    
    public function approve(Application $app, Request $request){
        $data = splitJsonArray($request->get('rows'));
        
        try {
            foreach ($data as $d) {
                $row = json_decode(stripslashes($d));
                $penugasanId = $row->penugasan_id;

                $penugasan = \Model\PenugasanQuery::create()->findOneByPenugasanId($penugasanId);

                if(in_array($penugasan->getStatus(), array(3,4))){
                    continue;
                }

                $penugasan->setStatus($request->get('approved'))
                    ->setAlasanPenolakan($request->get('alasan'));
                $penugasan->save();
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            $success = 'false';
            $result = sprintf("{'success' : $s, 'message' : '$s' }",$success,$message);
            die($result);
        }
        $result = sprintf("{'success' : true}");

        return $result;
    }
    
    public function getCetakPermohonan(Application $app, Request $request){
        $penugasanId = $request->get('id');

        $penugasan = \Model\PenugasanQuery::create()->findOneByPenugasanId($penugasanId);

        if(is_null($penugasan)) return "<h1>Data Tidak Ditemukan!</h1>";

        $ptk = $penugasan->getPtk();
        $tglLahir = date_to_bahasa($ptk->getTglLahir('Y-m-d'));

        $pangkat = ($ptk->getPangkatGolonganId()) ? $ptk->getPangkatGolongan()->getNama() : null;

        $sekolahInduk = $penugasan->getSekolahRelatedBySekolahInduk();
        $sekolahTujuan = $penugasan->getSekolahRelatedBySekolahTujuan();

        $ptk = $ptk->toArray();
        $ptk["golongan"] = $pangkat;
        
        $kepsek = \Model\PtkQuery::create()
                ->filterByTugasTambahanId(2)
                ->filterBySekolahId($sekolahInduk->getSekolahId())
                ->findOne();
        
        $kepsek = ($kepsek) ? $kepsek->toArray() : null;

        // Apply custom placed template
        $sourceTplDir = dirname(__FILE__) . "/../Templates";
        $fileName = "surat_permohonan_tambah_jam.twig";

        $loader = new \Twig_Loader_Filesystem($sourceTplDir);
        $twig = new \Twig_Environment($loader, array(
            "debug" => true
        ));
        $twig->addExtension(new \Twig_Extension_Debug());

        $outStr = $twig->render($fileName, array(
            'penugasan' => $penugasan->toArray(),
            'tglSurat' => date_to_bahasa($penugasan->getTglSurat('Y-m-d')),
            'ptk' => $ptk,
            'kepsek' => $kepsek,
            'tglLahir' => $tglLahir,
            'sekolahInduk' => $sekolahInduk->toArray(),
            'sekolahTujuan' => $sekolahTujuan->toArray()
        ));

        return $outStr;
    }
}
